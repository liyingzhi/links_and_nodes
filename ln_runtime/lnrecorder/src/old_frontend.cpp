#include <stdio.h>
#include <string_util/string_util.h>

#include "lnrecorder.h"
#include "backend.h"
#include "old_frontend.h"

namespace lnrecorder {

old_frontend::old_frontend(lnrecorder* parent, std::string input, const TopicPatterns& topic_patterns) :
	frontend(parent, topic_patterns)
{
	fp = fopen(input.c_str(), "rb");
	if(!fp)
		throw errno_exception_tb("could not open replay file %s for reading!", string_util::repr(input).c_str());
	read_header_frame();
	printf("reading old log file\n");
}
old_frontend::~old_frontend()
{
	fclose(fp);
}

void old_frontend::read_header_frame()
{
	any_frame frame;
	if(!read_frame_type(&frame))
		throw str_exception_tb("got EOF while trying to read header frame!");

	if(frame.type != HEADER_FRAME)
		throw str_exception_tb("got unexpected frame_Type %d when expecting header-frame!", frame.type);
	
	header_frame& hdr = frame.header;

	uint32_t this_version; //  = 0xffff0000 + version;
	int ret = fread(&this_version, sizeof(this_version), 1, fp);
	if(ret != 1)
		throw errno_exception_tb("could not read header frame's version!");
	if((this_version & 0xffff0000) != 0xffff0000)
		throw errno_exception_tb("invalid file version: %#x!", this_version);
	hdr.version = (uint16_t)(this_version & 0x0000ffff);
	if(hdr.version > FILE_VERSION)
		printf("warning: file is written in version %d, we are %d!\n", hdr.version, FILE_VERSION);
	file_version = hdr.version;
	
	ret = fread(&frame.timestamp, sizeof(frame.timestamp), 1, fp);
	if(ret != 1)
		throw errno_exception_tb("could not read header frames's timestamp!");
}


bool old_frontend::read_frame_type(any_frame* frame)
{
	uint32_t frame_type;
	int ret = fread(&frame_type, sizeof(&frame_type), 1, fp);
	if(ret == 0)
		return false;
	if(ret != 1)
		throw errno_exception_tb("could not read frame type!");	
	frame->type = (frame_types)frame_type;
	return true;
}

std::string old_frontend::read_field() {
	uint32_t field_len;
	int ret = fread(&field_len, sizeof(field_len), 1, fp);
	if(ret != 1)
		throw str_exception_tb("could not read field len!");
	if(field_buffer.size() < field_len)
		field_buffer.resize(field_len);
	ret = fread(&field_buffer[0], field_len, 1, fp);
	if(ret != 1)
		throw errno_exception_tb("could not read field of length %d!", field_len);
	return std::string(&field_buffer[0], field_len);
}


void old_frontend::write_frames(std::shared_ptr<backend> backend, bool* keep_running_ptr)
{
	parent->current_frontend = this;
	if(keep_running_ptr == NULL) {
		keep_running = true;
		keep_running_ptr = &keep_running;
	}
	target_be = backend;
	
	any_frame frame;
	int ret;
	
	while(*keep_running_ptr) {		
		if(!read_frame_type(&frame))
			break; // eof
		
		if(parent->verbosity >= 3)
			printf("have frame type %d\n", frame.type);

		switch(frame.type) {
		case HEADER_FRAME:
			break;
		case NEW_TOPIC_FRAME: {
			new_topic_frame& f = frame.new_topic;
	
			ret = fread(&f.topic_id, sizeof(f.topic_id), 1, fp);
			if(ret != 1)
				throw errno_exception_tb("could not read new topic frame's topic_id!");

			ret = fread(&f.message_size, sizeof(f.message_size), 1, fp);
			if(ret != 1)
				throw errno_exception_tb("could not read new topic frame's message size!");

			f.name = read_field();
			f.md = read_field();
			f.msg_def_hash = "";
			
			ret = fread(&frame.timestamp, sizeof(frame.timestamp), 1, fp);
			if(ret != 1)
				throw errno_exception_tb("could not read new topic frame's timestamp!");
			
			f.topic = topics_by_id[f.topic_id] = new lnrecorder_topic(parent, &f);			
			break;
		}
		case NEW_PACKET_FRAME: {
			new_packet_frame& f = frame.new_packet;

			// todo: use scatter/gather write!

			ret = fread(&f.topic_id, sizeof(f.topic_id), 1, fp);
			if(ret != 1)
				throw errno_exception_tb("could not read new packet frame's topic_id!");
	
			ret = fread(&frame.timestamp, sizeof(frame.timestamp), 1, fp);
			if(ret != 1)
				throw errno_exception_tb("could not read new packet frame's timestamp!");

			f.topic = topics_by_id[f.topic_id];
			if(!f.topic)
				throw str_exception_tb("pcap file contains unknown topic_id %d!", f.topic_id);
			
			ret = fread(&f.topic->packet[0], f.topic->packet.size(), 1, fp);
			if(ret != 1)
				throw errno_exception_tb("could not read new packet frame's data!");
			f.publisher_timestamp = frame.timestamp;
			
			break;
		}
		case DEL_TOPIC_FRAME: {
			del_topic_frame& f = frame.del_topic;
			
			ret = fread(&f.topic_id, sizeof(f.topic_id), 1, fp);
			if(ret != 1)
				throw errno_exception_tb("could not read del topic frame's topic_id!");

			ret = fread(&frame.timestamp, sizeof(frame.timestamp), 1, fp);
			if(ret != 1)
				throw errno_exception_tb("could not read del topic frame's timestamp!");

			topics_by_id_t::iterator ti = topics_by_id.find(f.topic_id);
			f.topic = ti->second;
			
			process_frame(&frame); // process before deleting topic
			
			if(ti != topics_by_id.end()) {
				delete f.topic;
				topics_by_id.erase(ti);
			}
			continue;
		}
		// those don't exist in this file format:
		case NEW_SERVICE_PEER_FRAME:
		case NEW_SERVICE_FRAME:
		case NEW_SERVICE_EVENT_FRAME:
			break;
		}
		process_frame(&frame);
	}
}

}
