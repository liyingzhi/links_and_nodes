#include <string_util/string_util.h>

#include "condition_helper.h"

condition_helper::condition_helper() {
	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&cond, NULL);
	locked = false;
}

condition_helper::~condition_helper() {
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
}

void condition_helper::lock() {
	int ret = pthread_mutex_lock(&mutex);
	if(ret)
		throw retval_exception_tb(ret, "could not lock mutex");
	locked = true;
}

void condition_helper::unlock() {
	int ret = pthread_mutex_unlock(&mutex);
	if(ret)
		throw retval_exception_tb(ret, "could not unlock mutex");
	locked = false;
}

void condition_helper::wait() {
	if(!locked)
		throw str_exception_tb("condition mutex is not locked!");
	int ret = pthread_cond_wait(&cond, &mutex);
	if(ret)
		throw retval_exception_tb(ret, "could not cond_wait");
}

bool condition_helper::timedwait(struct timespec* rel_ts) {
	if(!locked)
		throw str_exception_tb("condition mutex is not locked!");
#ifdef __WIN32__
	throw str_exception_tb("condition_helper::timedwait() not implemented on win32 because of clock_gettime!");
#else
	// create abs timeout
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += rel_ts->tv_sec;
	ts.tv_nsec += rel_ts->tv_nsec;
	while(ts.tv_nsec > (long)1e9) {
		ts.tv_nsec -= (long)1e9;
		ts.tv_sec ++;
	}
	int ret = pthread_cond_timedwait(&cond, &mutex, &ts);
	if(ret == ETIMEDOUT)
		return false;
	if(ret)
		throw retval_exception_tb(ret, "could not cond_timedwait");
#endif
	return true;
}

void condition_helper::signal() {
	int ret = pthread_cond_signal(&cond);
	if(ret)
		throw retval_exception_tb(ret, "could not signal condition");
}

void condition_helper::broadcast() {
	int ret = pthread_cond_broadcast(&cond);
	if(ret)
		throw retval_exception_tb(ret, "could broadcast condition");
}
       

       
