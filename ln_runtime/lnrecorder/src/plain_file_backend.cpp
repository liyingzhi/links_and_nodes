#include <stdio.h>

#include <string_util/string_util.h>

#include "plain_file_backend.h"

namespace lnrecorder {

file_backend::file* plain_file_backend::open(const char* name, const char* mode) {
	FILE* fp = fopen(name, mode);
	if(!fp)
		throw errno_exception_tb("fopen('%s', %s)", name, mode);
	plain_file* ret = new plain_file();
	ret->fp = fp;
	return ret;
}

size_t plain_file_backend::write(const void* src, unsigned int size, unsigned int n_elem, file_backend::file* fp_) {
	plain_file* fp = (plain_file*)fp_;
	return fwrite(src, size, n_elem, fp->fp);
}

size_t plain_file_backend::read(void* dst, unsigned int size, unsigned int n_elem, file_backend::file* fp_) {
	plain_file* fp = (plain_file*)fp_;
	return fread(dst, size, n_elem, fp->fp);
}

void plain_file_backend::close(file* fp_) {	
	plain_file* fp = (plain_file*)fp_;
	fclose(fp->fp);
	delete fp;
}

}
