#ifndef LNRDB_BACKEND_H
#define LNRDB_BACKEND_H

#include <stdint.h>

#include <map>

#include "backend.h"
#include "frames.h"
#include "lnrdb/lnrdb.h"
#include "lock.h"

namespace lnrecorder {

class lnrdb_backend :
		public backend
{
	std::unique_ptr<lnrdb::db> db;
	lock file_lock;

	typedef struct {
		lnrdb::table* table;
		uint32_t table_id; // client_id for service-clients, service_id for providers
		uint64_t offset;
	} table_and_offset_t;
	typedef std::pair<uint32_t, uint32_t> client_and_request_id;
	typedef std::map<client_and_request_id, table_and_offset_t > open_req_or_resp_t;
	open_req_or_resp_t open_requests;
	open_req_or_resp_t open_responses;

	void link_req_and_response(const table_and_offset_t& req, const table_and_offset_t& resp);
	void try_link_req_and_response(const table_and_offset_t& this_record, const client_and_request_id& this_id, bool is_request);

	template<typename lkup_type, typename key_type=uint32_t>
	struct foreign_key {
		lnrdb::table* table = NULL;
		std::map<lkup_type, key_type> keys;
	};
	foreign_key<uint32_t> service_peers;
	std::map<uint32_t, lnrdb::table*> service_tables;
	
	typedef std::map<uint32_t, new_service_frame> new_service_frames_t;
	new_service_frames_t new_service_frames; // only until they are first used via NEW_SERVICE_EVENT_FRAME

	any_frame* frame; // mainly to get timestamp
public:
	lnrdb_backend(lnrecorder* parent, std::string output);
	
	bool write_frame(any_frame* frame);
	
	void consume(new_topic_frame& frame);
	void consume(new_packet_frame& frame);
	void consume(del_topic_frame& frame);
	
	void consume(new_service_peer_frame& frame);
	void consume(new_service_frame& frame);
	void consume(new_service_event_frame& frame);
};

}

#endif // LNRDB_BACKEND_H

