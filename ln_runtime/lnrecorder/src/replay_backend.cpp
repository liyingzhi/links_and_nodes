#include <stdio.h>

#include <string_util/string_util.h>

#include "replay_backend.h"
#include "lnrecorder.h"
#include "lnrecorder_topic.h"

namespace lnrecorder {

replay_backend::replay_backend(lnrecorder* parent, double replay_start, double replay_stop,
			       unsigned int service_verbosity) :
	backend(parent),
	replay_start(replay_start),
	replay_stop(replay_stop),
	service_verbosity(service_verbosity)
{
	start_time = ln_get_time() - replay_start;
	if(replay_stop != 0)
		stop_time = ln_get_time() + replay_stop;
	else
		stop_time = 0;
	
	is_first_topic = true;
	initial_pause = 0;
	want_more = true;	
}

static std::string get_datetime(double ts) {
	time_t t = (time_t)ts;
	struct tm* btime = localtime(&t);
	char tstr[32];
	strftime(tstr, sizeof(tstr), "%Y-%m-%d %H:%M:%S", btime);
	unsigned int ms = (unsigned int)((ts - t) * 1e3);
	if(false && ms >= 1000)
		ms = 999;
	return string_util::format_string("%s.%03d", tstr, ms);
}

bool replay_backend::write_frame(any_frame* frame)
{
	switch(frame->type) {
	case HEADER_FRAME:
		break;
	case NEW_TOPIC_FRAME: {
		new_topic_frame& f = frame->new_topic;

		if(is_first_topic) {
			is_first_topic = false;
			file_start_time = frame->timestamp;
			initial_pause = frame->timestamp - file_start_time;
		}
		double ts = wait_frame(frame);
	
		printf("%9.3f: new topic %s (%s) of size %d",
		       ts - start_time, f.name.c_str(), f.md.c_str(), f.message_size);
		
		if((frame->timestamp - file_start_time) >= replay_start)
			f.topic->ensure_publishing();		
		break;
	}
	case NEW_PACKET_FRAME: {
		new_packet_frame& f = frame->new_packet;
		if((frame->timestamp - file_start_time) >= replay_start) {
			f.topic->ensure_publishing();
			double ts = wait_frame(frame);
			if(parent->verbosity >= 2)
				printf("%9.3f: new packet frame for %s\n", ts - start_time, f.topic->name.c_str());
			f.topic->write(ts);
		}		
		break;
	}
	case DEL_TOPIC_FRAME: {
		del_topic_frame& f = frame->del_topic;
		double ts = wait_frame(frame);
		printf("%9.3f: del topic %s (%s), published %d packets\n",
		       ts - start_time,
		       f.topic->name.c_str(), f.topic->md.c_str(), f.topic->n_packets);
		f.topic->stop_publishing();
		break;
	}
		
	case NEW_SERVICE_PEER_FRAME: {
		new_service_peer_frame& f = frame->new_service_peer;
		if(service_verbosity != 0)
			known_peers[f.peer_id] = f.peer;		
		break;
	}
	case NEW_SERVICE_FRAME: {
		new_service_frame& f = frame->new_service;
		if(service_verbosity != 0)
			known_services[f.service_id] = f;
		break;
	}
	case NEW_SERVICE_EVENT_FRAME: {
		new_service_event_frame& f = frame->new_service_event;
		if(service_verbosity == 0)
			break;
		
		known_services_t::iterator i = known_services.find(f.service_id);
		if(i == known_services.end()) {
			printf("got NEW_SERVICE_EVENT_FRAME packet with unknown service_id %d!\n", f.service_id);
			break;
		} 
		new_service_frame& svc = i->second;
		std::string peer = known_peers[f.peer_id];
			
		const char* service_what = f.item_type == 0 ? "request " : "response";
		char handling_time[64] = "";
		if(f.item_type == 1)
			snprintf(handling_time, 64, ", svc-handler-duration: %.3fms", (f.completion_time - f.request_time) * 1e3);
			
		printf("%s %s %d/%d %s from %s\n%23.23s transfer_time: %5.3fms, data_len: %d bytes, thread_id %#lx%s, peer: %s\n",
		       get_datetime(f.completion_time).c_str(),
		       service_what,
		       f.client_id, f.request_id,
		       svc.service_name.c_str(),
		       string_util::repr(svc.client_name).c_str(),			       
		       "", f.transfer_time * 1e3,
		       (unsigned int)f.data.size(),
		       f.thread_id,
		       handling_time,
		       peer.c_str());
		
		break;
	}
	}
	return want_more;
}

double replay_backend::wait_frame(any_frame* frame) {
	double ts = start_time + frame->timestamp - file_start_time;
	double now = ln_get_time();
	double time_left = ts - now  - initial_pause;
	
	if(time_left <= 0)
		return ts;	
	
	if(stop_time > 0 && now >= stop_time)
		want_more = false;
	
	if(parent->verbosity >= 3)
		printf("sleep %.3fs\n", time_left);
	
	ln_sleep_seconds(time_left);

	return ts;
}

}
