#include <ln/ln.h>

#include <string_util/string_util.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#ifndef __WIN32__
#include <sys/select.h>
#endif

#include "pcaplog.h"

using namespace std;
using namespace string_util;

namespace lnrecorder {

struct file_header_t {
	uint32_t magic;
	uint16_t major;
	uint16_t minor;
	uint32_t timezone_offset;
	uint32_t time_stamp_acc;
	uint32_t snap_len;
	uint32_t ll_header_type;
};

pcaplog::pcaplog(string log_fn, file_backend* fb, unsigned int max_log_size) : fb(fb), max_log_size(max_log_size) {
	if(max_log_size > 0) {
		string::size_type p = log_fn.find(".");
		string bn;
		string ext;
		if(p != string::npos) {
			bn = log_fn.substr(0, p);
			ext = log_fn.substr(p);
		} else {
			bn = log_fn;
			ext = "";
		}
		fns[0] = bn + ".1" + ext;
		fns[1] = bn + ".2" + ext;
	} else
		fns[0] = log_fn;
	
	fp = NULL;
	current_fn = 1;
	_open(false, log_fn);
}

void pcaplog::_open(bool choose_next, string alt_name) {
	string fn;
	if(max_log_size > 0 && choose_next) {
		current_fn = (current_fn + 1) % 2;
		fn = fns[current_fn];
	} else
		fn = alt_name;
	if(fb && fp)
		fb->close(fp);
	current_size = 0;
	fp = fb->open(fn.c_str(), "wb");

	file_header_t header;
	header.magic = 0xa1b2c3d4;
	header.major = 2;
	header.minor = 4;
	header.timezone_offset = 0;
	header.time_stamp_acc = 0;
	header.snap_len = 1024*1024*1024;
	header.ll_header_type = 147; // DLT_USER0

	int ret;
	ret = fb->write(&header, sizeof(header), 1, fp);
	if(ret != 1)
		throw str_exception_tb("failed to write header\n");
}

pcaplog::~pcaplog() {
	close();
}

void pcaplog::close() {
	if(fb && fp) {
		fb->close(fp);
		fp = NULL;
	}
}

void pcaplog::packet_start(struct timespec* ts) {
	if(!ts) {
#ifndef __WIN32__
		clock_gettime(CLOCK_REALTIME, &local_ts);
#else
		// C++11: timespec_get(&local_ts, TIME_UTC);
		double t = ln_get_time();
		local_ts.tv_sec = (unsigned long)t;
		local_ts.tv_nsec = (t - local_ts.tv_sec) * 1e9;
#endif		
		ts = &local_ts;
	}
	packet_ts = ts;
	current_packet_offset = 0;
}

void pcaplog::packet_add(const void* data, unsigned int len) {
	if(current_packet_offset + len > current_packet.size())
		current_packet.resize(current_packet_offset + len);
	memcpy(&current_packet[current_packet_offset], data, len);
	current_packet_offset += len;
}

void pcaplog::packet_done() {
	if(max_log_size > 0 && current_size >= max_log_size) {
		// switch log files
		_open();
	}
	packet_header_t phdr;
	// now write packet!
	phdr.sec = packet_ts->tv_sec;
	phdr.nsec = (uint32_t)packet_ts->tv_nsec;
	phdr.n_bytes = current_packet_offset;
	phdr.full_n_bytes = phdr.n_bytes;
	if(fb->write(&phdr, sizeof(phdr), 1, fp) != 1)
		throw str_exception_tb("failed to write packet header!\n");
	current_size += sizeof(phdr) + phdr.n_bytes;

	if(fb->write(&current_packet[0], current_packet_offset, 1, fp) != 1)
		throw errno_exception_tb("failed to write packet data!");
}


pcaplog* pcaplog::open_for_reading(std::string log_fn, file_backend* fb) {	
	pcaplog* ret = new pcaplog();
	try {
		ret->_open_for_reading(log_fn, fb);
	}
	catch(const exception& e) {
		delete ret;
		throw;
	}
	return ret;
}
pcaplog::pcaplog() : max_log_size(0) {
	fp = NULL;
	current_fn = 1;
}
void pcaplog::_open_for_reading(std::string log_fn, file_backend* fb) {
	this->fb = fb;
	fp = fb->open(log_fn.c_str(), "rb");

	file_header_t header;
	int ret = fb->read(&header, sizeof(header), 1, fp);
	if(ret == 0)
		throw str_exception_tb("eof");
	if(ret != 1)
		throw errno_exception_tb("can not read file header!");
	if(header.magic != 0xa1b2c3d4)
		throw str_exception("file has invalid file header %#x!", header.magic);
	if(header.major != 2)
		printf("warning: file has unknown major %d\n", header.major);
	if(header.minor != 4)
		printf("warning: file has unknown minor %d\n", header.minor);
	if(header.ll_header_type != 147)
		printf("warning: file has unexpected ll_header_type of %d\n", header.ll_header_type);
	current_packet_len = 0;
}

bool pcaplog::read_next_frame(struct timespec* ts) {
	int ret = fb->read(&phdr, sizeof(phdr), 1, fp);
	if(ret == 0)
		return false; // eof
	if(ret != 1)
		throw errno_exception_tb("can not read next frame!");
			
	ts->tv_sec = phdr.sec;
	ts->tv_nsec = phdr.nsec;
	// printf("next frame ts: %10d %10d, %d bytes\n", ts->tv_sec, ts->tv_nsec, phdr.n_bytes);

	packet_len = phdr.n_bytes;
	if(current_packet.size() < packet_len)
		current_packet.resize(packet_len);
	current_packet_len = packet_len;

	ret = fb->read(&current_packet[0], current_packet_len, 1, fp);
	if(ret == 0)
		throw str_exception_tb("eof"); // should not really happen, because there should be no header..
	if(ret != 1)
		throw errno_exception_tb("can not read next frame data!");

	current_packet_offset = 0;
	return true;
}
template<typename T>
void pcaplog::consume(T& value) {
	if(current_packet_offset + sizeof(T) > current_packet_len)
		throw str_exception_tb("read behind end of packet!");
	value = *(T*)&current_packet[current_packet_offset];
	current_packet_offset += sizeof(T);
}
void pcaplog::consume_data(uint8_t* dst, unsigned int len) {
	if((current_packet_offset + len) > current_packet_len)
		throw str_exception_tb("read behind end of packet!");
	memcpy(dst, &current_packet[current_packet_offset], len);
	current_packet_offset += len;
}
uint8_t* pcaplog::get_ptr(unsigned int inc_after) {
	if((current_packet_offset + inc_after) > current_packet_len)
		throw str_exception_tb("read behind end of packet! current_packet_offset: %d, inc_after: %d, current_packet_len: %d",
		current_packet_offset, inc_after, current_packet_len);
	uint8_t* ret = &current_packet[current_packet_offset];
	current_packet_offset += inc_after;
	return ret;
}
void pcaplog::skip(unsigned int inc_after) {
	if((current_packet_offset + inc_after) > current_packet_len)
		throw str_exception_tb("skip behind end of packet!");
	current_packet_offset += inc_after;
}

template void pcaplog::consume<uint64_t>(uint64_t&);
template void pcaplog::consume<uint32_t>(uint32_t&);
template void pcaplog::consume<double>(double&);
template void pcaplog::consume<uint8_t>(uint8_t&);

}
