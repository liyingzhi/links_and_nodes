#ifndef FRAMES_H
#define FRAMES_H

#include <stdio.h>
#include <stdint.h>

#include <string>

#include <ln/ln.h>

namespace lnrecorder {

class lnrecorder_topic;

struct frame_t {		
	virtual void print() = 0;
	virtual ~frame_t() {};
};


struct header_frame : public frame_t {
	uint32_t frame_type;
	uint16_t version;
	virtual void print();
};


struct new_topic_frame : public frame_t {
	uint32_t topic_id;
	uint32_t message_size;
	std::string name;
	std::string md;
	std::string msg_def;
	std::string client;
	std::string msg_def_hash;
	lnrecorder_topic* topic;
	virtual void print();
};

struct new_packet_frame : public frame_t {
	uint32_t topic_id;
	double publisher_timestamp;
	lnrecorder_topic* topic;
	virtual void print();
};

struct del_topic_frame : public frame_t {
	lnrecorder_topic* topic;
	uint32_t topic_id;
	virtual void print();
};


struct new_service_peer_frame : public frame_t {
	std::string peer;
	uint32_t peer_id;
	virtual void print();
};

struct new_service_frame : public frame_t {
	uint32_t service_id;
	std::string client_name;
	std::string service_name;
	std::string service_interface;
	std::string message_definition;
	std::string msg_def_hash;
	virtual void print();
};

struct new_service_event_frame : public frame_t {
	uint32_t service_id;
	uint8_t item_type;
	uint8_t flags;
	double completion_time;
	double transfer_time;
	double request_time;
	uint32_t peer_id;
	uint32_t client_id;
	uint32_t request_id;
	uint64_t thread_id;
	std::string data;
	virtual void print();
};


enum frame_types {
	HEADER_FRAME,
	
	NEW_TOPIC_FRAME,
	NEW_PACKET_FRAME,
	DEL_TOPIC_FRAME,
	
	NEW_SERVICE_PEER_FRAME,
	NEW_SERVICE_FRAME,
	NEW_SERVICE_EVENT_FRAME
};

struct any_frame {
	frame_types type;
	double timestamp; // out of band packet transport timestamp
	
	header_frame header;
	
	new_topic_frame new_topic;
	new_packet_frame new_packet;
	del_topic_frame del_topic;
	
	new_service_peer_frame new_service_peer;
	new_service_frame new_service;
	new_service_event_frame new_service_event;

	void print();
};

}

#endif // FRAMES_H
