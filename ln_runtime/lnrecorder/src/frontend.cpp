#include "lnrecorder.h"
#include "frontend.h"

namespace lnrecorder {

frontend::~frontend()
{
	for(topics_by_id_t::iterator i = topics_by_id.begin(); i != topics_by_id.end(); ++i)
		delete i->second;
}

void frontend::process_frame(any_frame* frame)
{
	if(parent->verbosity >= 3)
		frame->print();
	
	bool want_more = target_be->write_frame(frame);
	if(!want_more)
		*keep_running_ptr = false;
}



}
