/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include <stdio.h>
#include <string_util/exceptions.h>
#include "lock.h"

namespace lnrecorder {

using namespace string_util;

lock::lock() {
	locked = false;
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
#if !defined(__ANDROID__)
	pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_INHERIT);
#endif
	int ret = pthread_mutex_init(&mutex, &attr);
	pthread_mutexattr_destroy(&attr);
	if(ret)
		throw retval_exception_tb(ret, "pthread_mutex_init");
}

lock::~lock() {
	if(this->locked)
		unlock();
	pthread_mutex_destroy(&mutex);
}

void lock::operator()() {
	// printf("locking lock %p, this: %p\n", &rwlock, this);
	int ret = pthread_mutex_lock(&mutex);
	if(ret)
		throw retval_exception_tb(ret, "pthread_mutex_lock");
	this->locked = true;
}

bool lock::trylock() {
	int ret = pthread_mutex_trylock(&mutex);
	if(ret == 0) {
		this->locked = true;
		return true;
	}
	return false;
		
}

void lock::unlock() {
	int ret = pthread_mutex_unlock(&mutex);
	if(ret)
		throw retval_exception_tb(ret, "pthread_mutex_unlock");
 	this->locked = false;
}

bool lock::free() {
	return !locked;
}

}
