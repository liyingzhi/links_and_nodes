#include <stdio.h>

#include <string>
#include <algorithm>

#include <string_util/string_util.h>

#include "lnrecorder.h"
#include "lnrdb_frontend.h"

namespace lnrecorder {

lnrdb_frontend::lnrdb_frontend(lnrecorder* parent, std::string input, const TopicPatterns& topic_patterns) :
	frontend(parent, topic_patterns)
{
	db = lnrdb::open_for_reading(input);
	
	file_version = db->meta->get_unsigned_int("lnrecorder-version");
	if(file_version > FILE_VERSION)
		printf("warning: lnrdb is written in version %d, we are %d!\n", file_version, FILE_VERSION);
	else
		printf("reading lnrdb version %d\n", file_version);
	printf("WARNING: will only emit topic-data!\n"); // service requests & responses are currently not reconstructed from db!
}

lnrdb_frontend::topic::topic(lnrdb_frontend* parent, std::string table_name) :
	parent(parent)
{
	table = parent->db->open_table(table_name);

	new_topic_frame& f = frame.new_topic;
	f.topic_id = table->meta->get_unsigned_int("topic.id");
	f.message_size = table->meta->get_unsigned_int("topic.message_size");
	f.name = table->meta->get_string("topic.name");
	f.md = table->meta->get_string("topic.md");
	f.msg_def = table->meta->get_string("topic.msg_def");
	f.client = table->meta->get_string("topic.publisher");
	f.msg_def_hash = table->meta->get_string("topic.msg_def_hash");
	f.topic = parent->topics_by_id[f.topic_id] = new lnrecorder_topic(parent->parent, &f);
	
	new_packet_frame& fp = frame.new_packet;
	fp.topic_id = f.topic_id;
	fp.topic = f.topic;

	del_topic_frame& fd = frame.del_topic;
	fd.topic_id = f.topic_id;
	fd.topic = f.topic;
	
	frame.timestamp = first_seen = table->meta->get_double("topic.first_seen");
	last_seen = table->meta->get_double("topic.last_seen");
	
	next_packet_ts = &first_seen;

	is_published = false;
	is_eof = read_record();

	if(is_eof) {
		// not a single record...
		memset(&next_packet_header, 0, sizeof(next_packet_header));
		return;
	}
	
	if(parent->use_publisher_timestamp) {
		frame.timestamp = first_seen = next_packet_header.publisher_timestamp;

		// read last record
		uint64_t n_records = table->get_n_records();
		table->seek_record(n_records - 1);
		read_record();
		last_seen = next_packet_header.publisher_timestamp;
		
		table->seek_record(0);
		read_record();
	}
}

bool lnrdb_frontend::topic::read_record()
{
	lnrdb::record_item items[] = {
		{ &next_packet_header, sizeof(next_packet_header) },
		{ &frame.new_packet.topic->packet[0], frame.new_packet.topic->packet.size() }
	};
	return table->read_record(items, array_length(items));
}
double lnrdb_frontend::topic::send()
{
	if(!is_published) {
		is_published = true;
		
		// publish topic!
		frame.type = NEW_TOPIC_FRAME;
		parent->process_frame(&frame);
		
		if(is_eof) {
			next_packet_ts = &last_seen;
			return last_seen;
		}

		frame.type = NEW_PACKET_FRAME;
		if(parent->use_publisher_timestamp)
			next_packet_ts = &next_packet_header.publisher_timestamp;
		else
			next_packet_ts = &next_packet_header.timestamp;		
		return *next_packet_ts;
	}
	if(is_eof) {
		// unsubscribe now!
		frame.type = DEL_TOPIC_FRAME;
		frame.timestamp = *next_packet_ts;
		parent->process_frame(&frame);
		return 0;
	}
	// send packet!
	frame.timestamp = *next_packet_ts;
	frame.new_packet.publisher_timestamp = next_packet_header.publisher_timestamp;
	parent->process_frame(&frame);
	
	is_eof = read_record();
	if(is_eof)
		next_packet_ts = &last_seen;
	
	return *next_packet_ts;
}

bool cmp_first_seen(const lnrdb_frontend::topic* a, const lnrdb_frontend::topic* b)
{
	return *a->next_packet_ts < *b->next_packet_ts;
}

void lnrdb_frontend::write_frames(std::shared_ptr<backend> backend, bool* keep_running_ptr)
{
	parent->current_frontend = this;
	if(keep_running_ptr == NULL) {
		keep_running = true;
		keep_running_ptr = &keep_running;
	}
	target_be = backend;
	
	any_frame frame;

	std::list<topic> all_topics;
	typedef std::list<topic*> next_packets_t;
	next_packets_t next_packets;
	// read them
	std::list<std::string> table_names = db->get_table_names();
	for(auto&& i = table_names.begin(); i != table_names.end(); i++) {
		std::string& table_name = *i;
		if(table_name.substr(0, 7) != "topics/")
			continue;
		all_topics.emplace_back(this, table_name);
		next_packets.push_back(&all_topics.back());
	}
	
	// sort them
	next_packets.sort(cmp_first_seen);	

	// from now on keep it sorted!
	while(*keep_running_ptr) {
		
		if(!next_packets.size())
			break; // eof

		next_packets_t::iterator now = next_packets.begin();
		auto topic = *now;
		double next_ts = topic->send();
		// put at new correct spot in list
		if(next_ts == 0) {
			topics_by_id_t::iterator ti = topics_by_id.find(topic->frame.del_topic.topic_id);
			if(ti != topics_by_id.end()) {
				delete topic->frame.del_topic.topic;
				topics_by_id.erase(ti);
			}
			next_packets.erase(now);
			// we could also remove it from all_topics
			continue;
		}
		next_packets_t::iterator next = now;
		next++;
		if(next == next_packets.end())
			continue;
		next_packets_t::iterator next_after_now = next;
		while(next != next_packets.end() && *(*next)->next_packet_ts < next_ts)
			next ++;
		if(next == next_after_now)
			continue;
		// put at new position
		std::rotate(now, next_after_now, next);
	}
}

}
