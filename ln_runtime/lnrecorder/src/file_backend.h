#ifndef FILE_BACKEND_H
#define FILE_BACKEND_H

namespace lnrecorder {

class plain_file_backend;
class piped_file_backend;

class file_backend {
public:
	class file {
	protected:
		~file() {};
		friend class file_backend;
		friend class plain_file_backend;
		friend class piped_file_backend;
	public:
	};

	virtual ~file_backend() {};
	
	virtual file* open(const char* name, const char* mode) = 0;
	virtual size_t write(const void* src, unsigned int size, unsigned int n_elem, file* fp) = 0;
	virtual size_t read(void* dst, unsigned int size, unsigned int n_elem, file* fp) = 0;
	virtual void close(file* fp) = 0;

	virtual bool magic_matches(const char* name) = 0;
};

}

#endif /* FILE_BACKEND_H */

