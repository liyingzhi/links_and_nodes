#include <stdio.h>

#include <string>

#include <string_util/string_util.h>

#include "lnrecorder.h"
#include "pcap_frontend.h"
#include "file_backend.h"

namespace lnrecorder {

pcap_frontend::pcap_frontend(lnrecorder* parent, std::string input, const TopicPatterns& topic_patterns) :
	frontend(parent, topic_patterns) // todo: topic patterns are not implemented in here!
{
	file_backend* fb = parent->find_file_backend(input);
	plog.reset(pcaplog::open_for_reading(input, fb));
	read_header_frame();
	printf("reading pcap file version %d\n", file_version);
}

void pcap_frontend::read_header_frame()
{
	any_frame frame;
	if(!read_frame_type(&frame))
		throw str_exception_tb("got EOF while trying to read header frame!");

	if(frame.type != HEADER_FRAME)
		throw str_exception_tb("got unexpected frame_Type %d when expecting header-frame!", frame.type);
	
	header_frame& hdr = frame.header;
	
	uint32_t this_version; //  = 0xffff0000 + version;
	plog->consume(this_version);
	if((this_version & 0xffff0000) != 0xffff0000)
		throw errno_exception_tb("invalid file version: %#x!", this_version);
	hdr.version = (uint16_t)(this_version & 0x0000ffff);
	if(hdr.version > FILE_VERSION)
		printf("warning: file is written in version %d, we are %d!\n", hdr.version, FILE_VERSION);
	file_version = hdr.version;
}

bool pcap_frontend::read_frame_type(any_frame* frame)
{
	struct timespec ts;
	if(!plog->read_next_frame(&ts)) // eof
		return false;
	
	uint32_t frame_type; // todo: maybe add specialization for frame_types type to read uint32_t
	plog->consume(frame_type);
	frame->type = (frame_types)frame_type;
	frame->timestamp = ts.tv_sec + ts.tv_nsec/1e9;		
	return true;
}

std::string pcap_frontend::read_field() {
	uint32_t field_len;
	plog->consume(field_len);
	return std::string((const char*)plog->get_ptr(field_len), field_len);
}

void pcap_frontend::write_frames(std::shared_ptr<backend> backend, bool* keep_running_ptr)
{
	parent->current_frontend = this;
	if(keep_running_ptr == NULL) {
		keep_running = true;
		keep_running_ptr = &keep_running;
	}
	target_be = backend;
	
	any_frame frame;
	
	while(*keep_running_ptr) {		
		if(!read_frame_type(&frame))
			break; // eof
		
		switch(frame.type) {
		case HEADER_FRAME:
			break;
		case NEW_TOPIC_FRAME: {
			new_topic_frame& f = frame.new_topic;
			plog->consume(f.topic_id);
			plog->consume(f.message_size);
			f.name = read_field();
			f.md = read_field();
			f.msg_def = read_field();
			f.client = read_field();
			if(file_version < 2)
				f.msg_def_hash = ""; // unknown
			else
				f.msg_def_hash = read_field();
			
			f.topic = topics_by_id[f.topic_id] = new lnrecorder_topic(parent, &f);
			break;
		}
		case NEW_PACKET_FRAME: {
			new_packet_frame& f = frame.new_packet;
			plog->consume(f.topic_id);	
			f.topic = topics_by_id[f.topic_id];
			if(!f.topic)
				throw str_exception_tb("pcap file contains unknown topic_id %d!", f.topic_id);
			plog->consume_data(&f.topic->packet[0], f.topic->packet.size());
	
			if(file_version >= 2)
				plog->consume(f.publisher_timestamp);
			else
				f.publisher_timestamp = frame.timestamp; // on v1 files the pcap-timestamp was the publisher-timestamp			
			break;
		}
		case DEL_TOPIC_FRAME: {
			del_topic_frame& f = frame.del_topic;
			plog->consume(f.topic_id);
			
			topics_by_id_t::iterator ti = topics_by_id.find(f.topic_id);
			f.topic = ti->second;
			
			process_frame(&frame); // process before deleting topic
			
			if(ti != topics_by_id.end()) {
				delete f.topic;
				topics_by_id.erase(ti);
			}
			continue;
		}
		
		case NEW_SERVICE_PEER_FRAME: {
			new_service_peer_frame& f = frame.new_service_peer;
			f.peer = read_field();
			plog->consume(f.peer_id);			
			break;
		}
		case NEW_SERVICE_FRAME: {
			new_service_frame& f = frame.new_service;
			f.client_name = read_field();
			f.service_name = read_field();
			f.service_interface = read_field();
			f.message_definition = read_field();
			plog->consume(f.service_id);
			if(file_version < 2)
				f.msg_def_hash = ""; // unknown
			else
				f.msg_def_hash = read_field();			
			break;
		}
		case NEW_SERVICE_EVENT_FRAME: {
			new_service_event_frame& f = frame.new_service_event;
			plog->consume(f.service_id);
			plog->consume(f.item_type);
			plog->consume(f.flags);
			plog->consume(f.completion_time);
			plog->consume(f.transfer_time);
			plog->consume(f.request_time);
			plog->consume(f.peer_id);
			plog->consume(f.client_id);
			plog->consume(f.request_id);
			plog->consume(f.thread_id);
			f.data = read_field();			
			break;
		}
		}
		process_frame(&frame);
	}
}

}
