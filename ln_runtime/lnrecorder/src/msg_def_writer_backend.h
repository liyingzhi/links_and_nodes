#ifndef MSG_DEF_WRITER_BACKEND_H
#define MSG_DEF_WRITER_BACKEND_H

#include <stdint.h>

#include <memory>
#include <string>

#include "frames.h"
#include "backend.h"

namespace lnrecorder {

class msg_def_writer_backend :
		public backend
{
	std::string base_dir;
	void write_md(std::string name, std::string body);
public:	
	msg_def_writer_backend(lnrecorder* parent, std::string output);
	
	bool write_frame(any_frame* frame);
};

}

#endif // MSG_DEF_WRITER_BACKEND_H

