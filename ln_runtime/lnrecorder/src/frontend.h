#ifndef LNRECORDER_FRONTEND_H
#define LNRECORDER_FRONTEND_H

#include <memory>

#include "frames.h"
#include "topic_patterns.h"
#include "lnrecorder_topic.h"

namespace lnrecorder {

class backend;
class lnrecorder;
class lnrdb_backend;

class frontend {
	friend class lnrdb_backend;
protected:
	lnrecorder* parent;
	const TopicPatterns& topic_patterns;
	
	uint16_t file_version;
	std::shared_ptr<backend> target_be;
	topics_by_id_t topics_by_id;
	bool keep_running;
	bool* keep_running_ptr;
	
	void process_frame(any_frame* frame);
public:
	frontend(lnrecorder* parent, const TopicPatterns& topic_patterns) :
		parent(parent), topic_patterns(topic_patterns) {}
	virtual ~frontend();
	
	virtual void write_frames(std::shared_ptr<backend> backend, bool* keep_running_ptr=NULL) = 0;
};

}

#endif // LNRECORDER_FRONTEND_H
