#ifndef PCAP_FRONTEND_H
#define PCAP_FRONTEND_H

#include <string>
#include <memory>

#include "frontend.h"
#include "frames.h"
#include "pcaplog.h"

namespace lnrecorder {

class pcap_frontend :
		public frontend
{
	std::unique_ptr<pcaplog> plog;
	
	bool read_frame_type(any_frame* frame);
	std::string read_field();	
	void read_header_frame();
	
public:	
	pcap_frontend(lnrecorder* parent, std::string input, const TopicPatterns& topic_patterns);
	
	void write_frames(std::shared_ptr<backend> backend, bool* keep_running_ptr=NULL);
};

}

#endif // PCAP_FRONTEND_H
