#ifndef LNRDB_FRONTEND_H
#define LNRDB_FRONTEND_H

#include <stdint.h>

#include <map>

#include "frontend.h"
#include "frames.h"
#include "lnrdb/lnrdb.h"
#include "lock.h"

namespace lnrecorder {

class lnrdb_frontend :
		public frontend
{	
	std::unique_ptr<lnrdb::db> db;
	lock file_lock;

	bool use_publisher_timestamp = false;
	
	struct topic {
		lnrdb_frontend* parent;
		lnrdb::table* table;

		bool is_published;
		bool is_eof;
		
		// from meta:
		double first_seen; // publish start, not later than next_packet_ts...
		double last_seen; // unpublish
		
		struct {
			double timestamp;
			double publisher_timestamp;
		} next_packet_header;
		double* next_packet_ts;

		any_frame frame;
		
		topic(lnrdb_frontend* parent, std::string table_name);
		bool read_record();
		double send(); // returns timestamp of next event for this topic
	};
	friend class topic;
	friend bool cmp_first_seen(const lnrdb_frontend::topic* a, const lnrdb_frontend::topic* b);
public:
	lnrdb_frontend(lnrecorder* parent, std::string input, const TopicPatterns& topic_patterns);
	
	void write_frames(std::shared_ptr<backend> backend, bool* keep_running_ptr=NULL);
};

}

#endif // LNRDB_FRONTEND_H

