#include <string_util/string_util.h>

#include "topic_patterns.h"

namespace lnrecorder {

TopicPatterns::Pattern::Pattern(std::string pattern, std::string rate_or_change, bool only_topics, bool only_services, bool exclude) :
	pattern(pattern),
	only_topics(only_topics),
	only_services(only_services),
	exclude(exclude)
{
	if(rate_or_change == "change") {
		rate = -1;
		only_on_change = true;
	} else {
		rate = atof(rate_or_change.c_str());
		only_on_change = false;
	}
}

void TopicPatterns::clear()
{
	container.clear();
}
bool TopicPatterns::is_empty() const
{
	return container.size() == 0;
}

void TopicPatterns::add(std::string arg)
{
	bool exclude = false;
	if(arg.substr(0, 1) == "!") {
		exclude = true;
		arg = arg.substr(1, std::string::npos);
	}
	std::vector<std::string> parts = string_util::split_string(arg, "@", 1);
	std::string pattern = parts[0];
	bool only_topics = false;				
	bool only_services = false;
	
	if(pattern.substr(0, 6) == "topic:") {
		only_topics = true; 
		pattern = pattern.substr(6);
	} else if(pattern.substr(0, 8) == "service:") {
		only_services = true;
		pattern = pattern.substr(8);
	}
	
	if(parts.size() == 1)
		container.push_back(Pattern(pattern, "-1", only_topics, only_services, exclude));
	else
		container.push_back(Pattern(pattern, parts[1], only_topics, only_services, exclude));
}

bool TopicPatterns::have_match(const std::string& search, double* rate, bool* only_on_change) const
{
	if(is_empty()) {
		if(rate)
			*rate = -1;
		if(only_on_change)
			*only_on_change = false;
		return true;
	}

	// ignore if there is any exclude for this!
	for(container_t::const_iterator i = container.begin(); i != container.end(); ++i) {
		if(!i->exclude)
			continue;
		if(string_util::pattern_matches(i->pattern, search))
			return false; // excluded!
	}

	// no excludes, is there a match?
	for(container_t::const_iterator i = container.begin(); i != container.end(); ++i) {
		if(i->exclude)
			continue;
		if(!string_util::pattern_matches(i->pattern, search))
			continue;
		if(rate)
			*rate = i->rate;
		if(only_on_change)
			*only_on_change = i->only_on_change;		
		return true;			
	}
	return false;
}


std::string TopicPatterns::get_service_patterns() const
{
	if(is_empty())
		return "*";

	std::stringstream ret;
	bool first = true;
	for(container_t::const_iterator i = container.begin(); i != container.end(); ++i) {
		if(i->only_topics)
			continue;
		if(first)
			first = false;
		else
			ret << ",";
		if(i->exclude)
			ret << "!";
		ret << i->pattern;
	}
	return ret.str();
}

}
