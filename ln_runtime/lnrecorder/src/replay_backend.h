#ifndef REPLAY_BACKEND_H
#define REPLAY_BACKEND_H

#include <stdint.h>

#include <string>
#include <map>

#include "backend.h"
#include "frames.h"

namespace lnrecorder {

class replay_backend :
		public backend
{
	double file_start_time;
	double replay_start;
	double replay_stop;
	unsigned int service_verbosity;

	double start_time;
	double stop_time;
	bool is_first_topic;
	double initial_pause;

	typedef std::map<uint32_t, std::string> known_peers_t;
	known_peers_t known_peers;
	
	typedef std::map<uint32_t, new_service_frame> known_services_t;
	known_services_t known_services;
	
	double wait_frame(any_frame* frame);
	bool want_more;
public:
	replay_backend(lnrecorder* parent, double replay_start=0, double replay_stop=0, unsigned int service_verbosity=0);
	
	bool write_frame(any_frame* frame);
};

}

#endif // REPLAY_BACKEND_H
