#!/usr/bin/python

import os
import sys
import pprint
import numpy as np

lnrecorder_python_path = os.path.join(os.path.dirname(__file__), "python")
if lnrecorder_python_path not in sys.path:
    sys.path.insert(0, lnrecorder_python_path)

from lnrecorder import *
    
log_filename = sys.argv[1]
log = lnrecorder_log(log_filename)

mode = "raw_pcap"
mode = "raw_packet"
mode = "packet"
mode = "decoded_services"
mode = "decoded"

np.set_printoptions(linewidth=140, precision=4, suppress=True)

if mode == "raw_pcap":
    # iterate over raw pcap frames:
    for hdr, frame in log.iter_frames():
        print hdr
        print repr(frame)

elif mode == "raw_packet":
    # iterate over raw lnrecorder packets:
    for pkt in log.iter_raw_packets():
        print pkt

elif mode == "packet":
    # iterate over non-meta lnrecorder packets:
    for pkt in log.iter_packets():
        print pkt

elif mode == "decoded_services":
    # iterate over service lnrecorder packets and decode them:
    for pkt in log.iter_packets(only_services=True):
        print
        print 
        print pkt
        decoded = log.decode_packet(pkt, with_meta=False)
        pprint_odict(decoded)

elif mode == "decoded":    
    # iterate over non-meta lnrecorder packets and decode them:
    for pkt in log.iter_packets():
        print
        print 
        print pkt
        decoded = log.decode_packet(pkt, with_meta=False)
        pprint_odict(decoded)
