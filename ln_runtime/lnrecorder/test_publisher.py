#!/usr/bin/python

import random
import links_and_nodes as ln
import sys
import time
import copy

clnt = ln.client(sys.argv[0], sys.argv)

port = clnt.publish("test.topic/1", "lnrecorder_test/topic1")

print "ready"

p = port.packet
p.count = 0
while True:
    p.last.count = p.count
    p.last.time = p.time
    p.last.depth[0] = p.count
    p.last.depth[-1] = p.count

    for i in range(2, 0, -1):
        p.last3[i] = copy.deepcopy(p.last3[i - 1])
    p.last3[0] = copy.deepcopy(p.last)
    
    p.count += 1
    p.time = time.time()
    
    port.write()
    time.sleep(random.random() * 0.01)
