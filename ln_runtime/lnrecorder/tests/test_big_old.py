#!/usr/bin/python

from __future__ import print_function
import time
import sys
import os
import numpy as np
tests = os.path.dirname(os.path.abspath(__file__))
lnrecorder = os.path.dirname(tests)
ln_runtime = os.path.dirname(lnrecorder)
base = os.path.dirname(ln_runtime)
pp = os.path.join(base, "python")
if pp not in sys.path:
    sys.path.insert(0, pp)

from links_and_nodes.lnrecorder import lnrecorder_log
from links_and_nodes.lnrecorder.lnrecorder_packets import *

pcap = "big"
#pcap = "small"

log = lnrecorder_log(pcap)

a = time.time()

data_clock = []
have_topic_id = None
for p in log.iter_packets():
    if not isinstance(p, new_tpacket_packet):
        continue
    if p.topic_id != have_topic_id:
        if log.topics[p.topic_id].name == "alfred.telemetry":
            have_topic_id = p.topic_id
        continue
    d = log.decode_packet(p, with_meta=False)
    data_clock.append(d["data"]["clock"])
b = time.time()
print("iterate packets: %.3fs" % (b - a))
clock_diff = np.diff(data_clock, axis=0)
print("clock_diff shape: %s, clock ptp: %.3fms, mean: %.3fms, std: %.3fms" % (
    clock_diff.shape,
    clock_diff.ptp() * 1e3,
    clock_diff.mean() * 1e3,
    clock_diff.std() * 1e3))

c = time.time()
print("only statistics: %.3fs" % (c - b))

print("complete runtime: %.3fs" % (c - a))
