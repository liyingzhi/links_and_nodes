#!/usr/bin/python

import sys
import time
import links_and_nodes as ln
import pprint

class test_client(ln.services_wrapper):
    def __init__(self):
        self.clnt = ln.client(sys.argv[0], sys.argv)
        ln.services_wrapper.__init__(self, self.clnt)
        self.wrap_service("test_service", "get_times")

if __name__ == "__main__":
    c = test_client()
    print "ready"
    while True:
        for i in xrange(2):
            ret = c.test_service([0, 1, 0, 1])
            pprint.pprint(ret)
            time.sleep(0.5)
        time.sleep(3)
