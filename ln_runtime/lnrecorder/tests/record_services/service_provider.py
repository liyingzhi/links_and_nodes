#!/usr/bin/python

import sys
import links_and_nodes as ln
import traceback
import time
import pprint
from ctypes import *

class timespec(Structure):
    _fields_ = [
        ("tv_sec", c_long),
        ("tv_nsec", c_long)
    ]
    def __str__(self):
        return "<%s %s>" % (
            self.__class__.__name__,
            " ".join(["%s: %r" % (fn, getattr(self, fn)) for fn, tf in self.__class__._fields_]))
clock_ids = [0, 1]
try:
    libc = CDLL("libc.so.6")
    libc.clock_gettime
except:
    libc = CDLL("librt.so")
    libc.clock_gettime

clnt = ln.client(sys.argv[0], sys.argv)
get_times = clnt.get_service_provider("test_service", "get_times")

def on_get_times(svc_req, req, resp):
    resp.error_message = ""
    try:
        #print "got request"
        #pprint.pprint(req.dict())
        resp.times = []
        for clk_id in req.clock_ids:
            if clk_id not in clock_ids:
                raise Exception("invalid/unknown clock_id %r!" % clk_id)
            ct = resp.new_clock_time_packet()
            resp.times.append(ct)
            ct.clock_id = clk_id
            
            ts = timespec()
            libc.clock_gettime(c_uint(clk_id), byref(ts))
            ct.ts.tv_sec = ts.tv_sec
            ct.ts.tv_nsec = ts.tv_nsec
            #time.sleep(0.25)
        #print "sending response:"
        #pprint.pprint(resp.dict())
    except:
        print traceback.format_exc()
        resp.error_message = traceback.format_exc()
    svc_req.respond()
    return 0

get_times.set_handler(on_get_times)
get_times.do_register("main")

print "ready"
while True:
    clnt.wait_and_handle_service_group_requests("main", 1)
