import sys
import time

import links_and_nodes as ln
import lnrecorder

clnt = ln.client(sys.argv[0], sys.argv)
my_recorder = lnrecorder.lnrecorder_service_wrapper(clnt, "my_recorder")

for i in xrange(10):
    print "start"
    my_recorder.record("/tmp/log_%.3f.pcap" % time.time(), ["*"])
    print "recording"
    time.sleep(0.1)
    print "stop"
    my_recorder.stop()
