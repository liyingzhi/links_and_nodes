#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>

#include <memory>

#include <string_util/string_util.h>

#include "file.h"
#include "base_dir.h"

#ifndef __WIN32__
#  define OS_SEP "/"
#else
#  define OS_SEP "\\"
#endif


namespace lnrdb {

std::string base_dir::dir_entry(std::string entry, bool canonicalize)
{
	if(canonicalize) {
		entry = string_util::string_replace(entry, "/", "_");
		entry = string_util::string_replace(entry, "-", "_");
		entry = string_util::string_replace(entry, "\\", "_");
	}
	
	std::string ret = dir_name;
	ret += OS_SEP;
	ret += entry;
	return ret;
}

bool is_dir(std::string dn)
{
	struct stat buf;
	int ret = stat(dn.c_str(), &buf);
	if(ret == -1)
		return false;
	return (buf.st_mode & S_IFMT) == S_IFDIR;
}

bool is_file(std::string dn)
{
	struct stat buf;
	int ret = stat(dn.c_str(), &buf);
	if(ret == -1)
		return false;
	return (buf.st_mode & S_IFREG) == S_IFREG;
}

void mkdir(std::string dir)
{
	std::vector<std::string> dir_comps = string_util::split_string(dir, OS_SEP);
	for(unsigned int i = 0; i < dir_comps.size(); i++) {
		if(i == 0 && dir_comps[i] == "")
			continue;
		std::string dn = string_util::join_string(dir_comps, OS_SEP, 0, i + 1);
		if(!is_dir(dn)) {
			int ret = ::mkdir(dn.c_str(), 0777);
			if(ret == -1)
				throw errno_exception_tb("mkdir %s", dn.c_str());
		}
	}	
}

std::unique_ptr<file> base_dir::open_for_writing(std::string entry)
{
	return std::unique_ptr<file>(new file(dir_entry(entry), "wb"));
}

std::unique_ptr<file> base_dir::open_for_reading(std::string entry)
{
	return std::unique_ptr<file>(new file(dir_entry(entry), "rb"));
}

std::string base_dir::get_base_dir_basename()
{
	std::string::size_type p = dir_name.rfind(OS_SEP);
	if(p == std::string::npos)
		return dir_name;
	return dir_name.substr(p + 1);
}

std::vector<std::string> base_dir::list(bool dirs, bool files)
{
	DIR* dp = opendir(dir_name.c_str());
	if(!dp)
		throw errno_exception_tb("opendir %s", string_util::repr(dir_name).c_str());
	std::vector<std::string> ret;

	while(true) {
		errno = 0;
		struct dirent* entry = readdir(dp);
		if(!entry) {
			if(errno)
				throw errno_exception_tb("readdir %s", string_util::repr(dir_name).c_str());
			break;
		}
		if(entry->d_name[0] == '.')
			continue;
		std::string p = dir_name;
		p += OS_SEP;
		p += entry->d_name;
		if(
			(files & is_file(p))
			|| (dirs & is_dir(p))) {
			
			ret.push_back(entry->d_name);
		}
	}		
	
	closedir(dp);
	return ret;
}


}
