#ifndef DB_IMPL_H
#define DB_IMPL_H

#include <lnrdb/lnrdb.h>

#include <string>
#include <map>
#include <mutex>

#include "table_impl.h"
#include "base_dir.h"
#include "meta_info_impl.h"

namespace lnrdb {

class db_impl
	: public db,
	  public base_dir
{
	typedef std::map<std::string, table_impl*> tables_t;
	tables_t tables;
	std::mutex tables_mutex;
	
	void create(std::string name);
	void open(std::string dir);
	db_impl() {};
public:
	~db_impl();
	
	table* create_table(std::string name);
	// for reading:
	std::list<std::string> get_table_names();
	table* open_table(std::string name);

	friend db* open_for_reading_raw(std::string dir);
	friend db* create_raw(std::string name);
};

}

#endif /* DB_IMPL_H */

