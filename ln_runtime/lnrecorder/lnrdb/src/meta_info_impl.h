#ifndef META_INFO_IMPL_H
#define META_INFO_IMPL_H

#include <lnrdb/lnrdb.h>
#include "file.h"

#include <string>
#include <memory>

namespace lnrdb {

class meta_info_impl : public meta_info {
	std::unique_ptr<file> _file;
	
	typedef std::map<std::string, std::string> data_t;
	data_t data;
	
	void _read();
public:
	meta_info_impl(std::unique_ptr<file> _file);
		
	void add(std::string name, const char* format, void* data);
	void add(std::string name, unsigned int value);
	void add(std::string name, std::string value);

	std::string get(std::string name);
	std::string get_string(std::string name);
	unsigned int get_unsigned_int(std::string name);
	double get_double(std::string name);	
};

}

#endif // META_INFO_IMPL_H

