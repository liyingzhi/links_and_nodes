#include <memory>

#include <string_util/string_util.h>

#include "db_impl.h"
#include "meta_info_impl.h"

namespace lnrdb {

const uint32_t version = 1;

void db_impl::create(std::string name)
{
	mkdir(name);
	set_base_dir(name);

	meta.reset(new meta_info_impl(open_for_writing("meta")));
	meta->add("version", "u4", (void*)&version);
}

void db_impl::open(std::string dir_name)
{
	set_base_dir(dir_name);

	meta.reset(new meta_info_impl(open_for_reading("meta")));
	// scan for tables

	auto dirs = list(true, false);
	for(auto&& dn : dirs) {
		base_dir table_dir(dir_entry(dn, false));
		std::string meta_fn = table_dir.dir_entry("meta", false);
		if(!is_file(meta_fn))
			continue;
		
		table_impl* ret = new table_impl(this);
		ret->open(dn);
		tables[ret->name] = ret;
	}
}

table* db_impl::create_table(std::string name)
{
	std::lock_guard<std::mutex> lk(tables_mutex);
	table_impl* ret = new table_impl(this);
	ret->create(name);
	tables[name] = ret;
	return ret;
}

std::list<std::string> db_impl::get_table_names()
{
	std::list<std::string> ret;
	for(auto&& t : tables)
		ret.push_back(t.first);
	return ret;
}

table* db_impl::open_table(std::string name)
{
	auto&& ti = tables.find(name);
	if(ti == tables.end())
		throw str_exception_tb("invalid/unknown table name %s", string_util::repr(name).c_str());
	return ti->second;
}

db_impl::~db_impl()
{
	for(auto&& i : tables)
		delete i.second;
}

db* create_raw(std::string name)
{
	db_impl* ret = new db_impl();
	ret->create(name);
	return ret;
}

db* open_for_reading_raw(std::string dir)
{
	db_impl* ret = new db_impl();
	ret->open(dir);
	return ret;
}

std::unique_ptr<db> create(std::string name)
{
	return std::unique_ptr<db>(create_raw(name));
}

std::unique_ptr<db> open_for_reading(std::string name)
{
	return std::unique_ptr<db>(open_for_reading_raw(name));
}



}
