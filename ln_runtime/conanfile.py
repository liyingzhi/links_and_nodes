import os

from conans import ConanFile, tools

class links_and_nodes_runtime_conan(ConanFile):
    python_requires = "ln_conan/[~4]@common/unstable"
    python_requires_extend = "ln_conan.Base"
    
    name = "links_and_nodes_runtime"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = "os", "compiler", "build_type", "arch"
    exports = [ "../site_scons*" ]
    exports_sources = [
        "../SConstruct",
        
        "../libln*",
        # excludes are checked with fnmatch against results of positive patterns!
        "!libln/.*",
        "!libln/tests/*",
        "!libln/examples/*",
            
        "../share*",
        
        "../python*",
        "../external*",
        "!python/links_and_nodes/*",
        "!python/links_and_nodes_manager/*",
        "!python/pyparsing/*",
        "!python/pyutils/*.pyc",

        "../ln_runtime*",
        "!ln_runtime/.*",
    ] + ["!%s" % x for x in tools.Git().excluded_files()]
    
    resdir = "share/ln_runtime"
        
    def init(self):
        base = self.python_requires["ln_conan"].module.Base
        base.set_attrs(self)

    def requirements(self):
        # runtime only requirements
        self.requires("liblinks_and_nodes/%s" % self.same_branch_or("[>=1.2 <3]"))
        
        # runtime & build requirements
        self.requires("libstring_util/[~=1 >=1.2.1]@common/stable")
        
        if self.settings.get_safe("os") == "Linux":
            self.requires("procps/3.3.15@3rdparty/stable")

    def source(self):
        self.write_version_header("LIBLN_VERSION",      os.path.join("libln", "include", "ln", "version.h"))
        self.write_version_header("LN_RUNTIME_VERSION", os.path.join("ln_runtime", "ln_daemon", "src", "version.h"))
        self.write_version_file(os.path.join("ln_runtime", "version"))

    def build(self):
        opts = "--use-private-libtomcrypt"
        if self.settings.get_safe("os") == "Neutrino":
            opts += " --without-daemon-authentication"
            opts += " --without-lnrecorder" # needs -std=c++11
        self.scons_build("ln_runtime", opts=opts, verbose=True)

    def package(self):
        self.copy("COPYING", src="ln_runtime", dst=self.resdir)
        self.copy("version", src="ln_runtime", dst=self.resdir)

        self.output.info("cwd: %s" % os.getcwd())
        for binary in "ln_daemon,file_services,lnrecorder".split(","):
            install = os.path.join("build", "ln_runtime", binary, self.install_sandbox, self.prefix[1:])
            self.output.info("install: copy from %s/*" % install)
            self.copy("*", src=install)
        
    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.cpp_info.resdirs = [ self.resdir ]
        
