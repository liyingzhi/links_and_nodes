#ifndef LN_SHM_PORTS_H
#define LN_SHM_PORTS_H

#include <string>

class ln_shm_port {
public:
	string name;
#ifdef __ANDROID__
	int fd;
	unsigned int shm_size;
#endif
	unsigned int shm_version;
protected:
	ln_shm_port(string name, unsigned int shm_version);
	int throw_error(int ret, string s);
};

class ln_shm_source : public ln_port, public ln_source_port, public ln_sink_port, public ln_shm_port {
	shm_source_t source;

protected:
	int generate_packet(void* data, double timeout=-1 /*blocking*/);

public:
	ln_shm_source(ln_instance* instance, string shm_name, unsigned int element_size, unsigned int n_elements, thread_settings* ts, unsigned int shm_version);
	~ln_shm_source();


#include "port_virtuals.h"
#include "src_port_virtuals.h"

	void consume_packet(double ts, uint32_t counter, void* data);

	virtual void get_last_packet_req(request_t& answer);
	virtual void _need_thread_for_logging(bool needed);
	
	string get_id();

	virtual void sink_port_get_state(request_t& answer) {
		ln_sink_port::get_state(answer);
	};	
	
};

class ln_shm_destination : public ln_port, public ln_sink_port, public ln_shm_port {
	shm_target_t target;

public:
	ln_shm_destination(ln_instance* instance, string shm_name, unsigned int element_size, unsigned int n_elements, float rate, unsigned int shm_version);
	~ln_shm_destination();

#include "port_virtuals.h"
#include "dst_port_virtuals.h"

	string get_id();
	void consume_packet(double ts, uint32_t counter, void* data);
};

#endif // LN_SHM_PORTS_H
