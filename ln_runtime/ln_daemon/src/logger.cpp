/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <string.h>

#include <new>
#include <string>
#include <sstream>

#include <string_util/string_util.h>
#include "logger.h"
#include "util.h"

#include "os.h"

using namespace string_util;

logger* root_logger = NULL;
bool debug_enabled = false;

static void cleanup_unlock_mutex(void* mutex) {
	flolock* l = (flolock*)mutex;
	printf("THREAD CLEANUP unlock!\n");
	l->unlock();
}

void log(const char* format, ...) {
	va_list ap;
	va_start(ap, format);
	if(root_logger) {
		root_logger->vlog(format, ap);
	}
	else
		printf("no root logger\n");
	va_end(ap);
}

log_entry::log_entry(std::string msg) {
	this->msg = msg;
	timestamp = ln_get_time(); // wall clock time
}

void log_entry::format_time(char* ts_buffer, int ts_buffer_len) {
	time_t seconds = (time_t)timestamp;
	double mseconds = (timestamp - (double)seconds) * 1000.;
	struct tm btime;
#ifdef __WIN32__	
	memcpy(&btime, localtime(&seconds), sizeof(struct tm));
#else
	localtime_r(&seconds, &btime);
#endif
	
	strftime(ts_buffer, ts_buffer_len, "%Y-%m-%d %H:%M:%S", &btime);
	int sl = strlen(ts_buffer);
	snprintf(ts_buffer + sl, ts_buffer_len - sl, ".%03.0f", mseconds);
}


logger::logger(unsigned int max_size, bool to_stdout) : 
	l(false), max_size(max_size), fp(NULL), write_to_stdout(to_stdout) { 
	
	vlog_buffer = NULL;
	vlog_buffer_size = 1024;
}
logger::~logger() {
	if(vlog_buffer)
		free(vlog_buffer);
}

void logger::vlog(const char* format, va_list nap) {
	string msg;
	
	l();
	thread_cleanup_push(cleanup_unlock_mutex, &l);
	
	if((vlog_buffer == NULL) && (vlog_buffer = (char*)malloc(vlog_buffer_size)) == NULL)
		throw std::bad_alloc();
	
	while(true) {
		/* Try to print in the allocated space. */
#ifdef MSVC
		int n = vsnprintf(vlog_buffer, vlog_buffer_size, format, nap);
#else
		va_list ap;
		va_copy(ap, nap);
		int n = vsnprintf(vlog_buffer, vlog_buffer_size, format, ap);
		va_end(ap);
#endif
		/* If that worked, return the string. */
		if (n > -1 && (unsigned)n < vlog_buffer_size)
			break;
		/* Else try again with more space. */
		if (n > -1)    /* glibc 2.1 */
			vlog_buffer_size = n+1; /* precisely what is needed */
		else           /* glibc 2.0 */
			vlog_buffer_size *= 2;  /* twice the old size */
		char* np;
		if ((np = (char*)realloc(vlog_buffer, vlog_buffer_size)) == NULL)
			throw std::bad_alloc();
		vlog_buffer = np;
	}

	msg = std::string(vlog_buffer);
	
	l.unlock();
	thread_cleanup_pop(0);
	
	log(msg);
}

void logger::log(const char* format, ...) {
	va_list ap;
	va_start(ap, format);
	this->vlog(format, ap);
	va_end(ap);
}

void logging::log(const char* format, ...) {
	va_list ap;
	va_start(ap, format);
	_logger.vlog(format, ap);
	va_end(ap);
}

void logger::log(std::string msg) {
	_log(msg);
}
void logging::log(std::string msg) {
	_logger._log(msg);
}

#if !defined(__QNX__) && !defined(MSVC)
#include <cxxabi.h>
#endif
void logger::_log(std::string msg) {
	// printf("_log this: %p, pid: %d, msg: %s\n", this, getpid(), msg.c_str());
	l();
	thread_cleanup_push(cleanup_unlock_mutex, &l);

	try {
		log_entry le(msg);
		// printf("have log entry\n");
		if(write_to_stdout || fp) {
			char ts_buffer[64];
			le.format_time(ts_buffer, 64);
			string replace(strlen(ts_buffer), ' ');
			replace.insert(0, "\n");
			string msg = string_replace(le.msg, "\n", replace);
			int sl = msg.size() - 1;
			while(sl > 0 && strchr(" \r\n\t", msg[sl]))
				sl--;
			sl++;
			if(write_to_stdout) {
				printf("%s %.*s\n", ts_buffer, sl, msg.c_str());
				fflush(fp);
			}
			if(fp) {
				fprintf(fp, "%s %.*s\n", ts_buffer, sl, msg.c_str());
				fflush(fp);
			}
		}
		history.push_back(le);
		while(history.size() > max_size) {
			history.pop_front();
		}
	}
#if (!defined(__QNX__) && !defined(__VXWORKS__) && !defined(__WIN32__))
	catch (abi::__forced_unwind&) {
		fprintf(stderr, "logger exception: forced unwind - pthread_exit()?\n");
		fflush(stderr);
		throw;
	}
#endif
	catch(exception& e) {
		fprintf(stderr, "logger exception: %s\n", e.what());
		fflush(stderr);
	}
	catch(...) {
		fprintf(stderr, "unknown logger exception!\n");
		fflush(stderr);
		throw;
	}
	l.unlock();
	thread_cleanup_pop(0);
}

void logger::open_logfile(string log_file) {
	fp = fopen(log_file.c_str(), "a+");
}

void logger::flush_history(list<log_entry>& target) {
	// printf("flush lock\n");
	l();
	thread_cleanup_push(cleanup_unlock_mutex, &l);

	target = history;
	history.clear();
	// printf("flush unlock\n");
	l.unlock();
	thread_cleanup_pop(0);
}

void logging::append_py_log(py_list& all_logs) {
	if(!_logger.has_history())
		return;
	
	log_history_t history;
	_logger.flush_history(history);

	py_list* py_history = new py_list();
	for(log_history_t::iterator h = history.begin(); h != history.end(); h++) {
		log_entry& le = *h;
		
		py_list* py_log_entry = new py_list();
		py_log_entry->value.push_back(new py_float(le.timestamp));
		py_log_entry->value.push_back(new py_string(le.msg));
		
		py_history->value.push_back(py_log_entry);
	}
	
	py_list* py_named_history = new py_list();
	py_named_history->value.push_back(new py_string(_logging_id));
	py_named_history->value.push_back(py_history);
	
	all_logs.value.push_back(py_named_history);				
}
