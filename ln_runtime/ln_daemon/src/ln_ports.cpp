/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <stdio.h>

#include "ln_ports.h"

ln_port::ln_port(ln_instance* instance, string port_type, port_direction dir, unsigned int size) : port_type(port_type), instance(instance), direction(dir), size(size) {
	packet_count = 0;
	last_packet_time = 0;
	last_packet_source_time = 0;
	last_packet = malloc(size);

	logging_enabled = false;
	logging_only_ts = false;
	logging_divisor = 1;
	// log("%p ln_port alloces %d + 8 bytes for last packet: %p", this, size, last_packet);
}
ln_port::~ln_port() {
	//log("ln_port de-tor!");
	free(last_packet);
}

void ln_port::start_logging(int max_samples, float max_time, bool only_ts, unsigned int divisor) {
	if(max_samples < 1)
		throw str_exception_tb("can not log with max_samples < 1");
	if(!only_ts)
		log_buffer.configure(max_samples, size);
	else
		log_buffer.configure(max_samples, 0);
	if(max_time != -1)
		logging_end_time = ln_get_time() + max_time;
	else
		logging_end_time = -1;
	had_buffer_filled = false;
	logging_only_ts = only_ts;
	if(divisor < 1)
		divisor = 1;
	logging_divisor = divisor;
	logging_divisor_pos = 1;
	logging_enabled = true;
	_need_thread_for_logging(true);
}
void ln_port::stop_logging() {
	logging_enabled = false;
	_need_thread_for_logging(false);
}
void ln_port::retrieve_log(request_t& answer) {
	std::string data;
	retrieve_log_data(answer, data);
	answer["buffer"] = binary_data_repr(data);
}
void ln_port::retrieve_log_data(request_t& answer, std::string& data) {
	answer["success"] = repr("success");
	
	unsigned int n_samples;
	if(log_buffer.get_count()) {
		data = log_buffer.get_string(&n_samples);
	} else {
		n_samples = 0;
		data = "";
	}
	answer["n_samples"] = format_string("%lu", n_samples);
	answer["only_ts"] = format_string("%d", logging_only_ts);
	answer["divisor"] = format_string("%d", logging_divisor);
}


void ln_port::_append_to_log(void* data, double* ts, uint32_t* counter) {
	if(!logging_enabled)
		return;
	if(logging_divisor_pos % logging_divisor) {
		logging_divisor_pos ++;
		return;
	}
	logging_divisor_pos = 1;
	bool is_full = false;
	bool send_notification = false;
	// printf("port %s: append_to_log called\n", get_id().c_str());
	log_buffer.append(data, ts, counter, &is_full);
	// printf("log_buffer size: %d\n", log_buffer.get_count());

	send_notification = is_full && !had_buffer_filled;
	if(logging_end_time != -1 && ln_get_time() >= logging_end_time) {
		stop_logging();
		send_notification = true;
	}
	if(send_notification) {
		had_buffer_filled = true;
	}

}

py_tuple* ln_port::get_tuple(py_tuple* t, int with_last_packet) {
	if(!t) 
		t = new py_tuple();

	if(with_last_packet) {
		t->value.push_front(new py_int(logging_enabled));
		t->value.push_front(new py_string((char*)last_packet, size + sizeof(double)));
		t->value.push_front(new py_float(last_packet_source_time));
		t->value.push_front(new py_float(last_packet_time));
		t->value.push_front(new py_int((int)packet_count));
	}
	t->value.push_front(new py_int(size));

	if(direction == SINK_PORT)
		t->value.push_front(new py_string("SINK_PORT"));
	else
		t->value.push_front(new py_string("SOURCE_PORT"));
	t->value.push_front(eval_full(this->get_id()));
	return t;
}

// sink port base class
ln_sink_port::ln_sink_port(float rate) {
	source = NULL;
	// log("sink port with rate %f\n", rate);
	set_rate(rate);
	can_consume = true;
	could_not_consume = 0;
}
ln_sink_port::~ln_sink_port() {
	if(source) {
		log("WARNING: ln_sink_port was still connected. removing connection now!");
		source->remove_sink(this);
		source = NULL;
	}
	// log("ln_sink_port de-tor!");
}
py_tuple* ln_sink_port::get_tuple(py_tuple* t, int with_last_packet) {
	if(!t) 
		t = new py_tuple();
	if(source)
		t->value.push_front(eval_full(source->get_id()));
	else
		t->value.push_front(new py_special());

	double rate;
	if (allowed_wave_length == -1)
		rate = -1;
	else
		rate = 1. / allowed_wave_length;
	t->value.push_front(new py_float(rate));
	get_tuple_(t, with_last_packet);
	return t;
}


void ln_sink_port::_consume_packet(double ts, uint32_t counter, void* data) {
	if(allowed_wave_length != -1) {
		double this_wave_length = ts - get_last_packet_source_time();
		if(this_wave_length < 0)
			this_wave_length = allowed_wave_length;
		debug("port %s consume new packet? this_wave_length (ts:%.3f - last:%.3f = )%f < allowed_wave_length %f", get_id().c_str(),
		      ts, get_last_packet_source_time(),
		      this_wave_length, allowed_wave_length);
		if(this_wave_length < allowed_wave_length) {
			debug("port %s ... do not consume. 1/allowed_wave_length: %.2f", get_id().c_str(), 1. / allowed_wave_length);
			return;
		}
	}
	if(!can_consume) {
		could_not_consume ++;
		return;
	}
	get_last_packet_source_time() = ts;
	get_last_packet_time() = ln_get_time();
	get_packet_count() = counter;
	debug("%s: really consume new packet!", get_id().c_str());
	memcpy(get_last_packet(), data, get_size()); // NO ts at the beginning!
	_append_to_log(data, &ts, &counter);

	consume_packet(ts, counter, get_last_packet());
}

void ln_sink_port::get_state(request_t& answer) {
	if(answer.find("count") == answer.end()) {
		answer["success"] = repr("success");
		answer["count"] = format_string("%llu", get_packet_count());
		if(allowed_wave_length == -1)
			answer["rate"] = "-1";
		else
			answer["rate"] = format_string("%f", 1. / allowed_wave_length);
	}
	if(source) {
		py_value* py_source = eval_full(source->get_id());
		answer["source"] = repr(py_source);
		delete py_source;

		answer["src_source_thread_started"] = format_string("%d", source->started);
		if(source->started) {
			answer["src_thread_handle"] = format_string("%lu", source->thread_handle);
#if defined(__LINUX__) || defined(__VXWORKS__)
			answer["src_linux_tid"] = format_string("%d", source->tid);
#endif
		}
	} else {
		answer["source"] = "None";
	}
}


// source port base class
ln_source_port::ln_source_port(thread_settings* ts) {
	started = false;
#if defined(__LINUX__) || defined(__VXWORKS__)
	tid = 0;
#endif
	rate_est_info_size = 10;
	last_rate_est_time = 0;
	packet_counter = 0;
	packet_timestamp = 0;
	_n_logs_sink_usage = 0;
	os::create_mutex(&sinks_mutex);

	if(ts)
		this->ts = *ts;
}

ln_source_port::~ln_source_port() {
	log("%s: ln_source_port::detor will destroy sink-mutex!\n", get_id().c_str());
	os::destroy_mutex(&sinks_mutex);
	//log("ln_source_port de-tor!");
}

py_tuple* ln_source_port::get_tuple(py_tuple* t, int with_last_packet) {
	if(!t) 
		t = new py_tuple();
	py_list* py_sinks = new py_list;
	os::lock_mutex(&sinks_mutex);
	for(sinks_t::iterator i = sinks.begin(); i != sinks.end(); i++) {
		ln_sink_port* sink = *i;
		py_sinks->value.push_back(eval_full(sink->get_id()));
	}
	os::unlock_mutex(&sinks_mutex);		
	t->value.push_front(py_sinks);
	t->value.push_front(new py_float(_estimate_rate()));
	get_tuple_(t, with_last_packet);
	return t;
}

void ln_source_port::start() {
	if(!started) {
		log("%s: ln_source_port::start() starting source thread!\n", get_id().c_str());
		thread_handle = 0;
		os::create_thread(&thread_handle, ln_source_port::_static_thread, this);
		started = true;
	}
}
void ln_source_port::stop() {
	os::lock_mutex(&sinks_mutex);
	for(sinks_t::iterator i = sinks.begin(); i != sinks.end(); i++)
		(*i)->set_source(NULL);
	os::unlock_mutex(&sinks_mutex);

	if(started) {
		log("%s: ln_source_port::stop() cancelling source-thread\n", get_id().c_str());
		os::cancel_thread(thread_handle);
		started = false;
		os::join_thread(thread_handle);
		log("%s: ln_source_port::stop() got thread joined!\n", get_id().c_str());
#if defined(__LINUX__) || defined(__VXWORKS__)
		tid = 0;
#endif

	}
}


#ifdef __WIN32__
DWORD __stdcall ln_source_port::_static_thread(void* data) {
#else
thread_return_t ln_source_port::_static_thread(void* data) {
#endif
#if !defined(__WIN32__) && !defined(__ANDROID__)
	pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL); // cancel at cancellation points!
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
#endif

	ln_source_port* self = (ln_source_port*)data;
	try {
		self->_thread();
	}
	catch(exception& e) {
		printf("error in source_port thread!\n%s\n", e.what());
	}
	return (thread_return_t)0;
}

void* ln_source_port::_thread() {
	// printf("source thread of %s\n", get_id().c_str());
	started = true;
#if defined(__LINUX__) || defined(__VXWORKS__)
	// get linux thread id
	tid = os::gettid();
	debug("have tid %#x from os\n", tid);
#endif
	ts.apply();
	
	while(started) {
		// log("source thread is blocking on %s", get_id().c_str());
		//log("ln_source_port before generate packet\n");
		int ret = generate_packet(get_last_packet(), 0.5);
		//log("ln_source_port after generate packet\n");
		os::test_cancel(thread_handle); // for udp receivers
		// log("ln_source_port after test_cancel\n");
		if(!started)
			break;
		if(ret == 0)
			continue; // timeout
		get_packet_count() ++;
		// printf("port %s generated packet\n", get_id().c_str());
		// memcpy(last_packet, data, len); // ts should be also in here at the beginning!
		get_last_packet_time() = ln_get_time();

		_append_to_log(get_last_packet(), &packet_timestamp, &packet_counter);

		// if(!ret)
		// 	continue;
		// printf("source thread %s got new packet of length %d\n", get_id().c_str(), len);
		//if(_n_logs_sink_usage)
		//	log("%s: src-thread: will lock sink mutex\n", get_id().c_str());
		os::lock_mutex(&sinks_mutex);
		//if(_n_logs_sink_usage)
		//	log("%s: src-thread: got lock sink mutex\n", get_id().c_str());
		get_last_packet_source_time() = packet_timestamp;

		//log("port %s received new packet with ts %f len %d!", get_id().c_str(), ts, len);

		// printf("got timestamp from source: %f\n", ts);
		// log("have %d sinks\n", sinks.size());
		for(sinks_t::iterator i = sinks.begin(); i != sinks.end(); i++) {
			ln_sink_port* sink = *i;
			/*if(_n_logs_sink_usage) {
				log("%s: sending to sink %p\n", get_id().c_str(), sink);
				//log("...to sink %s", sink->get_id().c_str());
				}*/
			sink->_consume_packet(packet_timestamp, packet_counter, get_last_packet());
		}
		// rate estimation information
		double now = ln_get_time();
		if((now - last_rate_est_time) >= 1) {
			last_rate_est_time = now;
			// log("%s adding rate estimation info from %f count %d\n", this->get_id().c_str(), now, packet_count);
			rate_est_info.push_back(pair<double, uint64_t>(now, get_packet_count()));
			if(rate_est_info.size() > rate_est_info_size)
				rate_est_info.pop_front();
		}
		//if(_n_logs_sink_usage) log("%s: src-thread: will unlock sink mutex\n", get_id().c_str());
		os::unlock_mutex(&sinks_mutex);
		/*
		if(_n_logs_sink_usage) {
			log("%s: src-thread: unlocked sink mutex\n", get_id().c_str());
			_n_logs_sink_usage --;
		}
		*/
	}
	log("%s: source thread exiting!\n", get_id().c_str());
	return NULL;
}

ln_source_port::sinks_t::iterator ln_source_port::_find_sink(ln_sink_port* s) {
	// log("%s: ln_source_port::_find_sink() will lock sink-mutex!\n", get_id().c_str());
	os::lock_mutex(&sinks_mutex);
	// log("%s: ln_source_port::_find_sink() have lock sink-mutex!\n", get_id().c_str());
	sinks_t::iterator i = sinks.begin();
	for(; i != sinks.end(); i++)
		if(*i == s)
			break;
	os::unlock_mutex(&sinks_mutex);
	return i;
}
void ln_source_port::add_sink(ln_sink_port* s) {
	sinks_t::iterator i = _find_sink(s);
	if(i != sinks.end()) {
		// already in sinks list...
		log("the sink %s is already connected to this source %s\n", s->get_id().c_str(), get_id().c_str());
		return;
	}	
	os::lock_mutex(&sinks_mutex);
	sinks.push_back(s);
	_list_sinks();
	_n_logs_sink_usage = 0;
	os::unlock_mutex(&sinks_mutex);
	s->set_source(this);
	start();
}
void ln_source_port::_list_sinks() {
	// caller must hold sinks_mutex!
	log("%s: %d sinks:\n", get_id().c_str(), sinks.size());
	for(sinks_t::iterator i = sinks.begin(); i != sinks.end(); ++i) {
		log("%s:   sink: %s %p\n", get_id().c_str(), (*i)->get_id().c_str(), (*i));
		//usleep(100000);
	}
}

bool ln_source_port::remove_sink(ln_sink_port* s) {
	sinks_t::iterator i = _find_sink(s);
	if(i == sinks.end()) {
		// not in sinks list...
		log("the sink %s is not connected to this source %s\n", s->get_id().c_str(), get_id().c_str());
		return false;
	}
	os::lock_mutex(&sinks_mutex);
	sinks.erase(i);
	_list_sinks();
	_n_logs_sink_usage = 0;

	if(s->get_source() != this) {
		log("warning: sink_port %s has set the source_port to %s, not %s",
			s->get_id().c_str(), s->get_source()->get_id().c_str(), get_id().c_str());
		return false;
	} else
		s->set_source(NULL);

	if(!sinks.size() && started) {
		// stop source-thread!
		log("%s: request source thread to stop!\n", get_id().c_str());
		started = false;
		
		//os::cancel_thread(thread_handle);
		// but when canceling thread!
		//log("%s: ln_source_port::remove_sink() joining source-thread!\n", get_id().c_str());
		
		//log("%s: will release sink-mutex!\n", get_id().c_str());
		os::unlock_mutex(&sinks_mutex);
		//log("%s: released sink-mutex!\n", get_id().c_str());

		os::join_thread(thread_handle);
		log("%s: source thread stopped!\n", get_id().c_str());
		return true;
	}
	os::unlock_mutex(&sinks_mutex);
	return true;
}

void ln_source_port::get_state(request_t& answer) {
	answer["success"] = repr("success");
	
	if(!started) {
		// source thread not running... get data and make simple guess!
		generate_packet(get_last_packet(), 0); // non-blocking!
		get_packet_count() = packet_counter;
		double now = ln_get_time();
		rate_est_info.push_back(pair<double, uint64_t>(now, get_packet_count()));
		if(rate_est_info.size() > rate_est_info_size)
			rate_est_info.pop_front();
		debug("source thread not running, generated non-blocking packet %d, now %.3f\n", packet_counter, now);
	}

	// if source thread is running:
	answer["count"] = format_string("%llu", get_packet_count());
	answer["rate"] = format_string("%f", _estimate_rate());
	

	py_list py_sinks;
	os::lock_mutex(&sinks_mutex);
	for(sinks_t::iterator i = sinks.begin(); i != sinks.end(); i++) {
		ln_sink_port* sink = *i;
		py_sinks.value.push_back(eval_full(sink->get_id()));
	}
	os::unlock_mutex(&sinks_mutex);		

	answer["connected_sinks"] = repr(py_sinks);


	answer["source_thread_started"] = format_string("%d", started);
	if(started) {
		answer["thread_handle"] = format_string("%lu", thread_handle);
#if defined(__LINUX__) || defined(__VXWORKS__)
		answer["linux_tid"] = format_string("%d", tid);
#endif
	}

	sink_port_get_state(answer);
	
}

double ln_source_port::_estimate_rate() {
	os::lock_mutex(&sinks_mutex);
	double ret = 0;

	while(true) {
		if(rate_est_info.size() < 2)
			break;
		pair<double, uint64_t>& a = rate_est_info.front();
		pair<double, uint64_t>& b = rate_est_info.back();
		double duration = b.first - a.first;
		if(duration > 1.1 * rate_est_info_size) {
			rate_est_info.pop_front();
			continue;
		}
		ret = (b.second - a.second) / duration;
		break;
	}
	os::unlock_mutex(&sinks_mutex);
	return ret;
}

void thread_settings::apply() {
#ifndef __WIN32__	
	pthread_t tid = pthread_self();
#ifdef __LINUX__
	// if nothing specified use FIFO, lowest prio
	if(policy == -1)
		policy = SCHED_FIFO;
	if(prio == -1)
		prio = sched_get_priority_min(policy);
#endif
	
	if(policy != -1 || prio != -1) {
		struct sched_param param;
		int policy_;
		if (pthread_getschedparam(tid, &policy_, &param) != 0) {
			log("pthread_getschedparam(%p): %s\n", tid, strerror(errno));
		} else {
			if(policy != -1)
				policy_ = policy;
			
			if(prio != -1)
				param.sched_priority = prio;
			
			if (pthread_setschedparam(tid, policy_, &param) != 0)
				log("pthread_setschedparam(%p, %d, %d): %s\n", tid, policy_, param.sched_priority, strerror(errno));
		}
	}
	if(affinity_mask != -1)
		set_affinity_of_current_thread(affinity_mask);
#endif
}
