/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include <string_util/string_util.h>
#include "filter.h"

using namespace std;
using namespace string_util;

filter_t parse_filter(string filter) {
	// items sep by '\0', key-op-value sep by first ':'
	filter_t ret;
	vector<string> filter_items = split_string(filter, string("\0", 1));
	for(unsigned int i = 0; i < filter_items.size(); i++) {
		string& item = filter_items[i];

		vector<string> key_value = split_string(item, ":", 2);
		ret.push_back(filter_item_t(key_value[0], key_value[1], key_value[2]));
	}
	return ret;
}
