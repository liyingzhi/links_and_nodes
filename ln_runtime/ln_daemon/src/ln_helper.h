#ifndef LN_HELPER_H
#define LN_HELPER_H

#include <string>

using namespace std;

void vxworks_free_ln_helper();
void vxworks_set_prio(int tid, int prio, int affinity);
string vxworks_process_listing(bool with_env, bool with_threads, string filter);
int vxworks_gettid();

#endif // LN_HELPER_H
