#ifndef OS_LINUX_H
#define OS_LINUX_H

#include "os_posix_like.h"
#include <string_util/exceptions.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#ifdef __ANDROID__
// unix domain sockets:
#include <sys/un.h>

#define WIFCONTINUED(status) 0

#endif

#define HAVE_GETPWUID
#define OS_SHM_DIR "/dev/shm"

using namespace string_util;
namespace os {

typedef socklen_t socklen_t_type;

typedef pthread_t thread_handle_t;
typedef void* thread_return_t;
typedef thread_return_t (*thread_func_type)(void*);

int create_thread(thread_handle_t* thread_handle, thread_func_type thread, void* data);
int cancel_thread(thread_handle_t thread_handle);
int join_thread(thread_handle_t thread_handle);
void test_cancel(thread_handle_t thread_handle);

void sleep(double s);

//! kill process with given pid
/*!
  \param pid process pid to kill
  \return success or error code
  */
inline int kill(pid_t pid) {
	return ::kill(pid, 9);
}

//! send signal to process with given pid
/*!
  \param pid process pid to signal
  \parma signo signal number to send
  \return success or error code
  */
inline int send_signal(pid_t pid, int signo) {
	return ::kill(pid, signo);
}

//! initialize os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
 */
inline int pipe_init(ln_pipe_t& pipe) {
	int* pipe_fds = (int*)&pipe;
	if (::pipe(pipe_fds))
		throw errno_exception_tb("could not create output buffer pipe!");
	// printf("pipe_fds: %d, %d\n", pipe_fds[0], pipe_fds[1]);
	// printf("pipe: %d, %d\n", pipe.input_fd, pipe.output_fd);
	return 0;
}

//! closes os dependant pipe device
/*!
 \param reference to pipe device structure
 \return success or error code
 */
inline int pipe_close(ln_pipe_t& pipe) {
	if(pipe.input_fd != -1)
		close(pipe.input_fd);
	if(pipe.output_fd != -1)
		close(pipe.output_fd);
	return 0;
}

//! return temp dir as std::string
/*
  \return temp dir
 */
inline std::string get_temp_dir() {
	return std::string(OS_SHM_DIR);
}

int set_prio(pid_t pid, int tid, int prio, int policy);
int set_affinity(pid_t pid, int tid, int affinity);
int set_affinity_of_current_thread(int affinity);
pid_t gettid();

}

#endif // OS_LINUX_H
