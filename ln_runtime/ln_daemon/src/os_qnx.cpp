/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#ifdef __QNX__

#include <string_util/string_util.h>
#include "os_qnx.h"

#include <time.h>
#include <sys/neutrino.h>

#include "logger.h"

using namespace std;
using namespace string_util;

namespace os {

int create_thread(thread_handle_t* thread_handle, thread_func_type* thread, void* data) {
	return pthread_create(thread_handle, NULL, thread, data);
}

int cancel_thread(thread_handle_t thread_handle) {
	return pthread_cancel(thread_handle);
}

int join_thread(thread_handle_t thread_handle) {
	return pthread_join(thread_handle, NULL);
}

void test_cancel(thread_handle_t thread_handle) {
	pthread_testcancel();
}

double ts2double(struct timespec* ts) {
	return (double)ts->tv_sec + (ts->tv_nsec / 1e9);
}

void double2ts(double s, struct timespec* ts) {
	ts->tv_sec = (time_t)s;
	ts->tv_nsec = (long)((s - ts->tv_sec) * 1e9);
}

double ln_get_time() {
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	return ts2double(&ts);
}

void sleep(double s) {
	struct timespec ts;
	double2ts(s, &ts);
	nanosleep(&ts, NULL);
}
int set_prio(pid_t pid, int tid, int prio, int policy) {
	char ps[16] = "f";
	if(policy == SCHED_RR)
		ps[0] = 'r';
	else if(policy == SCHED_OTHER)
		ps[0] = 'o';
	string cmd = format_string("/bin/slay -P %d%s -T %d %d", prio, ps, tid, pid);
	system(cmd.c_str());
	return 0;
}
int set_affinity(pid_t pid, int tid, int affinity) {
	string cmd;
	if(tid != -1)
		cmd = format_string("/bin/slay -R 0x%x -T %d %d", affinity, tid, pid);
	else
		cmd = format_string("/bin/slay -R 0x%x %d", affinity, pid);
	system(cmd.c_str());
	return 0;
}

int set_affinity_of_current_thread(int affinity) {
	return ThreadCtl(_NTO_TCTL_RUNMASK_GET_AND_SET, &affinity);
}

pid_t gettid() {
	return (pid_t)pthread_self();
}

int kill(pid_t pid)
{
	return ::kill(pid, 9);
}

int send_signal(pid_t pid, int signo)
{
	return ::kill(pid, signo);
}

int pipe_init(ln_pipe_t& pipe)
{
	int* pipe_fds = (int*)&pipe;
	if (::pipe(pipe_fds))
		throw errno_exception_tb("could not create output buffer pipe!");
	return 0;
}

int pipe_close(ln_pipe_t& pipe)
{
	log("os::pipe_close in-fd%d out-fd%d\n", pipe.input_fd, pipe.output_fd);

	if(pipe.input_fd != -1)
		close(pipe.input_fd);
	if(pipe.output_fd != -1)
		close(pipe.output_fd);
	return 0;
}

std::string get_temp_dir()
{
	return std::string(OS_SHM_DIR);
}

}
#endif // __QNX__
