#ifndef LOG_TOKEN_H
#define LOG_TOKEN_H

#include <string>

class ln_instance;

class log_token {
	ln_instance* instance;
	std::string data;
	std::string token;

	int fd;
	unsigned int port;

	std::vector<char> token_input;
	unsigned int token_len;
	
	unsigned int total_written;
	int client_fd;
	void _close_client();
	
	static void on_new_log_connection(int fd, int why, void* data);
	void on_new_log_connection(int fd, int why);
	
	static void _on_token_input(int fd, int why, void* data);
	void on_token_input(int fd, int why);
	
	static void _on_data_output(int fd, int why, void* data);
	void on_data_output(int fd, int why);
public:
	log_token(ln_instance* instance, std::string data);
	~log_token();
	
	std::string get_token() { return token; }
	unsigned int get_port() { return port; }
};

#endif /* LOG_TOKEN_H */
