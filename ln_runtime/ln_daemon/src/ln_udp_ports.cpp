/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include <stdio.h>
#include <stdio.h>
#include <errno.h>

#include "ln_ports.h"

// udp sender sink port
ln_udp_sender::ln_udp_sender(ln_instance* instance, string target_host, int target_port, int size, float rate, string swap_endianess) : 
			ln_port(instance, "udp", SINK_PORT, size),
			ln_sink_port(rate), 
			target_host(target_host), target_port(target_port) {
	debug("ctor");
	if(swap_endianess.size())
		this->swap_endianess.set_format(format_string("8%s4", swap_endianess.c_str()));
	
	sfd = -1;
	debug("rate setting: %f\n", rate);

	sfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sfd == -1)
		throw errno_exception_tb("could not create udp_sender socket!");

	struct sockaddr_in target_addr;
	resolve_hostname(target_host, &target_addr);
	target_addr.sin_port = htons(target_port);

#ifdef IPTOS_DSCP_EF
	uint8_t tos = IPTOS_DSCP_EF;
	if(setsockopt(sfd, IPPROTO_IP, IP_TOS, &tos, sizeof(tos)) == -1)
		printf("could not set IP_TOS to %#x: %s\n", tos, strerror(errno));
#else
#  warning IPTOS_DSCP_EF not avaliable on this architecture!
#endif
	if(connect(sfd, (struct sockaddr*)&target_addr, sizeof(target_addr)))
		throw errno_exception_tb("could not connect udp_sender socket to target %s port %d!", target_host.c_str(), target_port);

	instance->register_port(this);
}
ln_udp_sender::~ln_udp_sender() {
	if(source) {
		log("ln_udp_sender was still connected. removing connection now!");
		source->remove_sink(this);
		source = NULL;
	}

	if(sfd != -1) {
		_ln_close_socket(sfd);
	}
}

void ln_udp_sender::consume_packet(double ts, uint32_t counter, void* data) {
	ssize_t to_send = size + sizeof(double) + sizeof(uint32_t);
	debug("sending %d bytes. ts: %.3f, user-data-ts: %f", to_send, ts, *(double*)data);

	struct msghdr message;
	memset(&message, 0, sizeof(struct msghdr));
	message.msg_iovlen = 3;
#ifdef MSVC
	struct iovec buffers[3];
#else
	struct iovec buffers[message.msg_iovlen];
#endif
	buffers[0].iov_base = (char*)&ts;
	buffers[0].iov_len = sizeof(double);
	buffers[1].iov_base = (char*)data;
	buffers[1].iov_len = size;
	buffers[2].iov_base = (char*)&counter;
	buffers[2].iov_len = sizeof(counter);
	message.msg_iov = buffers;

	// todo: endianess swap on sender side!
	if(swap_endianess)
		swap_endianess.swap_msg(&message);	
	
#if defined(__VXWORKS__)
	//size_t sent = send(sfd, (const char*)data, to_send, 0);
	ssize_t sent = sendmsg(sfd, &message, 0);
#elif defined(__WIN32__)
	ssize_t sent = sendmsg(sfd, &message, __WIN32__DGRAM_SEND);
#else
	//size_t sent = send(sfd, data, to_send, 0);	
	ssize_t sent = sendmsg(sfd, &message, 0);
#endif

	if(sent == -1) {
		log("error: port %s: sendmsg: %s\n", get_id().c_str(), strerror(errno));
		return;
	}
	if(sent != to_send) {
		log("warning: port %s wanted to send %d bytes to host %s port %d but only %d bytes went out!\n",
			get_id().c_str(), to_send, target_host.c_str(), target_port, sent);
	}
}


// udp receiver source port
ln_udp_receiver::ln_udp_receiver(ln_instance* instance, unsigned int size, thread_settings* ts, string swap_endianess) : 
	ln_port(instance, "udp", SOURCE_PORT, size),
	ln_source_port(ts) {
	
	debug("ctor");
	if(swap_endianess.size())
		this->swap_endianess.set_format(format_string("8%s4", swap_endianess.c_str()));
	
	this->size = size;
	sfd = -1;
	packet_counter = 0;
	packet = malloc(size + sizeof(double) + sizeof(uint32_t));  // to be able to receive timestamp & counter via udp!
	if(!packet)
		throw errno_exception_tb("could not allocate udp_receiver packet of size %d bytes!", size);
	memset(packet, 0, size + sizeof(double) + sizeof(uint32_t));
	sfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sfd == -1)
		throw errno_exception_tb("could not create udp_receiver socket!");

	struct sockaddr_in local_addr;
	os::socklen_t_type len = sizeof(local_addr);
	memset(&local_addr, 0, sizeof(local_addr));
	local_addr.sin_family = AF_INET;
	if(bind(sfd, (struct sockaddr*)&local_addr, sizeof(local_addr)))
		throw errno_exception_tb("could not bind udp_receiver port!");
	
	if(getsockname(sfd, (struct sockaddr*)&local_addr, &len))
		throw errno_exception_tb("could not get udp_receiver socket name!");
	udp_port = ntohs(local_addr.sin_port);

	// printf("port %s: ctor with %p %d bytes (size: %d)\n", get_id().c_str(), packet, size + sizeof(double) + sizeof(uint32_t), size);

	instance->register_port(this);
	start();
}
ln_udp_receiver::~ln_udp_receiver() {
	log("ln_udp_receiver de-tor!");
	stop();
	debug("after stop");
	if(packet)
		free(packet);
	debug("after free");
	if(sfd != -1)
		_ln_close_socket(sfd);
}

#if defined(__VXWORKS__)
#include "os.h"
#include <sys/select.h>
#endif
int ln_udp_receiver::generate_packet(void* data, double timeout) {
	unsigned int s = size + sizeof(double) + sizeof(uint32_t);

	unsigned int flags = 0;
#ifndef __WIN32__
	if(timeout == 0)
		flags = MSG_DONTWAIT;
	// todo: win32 non blocking receive flag?
#endif

	const void* target_buffer = packet;
	if(swap_endianess)
		target_buffer = swap_endianess.get_buffer();
	
	// todo: maybe use recv iov to fill first timestamp and then directly data and get rid of this additional memcpy!
#if defined(__VXWORKS__)
	fd_set readfds;
	fd_set excfds;
	while(true) {
		os::test_cancel(0);

		FD_ZERO(&readfds);
		FD_ZERO(&excfds);
		FD_SET(sfd, &readfds);
		FD_SET(sfd, &excfds);
		struct timeval tv = {0, 250000};
		tv.tv_sec = (unsigned int)timeout;
		tv.tv_usec = (unsigned int)(1e6 * (timeout - tv.tv_sec));
		if(timeout == -1)
			tv.tv_usec = 250000; // blocking
		int n = select(sfd + 1, &readfds, NULL, &excfds, &tv);
		if(n == 0) {
			// timeout!
			if(timeout >= 0)
				return 0;
			continue;
		}
		if(n == -1) {
			fprintf(stderr, "ln_udp_receiver::generate_packet() select(): %s\n", strerror(errno));
			return -1;
		}
		if(FD_ISSET(sfd, &readfds))
			break;
		if(FD_ISSET(sfd, &excfds)) {
			fprintf(stderr, "ln_udp_receiver::generate_packet() select(): fd %d is in exception-fds!\n", sfd);
			break;
		}
	}
	int n = recv(sfd, (char*)target_buffer, s, flags);
#else
	int n;
	if(timeout >= 0) {
		// with timeout
		fd_set readfds;
		fd_set excfds;
		while(true) {
			os::test_cancel(0);
			
			FD_ZERO(&readfds);
			FD_ZERO(&excfds);
			FD_SET(sfd, &readfds);
			FD_SET(sfd, &excfds);
			struct timeval tv = {0, 0};
			tv.tv_sec = (unsigned int)timeout;
			tv.tv_usec = (unsigned int)(1e6 * (timeout - tv.tv_sec));
			int n = select(sfd + 1, &readfds, NULL, &excfds, &tv);
			if(n == 0) // timeout!
				return 0;
			if(n == -1) {
				fprintf(stderr, "ln_udp_receiver::generate_packet() select(): %s\n", strerror(errno));
				return -1;
			}
			if(FD_ISSET(sfd, &readfds))
				break;
			if(FD_ISSET(sfd, &excfds)) {
				fprintf(stderr, "ln_udp_receiver::generate_packet() select(): fd %d is in exception-fds!\n", sfd);
				break;
			}
		}
	}
#if defined(__WIN32__)
	n = recv(sfd, (char*)target_buffer, s, flags);
#else
	// printf("port %s: recv on %p %d bytes (user site %d)\n", get_id().c_str(), packet, s, size);
	n = recv(sfd, (void*)target_buffer, s, flags); // blocking

#endif
#endif
	if(timeout == -1 && n < 0)
		throw errno_exception("udp_receiver port %s got error on receive:", get_id().c_str());
	if((unsigned int)n != s) {
		if(timeout >= 0)
			return 0; // no new packet
		log("warning udp_receiver port %s received packet of %d bytes - expected %d bytes!",
			get_id().c_str(), n, s);
	}

	// todo: endianess swap on receiver side!
	if(swap_endianess)
		swap_endianess.swap_into(packet);
	
	packet_timestamp = *(double*)packet;
	memcpy(data, (unsigned char*)packet + sizeof(double), size);
	packet_counter = *(uint32_t*)((unsigned char*)packet + sizeof(double) + size);
	debug("udp received packet ts: %.3f, counter: %d\n", packet_timestamp, packet_counter);
	return 1; // generated a packet
}

