#ifndef LOCK_H
#define LOCK_H

#include "os.h"
/*
#include <pthread.h>

#ifdef pthread_rwlock_t
typedef pthread_rwlock_t lock_t;
#else
#include <semaphore.h>
typedef sem_t lock_t;
#define SEM_LOCK
// #warning no pthread_rwlock_t
#endif
*/

class flolock;
#include "condition.h"

class flolock {
	os::mutex_handle_t rwlock;
	int initialized;
	bool locked;

	friend class os::condition;
public:
	flolock(bool locked=true);
	~flolock();

	bool free();

	// void lock():
	void operator()();
	bool trylock();
	void unlock();
  
};

#endif // LOCK_H
