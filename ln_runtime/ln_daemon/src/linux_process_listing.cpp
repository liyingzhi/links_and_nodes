/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#if defined(__LINUX__) && !defined(__ANDROID__)

#include <string.h>

#include "os.h"
#include <string_util/string_util.h>

#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "filter.h"

#ifdef NO_LIBPROCPS
using namespace std;
using namespace string_util;

#warning building without libprocps!

string linux_process_listing(bool with_env, bool with_threads, string filter_string) {
	py_list* ret = new py_list();
	string strret = repr(ret);
	delete ret;
	return strret;
}

#else // LIBPROCPS

#include <pty.h>
#include <proc/readproc.h>
#include <proc/wchan.h>
#include <sched.h>

#include <termios.h>

using namespace std;
using namespace string_util;

#ifndef OLD_LIBPROCPS
// newer libprocps
bool have_process = false;
ln_pipe_t cmd_pipe;
ln_pipe_t resp_pipe;
pid_t process_pid;

string linux_process_listing(bool with_env, bool with_threads, string filter_string) {
	py_list* ret = new py_list();
	static double uptime = -1; // time of boot in real!
	
	// get uptime
	if(uptime == -1) {
		FILE* fp = fopen("/proc/uptime", "rb");
		char line[1024];
		fgets(line, 1024, fp);
		fclose(fp);
		char* first = strtok(line, " ");
		uptime = time(NULL) - atol(first);

	}

	int flags = PROC_FILLMEM | PROC_FILLCOM	| PROC_FILLUSR | PROC_FILLGRP | PROC_FILLSTATUS | PROC_FILLSTAT | PROC_FILLARG | PROC_LOOSE_TASKS;
	if(with_env)
		flags |= PROC_FILLENV;

	PROCTAB* pt = openproc(flags);

	py_list* hptr = new py_list();
	py_list& h = *hptr;
	ret->value.push_back(hptr);
	bool have_header = false;

	py_list* t = new py_list();
	py_list& d = *t;
	ret->value.push_back(t);

	filter_t filter = parse_filter(filter_string);
	proc_t* taskp = NULL;
	proc_t* threadp = NULL;
	
	while(true) {
		if(!(taskp = readproc(pt, taskp)))
			break;
		proc_t* curptr = taskp;

#define get(t, n) { d.value.push_back(new py_ ## t(cur.n)); if(!have_header) { h.value.push_back(new py_string(string(#n))); } }
#define get_char(n) { d.value.push_back(new py_string(&cur.n, 1)); if(!have_header) { h.value.push_back(new py_string(string(#n))); } }

		while(true) {

			if(with_threads) {
				if(!(threadp = readtask(pt, taskp, threadp)))
					break; // no more threads...
				curptr = threadp;
			}
			proc_t& cur = *curptr;

			if(filter.size()) {
				bool ok = true;
				for(filter_t::iterator i = filter.begin(); i != filter.end(); ++i) {
					filter_item_t& item = *i;
#define compare_int(n) if(item.op == "==" && item.key == #n) { int value = atoi(item.value.c_str()); if(cur.n != value) { ok = false; break; } }
#define compare_long(n) if(item.op == "==" && item.key == #n) { long value = atol(item.value.c_str()); if(cur.n != value) { ok = false; break; } }
#define compare_ulong(n) if(item.op == "==" && item.key == #n) { unsigned long value = strtoul(item.value.c_str(), NULL, 0); if(cur.n != value) { ok = false; break; } }

					compare_int(tgid);
					compare_int(tid);
					compare_int(ppid);
					compare_long(priority);
					compare_long(nice);
					compare_ulong(rtprio);
					compare_ulong(sched);
					compare_int(pgrp);
					compare_int(session);
				}
				if(!ok)
					continue; // skip
			}
			get(int, tid);
			get(int, ppid);
			get_char(state);
			get(long, utime);
			get(long, stime);
			get(long, cutime);
			get(long, cstime);
			// d.value["start_time"] = new py_float(uptime + (double)cur.start_time / 100.);
			d.value.push_back(new py_float(uptime + (double)cur.start_time / 100.));
			if(!have_header) {
				h.value.push_back(new py_string(string("start_time")));
			}
			// get(long, start_time);

			//get(long, wchan);
			d.value.push_back(new py_string(lookup_wchan(cur.tid)));
			if(!have_header) {
				h.value.push_back(new py_string(string("wchan")));
			}

			get(long, priority);

			get(long, nice);
			get(long, size); // total number of pages
			get(long, resident); // number of resident set non-swapped pages (4k)
			get(long, share); // number of mmap'd pages
			get(long, dt); // dirty pages

			get(long, vm_size); // virtual memory in kb
			get(long, vm_lock); // locked memory in kb
			get(long, vm_rss); // resident set size in kb
			get(long, vm_data); // data in kb
			get(long, vm_stack); // stack size in kb
			get(long, vm_exe); // executable size in kb
			get(long, vm_lib); // library size in kb

			get(long, rtprio);
			get(long, sched);
			get(long, flags); // # kernel flags
			get(long, min_flt); // # minor page faults
			get(long, maj_flt); // # minor page faults

			if(with_env) {
				py_list* el = new py_list();
				char** ep = cur.environ;
				while(ep && *ep) {
					el->value.push_back(new py_string(*ep));
					ep++;
				}
				// d.value["environ"] = el;
				d.value.push_back(el);
				if(!have_header) {
					h.value.push_back(new py_string(string("environ")));
				}
			}

			if((flags & PROC_FILLCOM) || (flags & PROC_FILLARG)) {
				py_list* el = new py_list();
				char** ep = cur.cmdline;
				while(ep && *ep) {
					el->value.push_back(new py_string(*ep));
					ep++;
				}
				// d.value["cmdline"] = el;
				d.value.push_back(el);
				if(!have_header) {
					h.value.push_back(new py_string(string("cmdline")));
				}
			}

			get(string, euser);
			get(string, ruser);
			get(string, suser);
			get(string, fuser);
			get(string, rgroup);
			get(string, egroup);
			get(string, sgroup);
			get(string, fgroup);

			get(string, cmd);

			get(int, pgrp);
			get(int, session);
			get(int, nlwp); // number of threads
			get(int, tgid); // task group ID -> POSIX PID
			get(int, tty);

			get(int, euid); // efff.
			get(int, ruid); // real
			get(int, suid); // saved
			get(int, fuid); // for file access

			get(int, egid); // efff.
			get(int, rgid); // real
			get(int, sgid); // saved
			get(int, fgid); // for file access

			get(int, tpgid);
			get(int, exit_signal);
			get(int, processor);

			have_header = true;

			if(threadp) {
				freeproc(threadp);
				threadp = NULL;
			}
			
			if(!with_threads || taskp->nlwp <= 1)
				break; // no threads...
			// do_threads = true;
		}
		
		if(taskp) {
			freeproc(taskp);
			taskp = NULL;
		}
	}
	closeproc(pt);

	string strret = repr(ret);
	delete ret;
	return strret;
}

#else // older libprocps that tends to cause segfaults... thats why we fork in here...

#warning compiling workaround for older libprocps
bool have_process = false;
ln_pipe_t cmd_pipe;
ln_pipe_t resp_pipe;
pid_t process_pid;

string _linux_process_listing(bool with_env, bool with_threads, string filter_string) {
	py_list* ret = new py_list();
	static double uptime = -1; // time of boot in real!
	
	// get uptime
	if(uptime == -1) {
		FILE* fp = fopen("/proc/uptime", "rb");
		char line[1024];
		fgets(line, 1024, fp);
		fclose(fp);
		char* first = strtok(line, " ");
		uptime = time(NULL) - atol(first);

		// open kernel symbol table
		open_psdb(NULL);
	}

	int flags = PROC_FILLMEM | PROC_FILLCOM	| PROC_FILLUSR | PROC_FILLGRP | PROC_FILLSTATUS | PROC_FILLSTAT | PROC_FILLWCHAN | PROC_FILLARG | PROC_LOOSE_TASKS;
	if(with_env)
		flags |= PROC_FILLENV;

	PROCTAB* pt = openproc(flags);
	if(!pt)
		throw str_exception_tb("openproc %#x failed!", flags);

	py_list* hptr = new py_list();
	py_list& h = *hptr;
	ret->value.push_back(hptr);
	bool have_header = false;

	py_list* t = new py_list();
	py_list& d = *t;
	ret->value.push_back(t);

	filter_t filter = parse_filter(filter_string);
	
	proc_t task;
	proc_t thread;
	memset(&task, 0, sizeof(task));
	memset(&thread, 0, sizeof(thread));
	while(true) {
		if(!(readproc(pt, &task)))
			break;
		proc_t* curptr = &task;

#define get(t, n) { d.value.push_back(new py_ ## t(cur.n)); if(!have_header) { h.value.push_back(new py_string(string(#n))); } }
#define get_char(n) { d.value.push_back(new py_string(&cur.n, 1)); if(!have_header) { h.value.push_back(new py_string(string(#n))); } }

		while(true) {
			if(with_threads) {
				if(!(readtask(pt, &task, &thread)))
					break; // no more threads...
				curptr = &thread;
			}
			proc_t& cur = *curptr;

			if(filter.size()) {
				bool ok = true;
				for(filter_t::iterator i = filter.begin(); i != filter.end(); ++i) {
					filter_item_t& item = *i;
#define compare_int(n) if(item.op == "==" && item.key == #n) { int value = atoi(item.value.c_str()); if(cur.n != value) { ok = false; break; } }
#define compare_long(n) if(item.op == "==" && item.key == #n) { long value = atol(item.value.c_str()); if(cur.n != value) { ok = false; break; } }
#define compare_ulong(n) if(item.op == "==" && item.key == #n) { unsigned long value = strtoul(item.value.c_str(), NULL, 0); if(cur.n != value) { ok = false; break; } }

					compare_int(tgid);
					compare_int(tid);
					compare_int(ppid);
					compare_long(priority);
					compare_long(nice);
					compare_ulong(rtprio);
					compare_ulong(sched);
					compare_int(pgrp);
					compare_int(session);
				}
				if(!ok)
					continue; // skip
			}
			get(int, tid);
			get(int, ppid);
			get_char(state);
			get(long, utime);
			get(long, stime);
			get(long, cutime);
			get(long, cstime);
			// d.value["start_time"] = new py_float(uptime + (double)cur.start_time / 100.);
			d.value.push_back(new py_float(uptime + (double)cur.start_time / 100.));
			if(!have_header) {
				h.value.push_back(new py_string(string("start_time")));
			}
			// get(long, start_time);

			//get(long, wchan);
			d.value.push_back(new py_string(lookup_wchan(cur.wchan, cur.tid)));
			if(!have_header) {
				h.value.push_back(new py_string(string("wchan")));
			}

			get(long, priority);

			get(long, nice);
			get(long, size); // total number of pages
			get(long, resident); // number of resident set non-swapped pages (4k)
			get(long, share); // number of mmap'd pages
			get(long, dt); // dirty pages

			get(long, vm_size); // virtual memory in kb
			get(long, vm_lock); // locked memory in kb
			get(long, vm_rss); // resident set size in kb
			get(long, vm_data); // data in kb
			get(long, vm_stack); // stack size in kb
			get(long, vm_exe); // executable size in kb
			get(long, vm_lib); // library size in kb

			get(long, rtprio);
			get(long, sched);
			get(long, flags); // # kernel flags
			get(long, min_flt); // # minor page faults
			get(long, maj_flt); // # minor page faults

			if(with_env) {
				py_list* el = new py_list();
				char** ep = cur.environ;
				while(ep && *ep) {
					el->value.push_back(new py_string(*ep));
					ep++;
				}
				// d.value["environ"] = el;
				d.value.push_back(el);
				if(!have_header) {
					h.value.push_back(new py_string(string("environ")));
				}
			}

			if((flags & PROC_FILLCOM) || (flags & PROC_FILLARG)) {
				py_list* el = new py_list();
				char** ep = cur.cmdline;
				while(ep && *ep) {
					el->value.push_back(new py_string(*ep));
					ep++;
				}
				// d.value["cmdline"] = el;
				d.value.push_back(el);
				if(!have_header) {
					h.value.push_back(new py_string(string("cmdline")));
				}
			}

			get(string, euser);
			get(string, ruser);
			get(string, suser);
			get(string, fuser);
			get(string, rgroup);
			get(string, egroup);
			get(string, sgroup);
			get(string, fgroup);

			get(string, cmd);

			get(int, pgrp);
			get(int, session);
			get(int, nlwp); // number of threads
			get(int, tgid); // task group ID -> POSIX PID
			get(int, tty);

			get(int, euid); // efff.
			get(int, ruid); // real
			get(int, suid); // saved
			get(int, fuid); // for file access

			get(int, egid); // efff.
			get(int, rgid); // real
			get(int, sgid); // saved
			get(int, fgid); // for file access

			get(int, tpgid);
			get(int, exit_signal);
			get(int, processor);

			have_header = true;

			if(!with_threads || cur.nlwp <= 1)
				break; // no threads...
			// do_threads = true;
		}
	}
	closeproc(pt);

	string strret = repr(ret);
	delete ret;
	return strret;
}

string linux_process_listing(bool with_env, bool with_threads, string filter);

string retry(bool with_env, bool with_threads, string filter) {
	os::kill(process_pid);
	process_pid = 0;
	have_process = false;
	os::pipe_close(cmd_pipe);
	os::pipe_close(resp_pipe);
	printf("waiting before retry...\n");
	sleep(1);
	return linux_process_listing(with_env, with_threads, filter);
}

string linux_process_listing(bool with_env, bool with_threads, string filter) {
	if(!have_process) {
		signal(SIGPIPE, SIG_IGN);
		os::pipe_init(cmd_pipe);
		os::pipe_init(resp_pipe);
		process_pid = fork();

		if(process_pid == 0) {
			signal(SIGPIPE, SIG_IGN);
			signal(SIGTERM, SIG_DFL);
			signal(SIGINT, SIG_DFL);
			// new child process
			// close all file descriptors
			int table_size = getdtablesize();
			for(int i = 3; i < table_size; i++) {
				if(i != resp_pipe.output_fd && i != cmd_pipe.input_fd)
					close(i);
			}

			while(true) {
				// wait for requests
				int ret;
				uint32_t cmd;
				// printf("process listing: waiting for command\n");
				ret = read(cmd_pipe.input_fd, &cmd, sizeof(cmd));
				if(ret != sizeof(cmd)) {
					if(errno == EINTR)
						continue;
					break;
				}
				// printf("process listing: got command: 0x%x\n", cmd);
				bool with_env = cmd & 1;
				bool with_threads = cmd & 2;
				bool with_filter = cmd & 4;
				string filter = "";
				if(with_filter) {
					uint32_t filter_len;
					ret = read(cmd_pipe.input_fd, &filter_len, sizeof(filter_len));
					if(ret != sizeof(filter_len)) {
						if(errno == EINTR)
							continue;
						break;
					}
					static char* filter_string = NULL;
					static uint32_t filter_string_len = 0;
					if(filter_string_len < filter_len) {
						filter_string_len = filter_len;
						filter_string = (char*)malloc(filter_len);
					}
					ret = read(cmd_pipe.input_fd, filter_string, filter_len);
					if((unsigned)ret != filter_len) {
						if(errno == EINTR)
							continue;
						break;
					}
					filter = filter_string;
				}
				string pl = _linux_process_listing(with_env, with_threads, filter);
				uint32_t retlen = pl.size();
				// printf("process listing: write response of %d bytes\n", retlen);
				write(resp_pipe.output_fd, &retlen, sizeof(retlen));
				write(resp_pipe.output_fd, pl.c_str(), retlen);
			}
			// printf("process listing: exiting...\n");
			exit(0);
		}
		close(cmd_pipe.input_fd);
		close(resp_pipe.output_fd);
		have_process = true;
	}
	// send command to process
	uint32_t cmd = 0;
	if(with_env)
		cmd |= 1;
	if(with_threads)
		cmd |= 2;
	if(filter.size())
		cmd |= 4;
	int ret;
	while(true) {
		ret = write(cmd_pipe.output_fd, &cmd, sizeof(cmd));
		if(ret == sizeof(cmd))
			break;
		if(errno == EINTR)
			continue;
		return retry(with_env, with_threads, filter);
	}
	if(filter.size()) {
		// send filter length and filter string
		uint32_t filter_len = filter.size();
		while(true) {
			ret = write(cmd_pipe.output_fd, &filter_len, sizeof(filter_len));
			if(ret == sizeof(filter_len))
				break;
			if(errno == EINTR)
				continue;
			return retry(with_env, with_threads, filter);
		}
		while(true) {
			ret = write(cmd_pipe.output_fd, filter.c_str(), filter_len);
			if((unsigned)ret == filter_len)
				break;
			if(errno == EINTR)
				continue;
			return retry(with_env, with_threads, filter);
		}
	}
	// receive response
	// printf("process listing: waiting for response!\n");
	uint32_t resp_len;
	while(true) {
		ret = read(resp_pipe.input_fd, &resp_len, sizeof(resp_len));
		if(ret == sizeof(resp_len))
			break;
		if(errno == EINTR)
			continue;
		// printf("process listing: retry!\n");
		return retry(with_env, with_threads, filter);
	}
	// printf("process listing: will read resp of %d bytes\n", resp_len);
	static char* resp = NULL;
	static uint32_t max_resp_len = 0;
	if(resp_len > max_resp_len) {
		void* new_mem = realloc(resp, resp_len);
		if(!new_mem) {
			printf("process listing: failed to realloc...\n");
			return "";
		}
		resp = (char*)new_mem;
		max_resp_len = resp_len;
	}
	while(true) {
		// printf("process listing: try to read remaining %d bytes...\n", resp_len);
		ret = read(resp_pipe.input_fd, resp, resp_len);
		if((unsigned)ret == resp_len)
			break;
		if(ret == -1 && errno == EINTR)
			continue;
		return retry(with_env, with_threads, filter);
	}
	// printf("process listing: done\n");
	return string(resp, resp_len);
}

#endif // OLD_LIBPROCPS
#endif // LIBPROCPS
#endif 
