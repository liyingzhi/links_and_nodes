#ifndef UTIL_H
#define UTIL_H

#include <sstream>
#include <math.h>

#include "os.h"

using namespace std;
using namespace os;

int resolve_hostname(string hostname_str, struct sockaddr_in* sa);

class fps_counter {
	double* times;
	double* start;
	int count;
	int _size;
	float max_age;

public:
fps_counter(int size=100, float max_age=-1) : _size(size), max_age(max_age) {
		count = 0;
		times = new double[size];
		start = times;
	}
	~fps_counter() {
		delete[] times;
	}

	void resize(int new_size) {
		delete[] times;
		times = new double[new_size];
		start = times;
		_size = new_size;
		count = 0;
	}

	bool isfull() const { return count == _size; }

	int size() const { return _size; }

	double duration() const { 
		if(count <= 1)
			return 0;
		double* end = start;
		double* begin;
		if(count == _size)
			begin = start; // buffer full
		else
			begin = end - count; // not yet filled
		double start_time = *begin;
		double end_time = 0;
		begin++;
		while(1) {
			if(begin >= (times + _size))
				begin = times;
			if(begin == end)
				break;
			end_time = *begin;
			begin++;
		}
		return end_time - start_time;
	}

	void hit() {
		*start = ln_get_monotonic_time();
		if(count < _size)
			count++;
		start++;
		if(start == (times + _size))
			start = times;
	}

	void reset() {
		count = 0;
		start = times;
	}

	double get_avg() {
		if(count <= 1)
			return 0; // no fps data av. yet
		double* end = start;
		double* begin;
		if(count == _size)
			begin = start; // buffer full
		else
			begin = end - count; // not yet filled
		double last = *begin;
		begin++;
		double sum = 0;
		while(1) {
			if(begin >= (times + _size))
				begin = times;
			if(begin == end)
				break;
        
			double delta = *begin - last;
			sum += delta;
			last = *begin;
			begin++;
		}
		return (sum / double(count - 1)); // [s]
	}

	double get_stddev() {
		if(count <= 1)
			return 0; // no fps data av. yet
		double avg = get_avg();
		double* end = start;
		double* begin;
		if(count == _size)
			begin = start; // buffer full
		else
			begin = end - count; // not yet filled
		double last = *begin;
		begin++;
		double sum = 0;
		while(1) {
			if(begin >= (times + _size))
				begin = times;
			if(begin == end)
				break;
        
			double delta = *begin - last;
			sum += (avg - delta) * (avg - delta);
			last = *begin;
			begin++;
		}
		return sqrt((sum / double(count - 1))); // [s]
	}

	double get() {
		if(count <= 1)
			return 0; // no fps data av. yet
		return 1. / get_avg(); // [1/s]
	}

};

int getuid_by_name(const char *name);
int getgid_by_name(const char *name);
std::string read_file(const std::string& fn);


#endif // UTIL_H

