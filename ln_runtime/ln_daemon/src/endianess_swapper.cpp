/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <stdio.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "endianess_swapper.h"

using namespace std;

void endianess_swapper::set_format(string format) {
	this->format = format;

	unsigned int format_size = 0;
	format_counts.resize(0);
	for(unsigned int i = 0; i < format.size(); i++) {
		uint8_t count = atoi(format.substr(i, 1).c_str());
		format_size += count;
		format_counts.push_back(count);
	}
	buffer.resize(format_size);
}

void print_hex(void* ptr, unsigned int len) {
	uint8_t* cp = (uint8_t*)ptr;
	for(unsigned int i = 0; i < len; ++i) {
		printf("%02x ", *cp);
		cp++;
	}
	printf("\n");
	return;
}

void endianess_swapper::swap_into(void* target) {
	// swap from buffer into target
	uint8_t* src = &buffer[0];
	uint8_t* dst = (uint8_t*)target;
	for(unsigned int i = 0; i < format_counts.size(); i++) {
		uint8_t& count = format_counts[i];

		//dst += count;
		//for(unsigned int c = 0; c < count; c++)
		//	*(dst--) = *(src++);
		
		swap(count, dst, src);
		src += count;
		
		dst += count;
	}
	/*
	printf("input : ");
	print_hex(&buffer[0], buffer.size());
	printf("output: ");
	print_hex(target, buffer.size());
	*/
}

void endianess_swapper::swap_msg(struct msghdr* message) {
	format_counts_t::iterator i = format_counts.begin();

	struct iovec* current_buffer = message->msg_iov;
	// unsigned int n_buffers = message->msg_iovlen;

	unsigned int src_rem = current_buffer->iov_len;
	uint8_t* src = (uint8_t*)current_buffer->iov_base;
	uint8_t* dst = &buffer[0];
	while(i != format_counts.end()) {
		uint8_t& count = *i;
		
		if(!src_rem) {
			current_buffer++;
			src_rem = current_buffer->iov_len;
			src = (uint8_t*)current_buffer->iov_base;
		}

		swap(count, dst, src);
		src += count;

		dst += count;
		src_rem -= count;

		++i;
	}
	
	message->msg_iov->iov_base = (char*)&buffer[0];
	message->msg_iov->iov_len = buffer.size();
	message->msg_iovlen = 1;
}
