#ifndef LN_TCP_PORTS_H
#define LN_TCP_PORTS_H

class ln_tcp_receiver : public ln_port, public ln_source_port {
	int sfd;
	int tcp_port;
	unsigned int size;
	void* packet;
	endianess_swapper swap_endianess;
	
	struct sockaddr_in client_address;
	int client_fd;
	void incoming_connection();

	void close();

	static void incoming_connection_cb(int fd, int why, void* data);
protected:
	int generate_packet(void* data, double timeout=-1);

public:
	ln_tcp_receiver(ln_instance* instance, unsigned int size, thread_settings* ts=NULL, string swap_endianess="");
	~ln_tcp_receiver();

#include "port_virtuals.h"
#include "src_port_virtuals.h"

	string get_id() {
		return format_string("(%s, %d)", repr(port_type).c_str(), tcp_port);
	}
};

class ln_tcp_sender : public ln_port, public ln_sink_port {
	int sfd;
	string target_host;
	int target_port;
	endianess_swapper swap_endianess;
	
	struct msghdr message;
	vector<iovec> buffers;
	uint32_t bytes_to_send;
	uint32_t bytes_sent;
	
	bool reconnect_registered;
	int timeout_id;
	void register_reconnect();

	/* test sentinels
	void* data_at_start;
	uint32_t first_at_start;
	uint32_t last_at_start;
	*/
	
	bool keep_running;       
	ln_semaphore_handle_t trigger_send;
	os::thread_handle_t thread_handle;
	
#ifdef __WIN32__
	static thread_return_t WINAPI _sender_thread(void* data);
#else
	static thread_return_t _sender_thread(void* data);
#endif
	
#if defined(__LINUX__) || defined(__VXWORKS__)
	pid_t sender_tid;
#endif	
	thread_return_t sender_thread();
	
	bool send_registered;
	void register_send();
	void unregister_send();

	void try_send();
	
	bool try_connect();
	bool connect();
	void close();

	static void send_callback(int fd, int why, void* data);
	static bool try_connect_timeout(void* data);

public:
	ln_tcp_sender(ln_instance* instance, string target_host, int target_port, int size, float rate, string swap_endianess="");
	~ln_tcp_sender();

#include "port_virtuals.h"
#include "dst_port_virtuals.h"

	string get_id() {
		return format_string("(%s, %s, %d)", repr(port_type).c_str(), repr(target_host).c_str(), target_port);
	}
	void consume_packet(double ts, uint32_t counter, void* data);
};

#endif // LN_TCP_PORTS_H
