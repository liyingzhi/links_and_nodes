/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <string_util/string_util.h>

#include "ln_ports.h"

using namespace string_util;

ln_shm_port::ln_shm_port(string name, unsigned int shm_version)
	: name(name), shm_version(shm_version) {
#ifdef __ANDROID__
	fd = -1;
	shm_size = 0;
#endif
}

int ln_shm_port::throw_error(int ret, string s) {
	if(ret == -LNE_CHECK_ERRNO)
		throw errno_exception("%s", s.c_str());
	throw str_exception("%s: %s\n", s.c_str(), ln_format_error(ret));
}



// shm source port
ln_shm_source::ln_shm_source(ln_instance* instance, string shm_name, unsigned int element_size, unsigned int n_elements, thread_settings* ts, unsigned int shm_version):
	ln_port(instance, "shm", SOURCE_PORT, element_size),
	ln_source_port(ts),
	ln_sink_port(-1),
	ln_shm_port(shm_name, shm_version) {

	log(__func__);

	if(n_elements < 1)
		throw str_exception("can not create shm_source with %d elements!", n_elements);
	
	int ret;
	if((ret = create_shm_source(&source, shm_name.c_str(), element_size, n_elements, shm_version)))
		throw_error(ret, 
			    format_string(
				    "could not create shm source\n%s\nwith name %s\nand element_size %d,\nn_elements %d",
				    get_id().c_str(), repr(name).c_str(), element_size, n_elements));
#ifdef __ANDROID__
	// get fd and store in port!
	fd = get_shm_fd(source, &shm_size);
#endif	
	instance->register_port(this);
	// do not automatically start this -> only if there are subscribers (udp or rate != -1) // start();
}
ln_shm_source::~ln_shm_source() {
	// log("ln_shm_source de-tor!");
	//printf("stop shm source\n");
	stop();
	//printf("destroy shm source\n");
	destroy_shm_source(&source);
	//printf("done\n");
}
int ln_shm_source::generate_packet(void* data, double timeout) {
	int ret;
	if(timeout == 0) // return last packet!
		ret = read_last_from_shm_source(source, &packet_timestamp, &packet_counter, data, size);
	else
		ret = read_from_shm_source_timeout(source, &packet_timestamp, &packet_counter, data, size, timeout);
	if(ret == -1)
		throw_error(ret, format_string("could not wait for shm source %s", get_id().c_str()));
	return ret;
}
void ln_shm_source::consume_packet(double ts, uint32_t counter, void* data) {
	int ret;
	if((ret = write_to_shm_target(source, ts, data, size)))
		throw_error(ret, format_string("could not write %d bytes to shm target %s!",
									   size, repr(name).c_str()));
}
void ln_shm_source::get_last_packet_req(request_t& answer) {
	answer["success"] = repr("success");
	
	if(!started) {
		// source thread not running... get data and make simple guess!
		generate_packet(get_last_packet(), 0); // non-blocking!
		get_packet_count() = packet_counter;
		double now = ln_get_time(); /* using sometimes non-monotonous wall clock 
					       time seems right here, as date is given out */
		rate_est_info.push_back(pair<double, uint64_t>(now, get_packet_count()));
		if(rate_est_info.size() > rate_est_info_size)
			rate_est_info.pop_front();
	}
		
	answer["last_packet"] = binary_data_repr((char*)last_packet, size);
	answer["last_packet_time"] = format_string("%f", packet_timestamp);
	answer["packet_count"] = format_string("%lu", packet_counter);

	answer["logging_enabled"] = format_string("%d", logging_enabled);
	answer["logging_size"] = format_string("%u", log_buffer.get_size());
	answer["logging_count"] = format_string("%u", log_buffer.get_count());
	answer["logging_only_ts"] = format_string("%d", logging_only_ts);
	answer["logging_divisor"] = format_string("%d", logging_divisor);

}

void ln_shm_source::_need_thread_for_logging(bool needed) {
	if(needed && !ln_sink_port::source)
		start();
	else if(!sinks.size())
		stop();
}

string ln_shm_source::get_id() {
	if(instance->connection && instance->connection->get_protocol() > 23)
		return format_string("(%s, %s, %d)", repr(port_type).c_str(), repr(name).c_str(), shm_version);
	else
		return format_string("(%s, %s)", repr(port_type).c_str(), repr(name).c_str());
}




// shm destination sink port
ln_shm_destination::ln_shm_destination(ln_instance* instance, string shm_name, unsigned int element_size, unsigned n_elements, float rate, unsigned int shm_version) : 
			ln_port(instance, "shm", SINK_PORT, element_size),
			ln_sink_port(rate),
			ln_shm_port(shm_name, shm_version) {
	// log("new shm destination with name '%s', size %d, rate: %.7f\n", shm_name.c_str(), shm_size, rate);
	int ret;
	
	if(n_elements < 1)
		throw str_exception("can not create shm_destination with %d elements!", n_elements);

	if((ret = create_shm_target(&target, name.c_str(), element_size, n_elements, shm_version)))
		throw_error(ret, format_string("could not create shm target %s with name %s and element_size %d, n_elements %d\n", 
					       get_id().c_str(), repr(name).c_str(), element_size, n_elements));
#ifdef __ANDROID__
	// get fd and store in port!
	fd = get_shm_fd(target, &shm_size);
#endif	
	instance->register_port(this);
}
ln_shm_destination::~ln_shm_destination() {
	if(source) {
		log("ln_shm_destination was still connected. removing connection now!");
		source->remove_sink(this);
		source = NULL;
	}
	destroy_shm_target(&target);
}

void ln_shm_destination::consume_packet(double ts, uint32_t counter, void* data) {
	int ret;
	if((ret = write_to_shm_target(target, ts, data, size)))
		throw_error(ret, format_string("could not write %d bytes to shm target %s!", size, repr(name).c_str()));
}
string ln_shm_destination::get_id() {
	if(instance->connection && instance->connection->get_protocol() > 23)
		return format_string("(%s, %s, %d)", repr(port_type).c_str(), repr(name).c_str(), shm_version);
	else
		return format_string("(%s, %s)", repr(port_type).c_str(), repr(name).c_str());
}
