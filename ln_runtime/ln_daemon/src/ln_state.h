// -*- mode: cpp -*-
#ifndef LN_STATE
#define LN_STATE

#include <string>
#include <list>

#include <string_util/string_util.h>
#include "logger.h"

class ln_process;
class ln_instance;
class ln_manager_connection;

using namespace std;
using namespace string_util;

typedef void (*callback_t)(int, int, void*);

class ln_state {
	ln_instance* instance;
	string name;
	string os_state_fn;

	enum {
		IDLE = 0,
		CHECKING,
		UPPING,
		DOWNING
	} state;
	
	ln_process* current_process;
	string last_state;

	typedef py_dict props_t;
	props_t* props;

	void send_update(); // to ln_manager(s)
	
	// exit callbacks
	friend void check_exitcb(int pid, int exitcode, void* instance);
	friend void up_exitcb(int pid, int exitcode, void* instance);
	friend void down_exitcb(int pid, int exitcode, void* instance);
	void exec(string which, string cmd, callback_t exitcb, void* arg); // execute command as ln_process
	void process_finished(string new_state); // called after some process exited with new state
	
	string get_os_state(); // return current os-state
	void set_os_state(string up_down); // set net os-state

public:
	typedef struct {
		string check;
		string up;
		string down;
		bool os_state;
	} config_t;
	config_t config;

	ln_state(ln_instance* instance, string name, const config_t& config);
	~ln_state();

	void update(const config_t& config); // update config from manager
	std::string request_state(ln_manager_connection* conn, string up_down);
	void set_property(string name, py_value* value);
};

#endif // LN_STATE

