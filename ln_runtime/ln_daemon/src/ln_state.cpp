/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"

#include <stdio.h>

#include "ln_state.h"
#include "ln_instance.h"

ln_state::ln_state(ln_instance* instance, string name, const ln_state::config_t& config)
	: instance(instance), name(name), current_process(NULL), last_state(""), config(config) {
	state = IDLE;
	os_state_fn = os::get_temp_dir() + "/ln_os_state_" + string_replace(string_replace(name, " ", "_"), "/", "_");
	props = new py_dict();
}

ln_state::~ln_state() {
	delete props;
	log("state %s: %p detor!?\n", repr(name).c_str(), this);
}

//! update ln_state configuration with new one from manager
/*!
  \param config new ln_state configuration
  \return N/A
 */
void ln_state::update(const ln_state::config_t& config) {
	string old_check = config.check;
	this->config = config;

	if(old_check != config.check)
		last_state = "";
}

//! return current state on operating system
/*! this call reads the state from state file if present
  \return current state
  */
string ln_state::get_os_state() {
	FILE* fp = fopen(os_state_fn.c_str(), "r");
	if(!fp)
		return "DOWN";
	char data[1024];
	int n = fread(data, 1, 1023, fp);
	if(n == -1) {
		log("state %s: _get_os_state() fread() from file %s returned error %d: %s",
		    repr(name).c_str(), repr(os_state_fn).c_str(), errno,
		    strerror(errno));
		fclose(fp);
		return "unknown";
	}
	data[n] = 0;
	fclose(fp);
	return data;
}

//! set current state on operating system
/*! this call writes the state to tmp state file and sets
    current state
  \param up_down new up/down state
  \return N/A
 */
void ln_state::set_os_state(string up_down) {
	log("state %s _set_os_state(%s)\n", repr(name).c_str(), repr(up_down).c_str());

	FILE* fp = fopen(os_state_fn.c_str(), "w");
	if(!fp) {
		log("state %s _set_os_state(%s) fopen() for file %s returned error %d: %s",
		    repr(name).c_str(), repr(up_down).c_str(), repr(os_state_fn).
		    c_str(), errno, strerror(errno));
		return;
	}
	int n = fwrite(up_down.c_str(), up_down.size(), 1, fp);
	if(n != 1) {
		log("state %s _set_os_state(%s) fwrite() for file %s returned error %d: %s",
		    repr(name).c_str(), repr(up_down).c_str(), repr(os_state_fn).
		    c_str(), errno, strerror(errno));
	}
	fclose(fp);
}

//! execute command as ln_process
/*!
  \param cmd command to execute
  \param exitcb callback to call on processed finished
  \param arg argument passed to callback
  \return N/A
  */
void ln_state::exec(string which, string cmd, callback_t exitcb, void* arg) {
	// generate process config
	ln_process::config_t config = {
		name,       // string name;
		cmd,        // string cmdline;
		true,      // bool start_in_shell; // state commands are always executed in shell
		true,      // bool no_pty;
		true,      // bool log_output;
		1024,          // unsigned int buffer_size;
		string(""), // string change_directory;
		0,          // float term_timeout;
		0,          // int priority;
		-1,         // int policy: do not set any prio or policy!;
		-1,         // affinity: do not set!
		10,          // double max_output_frequency;
		true        // is_state
	};
	py_list empty_env;

	// create new process
	current_process = new ln_process(instance, &empty_env, config, false);
	current_process->set_exitcb(exitcb, arg);
	current_process->set_property("state_cmd", new py_string(which)); // freed in ~ln_process -> ~py_dict
	current_process->execute();	
	// process will be deleted by ln_process::terminated()->ln_instance::remove_process()
}

//! check exit callback, should be called by ln_process on process finished
void check_exitcb(int pid, int exitcode, void* instance) {
	ln_state* self = (ln_state*)instance;
	log("state %s: CHECK command had exitcode %d\n", repr(self->name).c_str(), exitcode);
	string new_state = exitcode == 0 ? "UP" : "DOWN";
	self->process_finished(new_state);
}

//! up exit callback, should be called by ln_process on process finished
void up_exitcb(int pid, int exitcode, void* instance) {
	ln_state* self = (ln_state*)instance;
	log("state %s: UP command had exitcode %d\n", repr(self->name).c_str(), exitcode);
	string new_state = exitcode == 0 ? "UP" : "ERROR";
	self->process_finished(new_state);
}

//! down exit callback, should be called by ln_process on process finished
void down_exitcb(int pid, int exitcode, void* instance) {
	ln_state* self = (ln_state*)instance;
	log("state %s: DOWN command had exitcode %d\n", repr(self->name).c_str(), exitcode);
	string new_state = exitcode == 0 ? "DOWN" : "ERROR";
	self->process_finished(new_state);
}

//! called if process has been terminated
void ln_state::process_finished(string new_state) {
	state = IDLE;
	last_state = new_state;
	current_process = NULL;
	if(config.os_state && (new_state == "UP" || new_state == "DOWN"))
		set_os_state(new_state);
	send_update();
	// process will be deleted by ln_process::terminated()->ln_instance::remove_process()
}

std::string ln_state::request_state(ln_manager_connection* conn, string up_down) {
	// manager expects to always get a state_notification after this!!
	log("state %s: requested: %s\n", repr(name).c_str(), repr(up_down).c_str());
	
	// ensure that we are not already doing something
	if (state != IDLE)
		throw str_exception_tb("state %s: already doing %d! refusing todo %s!", repr(name).c_str(), state, up_down.c_str());
	
	if(up_down == "") {
		// execute CHECK command!
		// need to execute check-command to check this state!
		if(config.os_state) { // os_states have no CHECK command
			last_state = get_os_state();
			send_update();
		} else {
			log("state %s: executing check cmd %s", repr(name).c_str(), repr(config.check).c_str());
			exec("CHECK", config.check, check_exitcb, this);
			state = CHECKING;
			last_state = "<checking>";
		}		
	} else if(up_down == "UP") {
		log("state %s: executing UP cmd %s", repr(name).c_str(), repr(config.up).c_str());
		exec("UP", config.up, up_exitcb, this);
		state = UPPING;
		last_state = "<upping>";
		
	} else if(up_down == "DOWN") {
		log("state %s: executing DOWN cmd %s", repr(name).c_str(), repr(config.down).c_str());
		exec("DOWN", config.down, down_exitcb, this);
		state = DOWNING;
		last_state = "<downing>";
	}
	return last_state;
}

//! send state update to ln_manager(s)
void ln_state::send_update() {
	log("state %s: send update to state %s", repr(name).c_str(), last_state.c_str());

	request_t answer;
	answer["response"] = repr("state_notification");
	answer["name"]     = repr(name);
	answer["up_down"]  = repr(last_state);

	for(ln_daemon::manager_connections_t::iterator c = instance->daemon->manager_connections.begin(); c != instance->daemon->manager_connections.end(); c++) {
		if((*c)->instance != instance)
			continue;
		(*c)->send(answer);
	}
}

void ln_state::set_property(string name, py_value* value) {
	props->value[name] = value->copy();
}
