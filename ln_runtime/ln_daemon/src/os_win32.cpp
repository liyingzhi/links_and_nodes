/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#ifdef __WIN32__

#include "os_win32.h"

#include <stdio.h>
#include <string.h>

#include "logger.h"

#include <time.h>

namespace os {
#ifdef EMACS_IS_INTELLIGENT
}
#endif

int ln_semaphore_create(ln_semaphore_handle_t* handle, const char* name, int create) {
	char* sn = NULL;
	char sem_name[1024];
	if(name) {
		snprintf(sem_name, 1024, "%s_sem", name);
		sn = sem_name;
	}
		
	if(create) {
		*handle = CreateSemaphore(
			NULL, // __in_opt  LPSECURITY_ATTRIBUTES lpSemaphoreAttributes,
			0,    // __in      LONG lInitialCount,
			1,    // __in      LONG lMaximumCount,
			sn
			);
	} else {
		*handle = OpenSemaphore(
			SEMAPHORE_ALL_ACCESS, // __in  DWORD dwDesiredAccess,
			0,                    // __in  BOOL bInheritHandle,
			sn              //__in  LPCTSTR lpName
			);
	}
	if(!*handle)
		return -1;
	return 0;
}
int ln_semaphore_close(ln_semaphore_handle_t handle) {
	CloseHandle(handle);
	return 0;
}
int ln_semaphore_wait(ln_semaphore_handle_t handle) {
	DWORD ret = WaitForSingleObject(handle, INFINITE);
	if(ret == WAIT_FAILED)
		return -1;
	return 0; // success
}
int ln_semaphore_trywait(ln_semaphore_handle_t handle) {
	DWORD ret = WaitForSingleObject(handle, 0);
	if(ret == WAIT_OBJECT_0)
		return 0; // success
	return -1;

}
int ln_semaphore_timedwait(ln_semaphore_handle_t handle, double seconds) {
	DWORD ret = WaitForSingleObject(handle, (DWORD)(seconds * 1e3));
	if(ret == WAIT_OBJECT_0)
		return 0; // success
	return -1;
}
int ln_semaphore_post(ln_semaphore_handle_t handle) {
	ReleaseSemaphore(
		handle, // __in       HANDLE hSemaphore,
		1,      // __in       LONG lReleaseCount,
		NULL    // __out_opt  LPLONG lpPreviousCount
		);
	return 0;
}

int create_mutex(mutex_handle_t* mutex_handle) {
	CRITICAL_SECTION* s = (CRITICAL_SECTION*)malloc(sizeof(CRITICAL_SECTION));
	InitializeCriticalSection(s);
	*mutex_handle = s;
	return 0;
}
int destroy_mutex(mutex_handle_t* mutex_handle) {
	DeleteCriticalSection(*mutex_handle);
	free(*mutex_handle);
	return 0;
}
int lock_mutex(mutex_handle_t* mutex_handle) {
	EnterCriticalSection(*mutex_handle);
	return 0;
}
int trylock_mutex(mutex_handle_t* mutex_handle) {
	BOOL ret = TryEnterCriticalSection(*mutex_handle);
	// return 0 if lock acquired!
	if(ret)
		return 0;
	// return -1 if lock is not acquired!
	return -1;
}
int unlock_mutex(mutex_handle_t* mutex_handle) {
	LeaveCriticalSection(*mutex_handle);
	return 0;
}


void condition_create(condition_handle_t* handle) {
	if(!handle)
		throw str_exception_tb("condition handle has to point to condition_handle_t object!");

	int ret = create_mutex(&handle->mutex);
	if(ret != 0)
		throw str_exception_tb("create_mutex returned %d", ret);

	ret = ln_semaphore_init(&handle->sem, 0);
	if(ret != 0)
		throw str_exception_tb("ln_semaphore_init returned %d", ret);

	handle->n_blocked_threads = 0;
	handle->in_destruct = 0;
}

void condition_destroy(condition_handle_t* handle) {
	if(!handle)
		throw str_exception_tb("condition handle can not be NULL!");

	handle->in_destruct = 1;
	condition_broadcast(handle);
	
	ln_semaphore_destroy(&handle->sem);
	destroy_mutex(&handle->mutex);
}

void condition_wait(condition_handle_t* cond, mutex_handle_t* mutex) {
	if(!cond)
		throw str_exception_tb("condition handle can not be NULL!");
	if(cond->in_destruct)
		throw str_exception_tb("condition is beeing destroyed!");
	
	lock_mutex(&cond->mutex);
	cond->n_blocked_threads ++;
	unlock_mutex(&cond->mutex);

	unlock_mutex(mutex);

	ln_semaphore_wait(cond->sem);

	lock_mutex(mutex);
}

void condition_signal(condition_handle_t* handle) {
	if(!handle)
		throw str_exception_tb("condition handle can not be NULL!");
	lock_mutex(&handle->mutex);
	if(handle->n_blocked_threads) {
		ln_semaphore_post(handle->sem);
		handle->n_blocked_threads --;		
	}
	unlock_mutex(&handle->mutex);
}

void condition_broadcast(condition_handle_t* handle) {
	if(!handle)
		throw str_exception_tb("condition handle can not be NULL!");
	lock_mutex(&handle->mutex);
	while(handle->n_blocked_threads) {
		ln_semaphore_post(handle->sem);
		handle->n_blocked_threads --;		
	}
	unlock_mutex(&handle->mutex);
}


int create_thread(thread_handle_t* thread_handle, thread_func_type thread, void* data) {
	// return pthread_create(thread_handle, NULL, thread, data);

	HANDLE ret = CreateThread(
		NULL,
		0,      // __in       SIZE_T dwStackSize,
		thread, // __in       LPTHREAD_START_ROUTINE lpStartAddress,
		data,   // __in_opt   LPVOID lpParameter,
		0,      // __in       DWORD dwCreationFlags,
		NULL    // __out_opt  LPDWORD lpThreadId
		);
	log("create thread return value: %p\n", ret);
	if(!ret)
		return -1;

	// if successfull:
	struct win32_thread_handle_and_cancel_flag* r = (struct win32_thread_handle_and_cancel_flag*)malloc(sizeof(struct win32_thread_handle_and_cancel_flag));
	r->thread = ret;
	r->cancel_requested = 0;
	
	*thread_handle = r;

	log("created thread handle %p\n", r);
	log("created thread handle %p\n", r->thread);

	return 0;
}


void test_cancel(thread_handle_t handle) {

	if(!handle)
		return;
	if(handle->cancel_requested) {
		ExitThread(0);
	}
}

void cancel_thread(thread_handle_t handle) {
	handle->cancel_requested = 1;
}

int join_thread(thread_handle_t t) {
	log("joining thread handle %p\n", t);
	log("joining thread handle %p\n", t->thread);

	DWORD ret = WaitForSingleObject(
		t->thread, // __in  HANDLE hHandle,
		INFINITE          // __in  DWORD dwMilliseconds
		);
	if(ret == WAIT_FAILED)
		return -1;
	CloseHandle(t->thread);
	t->thread = NULL;
	free(t);
	return 0; // success
}

char *strndup(const char *s, size_t n) {
	const char* cp = s;
	size_t i = 0;
	while(*cp) {
		i++;
		if(i >= n)
			break; // enough chars
		cp++;
	}
	i ++;
	char* result = (char*)malloc(i);
	memcpy(result, s, i);
	result[i - 1] = 0;
	return result;
}


double ln_get_time() {
	SYSTEMTIME t;
	GetSystemTime(&t);
	time_t time_offset = time(NULL);
	return (double)time_offset + (double)t.wMilliseconds / 1e3;
}

double ln_get_monotonic_time() {
	// the unit of GetTickCounts is milliseconds
	// (since system was started)
	return GetTickCount64() * 0.001;
}

void sleep(double s) {
	Sleep((DWORD)(s * 1e3));
}

//! initialize os dependant pipe device 
/*!
  \param reference to pipe device structure
  \return success or error code
  */
int pipe_init(ln_pipe_t& pipe) {
	// create output pipe
	SECURITY_ATTRIBUTES saAttr; 

	// Set the bInheritHandle flag so pipe handles are inherited. 
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES); 
	saAttr.bInheritHandle = TRUE; 
	saAttr.lpSecurityDescriptor = NULL; 

	BOOL pipe_ret = CreatePipe(
		&pipe.input_fd,
		&pipe.output_fd,
		&saAttr,
		0);

	if(pipe_ret == 0)
		return -1;
	return 0;
}

//! closes os dependant pipe device 
/*!
  \param reference to pipe device structure
  \return success or error code
  */
int pipe_close(ln_pipe_t& pipe) {
	if(pipe.output_fd)
		CloseHandle(pipe.output_fd);
	if(pipe.input_fd)
		CloseHandle(pipe.input_fd);
	return 0;
}

int set_prio(pid_t pid, int tid, int prio, int policy) {
	return -1;
}
int set_affinity(pid_t pid, int tid, int affinity) {
	return -1;
}

pid_t gettid() {
	return 0; // todo: currentthread?
}

int kill(pid_t pid) {
	return TerminateProcess((void*)(int)pid, 0);
}

int send_signal(pid_t pid, int signo) {
	return 0; /* sending signal to process does not work on windows */
}

std::string get_temp_dir() {
	char temp_dir[1024];
	GetTempPath(1024, temp_dir);
	return std::string(temp_dir);
}

}

int inet_aton(const char *cp, struct in_addr *inp) {
	inp->s_addr = inet_addr(cp);
	if(inp->s_addr == INADDR_NONE)
		return 0; // invalid
	return 1; // valid
}

string get_windows_error() {
	string ret;
	
	LPVOID lpMsgBuf;
	DWORD dw = GetLastError(); 
	
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) &lpMsgBuf,
		0, NULL );
	
	ret = format_string("GetLastError(): %d, FormatMessage(): %s", dw, lpMsgBuf);
	LocalFree(lpMsgBuf);
	return ret;
}

#endif
