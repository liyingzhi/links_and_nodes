/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#ifdef __VXWORKS__

#include "os_vxworks.h"

#include <time.h>
#include <stdio.h>
#include <ioLib.h>
#include <pipeDrv.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "logger.h"


namespace os {

int create_thread(thread_handle_t* thread_handle, thread_func_type thread, void* data) {
	int ret;
	pthread_attr_t attr;

	pthread_attr_init(&attr);
	ret = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	ret = pthread_create(thread_handle, &attr, thread, data);
	return ret;
}

int cancel_thread(thread_handle_t thread_handle) {
	return pthread_cancel(thread_handle);
}

int join_thread(thread_handle_t thread_handle) {
	return pthread_join(thread_handle, NULL);
}

void test_cancel(thread_handle_t thread_handle) {
	pthread_testcancel();
}

double ts2double(struct timespec* ts) {
	return (double)ts->tv_sec + (ts->tv_nsec / 1e9);
}

void double2ts(double s, struct timespec* ts) {
	ts->tv_sec = (time_t)s;
	ts->tv_nsec = (long)((s - ts->tv_sec) * 1e9);
}

double ln_get_time() {
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	return ts2double(&ts);
}

double ln_get_monotonic_time() {
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ts2double(&ts);
}	
	
void sleep(double s) {
	struct timespec ts;
	double2ts(s, &ts);
	nanosleep(&ts, NULL);
}

#define PIPE_NAME_MAX 512

//! initialize os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
  */
int pipe_init(ln_pipe_t& pipe) {
	static int pipe_count = 0;
	char name[PIPE_NAME_MAX];
	snprintf(name, PIPE_NAME_MAX, "/unixpipe_%d_%d", getpid(), pipe_count++);
	if (pipeDevCreate(name, 10, 1024) == ERROR) {
		log("pipeDevCreate failed with %s\n", strerror(errno));
		return -1;
	}

	pipe.input_fd  = open(name, O_RDONLY | O_NONBLOCK, 777);
	pipe.output_fd = open(name, O_WRONLY, 777);
	log("created pipe-dev (%d, %d): %s\n", pipe.input_fd, pipe.output_fd, name);

	return 0;
}

//! closes os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
  */
int pipe_close(ln_pipe_t& pipe) {
	char name[PIPE_NAME_MAX];
	if(ioctl (pipe.input_fd, FIOGETNAME, name) == ERROR) {
		log("getting pipe name failed with %s\n", strerror(errno));
		return -1;
	}

	log("closing pipe-dev (%d, %d): %s\n", pipe.input_fd, pipe.output_fd, name);

	if (ioctl(pipe.input_fd, FIOFLUSH, 0) == ERROR)
		log("FIOFLUSH failed on input_fd with %s\n", strerror(errno));
	if (pipe.output_fd != -1 && ioctl(pipe.output_fd, FIOFLUSH, 0) == ERROR)
		log("FIOFLUSH failed on output_fd with %s\n", strerror(errno));
	if (close(pipe.input_fd) == ERROR)
		log("close failed on input_fd with %s\n", strerror(errno));
	if (pipe.output_fd != -1 && close(pipe.output_fd) == ERROR)
		log("close failed on output_fd with %s\n", strerror(errno));

	if (pipeDevDelete(name, 0) == ERROR) {
		log("pipeDevDelete failed with %s\n", strerror(errno));
		return -1;
	}
	return 0;
}

int set_prio(pid_t pid, int tid, int prio, int policy) {
	if(prio != -1)
		vxworks_set_prio(tid, prio, -1);
	return 0;
}
int set_affinity(pid_t pid, int tid, int affinity) {
	if(affinity != -1)
		vxworks_set_prio(tid, -1, affinity);
	return 0;
}

int set_affinity_of_current_thread(int affinity) {
#warning no os::set_affinity_of_current_thread()
	return -1;
}

#undef inet_aton
STATUS vxworks_inet_aton(const char* str, in_addr* addr) {
	char* str_write = strdup(str);
	STATUS ret = inet_aton(str_write, addr);
	free(str_write);
	return ret;
}

int gettid() {
	int tid = vxworks_gettid();
	printf("gettid will return 0x%x\n", tid);
	return tid;
}

}

#endif // __VXWORKS__
