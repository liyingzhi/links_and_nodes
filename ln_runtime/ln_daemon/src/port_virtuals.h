virtual double& get_last_packet_source_time() {
	return last_packet_source_time;
}
virtual double& get_last_packet_time() {
	return last_packet_time;
}
virtual uint64_t& get_packet_count() { 
	return packet_count;
}
virtual void*& get_last_packet() {
	return last_packet;
}
virtual unsigned int& get_size() {
	return size;
}
virtual void _append_to_log(void* data, double* ts, uint32_t* counter) {
	ln_port::_append_to_log(data, ts, counter);
}
virtual py_tuple* get_tuple_(py_tuple* t=NULL, int with_last_packet=0) {
	return ln_port::get_tuple(t, with_last_packet);
}

