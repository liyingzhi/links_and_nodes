/*
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
*/

#include "os.h"
#include "ln_tcp_forward.h"

#include <time.h>

buffer::buffer(unsigned int size) : size(size) {
	len = 0;
	_mem.reserve(size);
	data = &_mem[0];
}

tcp_bridge::tcp_bridge(tcp_connection* forward, int fd, string name) 
	: forward(forward),
	  fd(fd),
	  name(name),
	  to_send(2048),
	  target(NULL),
	  daemon(forward->forward->instance->daemon) {
#ifndef __WIN32__	
	// set socket to non-blocking!
	fcntl(fd, F_SETFL, O_NONBLOCK);
#endif
	
	current_watch = -1;
	// set_watch(IO_WATCH_EXCEPT);

	log("%p tcp_bridge ctor '%s' '%s' with fd %d", this, forward->forward->get_id().c_str(), name.c_str(), fd);
}
tcp_bridge::~tcp_bridge() {
	set_watch(0);
	log("%p bridge detor with fd %d", this, fd);
	_ln_close_socket(fd);
}

void tcp_bridge::set_watch(int watch) {
	debug("%p tcp_bridge fd %d set watch %d", this, fd, watch);
	if(current_watch == watch)
		return; // nothing todo
	if(current_watch >= 0) { // remove old watch
		daemon->remove_io_watch(fd);
		current_watch = -1;
	}
	current_watch = watch;
	if(current_watch == 0)
		return;
	daemon->add_io_watch(
		fd, current_watch, _on_data, this,
		format_string("%p tcp_bridge for %s from %s / fd %d", this, forward->forward->get_id().c_str(), name.c_str(), fd));
}

void tcp_bridge::set_target(tcp_bridge* target) {
	this->target = target;
	set_watch(IO_WATCH_READ | IO_WATCH_EXCEPT);
}

void tcp_bridge::insert_received_data(const string& data) {
	if(data.size() > (unsigned)to_send.size)
		throw str_exception_tb("data size %d too large for buffer!", data.size());
	memcpy(to_send.data, data.c_str(), data.size()); // is smaller than to_send.size
	to_send.len = data.size();
	
	target->set_watch(target->current_watch | IO_WATCH_WRITE);
}

void tcp_bridge::_on_data(int fd, int why, void* data) {
	tcp_bridge* self = (tcp_bridge*)data;
	self->on_data(why);
}
void tcp_bridge::on_data(int why) {
	if(why & IO_WATCH_EXCEPT) {
		log("%s: exception on fd %d. closing...", forward->forward->get_id().c_str(), fd);
		forward->destroy();
		return;
	}
	if(why & IO_WATCH_READ) {
		if(to_send.len) {
			log("%s: %s still have %d bytes to send! refusing to read!", 
			    forward->forward->get_id().c_str(), name.c_str(), to_send.len);
		} else {
			to_send.len = recv(fd, (char*)to_send.data, to_send.size, 0);
			if(to_send.len == -1) {
				log("%s: read from %s fd %d failed: %d, %s", 
				    forward->forward->get_id().c_str(), name.c_str(), fd, errno, strerror(errno));
				forward->destroy();
				return;
			}
			if(to_send.len == 0) {
				log("%s: EOF from %s fd %d", forward->forward->get_id().c_str(), name.c_str(), fd);
				forward->destroy();
				return;
			}
			debug("%s: %d bytes from %s", forward->forward->get_id().c_str(), to_send.len, name.c_str());
			
			// try to send to remote side
			int sent = send(target->fd, (char*)to_send.data, to_send.len, 0);
			if(sent > 0 && sent != to_send.len) {
				// short send
				to_send.len -= sent;
				debug("%s: had short send. %d bytes remaining from %s", forward->forward->get_id().c_str(), to_send.len, name.c_str());
				memmove(to_send.data, (uint8_t*)to_send.data + sent, to_send.len);
				// deal like block!
				sent = -1;
				errno = EAGAIN;
			}
			if(sent == -1) {
				if(errno != EAGAIN && errno != EWOULDBLOCK) {
					log("%s: send to fd %d failed: %d, %s", 
					    forward->forward->get_id().c_str(), target->fd, errno, strerror(errno));
					forward->destroy();
					return;
				}
				// would block!
				// remote client read watch, add remote write watch
				set_watch(current_watch & ~IO_WATCH_READ);
				target->set_watch(target->current_watch | IO_WATCH_WRITE);
				// send later...
				return;
			} else {
				// successfully transferred.
				to_send.len = 0;
			}
		}
	}
	if(why & IO_WATCH_WRITE) {
		// we can accept write now!
		debug("%s: send %d bytes to %s", forward->forward->get_id().c_str(), target->to_send.len, name.c_str());
		int sent = send(fd, (char*)target->to_send.data, target->to_send.len, 0);
		if(sent == -1) {
			if(errno != EAGAIN && errno != EWOULDBLOCK) {
				log("%s: send to fd %d failed 2: %d, %s", 
				    forward->forward->get_id().c_str(), fd, errno, strerror(errno));
				forward->destroy();
				return;
			}
			// would still block!?
			// send later...
			return;
		} else {
			if(sent != target->to_send.len)
				log("%s: %s: error! short send of %d instead of %d\n", 
				    forward->forward->get_id().c_str(), name.c_str(), sent, target->to_send.len);

			// successfully transferred.
			target->to_send.len = 0;
			// enable read watch on target and disable write watch on our side
			target->set_watch(target->current_watch | IO_WATCH_READ);
			set_watch(current_watch & ~IO_WATCH_WRITE);
		}			
	}
}

tcp_connection::tcp_connection(ln_tcp_forward* forward, int client_fd, int remote_fd, int listen_port, bool send_magic) 
	: forward(forward),
	  client(this, client_fd, "client"),
	  remote(this, remote_fd, "remote") {

	log("%p tcp_connection ctor with client %p and remote %p", this, &client, &remote);
	client.set_target(&remote);
	remote.set_target(&client);

	struct sockaddr_in remote_addr_in;
	os::socklen_t_type len = sizeof(remote_addr_in);
	
	if(forward->remote_type == "tcp") {
		getsockname(remote_fd, (struct sockaddr*)&remote_addr_in, &len);
		remote_port = ntohs(remote_addr_in.sin_port);
		
	} else if(forward->remote_type == "unix") {
		remote_port = 0; /* only used to check whether client connection was forwared...
				    but clients always connect via tcp, so this unix remote port is
				    not an option, set to an invalid port number */
	}

	getpeername(client_fd, (struct sockaddr*)&remote_addr_in, &len);
	client_ip = inet_ntoa(remote_addr_in.sin_addr);
	client_port = ntohs(remote_addr_in.sin_port);

	if(send_magic) {
		struct sockaddr_in local_addr_in;
		os::socklen_t_type len = sizeof(local_addr_in);
		std::string listen_ip;
		int listen_port2 = 0;
		if(getsockname(client_fd, (struct sockaddr*)&local_addr_in, &len) == 0) {
			listen_ip = inet_ntoa(local_addr_in.sin_addr);
			listen_port2 = ntohs(local_addr_in.sin_port);
			if(listen_port2 != listen_port)
				log("warning: expected listen_port to be %d, but getsockname() returned %d!\n", listen_port, listen_port2);
		}
		
		std::string target_ip;
		int target_port = 0;
		if(getpeername(remote_fd, (struct sockaddr*)&remote_addr_in, &len) == 0) {
			target_ip = inet_ntoa(remote_addr_in.sin_addr);
			target_port = ntohs(remote_addr_in.sin_port);
		}
		
		string magic_for_remote = format_string(
			"FWD_%s:%d:%s:%d:%s:%d_",
			client_ip.c_str(), client_port,
			listen_ip.c_str(), listen_port2,
			target_ip.c_str(), target_port);
		magic_for_remote[3] = 0;
		magic_for_remote[magic_for_remote.size() - 1] = 0;

		client.insert_received_data(magic_for_remote);
	}
}

void tcp_connection::destroy() {
	log("%p tcp_connection destroy with client %p and remote %p", this, &client, &remote);
	forward->connections.erase(this);
	forward->notify_manager();

	if(!forward->keep_listening)
		forward->destroy();
	log("%p tcp_connection destroy call detor", this);
	delete this;
}

void ln_tcp_forward::_init() {
	destroyed = false;
	last_n_connections = 0;
	last_n_connections_time = time(NULL);
	is_x11_forwarding = false;
}

ln_tcp_forward::ln_tcp_forward(ln_instance* instance, string to_ip, int to_port, string listen_ip, int listen_port, bool keep_listening, string data, bool send_source) 
	: instance(instance),
	  to_ip(to_ip), 
	  to_port(to_port),
	  other_target(""),
	  listen_ip(listen_ip), 
	  listen_port(listen_port),
	  listen_fd(-1),
	  keep_listening(keep_listening),
	  data(data),
	  send_source(send_source)
{

	log(__func__);
	_init();
	remote_type = "tcp";	
	memset(&remote_addr_in, 0, sizeof(remote_addr_in));
	remote_addr_in.sin_family = AF_INET;

#ifdef __WIN32__
	remote_addr_in.sin_addr.s_addr = inet_addr(to_ip.c_str());
	if(remote_addr_in.sin_addr.s_addr == INADDR_NONE)
#else	
	if(!inet_aton(to_ip.c_str(), &remote_addr_in.sin_addr))
#endif
		throw errno_exception_tb("invalid to_ip %s!", repr(to_ip).c_str());
	
	remote_addr_in.sin_port = htons(to_port);
	remote_addr = (struct sockaddr*)&remote_addr_in;
	remote_addr_size = sizeof(remote_addr_in);

	_create_listen_side();
}


ln_tcp_forward::ln_tcp_forward(ln_instance* instance, string other_target, string listen_ip, int listen_port, bool keep_listening, string data, bool send_source) 
	: instance(instance),
	  to_ip(""), 
	  other_target(other_target),
	  listen_ip(listen_ip), 
	  listen_port(listen_port),
	  listen_fd(-1),
	  keep_listening(keep_listening),
	  data(data),
	  send_source(send_source)
{

	log(__func__);
	_init();

#if !defined(__WIN32__)
	if(other_target.substr(0, 5) == "unix:") {
		remote_type = "unix";
		memset(&remote_addr_un, 0, sizeof(remote_addr_un));
		remote_addr_un.sun_family = AF_UNIX;
		string path = other_target.substr(5);
		memcpy(remote_addr_un.sun_path, path.c_str(), path.size());

		remote_addr = (struct sockaddr*)&remote_addr_un;
		remote_addr_size = sizeof(remote_addr_un);
		
	} else
#endif
		throw errno_exception_tb("invalid/unknown target %s!", repr(other_target).c_str());
		
	_create_listen_side();
}

void ln_tcp_forward::_create_listen_side() {
	listen_fd = socket(AF_INET, SOCK_STREAM, 0);
	log("%p ln_tcp_forward got new listen fd %d", this, listen_fd);
	if(listen_fd == -1)
		throw errno_exception_tb("could not create tcp_forward listening socket!");

	struct sockaddr_in local_addr;
	os::socklen_t_type len = sizeof(local_addr);
	memset(&local_addr, 0, sizeof(local_addr));
	local_addr.sin_family = AF_INET;
	if(listen_ip.size()) {
#ifdef __WIN32__
		local_addr.sin_addr.s_addr = inet_addr(listen_ip.c_str());
		if(local_addr.sin_addr.s_addr == INADDR_NONE)
#else	
		if(!inet_aton(listen_ip.c_str(), &local_addr.sin_addr))
#endif
		{
			throw errno_exception_tb("invalid listen_ip %s!", repr(listen_ip).c_str());
		}
	} else
		local_addr.sin_addr.s_addr = INADDR_ANY;

	if(listen_port == -2) {
		// find free x-display forwarding port starting from 6010!
		is_x11_forwarding = true;
		listen_port = 6010;
		while(true) {
			local_addr.sin_port = htons(listen_port);
			if(!bind(listen_fd, (struct sockaddr*)&local_addr, sizeof(local_addr)))
				break;
			listen_port += 1;
		}
	} else {

#ifdef __VXWORKS__
		/*
		  did not work on sigyn:
		  setsockopt(SO_REUSEADDR): invalid argument (errno 22)
		  char on = 1;
		  if(setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
		  throw errno_exception("setsockopt(SO_REUSEADDR)");
		*/
#elif __WIN32__
		char on = 1;
		if(setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
			throw errno_exception_tb("setsockopt(SO_REUSEADDR)");
#else
		int on = 1;
		if(setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)
			throw errno_exception_tb("setsockopt(SO_REUSEADDR)");
#endif
		
		if(listen_port != -1)
			local_addr.sin_port = htons(listen_port);
		
		if(bind(listen_fd, (struct sockaddr*)&local_addr, sizeof(local_addr)))
			throw errno_exception_tb("could not bind tcp_forwarder port!");
	}

	int backlog = 1;
	if(keep_listening)
		backlog = 5;
	if(listen(listen_fd, backlog))
		throw errno_exception_tb("could not listen on tcp_forwarder socket!");

	if(getsockname(listen_fd, (struct sockaddr*)&local_addr, &len))
		throw errno_exception_tb("could not get tcp_forwarder socket name!");
	this->listen_port = ntohs(local_addr.sin_port);

	instance->daemon->add_io_watch(
		listen_fd, IO_WATCH_READ | IO_WATCH_EXCEPT, _on_connection, this, 
		format_string("%s server", get_id().c_str()));

	state = TCP_FORWARD_LISTENING;

	instance->register_tcp_forward(this);
	log("%p ln_tcp_forward listening on fd %d %s", this, listen_fd, get_id().c_str());
}

string ln_tcp_forward::get_id() {
	return format_string("<tcp_forward to %s:%d from %s:%d>", 
			     to_ip.c_str(),
			     to_port,
			     listen_ip.c_str(),
			     listen_port);
}

void ln_tcp_forward::destroy(bool do_delete) {
	log("%p ln_tcp_forward destroy: do_delete: %d", this, do_delete);
	if(destroyed) {
		instance->delete_tcp_forward(this, do_delete);
		return;
	}
	destroyed = true;
	if(listen_fd != -1) {
		// printf("remove io watch\n");
		instance->daemon->remove_io_watch(listen_fd);
		// printf("close listen fd\n");
		_ln_close_socket(listen_fd);
		listen_fd = -1;
	}
	// printf("iterate connection\n");
	for(connections_t::iterator i = connections.begin(); i != connections.end(); i++) {
		// printf("delete conneection %p\n", *i);
		delete *i;
	}
	connections.clear();
	log("delete tcp forward %s, id: %s, ptr %#x\n", repr(data).c_str(), get_id().c_str(), this);
	notify_manager();
	instance->delete_tcp_forward(this, do_delete);
	//printf("done\n");
	log("%p ln_tcp_forward destroy done", this);
}

ln_tcp_forward::~ln_tcp_forward() {
	log("%p ln_tcp_forward detor!", this);
	debug("ptr %#x\n", this);
	destroy(false);
}

void ln_tcp_forward::_on_connection(int fd, int why, void* data) {
	ln_tcp_forward* self = (ln_tcp_forward*)data;

	if(!(why & IO_WATCH_READ))
		return;

	self->on_connection();
}
void ln_tcp_forward::on_connection() {
	log("new incoming connection on %s ptr %#x\n", get_id().c_str(), this);
	
	struct sockaddr_in client_addr;
	os::socklen_t_type len = sizeof(client_addr);
	int client_fd = accept(listen_fd, (struct sockaddr*)&client_addr, &len);
	if(client_fd == -1) {
		log("error accepting client for %s: %s\n", get_id().c_str(), strerror(errno));
		return;
	}

	if(!keep_listening) {
		instance->daemon->remove_io_watch(listen_fd);
		log("%p ln_tcp_forward closing listen fd %d %s", this, listen_fd, get_id().c_str());
		_ln_close_socket(listen_fd);
		listen_fd = -1;
	}

	string client_ip = inet_ntoa(client_addr.sin_addr);

	log("%s: got client connection on fd %d from %s:%p. connecting to %s:%d...", 
	    get_id().c_str(), client_fd, client_ip.c_str(), ntohs(client_addr.sin_port), to_ip.c_str(), to_port);

	int flag = 1;
	setsockopt(client_fd, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(int));

	int remote_fd;
	if(remote_type == "tcp")
		remote_fd = socket(AF_INET, SOCK_STREAM, 0);
	else
		remote_fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(remote_fd == -1)
		throw errno_exception_tb("could not create tcp_forward remote %s socket!", remote_type.c_str());

	if(connect(remote_fd, remote_addr, remote_addr_size)) {
		log("%s: connection to remote fd %d failed: %s", 
		    get_id().c_str(), remote_fd, strerror(errno));
		_ln_close_socket(client_fd);
		_ln_close_socket(remote_fd);
		if(!keep_listening)
			destroy();
		return;
	}
	state = TCP_FORWARD_CONNECTED;
	log("%s: connected to remote at fd %d", get_id().c_str(), remote_fd);
	if(remote_type == "tcp") {
		flag = 1;
		setsockopt(remote_fd, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(int));
	}
	
	connections.insert(new tcp_connection(this, client_fd, remote_fd, listen_port, send_source));
	notify_manager();
}

bool ln_tcp_forward::is_source_port(int port, string& from_ip, int& from_port) {
	for(connections_t::iterator i = connections.begin(); i != connections.end(); i++) {
		tcp_connection* c = *i;
		if(c->remote_port == port) {
			from_ip = c->client_ip;
			from_port = c->client_port;
			return true;
		}
	}
	return false;
}

py_tuple* ln_tcp_forward::get_tuple(unsigned int protocol) {
	py_tuple* t = new py_tuple();

	if(remote_type == "tcp") {
		t->value.push_back(new py_string(to_ip));
		t->value.push_back(new py_int(to_port));
	} else {
		t->value.push_back(new py_string(""));
		t->value.push_back(new py_string(other_target));

	}
	t->value.push_back(new py_string(listen_ip));
	t->value.push_back(new py_int(listen_port));
	t->value.push_back(new py_int(keep_listening));
	t->value.push_back(new py_string(data));

	if(protocol > 23) {
		t->value.push_back(new py_int(connections.size()));
		t->value.push_back(new py_int((int)(time(NULL) - last_n_connections_time)));
	}
	
	return t;
}

void ln_tcp_forward::notify_manager() {
	// send len(self.connections)
	// send if in destroyed

	if(!instance->connection)
		return; // no manager connected

	if(connections.size() == last_n_connections && !destroyed)
		return;
	
	last_n_connections = connections.size();
	last_n_connections_time = time(NULL);

	request_t req;
	req["request"] = repr("tcp_forward_change");
	req["forward"] = repr(get_tuple(24));
	req["forward_id"] = repr(get_id());
	req["is_in_destroy"] = repr(destroyed);

	try {
		instance->connection->send(req);
	}
	catch(const exception& e) {
		log("%p ln_tcp_forward %s: ignore exception when trying to notify manager:\n%s", this, get_id().c_str(), e.what());
	}
}
