#ifndef OS_WIN32_H
#define OS_WIN32_H

#ifdef UNICODE
#error ln_daemon should be compiled without UNICODE defined!
#endif

// #define WINVER 0x0400 // Win95 / WinNT
// #define WINVER 0x0501 // since Win2k
#define WINVER 0x0600 // since WinXp

#include <winsock2.h>
#include <windows.h>
#include <iostream>

#include <ws2tcpip.h>
#include <mswsock.h>

#include <stdint.h>

#ifdef I586_MINGW32MSVC
#include <unistd.h>
typedef int pid_t;
#else
#include <sys/types.h>
#ifdef MSVC
typedef int pid_t; // needed for msvc9
#endif
#endif

typedef struct ln_pipe {
	HANDLE input_fd;
	HANDLE output_fd;
} ln_pipe_t;
#define ln_pipe_invalid NULL

#define thread_cleanup_push(fcn, ptr)
#define thread_cleanup_pop(exec)

// semaphore
typedef HANDLE ln_semaphore_handle_t;

namespace os {

typedef int socklen_t_type; // todo?

	/*
	  use those from ln-c-api!
int ln_semaphore_create(ln_semaphore_handle_t* handle, const char* name, int create);
int ln_semaphore_close(ln_semaphore_handle_t handle);
int ln_semaphore_wait(ln_semaphore_handle_t handle);
int ln_semaphore_trywait(ln_semaphore_handle_t handle);
int ln_semaphore_timedwait(ln_semaphore_handle_t handle, double seconds);
int ln_semaphore_post(ln_semaphore_handle_t handle);
	*/

// mutex
typedef CRITICAL_SECTION* mutex_handle_t;
int create_mutex(mutex_handle_t* mutex_handle);
int destroy_mutex(mutex_handle_t* mutex_handle);
int lock_mutex(mutex_handle_t* mutex_handle);
int trylock_mutex(mutex_handle_t* mutex_handle);
int unlock_mutex(mutex_handle_t* mutex_handle);

// condition
typedef struct {
	mutex_handle_t mutex;
	unsigned int n_blocked_threads;
	ln_semaphore_handle_t sem;
	unsigned int in_destruct;
} condition_handle_t;
void condition_create(condition_handle_t* handle);
void condition_destroy(condition_handle_t* handle);
void condition_wait(condition_handle_t* cond, mutex_handle_t* mutex);
void condition_signal(condition_handle_t* handle);
void condition_broadcast(condition_handle_t* handle);

// thread
struct win32_thread_handle_and_cancel_flag {
	HANDLE thread;
	int cancel_requested;
};
typedef struct win32_thread_handle_and_cancel_flag* thread_handle_t;
#define thread_return_t DWORD
#ifdef MSVC
#define thread_func_type LPTHREAD_START_ROUTINE

#else
typedef thread_return_t WINAPI (*thread_func_type)(void*);
#endif

int create_thread(thread_handle_t* thread_handle, thread_func_type thread, void* data);
void test_cancel(thread_handle_t);
void cancel_thread(thread_handle_t t);
int join_thread(thread_handle_t t);

void sleep(double s);

//! kill process with given pid
/*!
  \param pid process pid to kill
  \return success or error code
  */
int kill(pid_t pid);

//! send signal to process with given pid
/*!
  \param pid process pid to signal
  \parma signo signal number to send
  \return success or error code
  */
int send_signal(pid_t pid, int signo);

//! initialize os dependant pipe device
/*!
  \param reference to pipe device structure
  \return success or error code
 */
int pipe_init(ln_pipe_t& pipe);

//! closes os dependant pipe device
/*!
 \param reference to pipe device structure
 \return success or error code
 */
int pipe_close(ln_pipe_t& pipe);

//! return temp dir as std::string
/*
  \return temp dir
 */
std::string get_temp_dir();

int set_prio(pid_t pid, int tid, int prio, int policy);
int set_affinity(pid_t pid, int tid, int affinity);
pid_t gettid();

}
#ifdef MSVC
#include <process.h>
#define getpid _getpid
#endif

#define THREAD_CALL WINAPI
char *strndup(const char *s, size_t n);
#define SHUT_RDWR SD_BOTH

// sendmsg support
#define msghdr _WSAMSG 
#define msg_name name
#define msg_namelen namelen
#define msg_iov lpBuffers
#define msg_iovlen dwBufferCount
#define msg_control Control
#define msg_flags dwFlags

#define iovec _WSABUF
#define iov_base buf
#define iov_len len
typedef char* iov_base_t;
#define __WIN32__DGRAM_SEND 1

#if defined(I586_MINGW32MSVC) || !defined(ETIMEDOUT)
#define ETIMEDOUT EAGAIN
#define EWOULDBLOCK EAGAIN
#endif
#define MSG_DONTWAIT 0 // not avaliable on windows

#define random() rand()
#define srandom(a) srand(a)
int inet_aton(const char *cp, struct in_addr *inp);

#ifdef MSVC
#define __func__ __FUNCTION__
int string_util_vsnprintf(char* str, size_t size, const char* format, va_list ap);
int string_util_snprintf(char* str, size_t size, const char* format, ...);

#define snprintf string_util_snprintf
#define vsnprintf string_util_vsnprintf
#endif

#ifdef MSVC
typedef int ssize_t;
#endif

// in library/os_win32
extern "C" ssize_t sendmsg(int socket, const struct msghdr *msg, int flags);

#endif // OS_WIN32_H
