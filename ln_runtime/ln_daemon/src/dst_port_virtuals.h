virtual py_tuple* get_tuple(py_tuple* t=NULL, int with_last_packet=0) {
	return ln_sink_port::get_tuple(t, with_last_packet);
}

virtual void get_state(request_t& answer) {
	ln_sink_port::get_state(answer);
}
