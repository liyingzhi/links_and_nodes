#include <string>
#include <sstream>
#include <stdint.h>

using namespace std;

class telnet_connection {
	int fd;
	
	uint8_t iacseq[16];
	unsigned int iaclen;
	
	void sendall(const char* data, uint32_t len);
	string process_raw(stringstream& ret, const char* data, unsigned int data_len);

public:
       
	stringstream output;
	telnet_connection(string host, int port=23);
	~telnet_connection();

	string read(double timeout=0.5, bool once=false);
	void write(string data);
	bool is_prompt(string data);
	string wait_prompt();

};
