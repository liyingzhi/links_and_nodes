import os
import sys
from conans import tools, ConanFile, AutoToolsBuildEnvironment

class links_and_nodes_ln_msgdef_conan(ConanFile):
    name = "links_and_nodes_ln_msgdef"
    url = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    author = "Florian Schmidt florian.schmidt@dlr.de"
    license = "GPLv3"
    description = "links_and_nodes standard set of message definitions"
    settings = None
    exports_sources = [
        # all of python for scons to be able to build python
        "message_definitions/*",
        "../ln_runtime/file_services/message_definitions/*",
        "../ln_runtime/lnrecorder/message_definitions/*",
    ] + ["!%s" % x for x in tools.Git().excluded_files()]
    
    def package(self):
        self.copy("message_definitions*")
        self.copy("ln/*", "message_definitions/")
        self.copy("lnrecorder/*", "message_definitions/")
        
    def package_info(self):
        self.env_info.LN_MESSAGE_DEFINITION_DIRS.append(os.path.join(self.package_folder, "message_definitions"))
