#!/bin/bash

pushd `dirname ${BASH_SOURCE[0]}` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

export LN_BASE=$SCRIPTPATH/..

set -e

export SPHINXBUILDFLAGS=-a
(cd $LN_BASE; scons --from-env --subdirs=documentation)

TMP=$(mktemp -d)
echo temp dir: $TMP
git clone -b gh-pages git@rmc-github.robotic.dlr.de:common/links_and_nodes.git $TMP
rsync -ravI $LN_BASE/build/documentation/html/ $TMP/
(
    cd $TMP/
    git add --all
    git commit -m "doc update"
    git push
)
rm -rf $TMP
