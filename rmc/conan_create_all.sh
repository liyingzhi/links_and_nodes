#!/bin/bash

set -e

if [ -z "$1" ]; then
    ver=2.0.0
else
    ver=$1
    shift
fi

if [ -z "$1" ]; then
    pyver=3
else
    pyver=$1
    shift
fi

if [ -z "$1" ]; then
    channel=common/snapshot
else
    channel=$1
    shift
fi

function doit() {
    folder=$1; shift
    name=$1; shift
    echo conan create $folder $name/$ver@$channel -b missing $@
    conan create $folder $name/$ver@$channel -b missing $@
}
    
doit libln                          liblinks_and_nodes
doit ln_runtime                     links_and_nodes_runtime
doit share                          links_and_nodes_ln_msgdef
doit python/links_and_nodes_base    links_and_nodes_base_python  -o python_version=$pyver
doit python/links_and_nodes         links_and_nodes_python       -o python_version=$pyver
doit python/links_and_nodes_manager links_and_nodes_manager      -o python_version=$pyver
doit python/lnrdb                   lnrdb_python                 -o python_version=$pyver

echo "you can now test this ln_manager with this command:"
echo "  conan test tests links_and_nodes_manager/$ver@common/snapshot -o links_and_nodes_manager:python_version=$pyver"

