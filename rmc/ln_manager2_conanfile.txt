# conanfile to be able to run ln_manager from source dir

[requires]
## for python extension:
boost_python/1.66.0@3rdparty/stable

## for python programs (ln_manager, ln_generate) we need:
python2-dist/[~1.3]@tools/stable

[options]
boost_python:python_version=2

[generators]
virtualenv
virtualrunenv
virtualbuildenv

