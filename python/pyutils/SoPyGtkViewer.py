"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

from __future__ import print_function

import math
import time
import traceback

import gobject # todo: transition to Gtk3 and some working OpenGL widget!
import gtk

import gtk.gtkgl
from OpenGL.GL import *
from pivy.coin import *
import pyutils
import pyutils.fps_counter
import pyutils.inventor

for i in range(5):
    globals()["BUTTON%d" % (i+1)] = 2**i

import pivy
is_newer_pivy = hasattr(pivy, "__version__") and tuple(map(int, pivy.__version__.split("."))) >= (0, 6, 5)

class SoPyGtkViewer(gtk.DrawingArea, gtk.gtkgl.Widget):
    def __init__(self):
        self.before_seek_mode = "IDLE"
        self.get_depth_buffer = False
        self.additional_render_callback = None
        self.depth_buffer_type = GL_UNSIGNED_BYTE
        self.special_motion_handler = None
        self.notify_keypress = None
        self.notify_popup = None
        self.on_interact_button = None
        self.clear_color = None
        pygtk_register_sensor_manager()
        # init gtk base widget
        gtk.DrawingArea.__init__(self)  # Calls gtk.DrawingArea initializer
        self.cross_cursor = gtk.gdk.Cursor(gtk.gdk.CROSSHAIR)
        self.hand_cursor = gtk.gdk.Cursor(gtk.gdk.HAND2)
        self.pointer_cursor = gtk.gdk.Cursor(gtk.gdk.LEFT_PTR)
        # init gtkglext widget
        display_mode = (gtk.gdkgl.MODE_RGB | gtk.gdkgl.MODE_DEPTH| gtk.gdkgl.MODE_DOUBLE)
        try:                              
            glconfig = gtk.gdkgl.Config(mode=display_mode)
        except gtk.gdkgl.NoMatches:
            display_mode &= ~gtk.gdkgl.MODE_SINGLE
            glconfig = gtk.gdkgl.Config(mode=display_mode) 
        self.set_gl_capability(glconfig)
        # init event handlers
        self.connect("expose_event", self.on_expose_event )
        self.connect("configure_event", self.on_configure)
        self.connect("realize", self.on_realize)
        self.connect("button-press-event", self.on_button)
        self.connect("button-release-event", self.on_button)
        self.connect("scroll-event", self.on_scroll)
        self.connect("motion-notify-event", self.on_motion)
        self.connect("key-press-event", self.on_keypress, "press")
        self.connect("key-release-event", self.on_keypress, "release")
        self.add_events(gtk.gdk.BUTTON_PRESS_MASK | gtk.gdk.BUTTON_RELEASE_MASK |
                        gtk.gdk.BUTTON_MOTION_MASK | gtk.gdk.KEY_PRESS_MASK |
                        gtk.gdk.KEY_RELEASE_MASK |  gtk.gdk.SCROLL_MASK)
        # initialize scenemanager instance
        self.manager = SoSceneManager()
        self.manager.activate()
        self.manager.setRenderCallback(self.render_callback, None)
        self.camera = None
        # actions
        self.searchaction = SoSearchAction()
        self.matrixaction = SoGetMatrixAction(SbViewportRegion(100,100))
        # clipping
        self.autoclipvalue = 0.6
        # init dragging
        self.buttons = 0
        self.log_size = 16
        self.log = []
        self.spinprojector = SbSphereSheetProjector(SbSphere(SbVec3f(0, 0, 0), 0.8))
        volume = SbViewVolume()
        volume.ortho(-1, 1, -1, 1, -1, 1)
        self.spinprojector.setViewVolume(volume)
        self.currentmode = "IDLE"
        self.set_property("can-focus", True)
        # hack
        self.drop_events = False
        self.motion_fps = pyutils.fps_counter.fps_counter()
        self.skip_event = 3

    def render_callback(self, userData, scene_manager):
        self.redraw_canvas()

    def setCamera(self, cam):
        self.camera = cam
        
    def getCamera(self):
        return self.camera
        
    def setSceneGraph(self, root):
        # search camera
        action = SoSearchAction()
        action.setType(SoCamera.getClassTypeId())
        action.apply(root)
        p = action.getPath()
        if not p:
            # add camera
            new_root = SoSeparator()
            new_root.ref()
            new_root.addChild(SoDirectionalLight())
            self.camera = SoPerspectiveCamera()
            new_root.addChild(self.camera)
            new_root.addChild(root)
            root = new_root
        else:
            self.camera = p.getTail()
            #print("found camera:", self.camera.getTypeId().getName())
        self.manager.setSceneGraph(root)
        self.sceneroot = root
        self.camera.viewAll(root, self.manager.getViewportRegion())

    def viewAll(self):
        self.camera.viewAll(self.sceneroot, self.manager.getViewportRegion())

    def getSceneGraph(self):
        return self.sceneroot

    def getSceneManager(self):
        return self.manager

    def on_configure(self, viewer, event):
        alloc = self.get_allocation()
        self.width = alloc.width
        self.height = alloc.height
        self.manager.setWindowSize(SbVec2s(self.width, self.height))
        self.window.set_cursor(self.hand_cursor)

    def on_realize(self, *args):
        pass
        
    def on_expose_event(self, da, event):
        #print("expose", event.area.x, event.area.y, event.area.width, event.area.height)
        gldrawable = self.get_gl_drawable()
        glcontext = self.get_gl_context()
        gldrawable.gl_begin(glcontext)
        glEnable(GL_DEPTH_TEST)
        if self.clear_color is not None:
            glClearColor(*self.clear_color)
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # auto clipping
        self.setClippingPlanes()
        # real rendering
        self.manager.render()

        if self.get_depth_buffer:
            self.last_depth_buffer = glReadPixels(0, 0, self.width, self.height, GL_DEPTH_COMPONENT, self.depth_buffer_type)

        if self.additional_render_callback:
            try:
                self.additional_render_callback(self)
            except:
                print("additional_render_callback threw exception:\n%s" % traceback.format_exc())
                self.additional_render_callback = None

        gldrawable.swap_buffers()
        gldrawable.gl_end()
        return False

    def redraw_canvas(self):
        if not self.window:
            return
        self.queue_draw_area(0, 0, self.width, self.height)
        #self.window.process_updates(True)

    def set_mode(self, mode):
        self.currentmode = mode
        if mode == "DRAGGING":
            self.log = [] # release log
            self.window.set_cursor(self.hand_cursor)
        elif mode == "IDLE":
            self.window.set_cursor(self.hand_cursor)
        elif mode == "SEEK_WAIT_MODE":
            self.window.set_cursor(self.cross_cursor)
        elif mode == "INTERACT":
            self.window.set_cursor(self.pointer_cursor)

    def on_button(self, da, event):
        self.grab_focus()
        ry = self.height - event.y
        self.last_normalized_position = SbVec2f(
            event.x / float(max(self.width - 1, 1)),
            ry / float(max(self.height - 1, 1)))
        if event.type == gtk.gdk.BUTTON_PRESS and self.buttons == 0 and event.button == 3 and self.notify_popup:
            return self.notify_popup(event)
            
        if event.type == gtk.gdk.BUTTON_PRESS:
            self.buttons |= 2**(event.button-1)
        else:
            self.buttons ^= 2**(event.button-1)


        if self.currentmode == "SEEK_WAIT_MODE":
            if event.type == gtk.gdk.BUTTON_PRESS:
                self.seekToPoint(event.x, ry)
        elif self.currentmode == "INTERACT":
            if self.on_interact_button is not None:
                if self.on_interact_button(event):
                    return
            # send event down to scene graph
            ev = SoMouseButtonEvent()
            ev.setButton(SoMouseButtonEvent.BUTTON1 + event.button - 1)
            if event.type == gtk.gdk.BUTTON_PRESS:
                ev.setState(SoButtonEvent.DOWN)
            if event.type == gtk.gdk.BUTTON_RELEASE:
                ev.setState(SoButtonEvent.UP)
            ev.setPosition(SbVec2s(int(event.x), int(ry)))
            t = SbTime()
            t.setMsecValue(event.get_time())
            ev.setTime(t)           
            s = event.get_state()
            ev.setShiftDown(s & gtk.gdk.SHIFT_MASK)
            ev.setCtrlDown(s & gtk.gdk.CONTROL_MASK)
            ev.setAltDown(s & gtk.gdk.MOD1_MASK)
            # print("interact button")
            self.manager.setViewportRegion(SbViewportRegion(self.width, self.height));
            self.manager.processEvent(ev)           
        else:
            if self.buttons & BUTTON1 and self.buttons & BUTTON2:
                self.set_mode("ZOOMING")
            elif self.buttons & BUTTON1:
                self.set_mode("DRAGGING")
            elif self.buttons & BUTTON2:
                self.set_mode("PANNING")
            else:
                self.set_mode("IDLE")
        return
        
    def on_scroll(self, da, event):
        ry = self.height - event.y
        if self.currentmode == "INTERACT":
            # send event down to scene graph
            # does not work,
            # we manager.processEvent does not pass throu buttons 4 and 5
            # and we have no real release event
            ev = SoMouseButtonEvent()
            if event.direction == gtk.gdk.SCROLL_UP:
                ev.setButton(SoMouseButtonEvent.BUTTON4)
            else:
                ev.setButton(SoMouseButtonEvent.BUTTON5)
            if event.type == gtk.gdk.BUTTON_PRESS:
                ev.setState(SoButtonEvent.DOWN)
            ev.setPosition(SbVec2s(int(event.x), int(ry)))
            t = SbTime()
            t.setMsecValue(event.get_time())
            ev.setTime(t)           
            s = event.get_state()
            ev.setShiftDown(s & gtk.gdk.SHIFT_MASK)
            ev.setCtrlDown(s & gtk.gdk.CONTROL_MASK)
            ev.setAltDown(s & gtk.gdk.MOD1_MASK)
            self.manager.processEvent(ev)           
            return
        if event.direction == gtk.gdk.SCROLL_UP:
            self.zoom(0.1) # SoGuiFullViewerP::zoom(this->getCamera(), 0.1f);
        if event.direction == gtk.gdk.SCROLL_DOWN:
            self.zoom(-0.1) # SoGuiFullViewerP::zoom(this->getCamera(), -0.1f);
        
    def on_motion(self, da, event):
        if self.drop_events:
            return
        ry = self.height - event.y
        posn = SbVec2f(event.x / float(max(self.width - 1, 1)),
                       ry / float(max(self.height - 1, 1)))
        prevnormalized = self.last_normalized_position
        self.last_normalized_position = posn
        
        if self.currentmode == "INTERACT":
            # send event down to scene graph
            #if self.motion_fps() > 5 and self.skip_event and False:
            #    print("drop event")
            #    self.skip_event -= 1
            #    return
            self.skip_event = 10
            ev = SoLocation2Event()
            #ev.setPosition(SbVec2s(int(event.x), int(ry)))
            ev.setPosition(SbVec2s(int(event.x), int(ry)))
            t = SbTime(time.time())
            #t.setMsecValue(event.get_time())
            #now = time.time()
            #t.setMsecValue(int(1000 * (now - int(now))))
            ev.setTime(t)
            s = event.get_state()
            ev.setShiftDown(s & gtk.gdk.SHIFT_MASK)
            ev.setCtrlDown(s & gtk.gdk.CONTROL_MASK)
            ev.setAltDown(s & gtk.gdk.MOD1_MASK)
            #print("interact motion")
            self.manager.setViewportRegion(SbViewportRegion(self.width, self.height));
            if self.special_motion_handler:
                self.special_motion_handler(ev)
            else:
                self.manager.processEvent(ev)

        elif self.currentmode == "DRAGGING":
            self.addToLog((event.x, ry), event.get_time())
            self.spin(posn)
        elif self.currentmode == "PANNING":
            aspect = self.width / float(self.height)
            cam = self.camera
            vv = cam.getViewVolume(aspect)
            panningplane = vv.getPlane(cam.focalDistance.getValue())
            self.pan(aspect, panningplane, posn, prevnormalized)
        elif self.currentmode == "ZOOMING":
            self.zoomByCursor(posn, prevnormalized)
        
    def on_keypress(self, viewer, event, event_type):
        press = event_type == "press"
        if press and event.keyval == gtk.keysyms.Escape and self.currentmode == "INTERACT":
            self.set_mode("IDLE")
        elif press and event.keyval == gtk.keysyms.Escape and self.currentmode != "INTERACT":
            self.set_mode("INTERACT")
        elif press and event.string == "s" and self.currentmode != "SEEK_WAIT_MODE":
            self.before_seek_mode = self.currentmode
            self.set_mode("SEEK_WAIT_MODE")
        elif press and event.string == "s" and self.currentmode == "SEEK_WAIT_MODE":
            self.set_mode(self.before_seek_mode)
        elif press and self.currentmode == "INTERACT":
            # send event down to scene graph
            ev = SoKeyboardEvent()
            ev.setState(press)
            t = SbTime()
            t.setMsecValue(event.get_time())
            ev.setTime(t)
            s = event.get_state()
            ev.setShiftDown(s & gtk.gdk.SHIFT_MASK)
            ev.setCtrlDown(s & gtk.gdk.CONTROL_MASK)
            ev.setAltDown(s & gtk.gdk.MOD1_MASK)
            ev.setKey(self._map_key(event))
            ev.setPrintableCharacter(event.string)
            self.manager.processEvent(ev)           
        elif press and self.notify_keypress is not None:
            self.notify_keypress(event.string)

    def _map_key(self, ev):
        gk = ev.keyval
        if gk >= gtk.keysyms.A and gk <=gtk.keysyms.Z:
            ck = gk + 0x020
        else:
            ck = gk
        return ck
            
    def seekToPoint(self, x, y):
        """
        Call this method to initiate a seek action towards the 3D
        intersection of the scene and the ray from the screen coordinate's
        point and in the same direction as the camera is pointing.
        
        Returns \c TRUE if the ray from the \a screenpos position intersect
        with any parts of the onscreen geometry, otherwise \c FALSE.
        """
        if self.camera is None: return False

        rpaction = SoRayPickAction(self.getViewportRegion())
        screenpos = SbVec2s(int(x), int(y))
        rpaction.setPoint(screenpos)
        rpaction.setRadius(2)
        rpaction.apply(self.sceneroot)
        picked = rpaction.getPickedPoint()
        if not picked: return False
        
        hitpoint = picked.getPoint()

        # move point to the camera coordinate system, consider
        # transformations before camera in the scene graph
        cameramatrix, camerainverse = self.getCameraCoordinateSystem(self.camera, self.sceneroot)
        hitpoint = camerainverse.multVecMatrix(hitpoint)

        fd = 0.5 * (hitpoint - self.camera.position.getValue()).length()
        self.camera.focalDistance = fd
        newdir = hitpoint - self.camera.position.getValue()
        newdir.normalize();
        # find a rotation that rotates current camera direction into new
        # camera direction.
        olddir = self.camera.orientation.getValue().multVec(SbVec3f(0, 0, -1))
        
        diffrot = SbRotation(olddir, newdir)        
        self.camera.position = hitpoint - fd * newdir
        self.camera.orientation = self.camera.orientation.getValue() * diffrot

        self.set_mode(self.before_seek_mode)
        return True

    def getViewportRegion(self):
        return self.manager.getGLRenderAction().getViewportRegion()

    def getCameraCoordinateSystem(self, camera, root):
        """
        Returns the coordinate system the current camera is located in. If
        there are transformations before the camera in the scene graph,
        this must be considered before doing certain operations. \a matrix
        and \a inverse will not contain the transformations caused by the
        camera fields, only the transformations traversed before the camera
        in the scene graph.
        """
        self.searchaction.reset()
        self.searchaction.setSearchingAll(TRUE)
        self.searchaction.setInterest(SoSearchAction.FIRST)
        self.searchaction.setNode(camera)
        self.searchaction.apply(root)
        p = self.searchaction.getPath()
        if p:
            self.matrixaction.apply(p)
            matrix = self.matrixaction.getMatrix()
            inverse = self.matrixaction.getInverse()
        else:
            matrix = SbMatrix.identity()
            inverse = SbMatrix.identity()
        self.searchaction.reset()
        return matrix, inverse
    
    def setClippingPlanes(self):
        """
        Position the near and far clipping planes just in front of and
        behind the scene's bounding box. This will give us the optimal
        utilization of the z buffer resolution by shrinking it to its
        minimum depth.
        Near and far clipping planes are specified in the camera fields
        nearDistance and farDistance.
        """
        # This is necessary to avoid a crash in case there is no scene
        # graph specified by the user.
        if self.camera is None: return
        if not hasattr(self, "autoclipbboxaction"):
            self.autoclipbboxaction = SoGetBoundingBoxAction(self.getViewportRegion())
        else:
            self.autoclipbboxaction.setViewportRegion(self.getViewportRegion())
        self.autoclipbboxaction.apply(self.sceneroot)
        # rotate bbox in camera coordinate system
        xbox = self.autoclipbboxaction.getXfBoundingBox()
        mat, inverse = self.getCameraCoordinateSystem(self.camera, self.sceneroot)
        xbox.transform(inverse)
        mat.setTranslate(-self.camera.position.getValue())
        xbox.transform(mat)
        mat.setRotate(self.camera.orientation.getValue().inverse())
        xbox.transform(mat)
        box = xbox.project()
        # Bounding box was calculated in camera space, so we need to "flip"
        # the box (because camera is pointing in the (0,0,-1) direction
        # from origo.
        nearval = -box.getMax()[2]
        farval = -box.getMin()[2]
        # Check if scene is completely behind us.
        if farval <= 0.0:
            return
        if self.camera.isOfType(SoPerspectiveCamera.getClassTypeId()):
            # Disallow negative and small near clipping plane distance.
            # From glFrustum() documentation: Depth-buffer precision is
            # affected by the values specified for znear and zfar. The
            # greater the ratio of zfar to znear is, the less effective the
            # depth buffer will be at distinguishing between surfaces that
            # are near each other. If r = far/near, roughly log (2) r bits
            # of depth buffer precision are lost. Because r approaches
            # infinity as znear approaches zero, you should never set znear
            # to zero.
            depthbits = glGetIntegerv(GL_DEPTH_BITS)
            use_bits = int(float(depthbits) * (1.0 - self.autoclipvalue))
            r = 2.0 ** use_bits
            nearlimit = farval / r
            if nearlimit >= farval:
                # (The "5000" magic constant was found by fiddling around a bit
                # on an OpenGL implementation with a 16-bit depth-buffer
                # resolution, adjusting to find something that would work well
                # with both a very "stretched" / deep scene and a more compact
                # single-model one.)
                nearlimit = farval / 5000.0
            # adjust the near plane if the the value is too small.
            if nearval < nearlimit:
                nearval = nearlimit
            # Some slack around the bounding box, in case the scene fits
            # exactly inside it. This is done to minimize the chance of
            # artifacts caused by the limitation of the z-buffer
            # resolution. One common artifact if this is not done is that the
            # near clipping plane cuts into the corners of the model as it's
            # rotated.
            SLACK = 0.001
            # FrustumCamera can be found in the SmallChange CVS module. We
            # should not change the nearDistance for this camera, as this will
            # modify the frustum.
            if self.camera.getTypeId().getName() == "FrustumCamera":
                nearval = self.camera.nearDistance.getValue()
                farval *= (1.0 + SLACK)
                if farval <= nearval:
                    # nothing is visible, so just set farval to som value > nearval.
                    farval = nearval + 10.0
            else:
                nearval *= 1.0 - SLACK
                farval *= 1.0 + SLACK
        if nearval != self.camera.nearDistance.getValue():
            self.camera.nearDistance = nearval
        if farval != self.camera.farDistance.getValue():
            self.camera.farDistance = farval

    def zoomByCursor(self, thispos, prevpos):
        """
        Calculate a zoom/dolly factor from the difference of the current
        cursor position and the last.
        """
        # There is no "geometrically correct" value, 20 just seems to give
        # about the right "feel".
        self.zoom((thispos[1] - prevpos[1]) * 20.0)
        
    def addToLog(self, pos, time):
        """
        This method adds another point to the mouse location log, used for spin
        animation calculations.
        """
        if len(self.log) > 0 and pos == self.log[0]:
            return
        self.log.insert(0, (pos, time))
        while len(self.log) > self.log_size:
            del self.log[-1]
        
    def spin(self, pointerpos):
        """
        Uses the sphere sheet projector to map the mouseposition unto
        a 3D point and find a rotation from this and the last calculated point.
        """
        if len(self.log) < 2:
            return
        lastpos = self.log[1][0]
        lastpos = SbVec2f((
            lastpos[0] / float(max(self.width - 1, 1)),
            lastpos[1] / float(max(self.height - 1, 1))))

        self.spinprojector.project(lastpos)
        r = SbRotation()
        self.spinprojector.projectAndGetRotation(pointerpos, r)
        r.invert()
        self.reorientCamera(r)
        # todo
        # Calculate an average angle magnitude value to make the transition
        # to a possible spin animation mode appear smooth.
        #SbVec3f dummy_axis, newaxis;
        #float acc_angle, newangle;
        #self.spinincrement.getValue(dummy_axis, acc_angle)
        #acc_angle *= this->spinsamplecounter; // weight
        #r.getValue(newaxis, newangle);
        #acc_angle += newangle;
        
        #this->spinsamplecounter++;
        #acc_angle /= this->spinsamplecounter;
        # FIXME: accumulate and average axis vectors aswell? 19990501 mortene.
        #this->spinincrement.setValue(newaxis, acc_angle);
        # Don't carry too much baggage, as that'll give unwanted results
        # when the user quickly trigger (as in "click-drag-release") a spin
        # animation.
        #if (this->spinsamplecounter > 3) this->spinsamplecounter = 3;

    def reorientCamera(self, rot):
        """
        Rotate the camera by the given amount, then reposition it so we're
        still pointing at the same focal point.
        """
        cam = self.camera
        if cam is None: return
        # Find global coordinates of focal point.
        # SbVec3f direction; # todo remove
        direction = cam.orientation.getValue().multVec(SbVec3f(0, 0, -1)) # todo test return
        focalpoint = cam.position.getValue() + cam.focalDistance.getValue() * direction
        # Set new orientation value by accumulating the new rotation.
        cam.orientation = rot * cam.orientation.getValue()
        # Reposition camera so we are still pointing at the same old focal point.
        direction = cam.orientation.getValue().multVec(SbVec3f(0, 0, -1))
        cam.position = focalpoint - cam.focalDistance.getValue() * direction

    def pan(self, aspectratio, panningplane, currpos, prevpos):
        # Move camera parallel with the plane orthogonal to the camera
        # direction vector.
        cam = self.camera
        if cam is None: return
        if currpos == prevpos: return # useless invocation
        
        # Find projection points for the last and current mouse coordinates.
        vv = cam.getViewVolume(aspectratio)
        
        line = vv.projectPointToLine(currpos)
        line = SbLine(*line)
        if is_newer_pivy:
            current_planept = panningplane.intersect(line)
        else:
            current_planept = SbVec3f() # output argument
            panningplane.intersect(line, current_planept)

        line = vv.projectPointToLine(prevpos)
        line = SbLine(*line)
        old_planept = SbVec3f() # output argument
        
        if is_newer_pivy:
            old_planept = panningplane.intersect(line)
        else:
            old_planept = panningplane.intersect(line)
        
        # Reposition camera according to the vector difference between the
        # projected points.
        cam.position = cam.position.getValue() - (current_planept - old_planept)
        
    def zoom(self, diffvalue):
        cam = self.camera
        if cam is None:
            return # can happen for empty scenegraph
        t = cam.getTypeId()
        tname = t.getName()
        # This will be in the range of <0, ->>.
        multiplicator = float(math.exp(diffvalue))
        if t.isDerivedFrom(SoOrthographicCamera.getClassTypeId()):
            # Since there's no perspective, "zooming" in the original sense
            # of the word won't have any visible effect. So we just increase
            # or decrease the field-of-view values of the camera instead, to
            # "shrink" the projection size of the model / scene.
            cam.height = cam.height.getValue() * multiplicator
            return
        # FrustumCamera can be found in the SmallChange CVS module (it's
        # a camera that lets you specify (for instance) an off-center
        # frustum (similar to glFrustum())
        if not t.isDerivedFrom(SoPerspectiveCamera.getClassTypeId()) and tname != "FrustumCamera":
            print(("Warning: %r.zoom: Unknown camera type, will zoom by" +
                    "moving position, but this might not be correct.") % self)
        oldfocaldist = cam.focalDistance.getValue()
        newfocaldist = oldfocaldist * multiplicator
        direction = cam.orientation.getValue().multVec(SbVec3f(0, 0, -1))
        oldpos = cam.position.getValue()
        newpos = oldpos + (newfocaldist - oldfocaldist) * -direction
        cam.position = newpos
        cam.focalDistance = newfocaldist

timer_timeout = None
idle_active = None
delay_timeout = None
def _sensor_manager_changed_callback(arg=None):
    global timer_timeout, idle_active, delay_timeout
    sensormanager = SoDB.getSensorManager()
    timevalue = SbTime()
    if sensormanager.isTimerSensorPending(timevalue) and False:
        interval = timevalue - SbTime.getTimeOfDay()
        # On a system with some load, the interval value can easily get
        # negative. For Xt, this means it will never trigger -- which
        # causes all kinds of problems. Setting it to 0 will make it
        # trigger more-or-less immediately.
        if interval.getValue() < 0.0:
            interval.setValue(0.)
        if timer_timeout is not None:
            gobject.source_remove(timer_timeout)
        if interval.getMsecValue() < 1.0:
            timer_timeout = gobject.idle_add(_sensor_manager_timeout)
        else:
            timer_timeout = gobject.timeout_add(interval.getMsecValue(), _sensor_manager_timeout)
    elif timer_timeout is not None:
        gobject.source_remove(timer_timeout)
        timer_timeout = None

    if sensormanager.isDelaySensorPending():
        if idle_active is None:
            idle_active = gobject.idle_add(_sensor_manager_idle)
        if delay_timeout is None:
            timeout = SoDB.getDelaySensorTimeout().getMsecValue()
            delay_timeout = gobject.timeout_add(timeout, _sensor_manager_delay_timeout)
    else:
        if idle_active is not None:
            gobject.source_remove(idle_active)
            idle_active = None
        if delay_timeout is not None:
            gobject.source_remove(delay_timeout)
            delay_timeout = None
    return False
SensorManagerRegistered = False
def pygtk_register_sensor_manager():
    global SensorManagerRegistered
    if SensorManagerRegistered:
        return
    SensorManagerRegistered = True
    sm = SoDB.getSensorManager()
    sm.setChangedCallback(_sensor_manager_changed_callback, sm)  
    gobject.timeout_add(10, _sensor_manager_idle2)
def _sensor_manager_timeout():
    global timer_timeout
    #import pdb
    #pdb.set_trace()
    SoDB.getSensorManager().processTimerQueue()
    timer_timeout = None
    _sensor_manager_changed_callback()
    return False
def _sensor_manager_idle():
    global idle_active
    SoDB.getSensorManager().processDelayQueue(True)
    idle_active = None
    _sensor_manager_changed_callback()
def _sensor_manager_idle2():
    sm = SoDB.getSensorManager()
    sm.processImmediateQueue()
    sm.processDelayQueue(False)
    sm.processTimerQueue()
    return True
def _sensor_manager_delay_timeout():
    global delay_timeout
    SoDB.getSensorManager().processDelayQueue(False);
    delay_timeout = None
    _sensor_manager_changed_callback()
