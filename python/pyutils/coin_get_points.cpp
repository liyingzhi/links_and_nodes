#include <Inventor/SoDB.h>
#include <Inventor/actions/SoWriteAction.h>
#include <Inventor/SoInput.h>
#include <Inventor/nodes/SoNodes.h>
#include <Inventor/nodes/SoIndexedTriangleStripSet.h>
#include <Inventor/actions/SoCallbackAction.h>
#include <Inventor/actions/SoGetMatrixAction.h>
#include <Inventor/SoPrimitiveVertex.h>

#include <set>
#include <vector>
#include <string>

#include "coin_get_points.h"

using namespace std;

class my_vec : public SbVec3f {
public:
	my_vec(const SbVec3f& o) {
		for(int i = 0; i < 3; i++)
			(*this)[i] = o[i];
	}
	bool operator<(const my_vec& o) const {
		if((*this)[0] < o[0])
			return true;
		if((*this)[0] > o[0])
			return false;
		if((*this)[1] < o[1])
			return true;
		if((*this)[1] > o[1])
			return false;
		if((*this)[2] < o[2])
			return true;
		return false;
	}
};
typedef set<my_vec> points_t;

void triangle_cb(void* userdata, SoCallbackAction* action,
				 const SoPrimitiveVertex * v1,
				 const SoPrimitiveVertex * v2,
				 const SoPrimitiveVertex * v3) {
	points_t& points = *(points_t*)userdata;

	const SbVec3f vtx[] = { v1->getPoint(), v2->getPoint(), v3->getPoint() };
	SbMatrix mm = action->getModelMatrix();        
	SbVec3f vx;
	for (int j=0; j < 3; j++) {
		mm.multVecMatrix(vtx[j], vx); 
		points.insert(my_vec(vx));
	}
}


double* coin_get_points(SoNode* base, int* n_points) {
	SoCallbackAction action;
	points_t points;
	action.addTriangleCallback(SoSphere::getClassTypeId(), triangle_cb, &points);
	action.addTriangleCallback(SoCylinder::getClassTypeId(), triangle_cb, &points);
	action.addTriangleCallback(SoCube::getClassTypeId(), triangle_cb, &points);
	action.addTriangleCallback(SoCone::getClassTypeId(), triangle_cb, &points);
	action.addTriangleCallback(SoIndexedFaceSet::getClassTypeId(), triangle_cb, &points);
	action.addTriangleCallback(SoIndexedTriangleStripSet::getClassTypeId(), triangle_cb, &points);
	action.addTriangleCallback(SoTriangleStripSet::getClassTypeId(), triangle_cb, &points);
	action.addTriangleCallback(SoFaceSet::getClassTypeId(), triangle_cb, &points);
	action.apply(base);

	// now create convex hull
	static vector<double> all_points;
	int p = 0;
	*n_points = points.size();
	all_points.resize(points.size() * 3);
	for(points_t::iterator i = points.begin(); i != points.end(); i++) {
		for(int k = 0; k < 3; k++) {
			all_points[p++] = (*i)[k];
		}
	}
	return &all_points[0];
}

SoNode* coin_load_from_file(const char* filename) {
	SoInput* input = new SoInput();
	input->openFile(filename);

	SoNode* node = SoDB::readAll(input);
	node->ref();

	input->closeFile();
	delete input;

	return node;
}

#ifdef MAIN

int main(int argc, char* argv[]) {
	SoNode* base = coin_load_from_file(argv[1]);
	int n_points;
	double* pts = coin_get_points(base, &n_points);
	printf("n_points: %d\n", n_points);
	for(int p = 0; p < n_points; p++) {
		printf("p%4d: ", p);
		for(int i = 0; i < 3; i++)
			printf("%7.3f ", pts[p * 3 + i]);
		printf("\n");
	}
}
#endif
