#include <Python.h>
#include <semaphore.h>

#include <Inventor/SoDB.h>
#include <Inventor/SoPrimitiveVertex.h>
#include <Inventor/actions/SoCallbackAction.h>
#include <Inventor/nodes/SoNodes.h>
#include <Inventor/details/SoDetails.h>
#include "swig_util.h"

#include <numpy/arrayobject.h>

#include <string>
#include <map>

using namespace std;

#ifndef Py_RETURN_NONE
#define Py_RETURN_NONE return Py_INCREF(Py_None), Py_None
#endif



bool operator<(const SbVec3f& a, const SbVec3f& b) {
	if(a[0] < b[0])
		return true;
	if(a[0] > b[0])
		return false;

	if(a[1] < b[1])
		return true;
	if(a[1] > b[1])
		return false;

	if(a[2] < b[2])
		return true;
	return false;
}

typedef map<SbVec3f, unsigned int> vertices_t;
struct verts_n_faces {
	PyObject* verts;
	unsigned int verts_max;
	unsigned int verts_n;

	PyObject* faces;
	unsigned int faces_max;
	unsigned int faces_n;

	const SbMatrix* base;
};

void set_vertex(verts_n_faces* data, unsigned int i, const SbVec3f& v) {
	// printf("set vertex %d\n", i);
	if(data->verts == NULL) {
		data->verts_n = 0;
		data->verts_max = 1024;
		npy_intp dims[] = {data->verts_max, 3};
		data->verts = PyArray_SimpleNew(2, dims, NPY_FLOAT);
		// printf("new verts: %p\n", data->verts);
	}
	if(i >= data->verts_max) {
		// enlargen!
		while(i >= data->verts_max)
			data->verts_max *= 2;
		// printf("enlargen vertex to %d!\n", data->verts_max);
		npy_intp int_dims[] = {data->verts_max, 3};
		PyArray_Dims dims = {int_dims, 2};
		// printf("old verts: %p, name: %s\n", data->verts, data->verts->ob_type->tp_name);
		// data->verts = 
		PyArray_Resize((PyArrayObject*)data->verts, &dims, 1, NPY_CORDER);
		// printf("enlargened verts: %p, type: %s\n", data->verts, data->verts->ob_type->tp_name);
	}
	
	*(float*)PyArray_GETPTR2(data->verts, i, 0) = v[0];
	*(float*)PyArray_GETPTR2(data->verts, i, 1) = v[1];
	*(float*)PyArray_GETPTR2(data->verts, i, 2) = v[2];

	if(i >= data->verts_n)
		data->verts_n = i + 1;
}
void add_face(verts_n_faces* data, unsigned int* vtx_ids) {
	// printf("add face %d\n", data->faces_n);
	if(data->faces == NULL) {
		data->faces_n = 0;
		data->faces_max = 1024;
		npy_intp dims[] = {data->faces_max, 3};
		data->faces = PyArray_SimpleNew(2, dims, NPY_UINT);
	}
	if(data->faces_n + 1 == data->faces_max) {
		// enlargen!
		data->faces_max *= 2;
		// printf("enlargen faces to %d!\n", data->faces_max);
		npy_intp int_dims[] = {data->faces_max, 3};
		PyArray_Dims dims = {int_dims, 2};
		// data->faces = 
		PyArray_Resize((PyArrayObject*)data->faces, &dims, 1, NPY_CORDER);
	}
	
	*(unsigned int*)PyArray_GETPTR2(data->faces, data->faces_n, 0) = vtx_ids[0];
	*(unsigned int*)PyArray_GETPTR2(data->faces, data->faces_n, 1) = vtx_ids[1];
	*(unsigned int*)PyArray_GETPTR2(data->faces, data->faces_n, 2) = vtx_ids[2];
	data->faces_n++;
}

void _coin_util_get_verts_n_faces_cb(
        void* userdata, 
        SoCallbackAction* cb,
        const SoPrimitiveVertex* v1,
        const SoPrimitiveVertex* v2,
        const SoPrimitiveVertex* v3) {

        struct verts_n_faces* data = (verts_n_faces*)userdata;

        SbMatrix mm = cb->getModelMatrix();        
        if(data->base) {
                const SbMatrix& base = *data->base;
                mm = mm * base;
        }

	const SoDetail* d = v1->getDetail();
	unsigned int vtx_ids[3];
	if(d->isOfType(SoFaceDetail::getClassTypeId())) {
		const SoFaceDetail* fd = (const SoFaceDetail*) d;
		for(unsigned int i = 0; i < 3; i++) {
			const SoPointDetail* pd = fd->getPoint(i);
			vtx_ids[i] = pd->getCoordinateIndex();
		}
	}

        const SbVec3f* vtx[] = { &v1->getPoint(), &v2->getPoint(), &v3->getPoint() };
        SbVec3f vx[3];
        for (int j = 0; j < 3; j++) {
		mm.multVecMatrix(*vtx[j], vx[j]); 

		set_vertex(data, vtx_ids[j], vx[j]);
		/*
		vertices_t::iterator vit = data->vertices.find(vx[j]);
		if(vit == data->vertices.end()) {
			unsigned int vid = data->vertices.size();
			data->vertices[vx[j]] = vid;
			PyList_Append(data->verts, Py_BuildValue("(fff)", vx[j][0], vx[j][1], vx[j][2]));
			vtx_ids[j] = vid;
		}  else {
			vtx_ids[j] = vit->second;
		}
		*/
	}
	add_face(data, vtx_ids);
	/*
        PyList_Append(
                data->faces, 
                Py_BuildValue("(III)", vtx_ids[0], vtx_ids[1], vtx_ids[2]));
	*/
}


PyObject* _coin_util_real_get_verts_and_faces(const SoNode* sep, const SbMatrix* base=NULL) {
	verts_n_faces data;
	memset(&data, 0, sizeof(data));
        
        SoCallbackAction myAction;
	myAction.addTriangleCallback(SoBase::getClassTypeId(), _coin_util_get_verts_n_faces_cb, &data);
	myAction.apply((SoNode*)sep);

	if(data.verts) {
		npy_intp int_dims[] = {data.verts_n, 3};
		PyArray_Dims dims = {int_dims, 2};
		PyArray_Resize((PyArrayObject*)data.verts, &dims, 1, NPY_CORDER);
	}

	if(data.faces) {
		npy_intp int_dims[] = {data.faces_n, 3};
		PyArray_Dims dims = {int_dims, 2};
		PyArray_Resize((PyArrayObject*)data.faces, &dims, 1, NPY_CORDER);
	}


	PyObject* ret = Py_BuildValue("(OO)", data.verts, data.faces);
	Py_DECREF(data.faces);
	Py_DECREF(data.verts);

	return ret;
}

PyObject* _coin_util_get_verts_and_faces(PyObject* self, PyObject *args) {
        PyObject* model;
	if(!PyArg_ParseTuple(args, "O", &model))
		return NULL;

        SoNode* sep = (SoNode*)get_ptr(model);
        return _coin_util_real_get_verts_and_faces(sep);
}


void _coin_util_get_triangles_cb(
        void* userdata, 
        SoCallbackAction* cb,
        const SoPrimitiveVertex* v1,
        const SoPrimitiveVertex* v2,
        const SoPrimitiveVertex* v3) {

        pair<PyObject*, const SbMatrix*>* data = (pair<PyObject*, const SbMatrix*>*)userdata;

        PyObject* triangle_list = data->first;
        const SbVec3f vtx[] = { v1->getPoint(), v2->getPoint(), v3->getPoint() };
        SbMatrix mm = cb->getModelMatrix();        
        SbVec3f vx[3];
        if(data->second) {
                const SbMatrix& base = *(data->second);
                mm = mm * base;
        }
        for (int j=0; j < 3; j++)
                mm.multVecMatrix(vtx[j], vx[j]); 
        PyList_Append(
                triangle_list, 
                Py_BuildValue(
                        "(fff)(fff)(fff)",
                        vx[0][0], vx[0][1], vx[0][2],
                        vx[1][0], vx[1][1], vx[1][2],
                        vx[2][0], vx[2][1], vx[2][2]));
}

PyObject* _coin_util_real_get_triangles(const SoNode* sep, const SbMatrix* base=NULL) {
        PyObject* triangle_list = PyList_New(0); // 1024 * 10 * 5);

        pair<PyObject*, const SbMatrix*> data = pair<PyObject*, const SbMatrix*>(triangle_list, base);
        
        SoCallbackAction myAction;
	myAction.addTriangleCallback(SoBase::getClassTypeId(), _coin_util_get_triangles_cb, &data);
	/*
	myAction.addTriangleCallback(SoSphere::getClassTypeId(), _coin_util_get_triangles_cb, &data);
        myAction.addTriangleCallback(SoCylinder::getClassTypeId(), _coin_util_get_triangles_cb, &data);
	myAction.addTriangleCallback(SoCube::getClassTypeId(), _coin_util_get_triangles_cb, &data);
	myAction.addTriangleCallback(SoCone::getClassTypeId(), _coin_util_get_triangles_cb, &data);
	myAction.addTriangleCallback(SoIndexedFaceSet::getClassTypeId(), _coin_util_get_triangles_cb, &data);
	myAction.addTriangleCallback(SoFaceSet::getClassTypeId(), _coin_util_get_triangles_cb, &data);
	myAction.addTriangleCallback(SoIndexedTriangleStripSet::getClassTypeId(), _coin_util_get_triangles_cb, &data);
	*/
	myAction.apply((SoNode*)sep);

        return triangle_list;
}

PyObject* _coin_util_get_triangles(PyObject* self, PyObject *args) {
        PyObject* model;
	if(!PyArg_ParseTuple(args, "O", &model))
		return NULL;

        SoNode* sep = (SoNode*)get_ptr(model);
        return _coin_util_real_get_triangles(sep);
}

string last_name;
SoCallbackAction::Response _coin_util_extend_triangles_list_cb(
        void * userdata,
        SoCallbackAction* cb,
        const SoNode* node) {

        pair<PyObject*, PyObject*>* data = (pair<PyObject*, PyObject*>*)userdata;
        PyObject* robot_names = data->first;
        PyObject* triangle_list = data->second;

        // is it a robot base node?
        const char* namep = node->getName();
        if(namep && *namep) {
                PyObject* node_name = PyString_FromString(namep);
                int ret = PySet_Contains(robot_names, node_name);
                Py_DECREF(node_name);
                if(ret)
                        return SoCallbackAction::PRUNE;
        }
        SoType t = node->getTypeId();
        if(t != SoSeparator::getClassTypeId() && t != SoGroup::getClassTypeId()) {
                const SbMatrix mm = cb->getModelMatrix();        
                PyObject* this_triangle_list = _coin_util_real_get_triangles(node, &mm);
                PyList_Append(
                        triangle_list, 
                        Py_BuildValue("sO", 
                                      last_name.c_str(),
                                      this_triangle_list));
                return SoCallbackAction::PRUNE;
        } else if(*namep)
                last_name = namep;

        return SoCallbackAction::CONTINUE;
}

PyObject* _coin_util_get_non_robot_triangles(PyObject* self, PyObject *args) {
        PyObject* model;
        PyObject* robot_names;
	if(!PyArg_ParseTuple(args, "OO", &model, &robot_names))
		return NULL;

        SoSeparator* sep = (SoSeparator*)get_ptr(model);

        PyObject* triangle_list = PyList_New(0);
        pair<PyObject*, PyObject*> data = pair<PyObject*, PyObject*>(robot_names, triangle_list);

        SoCallbackAction myAction;

	myAction.addPreCallback(SoSeparator::getClassTypeId(), _coin_util_extend_triangles_list_cb, &data);
	myAction.addPreCallback(SoGroup::getClassTypeId(), _coin_util_extend_triangles_list_cb, &data);
	myAction.addPreCallback(SoSphere::getClassTypeId(), _coin_util_extend_triangles_list_cb, &data);
        myAction.addPreCallback(SoCylinder::getClassTypeId(), _coin_util_extend_triangles_list_cb, &data);
	myAction.addPreCallback(SoCube::getClassTypeId(), _coin_util_extend_triangles_list_cb, &data);
	myAction.addPreCallback(SoCone::getClassTypeId(), _coin_util_extend_triangles_list_cb, &data);
	myAction.addPreCallback(SoIndexedFaceSet::getClassTypeId(), _coin_util_extend_triangles_list_cb, &data);
	myAction.addPreCallback(SoFaceSet::getClassTypeId(), _coin_util_extend_triangles_list_cb, &data);
	myAction.addPreCallback(SoIndexedTriangleStripSet::getClassTypeId(), _coin_util_extend_triangles_list_cb, &data);
	myAction.apply(sep);

        return triangle_list;
}

PyObject* _coin_util_get_shape(PyObject* self, PyObject *args) {
        PyObject* triangles;
        PyObject* coordinate3;
        PyObject* indexedFaceSet;
	if(!PyArg_ParseTuple(args, "OOO", &triangles, &coordinate3, &indexedFaceSet))
		return NULL;

        SoCoordinate3* c3 = (SoCoordinate3*)get_ptr(coordinate3);
        SoIndexedFaceSet* fs = (SoIndexedFaceSet*)get_ptr(indexedFaceSet);

        SbVec3f p[3];
        for(int i = 0; i < PyList_GET_SIZE(triangles); i++) {
                if(!PyArg_ParseTuple(
                           PyList_GET_ITEM(triangles, i), 
                           "(fff)(fff)(fff)", 
                           &p[0][0], &p[0][1], &p[0][2],
                           &p[1][0], &p[1][1], &p[1][2],
                           &p[2][0], &p[2][1], &p[2][2]))
                        return PyErr_Format(
                                PyExc_RuntimeError, 
                                "invliad triangle %d in triangle list %p", 
                                i, triangles);
                for(int k = 0; k < 3; k++) {
                        c3->point.set1Value(i * 3 + k, p[k]);
                        // fs->coordIndex.set1Value(i * 4 + k, i * 3 + (2-k));
                        fs->coordIndex.set1Value(i * 4 + k, i * 3 + k);
                }
		fs->coordIndex.set1Value(i * 4 + 3, -1);
        }

        Py_RETURN_NONE;
}


static PyMethodDef _coin_util_module_methods[] = {
        {"get_verts_and_faces", (PyCFunction)_coin_util_get_verts_and_faces, METH_VARARGS, "get_verts_and_faces"},
        {"get_triangles", (PyCFunction)_coin_util_get_triangles, METH_VARARGS, "get_triangles"},
        {"get_non_robot_triangles", (PyCFunction)_coin_util_get_non_robot_triangles, METH_VARARGS, "get_non_robot_triangles"},

        {"get_shape", (PyCFunction)_coin_util_get_shape, METH_VARARGS, "get_shape"},
        {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC init_coin_util(void) {
        PyObject* module = Py_InitModule3("_coin_util", _coin_util_module_methods, "python coin utils");        
        if(!module)
                return;

	import_array();

        while(1) {
                PyObject* dict = PyModule_GetDict(module);
                if(!dict) break;

                // PyDict_SetItemString(dict, "RPCFEATURE_AUTO", PyInt_FromLong(RPCFEATURE_AUTO));

                /*
                PyObject* o = PyDict_New();
                PyDict_SetItem(o, PyInt_FromLong(CODEC_TYPE_UNKNOWN),
                               PyString_FromString("UNKNOWN"));
                PyDict_SetItem(o, PyInt_FromLong(CODEC_TYPE_VIDEO),
                               PyString_FromString("VIDEO"));
                PyDict_SetItem(o, PyInt_FromLong(CODEC_TYPE_AUDIO),
                               PyString_FromString("AUDIO"));
                PyDict_SetItem(o, PyInt_FromLong(CODEC_TYPE_DATA),
                               PyString_FromString("DATA"));
                PyDict_SetItem(o, PyInt_FromLong(CODEC_TYPE_SUBTITLE),
                               PyString_FromString("SUBTITLE"));
                PyDict_SetItemString(dict, "CodecTypes", o);
                */
                return;
        }
        PyErr_SetString(PyExc_ImportError, "_coin_util: init failed");
        return;
}
