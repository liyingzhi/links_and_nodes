"""
Copyright 2014 Florian Schmidt

This file is part of pygtksvnb.

pygtksvnb is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pygtksvnb is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# do not enforce this on the user: from __future__ import print_function

import gi
gi.require_version('PangoCairo', '1.0')
gi.require_version('GLib', '2.0')

gtksource_versions = "4", "3.0"
for ver in gtksource_versions:
    try:
        gi.require_version('GtkSource', ver)
        gtksource_version = tuple(map(int, ver.split(".")))
        break
    except:
        pass
else:
    raise Exception("need one of these GtkSource versions: %s (try apt-get install gir1.2-gtksource-4)" % (", ".join(gtksource_versions)))

from gi.repository import Gtk, Gdk, GLib, Pango, PangoCairo, GtkSource

import re
import os
import sys
import time
import pprint
import traceback
import sys
if sys.version_info[0] >= 3:
    unicode = str
else:
    pass

# import code ? http://docs.python.org/2/library/code.html?highlight=code#code

class user_gui(Gtk.VBox):
    def __init__(self, cell):
        self.cell = cell
        Gtk.VBox.__init__(self)

    def __contains__(self, name):
        return name in self.__dict__

    def add(self, widget):
        self.pack_start(widget, False, False)
        widget.show()

def get_monospace_font_name():
    fmap = PangoCairo.font_map_get_default()
    fonts = dict((font.get_name().lower(), font) for font in fmap.list_families())
    have_misc_fixed = bool([fn for fn in list(fonts.keys()) if "misc fixed" in fn])
    if have_misc_fixed:
        return "Misc Fixed medium 10"
    have_fixed = bool([fn for fn in list(fonts.keys()) if "fixed" in fn])
    if have_fixed:
        return "Fixed semicondensed 10"
    return "monospace 9"

def get_monospace_font_description():
    font = get_monospace_font_name()
    return Pango.FontDescription(font)

        
class notebook_cell(object):    
    line_numbers = False
    all_cells = []
    
    def __init__(self, nb, text="", font=None):
        notebook_cell.all_cells.append(self)
        self.nb = nb

        if font is None:
            fd = get_monospace_font_description()
        else:
            import pango
            fd = pango.FontDescription(font)
        self.fd = fd

        self.input_label = l = Gtk.Label("In [ ]:")
        l.get_style_context().add_class("input-label")
        l.set_alignment(0, 0)
        l.set_size_request(60, -1)
        
        self.input_widget = f = Gtk.Frame()
        #f.set_size_request(-1, 50)

        self._scroll_check = None
        self.sv = tv = GtkSource.View()
        f.add(tv)
        tv.get_style_context().add_class("input-textview")
        tv.connect("key-press-event", self.on_tv_key_press_event)
        tv.connect("focus", self.on_focus)
        tv.connect("focus-in-event", self.on_focus)
        b = tv.get_buffer()
        if text is not None:
            b.set_text(text)
        b.connect("changed", self.on_input_change)
        if True:
            tv.set_show_line_numbers(notebook_cell.line_numbers)
            tv.set_show_line_marks(False)
            tv.set_auto_indent(True)
            tv.set_tab_width(4)
            tv.set_insert_spaces_instead_of_tabs(True)
            tv.set_show_right_margin(False)
            b.set_max_undo_levels(100)
            lm = GtkSource.LanguageManager()
            l = lm.get_language("python")
            b.set_language(l)
            b.set_highlight_syntax(True)
            tv.modify_font(fd)

        self.output_label = l = Gtk.Label()
        l.set_no_show_all(True)
        l.get_style_context().add_class("output-label")
        l.set_alignment(0, 0)
        
        self.output_widget = Gtk.VBox()

        self.gui = user_gui(self)
        self.output_widget.pack_start(self.gui, False, False, 0)
        
        self.out_tv = tv = Gtk.TextView()
        tv.get_style_context().add_class("output-textview")
        self.out_tv.connect("key-press-event", self.on_out_tv_key_press_event)
        self.output_widget.pack_start(tv, False, False, 0)
        self.out_buffer = self.out_tv.get_buffer()
        tv.set_no_show_all(True)
        tv.set_editable(False)
        tv.modify_font(fd)
        b = tv.get_buffer()
        b.create_tag("red", foreground="red") # , weight=pango.WEIGHT_BOLD)
        #b.create_tag("underlined", underline=pango.UNDERLINE_SINGLE)
            
        self.output_visible = False

    def get_insert(self):
        buffer = self.sv.get_buffer()
        return buffer.get_iter_at_mark(buffer.get_insert()).get_offset()
        
    def search_select_next(self, search, last_pos=None, reverse=False):
        buf = self.sv.get_buffer()
        text = self.get_input_text()
        l = len(search)
        if not reverse:
            if last_pos is None:
                offset = 0
            else:
                offset = last_pos + l
            pos = text.find(search, offset)
        else:
            if last_pos is None:
                last_pos = -1
            pos = text.rfind(search, 0, last_pos)
        if pos == -1:
            if hasattr(self, "cursor_before_search") and self.cursor_before_search is not None:
                buf.place_cursor(self.cursor_before_search)
                self.cursor_before_search = None
            return None
        start = buf.get_iter_at_offset(pos)
        end = buf.get_iter_at_offset(pos + l)
        if not hasattr(self, "cursor_before_search") or self.cursor_before_search is None:
            self.cursor_before_search = buf.get_iter_at_mark(buf.get_insert())
        buf.select_range(start, end)
        self._focus(skip_grab=False)
        return pos
        
    def search_all(self, text, search):
        offset = 0
        l = len(search)
        while True:
            pos = text.find(search, offset)
            if pos == -1:
                return
            yield pos, l
            offset = pos + l

    def highlight_search(self, search):
        buf = self.sv.get_buffer()
        text = self.get_input_text()
        method = self.search_all
        if not hasattr(self, "highlight_search_tag"):
            self.highlight_search_tag = buf.create_tag("highlight_search_tag", background="yellow")
        else:
            buf.remove_tag(self.highlight_search_tag, buf.get_start_iter(), buf.get_end_iter())
        if not search:
            return True
        anything = False
        for pos, l in method(text, search):
            start = buf.get_iter_at_offset(pos)
            end = buf.get_iter_at_offset(pos + l)
            buf.apply_tag(self.highlight_search_tag, start, end)
            anything = True
        return anything
            
    def attach(self, index):
        # each cell has 2 rows to attach to!!
        tr = index * 2
        self.nb.table.attach(
            self.input_label, 0, 1, tr, tr + 1,
            Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 2)
        self.nb.table.attach(
            self.input_widget, 1, 2, tr, tr + 1,
            Gtk.AttachOptions.EXPAND|Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 0)
        tr += 1
        self.nb.table.attach(
            self.output_label, 0, 1, tr, tr + 1,
            Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 0)
        self.nb.table.attach(
            self.output_widget, 1, 2, tr, tr + 1,
            Gtk.AttachOptions.FILL, Gtk.AttachOptions.FILL,
            0, 0)
    
    def deattach(self):
        t = self.nb.table
        t.remove(self.input_label)
        t.remove(self.input_widget)
        t.remove(self.output_label)
        t.remove(self.output_widget)
        
    def reattach(self, index):
        # each cell has 2 rows to attach to!!
        tr = index * 2
        self.nb.table.child_set(
            self.input_label, **{
                "left-attach": 0,
                "right-attach": 1,
                "top-attach": tr,
                "bottom-attach": tr + 1})
        self.nb.table.child_set(
            self.input_widget, **{
                "left-attach": 1,
                "right-attach": 2,
                "top-attach": tr,
                "bottom-attach": tr + 1})
        tr += 1
        self.nb.table.child_set(
            self.output_label, **{
                "left-attach": 0,
                "right-attach": 1,
                "top-attach": tr,
                "bottom-attach": tr + 1})
        self.nb.table.child_set(
            self.output_widget, **{
                "left-attach": 1,
                "right-attach": 2,
                "top-attach": tr,
                "bottom-attach": tr + 1})

    def find_cell_index(self):
        return self.nb.cells.index(self)

    def on_input_change(self, b):
        self.cursor_before_search = None
        self.nb.changed()
        return True
    
    def on_out_tv_key_press_event(self, tv, ev):
        pass_to_sv_handler = False
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Down:
            # insert new cell below us
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Up:
            # insert new cell above us
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Down:
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Up:
            pass_to_sv_handler = True
        if ev.keyval == Gdk.KEY_Up:
            pass_to_sv_handler = True
        if ev.keyval == Gdk.KEY_Down:
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('l'):
            pass_to_sv_handler = True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('s'):
            pass_to_sv_handler = True
        if pass_to_sv_handler:
            self.on_tv_key_press_event(tv, ev)        
        return False

    def _do_scroll_check(self):
        tv = self.sv
        b = tv.get_buffer()
        
        # current cursor position:
        insert_mark = b.get_insert()
        insert_iter = b.get_iter_at_mark(insert_mark)
        loc = tv.get_iter_location(insert_iter)
        left = loc.x
        right = loc.x + loc.width
        top = loc.y
        bottom = loc.y + loc.height

        # in viewport:
        r = tv.get_allocation()
        top += r.y
        bottom += r.y
        left += r.x
        right += r.x

        # check vertical scroll:
        vadj = self.nb.sw.get_vadjustment()
        vvalue = vadj.get_value()
        top -= vvalue
        bottom -= vvalue

        r = self.nb.sw.get_allocation()
        scroll_down = bottom - r.height
        scroll_up = -top
        if scroll_down > 0:
            vadj.set_value(vvalue + scroll_down)
        elif scroll_up > 0:
            vvalue -= scroll_up
            if vvalue < 0:
                vvalue = 0
            vadj.set_value(vvalue)

        # check horizontal scroll:
        hadj = self.nb.sw.get_hadjustment()
        hvalue = hadj.get_value()
        left -= hvalue
        right -= hvalue

        r = self.nb.sw.get_allocation()
        scroll_left = left - r.width
        scroll_right = -right
        if scroll_left > 0:
            hadj.set_value(hvalue + scroll_left)
        elif scroll_right > 0:
            hvalue -= scroll_right
            if hvalue < 0:
                hvalue = 0
            hadj.set_value(hvalue)

        self._scroll_check = None
        return False
    
    def schedule_scroll_check(self):
        if self._scroll_check is None:
            self._scroll_check = GLib.idle_add(self._do_scroll_check)
        
    def on_tv_key_press_event(self, tv, ev):
        is_sv = tv == self.sv
        if (ev.state & Gdk.ModifierType.CONTROL_MASK and (ev.keyval == ord("f") or ev.keyval == ord("F") or ev.keyval == ord("r"))) or ev.keyval == Gdk.KEY_F3:
            preset = None
            buffer = tv.get_buffer()
            if buffer.get_has_selection():
                start, end = buffer.get_selection_bounds()
                preset = buffer.get_text(start, end, False)
            reverse = ev.keyval == ord("r") or ev.state & Gdk.ModifierType.SHIFT_MASK
            self.nb.search(reverse=reverse, preset=preset)
            return True
        if ev.keyval == Gdk.KEY_BackSpace:
            b = self.sv.get_buffer()
            insert_mark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insert_mark)
            if insert_iter.get_offset() > 0:
                if is_sv: self.schedule_scroll_check()
                return False
            if self.get_input_text() != "":
                if is_sv: self.schedule_scroll_check()
                return False
            if len(self.nb.cells) == 1:
                if is_sv: self.schedule_scroll_check()
                return False
            idx = self.nb.delete_cell(self)
            cell = self.nb.cells[idx]
            cell.focus()
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord("-"):
            # split current cell at cursor
            b = self.sv.get_buffer()
            insert_mark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insert_mark)
            rest = b.get_text(insert_iter, b.get_end_iter(), False)
            b.delete(insert_iter, b.get_end_iter())
            
            idx = self.find_cell_index()
            cell = self.nb.new_cell_at(index=idx + 1)
            b2 = cell.sv.get_buffer()
            b2.set_text(rest)
            cell.focus()
            return True 
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Page_Up:
            # join with cell above
            idx = self.find_cell_index()
            if idx > 0:
                cell_above = self.nb.cells[idx - 1]

                b = self.sv.get_buffer()
                this = b.get_text(b.get_start_iter(), b.get_end_iter(), False)
            
                b2 = cell_above.sv.get_buffer()
                above = b2.get_text(b2.get_start_iter(), b2.get_end_iter(), False)
                b2.set_text(above.rstrip("\n") + "\n" + this.lstrip("\n"))
                cell_above.focus()
                self.nb.delete_cell(self)
            return True 
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Page_Down:
            # join with cell above
            idx = self.find_cell_index()
            if idx > 0:
                cell_above = self.nb.cells[idx + 1]

                b = self.sv.get_buffer()
                this = b.get_text(b.get_start_iter(), b.get_end_iter(), False)
            
                b2 = cell_above.sv.get_buffer()
                above = b2.get_text(b2.get_start_iter(), b2.get_end_iter(), False)
                b2.set_text(this.rstrip("\n") + "\n" + above.lstrip("\n"))
                cell_above.focus()
                self.nb.delete_cell(self)
            return True 
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Down:
            # insert new cell below us
            idx = self.find_cell_index()
            cell = self.nb.new_cell_at(index=idx + 1)
            cell.focus()
            return True
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Up:
            # insert new cell above us
            idx = self.find_cell_index()
            cell = self.nb.new_cell_at(index=idx)
            cell.focus()
            return True 
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Down:
            idx = self.find_cell_index()
            if idx + 1 < len(self.nb.cells):
                # move to cell below us
                cell = self.nb.cells[idx + 1]
                cell.focus()
            return True 
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Up:
            idx = self.find_cell_index()
            if idx > 0:
                # move to cell above us
                cell = self.nb.cells[idx - 1]
                cell.focus()
            return True 
        if ev.keyval == Gdk.KEY_Up:
            b = tv.get_buffer()
            insert_mark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insert_mark)
            if b.get_line_count() == 1:
                line2_iter = b.get_end_iter()
            else:
                line2_iter = b.get_iter_at_line(1)
            if insert_iter.compare(line2_iter) < 0 or (insert_iter.compare(line2_iter) == 0 and b.get_line_count() == 1):
                # we are already at the first line!
                idx = self.find_cell_index()
                if idx > 0:
                    cell = self.nb.cells[idx - 1]
                    cell.focus()
                    b = cell.sv.get_buffer()
                    b.place_cursor(b.get_end_iter())
                return True 
            if is_sv: self.schedule_scroll_check()
            return False
        if ev.keyval == Gdk.KEY_Down:
            b = tv.get_buffer()
            insert_mark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insert_mark)
            line2_iter = b.get_iter_at_line(b.get_line_count())
            if insert_iter.compare(line2_iter) >= 0:
                # we are already at the last line!
                idx = self.find_cell_index()
                if idx + 1 < len(self.nb.cells):
                    cell = self.nb.cells[idx + 1]
                    cell.focus()
                    b = cell.sv.get_buffer()
                    b.place_cursor(b.get_start_iter())
                return True 
            if is_sv: self.schedule_scroll_check()
            return False
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('l'):
            notebook_cell.line_numbers = not notebook_cell.line_numbers
            for cell in notebook_cell.all_cells:
                cell.sv.set_show_line_numbers(notebook_cell.line_numbers)
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('s'):
            self.nb.save()
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == Gdk.KEY_Return:
            if not self.execute():
                return True
            if not ev.state & Gdk.ModifierType.SHIFT_MASK:
                return True
        if ev.state & Gdk.ModifierType.SHIFT_MASK and ev.keyval == Gdk.KEY_Return:
            # select next cell or add new cell if last cell
            idx = self.find_cell_index()
            if idx + 1 == len(self.nb.cells):
                # we are last! -> add new cell!
                cell = self.nb.new_cell() # index=idx + 1)
            else:
                # just select next cell
                cell = self.nb.cells[idx + 1]
            cell.focus()
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('a'):
            # goto start of line
            b = tv.get_buffer()
            insertmark = b.get_insert()
            insertiter = b.get_iter_at_mark(insertmark)
            line_top, line_height = tv.get_line_yrange(insertiter)
            lineiter, line_top = tv.get_line_at_y(line_top)
            b.place_cursor(lineiter)
            return True
        if ev.state & Gdk.ModifierType.CONTROL_MASK and ev.keyval == ord('e'):
            # goto end of line
            b = tv.get_buffer()
            insertmark = b.get_insert()
            insert_iter = b.get_iter_at_mark(insertmark)
            tv.forward_display_line_end(insert_iter)
            b.place_cursor(insert_iter)
            return True
        if is_sv: self.schedule_scroll_check()

    def get_input_text(self):
        b = self.sv.get_buffer()
        return b.get_text(b.get_start_iter(), b.get_end_iter(), False)
    def get_output_text(self):
        b = self.out_buffer
        return b.get_text(b.get_start_iter(), b.get_end_iter(), False)
    def set_output_text(self, text):
        self.out_buffer.set_text(text)
        self.output_visible = True
        self.out_tv.show()       
        self.output_label.show()

    def replace_rprint_magic(self, m):
        indent = m.group(1)
        expression = m.group(2)
        return '%sdo_pprint(%s, %s)' % (
            indent, repr(expression.strip()), expression)
        #return '%sprint %s,; pprint.pprint((%s))' % (indent, repr(expression.strip() + ": "), expression)
    
    def replace_print_magic(self, m):
        indent = m.group(1)
        expression = m.group(2)
        if sys.version_info[0] >= 3:
            return '%sprint(%s, (%s))' % (indent, repr(expression.strip() + ":"), expression)
        return '%sprint %s, (%s)' % (indent, repr(expression.strip() + ":"), expression)
    
    def clear(self):
        self.out_buffer.set_text("")

    def execute(self, redirect_stdout=True):
        src = self.get_input_text()

        if not src.strip():
            # nothing to execute
            self.had_output = False
            self.out_buffer.set_text("")
            self.output_label.hide()
            self.out_tv.hide()       
            self.output_visible = False
            return False

        names = list([line for line in src.split("\n") if line.startswith("## ") and "name:" in line])
        if names:
            self.name = names[0].split("name:", 1)[1].strip()
            idx = self.find_cell_index()
            self.nb.insert_cell_name(idx, self.name)

        # preprocess source
        if not hasattr(self, "pprint_re"):
            self.pprint_re = re.compile("^([ \t]*)%%[ \t]*(.*)$", flags=re.M)
            self.print_re = re.compile("^([ \t]*)%[ \t]*(.*)$", flags=re.M)
        src = re.sub(self.pprint_re, self.replace_rprint_magic, src)
        src = re.sub(self.print_re, self.replace_print_magic, src)

        self.nb.executions += 1
        self.execution_id = self.nb.executions
        self.input_label.set_text("In [%d]:" % self.execution_id)

        self.had_output = False
        old_output = self.get_output_text()
        self.clear()
        
        self.sv.get_style_context().add_class("active-input-textview")
        for i in range(10):
            Gtk.main_iteration_do(False)
        
        self.nb.module.cell = self
        self.nb.module.clear = self.clear
        self.nb.module.cell_id = self.nb.cells.index(self)
        self.grab_focus = False
        #self.grab_focus = True
        #if self.nb.module.cells:
        #    self.nb.module.cells[-1].grab_focus = False
        self.nb.module.cells.append(self)
        self.stop_request_at_start = self.nb.stop_request
        if redirect_stdout:
            sys.stdout = self
        self.nb.module.sys = sys
        
        self.nb.already_executed.add(self.nb.cells.index(self))
        if hasattr(self, "name"):
            self.nb.already_executed.add(self.name)
        
        try:
            exec(src, self.nb.module.__dict__)
        except:
            tb = traceback.format_exc().strip().split("\n")
            del tb[1:3]
            if "stop requested" in tb[-1]:
                del tb[-3:-1]
            tb = "\n".join(tb)
            if redirect_stdout:
                self.write("\n" + tb, tags=["red"])
            else:
                sys.stdout.write("\n" + tb)

        # todo: not working with Gtk3
        self.sv.get_style_context().remove_class("active-input-textview")

        del self.nb.module.cells[-1]
        if self.nb.module.cells:
            self.nb.module.cell = self.nb.module.cells[-1]
        else:
            self.nb.module.cell = None

        if redirect_stdout:
            sys.stdout = self.nb.original_stdout

        if not self.had_output:
            self.output_label.hide()
            self.out_tv.hide()       
            self.output_visible = False
        else:
            b = self.out_buffer
            self.nb.outputs[self.execution_id] = b.get_text(b.get_start_iter(), b.get_end_iter(), False)
            if old_output != self.get_output_text():
                self.nb.changed()

        if redirect_stdout and self.grab_focus:
            self.sv.grab_focus() # steal back focus for e.g. Gtk.Window creation or plotting
        return True
    
    def flush(self):
        # for stdout emulation, flush output
        for i in range(10):
            Gtk.main_iteration_do(False)
    def write(self, text, tags=None):
        if type(text) != unicode:
            text = text.decode("utf-8")

        #self.nb.original_stdout.write("write: %r\n" % text)

        b = self.out_buffer
        if tags is None:
            b.insert(b.get_end_iter(), text)
        else:
            b.insert_with_tags_by_name(b.get_end_iter(), text, *tags)
        Gtk.main_iteration_do(False)
        
        if not self.had_output:
            self.output_label.set_text("Out[%d]:" % self.execution_id)
            self.had_output = True

        if not self.output_visible:
            self.output_visible = True
            self.out_tv.show()       
            self.output_label.show()

    def _focus(self, skip_grab=False):
        if not skip_grab:
            self.sv.grab_focus()
        self.schedule_scroll_check()
        return False
    
    def focus(self):
        self.nb.current_cell = self
        self.sv.grab_focus()
        Gtk.main_iteration_do(False)
        GLib.idle_add(self._focus) # why no priority arg? GLib.PRIORITY_DEFAULT_IDLE
                
    def on_focus(self, widget, direction):
        self.nb.current_cell = self
        if self.nb.module.cells:
            self.nb.module.cells[-1].grab_focus = False
        if hasattr(self, "name") and self.name:
            self.nb.highlight_cell_name(self.find_cell_index())
        else:
            self.nb.highlight_cell_name(-1)


