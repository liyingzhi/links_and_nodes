#include "mex.h"
#include "matrix.h"
#include <math.h>
#include <string.h>
#include <string>
#include <set>
#include <list>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/select.h>

using namespace std;

#define printf mexPrintf
#define dbg { printf("%s:%d in %s()\n", __FILE__, __LINE__, __func__); }

#define RECEIVE_BUFFER 1024 * 100

set<int> fds;

int parse_packet(mxArray** ret, char* buffer, int len) {
        /* parse packet line by line and generate a matlab matrix! */
        char* cp = buffer;
        mxArray* mat;
        list<mxArray*> objects;

        while(cp - buffer < len) {
                char* line = cp;
                while(*cp && *cp != '\n')
                        cp++;
                *cp = 0;
                if(line == cp)
                        break;
                
                if(!strncmp(line, "string:", 7)) {
                        // string: this is a one line string
                        objects.push_back(mxCreateString(line + 7));
                        cp++;
                } else if(!strncmp(line, "mat:", 4)) {
                        // mat: M N val1 val2...
                        list<double> args;
                        char* p = line + 4;
                        while(p != cp) {
                                // skip whitespace
                                while(p != cp && strchr(" \r\n", *p)) p++;
                                if(!*p)
                                        break;
                                args.push_back(atof(p));
                                // skip number
                                while(p != cp && !strchr(" \r\n", *p)) p++;
                        }
                        if(args.size() < 3) {
                                printf("ignoring invalid 'mat' line - to few doubles!: '%s'\n",
                                       line);
                                continue;
                        }
                        int m = (int)args.front(); args.pop_front();
                        int n = (int)args.front(); args.pop_front();
                        if(args.size() != m * n) {
                                printf("ignoring invalid 'mat' line - to few doubles. want to read %d x %d doubles - got %d: '%s'\n",
                                       m, n,
                                       args.size(),
                                       line);
                                continue;
                        }
                        mat = mxCreateDoubleMatrix(m, n, mxREAL);
                        objects.push_back(mat);
                        double* data = mxGetPr(mat);
                        // fill matrice
                        for(int i = 0; i < m * n; i++) {
                                data[i] = args.front(); 
                                args.pop_front();
                        }
                        cp++;
                } else {
                        objects.push_back(mxCreateString(line));
                        cp++;
                }
        }
        if(objects.size() == 1) {
                // return single object
                *ret = objects.front();
                objects.pop_front();
                return 0;
        }
        // return array of objects
        int count = objects.size();
        *ret = mxCreateCellArray(1, &count);
        // fill array
        while(objects.size()) {
                count --;
                mxSetCell(*ret, count, objects.back());
                objects.pop_back();
        }
        return 0;
}


int write_packet_object(const mxArray* input, char* buffer, int buffer_len) {
        int written = 0;
        if(mxIsDouble(input)) {
                double* data = mxGetPr(input);
                int m = mxGetM(input);
                int n = mxGetN(input);
                written += snprintf(buffer, buffer_len, "mat:%d %d ", m, n);
                for(int i = 0; i < n * m; i++)
                        written += snprintf(buffer + written, buffer_len - written, "%s%.16g", 
                                            i != 0 ? " " : "",
                                            data[i]);
                written += snprintf(buffer + written, buffer_len - written, "\n");
        } else if(mxIsChar(input)) {
                strcpy(buffer, "string:");
                mxGetString(input, buffer + 7, buffer_len - 7);
                strcat(buffer, "\n");
                written = strlen(buffer);
        } else {
                printf("unknown input object: %s\n", mxGetClassName(input));
                mexErrMsgTxt("unknown input object type!\n");
        }
        return written;
}

int generate_packet(const mxArray* input, char* buffer, int buffer_len) {
        if(!mxIsCell(input))
                return write_packet_object(input, buffer, buffer_len);
        
        // iterate thru the cell
        int n = mxGetNumberOfElements(input);
        int written = 0;
        for(int i = 0; i < n; i++) {
                mxArray* o = mxGetCell(input, i);
                int len = write_packet_object(o, buffer, buffer_len - written);
                buffer += len;
                written += len;
                // printf("generate packet. written: %d\n", written);
        }
        return written;
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if(nrhs < 1)
        mexErrMsgTxt("wrong lhs/rhs count. usage: retval = mxudp_receiver(command, [args...])\n");

    char buffer[128];
    mxGetString(prhs[0], buffer, 128);
    string command = buffer;

    if(command == "create") {
            /* create new udp socket to listen on */
            if(nlhs < 1)
                    mexErrMsgTxt("need one return value! (socket deskriptor)!\n");
            
            int fd = socket(PF_INET, SOCK_DGRAM, 0);
            if(fd == -1)
                    mexErrMsgTxt("error creating new INET/DGRAM socket!\n");
            fds.insert(fd);
            
            struct sockaddr_in sa = {AF_INET, 0, INADDR_ANY};
			socklen_t sa_len = sizeof(sa);
            if(bind(fd, (struct sockaddr*)&sa, sa_len)) {
                    printf("bind(%d): %s\n", fd, strerror(errno));
                    close(fd);
                    mexErrMsgTxt("error calling bind on newly created socket!\n");
            }
            if(getsockname(fd, (struct sockaddr*)&sa, &sa_len)) {
                    printf("getsockname(%d): %s\n", fd, strerror(errno));
                    close(fd);
                    mexErrMsgTxt("error calling bind on newly created socket!\n");
            }
			fprintf(stderr, "port: %d\n", ntohs(sa.sin_port));
            plhs[0] = mxCreateDoubleScalar(fd);

    } else if(command == "recv") {
            /* call recv on a previously created socket */
            if(nrhs < 2)
                    mexErrMsgTxt("recv needs a previously created socket deskriptor!\n");
            if(nlhs < 3)
                    mexErrMsgTxt("recv returns: [from_ip, from_port, packet]!\n");

            int fd = (int)*mxGetPr(prhs[1]);
            char buffer[RECEIVE_BUFFER];
            struct sockaddr_in from;
            socklen_t from_len = sizeof(from);
            ssize_t len = recvfrom(fd, buffer, RECEIVE_BUFFER - 1, 0, (struct sockaddr*)&from, &from_len);
            if(len == -1) {
                    printf("recv(): %s\n", strerror(errno));
                    mexErrMsgTxt("recv returns -1!\n");
            }
            char* from_ip = inet_ntoa(from.sin_addr);
            int from_port = ntohs(from.sin_port);
            plhs[0] = mxCreateString(from_ip);
            plhs[1] = mxCreateDoubleScalar(from_port);
            parse_packet(&plhs[2], buffer, len);

    } else if(command == "has_data") {
            /* see if there is data to read on this socket */
            if(nrhs < 2)
                    mexErrMsgTxt("has_data needs a previously created socket deskriptor!\n");
            if(nlhs < 1)
                    mexErrMsgTxt("has_data returns if there is data to read!\n");

            int fd = (int)*mxGetPr(prhs[1]);
            struct timeval timeout = { 0, 0 };
            fd_set readfds;
            FD_ZERO(&readfds);
            FD_SET(fd, &readfds);
            int n = select(fd + 1, &readfds, NULL, NULL, &timeout);
            plhs[0] = mxCreateDoubleScalar(n);

    } else if(command == "close_all") {
            /* close all opened socket */
            for(set<int>::iterator i = fds.begin(); i != fds.end(); i++)
                    close(*i);
            fds.clear();

    } else if(command == "close") {
            /* call close on a previously created socket */
            if(nrhs < 2)
                    mexErrMsgTxt("recv needs a previously created socket deskriptor!\n");

            int fd = (int)*mxGetPr(prhs[1]);
            close(fd);
            fds.erase(fd);

    } else if(command == "sendto") {
            /* call sendto on a previously created socket */
            if(nrhs < 2)
                    mexErrMsgTxt("sendto needs these arguments: [socket_deskriptor, target_ip, target_port, data]\n");
            if(nlhs < 1)
                    mexErrMsgTxt("sendto needs one return parameter: bytes_sent!\n");

            char target_ip[128];

            int fd = (int)*mxGetPr(prhs[1]);
            mxGetString(prhs[2], target_ip, 128);
            int target_port = (int)*mxGetPr(prhs[3]);

            struct sockaddr_in sa = { AF_INET, htons(target_port), INADDR_ANY };
            if(!inet_aton(target_ip, &sa.sin_addr))
                    mexErrMsgTxt("sendto invalid target_ip address specified!\n");
            
            char buffer[RECEIVE_BUFFER];
            int buffer_len = RECEIVE_BUFFER;
            buffer_len = generate_packet(prhs[4], buffer, buffer_len);
            ssize_t len = sendto(fd, buffer, buffer_len, 0, (struct sockaddr*)&sa, sizeof(sa));
            if(len == -1) {
                    printf("sendto(): %s\n", strerror(errno));
                    mexErrMsgTxt("sendto returns -1!\n");
            }
            plhs[0] = mxCreateDoubleScalar(len);
    } else {
            printf("unknown command %s\n", command.c_str());
    }
    
}
