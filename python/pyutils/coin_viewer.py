"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

__all__ = ["coin_node", "coin_viewer", "get_coin_viewer"]

from __future__ import print_function

import os
import fnmatch
import traceback

from numpy import *
from numpy.linalg import *

import gtk
from pivy.coin import *

import pyutils
import pyutils.inventor
import pyutils.SoPyGtkViewer

coord_file = os.path.join(os.path.dirname(__file__), "coord_015.iv")

def get_iv_source(node):
    tfn = "/dev/shm/out"
    out = SoOutput()
    out.openFile(tfn)
    out.setStage(SoOutput.COUNT_REFS)
    node.writeInstance(out)
    out.setStage(SoOutput.WRITE)
    node.writeInstance(out)
    out.closeFile()
    with open(tfn, "rb") as fp:
        return fp.read()

class coin_node(object):
    def __init__(self, name, parent, frame=eye(4)):
        self.name = name
        self.parent = parent
        self.root = self.parent.root
        self._already_added = set()
        self._transforms = {}
        self._manipulators = {}
        self._has_coord = False
        self.separator = SoSeparator()
        self.separator.setName(name)
        self.draw_style = None
        self.root.addChild(self.separator)
        self.add_transform("origin", frame)

    def _get_draw_style(self):
        if self.draw_style is None:
            self.draw_style = SoDrawStyle()
            self.separator.insertChild(self.draw_style, 0)
        return self.draw_style

    def hide(self):
        ds = self._get_draw_style()
        ds.style = SoDrawStyle.INVISIBLE

    def show(self, mode="filled"):
        ds = self._get_draw_style()
        if mode == "lines":
            ds.style = SoDrawStyle.LINES
        elif mode == "points":
            ds.style = SoDrawStyle.POINTS
        else: # filled
            ds.style = SoDrawStyle.FILLED

    def serialize(self):
        data = dict(
            name=self.name,
            childs=[]
        )
        for idx in range(self.separator.getNumChildren()):
            child = self.separator.getChild(idx)
            # is it one of our transforms?
            for tname, t in self._transforms.items():
                if child == t:
                    # yes!
                    frame = self.get_transform(tname)
                    data["childs"].append(("transform", tname, frame))
                    break
            else:
                # no
                for name, value in self.__dict__.items():
                    if not isinstance(value, SoBase):
                        continue
                    if value == child:
                        attr_name = name
                        break
                else:
                    attr_name = None
                src = get_iv_source(child)
                data["childs"].append(("node", attr_name, src))
        return data

    def unserialize(self, data):
        for child in data["childs"]:
            if child[0] == "node":
                attr_name = child[1]
                src = child[2]
                node = pyutils.inventor.load_string(src)
                node = node.getChild(0)
                if attr_name is not None:
                    self.__setattr__(attr_name, node)
                else:
                    self.add(node)
            elif child[0] == "transform":
                tname = child[1]
                frame = child[2]
                if tname in self._transforms:
                    self.set_transform(tname, frame)
                else:
                    self.add_transform(tname, frame)
            else:
                print("  unknown node child %r" % (child, ))

    def __contains__(self, name):
        return name in self.__dict__

    def load(self, fn):
        return pyutils.inventor.load(fn)

    def enable_coord(self):
        if not self._has_coord:
            coord = self.parent._get_coord()
            self.separator.insertChild(coord, 0)
            self._has_coord = True
    def disable_coord(self):
        if not self._has_coord:
            return
        self.separator.removeChild(self.parent._get_coord())
    def toggle_coord(self):
        if self._has_coord:
            self.disable_coord()
        else:
            self.enable_coord()

    def enable_manip(self, tname, mtype="Transformer"):
        manip_name = "So%sManip()" % mtype
        manip = eval(manip_name)
        transform_path = pyutils.inventor.get_path(self._transforms[tname], self.separator)
        manip.replaceNode(transform_path)
        self._manipulators[tname] = manip
    def disable_manip(self, tname):
        manip = self._manipulators[tname]
        manip_index = self.separator.findChild(manip)
        path = pyutils.inventor.get_path(manip, self.separator)
        manip.replaceManip(path, self._transforms[tname])
        del self._manipulators[tname]
        
    def add_transform(self, tname, frame=eye(4)):
        t = SoTransform()
        t.setMatrix(pyutils.inventor.get_ivmatrix(frame))
        setattr(self, "%s_transform" % tname, t)
        self._transforms[tname] = t
        return t
    def set_transform(self, tname, frame):
        self._transforms[tname].setMatrix(pyutils.inventor.get_ivmatrix(frame))
    def get_transform(self, tname):
        m = SbMatrix()
        if tname in self._manipulators:
            # manipulator is active!
            t = self._manipulators[tname]
        else:
            t = self._transforms[tname]
        m.setTransform(t.translation.getValue(), t.rotation.getValue(), SbVec3f(1, 1, 1))
        return array(m.getValue()).transpose()
        #return array(self._transforms[tname].matrix.getValue()).transpose()

    def add(self, node):
        if node in self._already_added:
            return
        self.separator.addChild(node)

    def __setattr__(self, name, value):
        had_sep = hasattr(self, "separator")
        self.__dict__[name] = value
        if had_sep and isinstance(value, SoBase):
            #print("adding new attr %r to sep!"% name, type(value), value)
            self.separator.addChild(value)
            self._already_added.add(value)
        #else:
        #    print("NOT! adding new attr %r to sep!" % name, type(value), value)
        

class coin_viewer(object):
    def __init__(self, name):
        global coord_file
        self.name = name

        #if hasattr(self.app.config, "coord_file"):
        #    coord_file = self.app.config.coord_file

        self.window = gtk.Window()
        self.window.connect("delete_event", self.on_delete)
        self.window.set_default_size(780, 580)
        self.viewer = pyutils.SoPyGtkViewer.SoPyGtkViewer()
        self.window.add(self.viewer)
        self.window.show_all()
        self.window.set_title("coin viewer 0x%x" % id(self))

        # popup menu
        m = self.popup_menu = gtk.Menu()
        self.pick_active = None
        self.pick_item = gtk.MenuItem("pick object")
        self.pick_item.connect("activate", self.on_pick)
        m.append(self.pick_item)
        self.unpick_item = gtk.MenuItem("UNpick object")
        self.unpick_item.connect("activate", self.on_unpick)
        m.append(self.unpick_item)
        #self.viewer.notify_popup = self.on_popup_menu
        self.popup_menu.show_all()

        # pick objects
        self.viewer.on_interact_button = self.on_interact_button

        self.root = SoSeparator()
        self.root.renderCaching = SoSeparator.OFF
        self.root.ref()
        self.viewer.setSceneGraph(self.root)
        self.childs = {}

        self.had_viewAll = False
        self.translation_enabled = True
        self.rotation_enabled = True

        self.translation_scale = 1
        self.rotation_scale = 1
        self.on_user_button = None


    def on_spacemouse(self, app, axes, buttons):
        pose = array(axes)
        if self.translation_enabled:
            t = 1./1000. * 0.2 * self.translation_scale / 10. / 2.
        else:
            t = 0
        if self.rotation_enabled:
            r = 1./1000. * self.rotation_scale / 5. / 10.
        else:
            r = 0
        #t = t / self.handler.fps * 25.
        #r = r / self.handler.fps * 25.
        pose[3], pose[4], pose[5] = pose[3], -pose[5], pose[4]
        T = pyutils.matrix.pose2tr(
            pose,
            translation_scale=t,
            rotation_scale=r)
        #R = T[0:3, 0:3]
        #T[0:3, 0:3] = R
        T[1, 3], T[2, 3] = -T[2, 3], T[1, 3]
        

        rot = self.viewer.camera.orientation.getValue()
        translation = eye(4)
        translation[0:3, 3] = rot.multVec(SbVec3f(T[0:3, 3])).getValue()
        rotation = eye(4)
        rotation[0:3, 0:3] = T[0:3, 0:3]
        
        m = SbMatrix(*list(rotation.transpose().flatten()))
        smrot = SbRotation(m)
        smrot = rot.inverse() * smrot * rot
        m.setRotate(smrot)
        m = array(m.getValue()).transpose()
        rotation[0:3, 0:3] = m[0:3, 0:3]

        trans = self.sm_frame[0:3, 3]
        rotated = dot(rotation, self.sm_frame)
        rotated[0:3, 3] = trans
        self.sm_frame = dot(translation, rotated)        
        self.sm_node.set_transform("origin", self.sm_frame)

        return True

    def enable_space_mouse_on(self, node_name):
        if node_name not in self.childs:
            raise Exception("nodes does not exist")
        self.sm_node = self.childs[node_name]
        self.sm_frame = self.sm_node.get_transform("origin")
        self.app.add_hook("sm_input", self.on_spacemouse, from_who="coin_viewer")

    def on_interact_button(self, ev):
        if ev.type != gtk.gdk.BUTTON_PRESS:
            return False # pass
        is_pick_button = ev.button == 1
        is_coord_button = ev.button == 2
        is_user_button = ev.button == 3

        rpaction = SoRayPickAction(self.viewer.getViewportRegion())
        ry = self.viewer.height - ev.y
        rpaction.setPoint(SbVec2s(int(ev.x), int(ry)))
        rpaction.setRadius(2)
        rpaction.apply(self.viewer.sceneroot)
        picked = rpaction.getPickedPoint()
        if not picked:
            if is_pick_button and self.pick_active:
                self.pick_active.disable_manip("origin")
                self.pick_active = None
            return False # pass event
        hitpoint = picked.getPoint()
        if is_pick_button and self.pick_active:
            return False
        path = picked.getPath()
        second = path.getNode(2)
        # search matching node
        for name, node in self.childs.items():
            if node.separator == second:
                if is_pick_button:
                    node.enable_manip("origin", "TransformBox")
                    self.pick_active = node
                if is_user_button:                    
                    point = array(picked.getPoint())
                    normal = array(picked.getNormal())
                    obji = inv(node.get_transform("origin"))
                    opoint = dot(obji[:3, :3], point) + obji[:3, 3]
                    onormal = dot(obji[:3, :3], normal)
                    if self.on_user_button is not None:
                        try:
                            self.on_user_button(
                                node, 
                                opoint,
                                onormal)
                        except:
                            print("got exception calling on_user_button %r:\n%s" % (self.on_user_button, traceback.format_exc()))
                    return True
                if is_coord_button:
                    node.toggle_coord()
                return True # handled
        return False

    def on_pick(self, item):
        print("pick")
    def on_unpick(self, item):
        print("UNpick")        
    def on_popup_menu(self, ev):
        self.unpick_item.set_sensitive(self.pick_active is not None)
        self.popup_menu.popup(None, None, None, ev.button, ev.time)

    def present(self):
        self.window.present()        

    def on_delete(self, window, ev):
        self.window.hide()
        del self.window
        del self.viewer
        del _coin_viewers[self.name]
        return True

    def get_node(self, node_name, frame=eye(4)):
        if node_name in self.childs:
            return self.childs[node_name]
        n = coin_node(node_name, self, frame)
        self.childs[node_name] = n
        return n
        
    def viewAll_once(self):
        if self.had_viewAll:
            return
        self.had_viewAll = True
        self.viewer.viewAll()

    def _get_coord(self):
        if not hasattr(self, "coord"):
            self.coord = pyutils.inventor.load(coord_file)
            self.coord.ref()
        return self.coord

    def place_coord(self, name, frame, scale=1):
        coord_name = "coord_%s" % name
        node = self.get_node(coord_name, frame)
        if not hasattr(node, "coord"):
            node.scale = SoScale()
            node.coord = self._get_coord()
        else:
            node.set_transform("origin", frame)
        node.scale.scaleFactor = scale, scale, scale

    def place_plane(self, name, point, normal, color=(1, 0, 0, 0.1), extends=(1, 1)):
        coord_name = "plane_%s" % name

        point = array(point)
        normal = array(normal) / norm(normal)

        xdir = array([1, 0, 0])
        ydir = array([0, 1, 0])

        if dot(normal, xdir) < dot(normal, ydir):
            dir1 = xdir
        else:
            dir1 = ydir

        dir2 = cross(normal, dir1)
        dir2 /= norm(dir2)

        dir1 = cross(dir2, normal)
        dir1 /= norm(dir1)

        ext = array(extends, dtype=float) / 2.0

        ext1 = ext[0] * dir1
        ext2 = ext[1] * dir2
        
        points = [
           point + ext1 + ext2,
           point - ext1 + ext2,
           point - ext1 - ext2,
           point + ext1 - ext2,
           ]
        points = array(points).tolist()
        node = self.get_node(coord_name)
        first = False
        if "coords" not in node:
            first = True
            node.coords = SoCoordinate3()
            node.mat = SoMaterial()
            node.hints = SoShapeHints()
            node.hints.vertexOrdering = SoShapeHints.CLOCKWISE #  or COUNTERCLOCKWISE,
            node.hints.shapeType = SoShapeHints.UNKNOWN_SHAPE_TYPE
            node.ifs = SoIndexedFaceSet()
            
        node.mat.diffuseColor = color[:3]
        if len(color) > 3:
            node.mat.transparency = 1 - color[3] # alpha to transparency!
        else:
            node.mat.transparency = 0
        
        node.coords.point.setValues(points)

        if first:
            # set ifs
            node.ifs.coordIndex.setValues([
                0, 1, 2, 3, -1])

    def place_arrow(self, name, a, b, color=None, arrow_len=None):
        arrow_name = "arrow_%s" % name
        node = self.get_node(arrow_name)
        direction = b[:3] - a[:3]
        l = norm(direction)

        if "transform" not in node:
            if color:
                node.m = SoMaterial()
                node.add(node.m)

            node.transform = SoMatrixTransform()
            node.add(node.transform)

            node.sc = SoCoordinate3()
            node.add(node.sc)
            node.ifs = SoIndexedLineSet()
            node.add(node.ifs)
            node.t = SoTranslation()
            node.add(node.t)
            node.r = SoRotation()
            node.add(node.r)
            node.cone = SoCone()
            node.add(node.cone)

        if color is not None:
            node.m.diffuseColor.setValue(color)
        node.sc.point.setValues(0, 2, [(0, 0, 0), direction])
        node.ifs.coordIndex.setValues(0, 2, [0, 1])
        if arrow_len is None:
            arrow_len = 0.3 * l
            f = 0.15
        else:
            f = (arrow_len / l) / 2.
        node.cone.height = arrow_len
        node.cone.bottomRadius = arrow_len / 6.
        nl = l - (node.cone.height.getValue() / 2)
        f = nl / l
        node.t.translation = direction * f
        def get_rotation(v):
            rot = pyutils.matrix.get_any_rotation_from_vector(v)
            m = SbMatrix(*list(rot.transpose().flatten()))
            return SbRotation(m)
        node.r.rotation.setValue(get_rotation(direction))
        
        start_frame = eye(4)
        start_frame[:3, 3] = a[:3]
        node.transform.matrix = pyutils.inventor.get_ivmatrix(start_frame)

    def show_points(self, name, points, point_size=1, color=(1, 1, 1), trafo=None):
        root = self.get_node("points_%s" % name)
        if "c3" not in root:
            root.transform = SoMatrixTransform()
            root.c3 = SoCoordinate3()
            root.ds = SoDrawStyle()
            root.mat = SoMaterial()
            root.mb = SoMaterialBinding()
            root.ps = SoPointSet()
        root.c3.point.deleteValues(0)
        root.c3.point.setValues(points)
        root.ds.pointSize = point_size
        if len(color) == 3 or len(color) != len(points):
            root.mat.diffuseColor = color
            root.mb.value = SoMaterialBinding.OVERALL
        else:
            root.mat.diffuseColor.setValues(0, len(color), color)
            root.mb.value = SoMaterialBinding.PER_VERTEX
        if trafo is None:
            trafo = eye(4)
        root.transform.matrix = pyutils.inventor.get_ivmatrix(trafo)
        return root

    def clear(self, pattern=None):
        to_del = []
        for node_name, node in self.childs.items():
            if pattern is None or fnmatch.fnmatch(node_name, pattern):
                if self.pick_active == node:
                    self.pick_active = None
                to_del.append(node_name)
        for node_name in to_del:
            self.root.removeChild(self.childs[node_name].separator)
            del self.childs[node_name]

    def serialize(self):
        data = dict(
            nodes=[],
            camera=get_iv_source(self.viewer.camera)
        )        
        for idx in range(self.root.getNumChildren()):
            child = self.root.getChild(idx)
            for node_name, node in self.childs.items():
                if node.separator == child:
                    break
            else:
                continue
            data["nodes"].append((node_name, node.serialize()))
        return data

    def set_camera(self, frame):
        sbm = pyutils.inventor.get_ivmatrix(frame)
        self.viewer.camera.position.setValue(frame[:3, 3])
        self.viewer.camera.orientation.setValue(SbRotation(sbm))

    def unserialize(self, data):
        node = pyutils.inventor.load_string(data["camera"])
        camera = node.getChild(0)
        self.viewer.camera.position.setValue(camera.position)
        self.viewer.camera.orientation.setValue(camera.orientation)
        self.viewer.camera.focalDistance = camera.focalDistance

        for node_name, node_data in data["nodes"]:
            n = coin_node(node_name, self)
            n.unserialize(node_data)
            self.childs[node_name] = n
        
    def save(self, fn):
        self.app.save(fn, self.serialize())
    def load(self, fn):
        self.unserialize(self.app.load(fn))

        
_coin_viewers = {}
def get_coin_viewer(name):
    if name in _coin_viewers:
        return _coin_viewers[name]
    v = coin_viewer(name)
    _coin_viewers[name] = v
    return v 
