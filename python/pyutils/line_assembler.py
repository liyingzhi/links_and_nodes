"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""
import io

class line_assembler(object):
    """
    use to receive parts or (multiple-) complete lines and either call
    `callback` on each line or collect them in `self.complete_lines` which can
    be retrieved (polling) via `get_line()` (check `has_complete_line()` before)

    call .write() to feed data.

    can be used with bytes(py2 strings) or strings(py2 unicode, the default).
    call `self.expect_bytes()` to use bytes.
    """
    def __init__(self, callback, *args, **kwargs):
        self.callback = callback
        if "max_length" in kwargs:
            self.max_length = kwargs["max_length"]
        else:
            self.max_length = None
        self.args = args
        self.expect_bytes(False)

    def expect_bytes(self, expect_bytes=True):
        if expect_bytes:
            self.line_end = b"\n"
            self.collector_class = io.BytesIO
        else:
            self.line_end = "\n"
            self.collector_class = io.StringIO
        self.reset()

    def reset(self):
        self.incomplete_line = self.collector_class()
        self.complete_lines = []
        self.rest = None
        
    def write(self, data):
        p = 0
        self.data = data
        while p is not None and p < len(data):
            np = data.find(self.line_end, p)
            self.incomplete = False
            if np == -1:
                # this line is not completed                                         
                this_part = data[p:]
                if self.max_length is None or (len(self.incomplete_line.tell()) + len(this_part)) < self.max_length:
                    self.incomplete_line.write(this_part)
                    return # wait for more data                                      
                self.incomplete = True
            # got complete line from p to np or maxlength exceeded
            self.rest = np + 1
            if self.callback is None:
                self.incomplete_line.write(data[p:np])
                self.complete_lines.append(self.incomplete_line.getvalue())
            else:
                self.incomplete_line.write(data[p:np])
                if self.callback(self.incomplete_line.getvalue(), *self.args) is False:
                    #self.incomplete_line = ""
                    self.incomplete_line.seek(0)
                    self.incomplete_line.truncate()
                    break
            self.incomplete_line.seek(0)
            self.incomplete_line.truncate()
            p = self.rest

    def has_complete_line(self):
        return len(self.complete_lines)

    def get_line(self):
        return self.complete_lines.pop(0)

    def pop_incomplete_line(self):
        ret = self.incomplete_line.getvalue()
        self.incomplete_line.seek(0)
        self.incomplete_line.truncate()
        return ret

    def get_rest(self, max_len=None):
        bytes_left = len(self.data) - self.rest
        if max_len is None or bytes_left <= max_len:
            ret = self.data[self.rest:]
            self.rest = None
            return ret
        ret = self.data[self.rest:self.rest + max_len]
        self.rest += len(ret)
        return ret
    def has_rest(self):
        return self.rest is not None and self.rest < len(self.data)

