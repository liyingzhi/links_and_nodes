"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""

from __future__ import print_function

from pivy.coin import *
from numpy import *
from numpy.linalg import *
import pyutils
import pyutils.matrix
#import pyutils.coin_get_points
#import coin_get_points
#import qhull_interface

def load(fn):
    """
    read the inventor file fn and return the resulting node
    """
    sceneInput = SoInput()
    if not sceneInput.openFile(fn):
        raise ValueError("could not open %r" % fn)
    node = SoDB.readAll(sceneInput)
    if node == None:
        raise ValueError("error reading from %r" % fn)
    return node

def load_string(data):
    """
    read the inventor data and return the resulting node
    """
    sceneInput = SoInput()
    sceneInput.setBuffer(data)
    node = SoDB.readAll(sceneInput)
    if node == None:
        raise ValueError("error reading from data:\n%s" % data)
    return node

def save(fn, obj, binary=False, compression=False, remove_refs=False, precission=7):
    """
    write out the inventor node obj and all its children to the file named
    fn
    """
    out = SoOutput()
    if fn is not None:
        out.openFile(fn)
    out.setBinary(binary)
    if precission is not None:
        out.setFloatPrecision(precission)

    if compression:
        out.setCompression("BZIP2", 1.0)
        #out.setCompression("GZIP", 1.0)
    out.setStage(SoOutput.COUNT_REFS)

    if remove_refs:
        obj_copy = obj.copy(False)
        obj_copy.ref()
    else:
        obj_copy = obj

    obj_copy.writeInstance(out)
    out.setStage(SoOutput.WRITE)
    obj_copy.writeInstance(out)
    out.closeFile()
    if remove_refs:
        obj_copy.unref()

def save_vrml2(fn, obj, do_convert=True, binary=False):
    """
    write complete scene to vrml2.0/97
    """
    if do_convert:
        tovrml2 = SoToVRML2Action()
        tovrml2.apply(obj)
        new_root = tovrml2.getVRML2SceneGraph()
        new_root.ref()
    else:
        new_root = obj
        new_root.ref()
    
    out = SoOutput()
    if fn is not None:
        out.openFile(fn)
    out.setBinary(binary)
    #out.setCompact(True)
    out.setHeaderString("#VRML V2.0 utf8")
    wra = SoWriteAction(out)
    wra.apply(new_root)
    #out.setFloatPrecision(8)
    #out.setStage(SoOutput.COUNT_REFS)
    
    #obj.writeInstance(out)
    #out.setStage(SoOutput.WRITE)
    #obj.writeInstance(out)
    out.closeFile()
    new_root.unref()
    
def save_vrml1(fn, obj):
    """
    write complete scene to vrml 1.0
    """
    tovrml2 = SoToVRML2Action()
    tovrml2.apply(obj)
    new_root2 = tovrml2.getVRML2SceneGraph()
    new_root2.ref()

    tovrml = SoToVRMLAction()
    tovrml.apply(new_root2)
    new_root = tovrml.getVRMLSceneGraph()
    new_root.ref()
    new_root2.unref()
    
    out = SoOutput()
    if fn is not None:
        out.openFile(fn)
    #out.setBinary(True)
    out.setHeaderString("#VRML V1.0 ascii")
    wra = SoWriteAction(out)
    wra.apply(new_root)
    #out.setFloatPrecision(8)
    #out.setStage(SoOutput.COUNT_REFS)
    
    #obj.writeInstance(out)
    #out.setStage(SoOutput.WRITE)
    #obj.writeInstance(out)
    out.closeFile()
    new_root.unref()
    

def save_dxf(fn, obj, vpr):
    """
    write complete scene to dxf
    """
    import sdxf
    d = sdxf.Drawing()
    #triangles = pyutils._coin_util.get_triangles(obj)
    blocks = [0]
    color = [0]
    def traverse(parent, base=None, path=None, into=None):
        if path is None:
            type_name = str(parent.getClassTypeId().getName())
            path = [type_name]
        if base is not None:
            start_base = base
        else:
            start_base = eye(4)
            
        base = start_base
        n = parent.getNumChildren()
        getmatrixaction = SoGetMatrixAction(vpr)
        need_triangle_callback = False
        for i, child in enumerate(map(parent.getChild, range(n))):
            type_name = str(child.getClassTypeId().getName())
            if type_name in ("Info", ):
                continue # completely ignore
            getmatrixaction.apply(child) # add to local transformation matrix
            T = array(getmatrixaction.getMatrix().getValue()).transpose()
            base = dot(base, T)
            if type_name in ("Separator", "Group"):
                new_path = list(path)
                new_path.append("%s:%d" % (type_name, i))
                if hasattr(child, "name"):
                    n = child.name.getValue()
                else:
                    n = "block_%d" % blocks[0]
                blocks[0] += 1
                new_block = sdxf.Block(n)
                into.append(new_block)
                traverse(child, base, new_path, into=new_block)
                continue
            if type_name in (
                "File", "Material",
                "Transform", "Translation",
                "RotationXYZ", "MatrixTransform",
                "Scale", "ShapeHints", "Normal",
                "NormalBinding"): # todo: scale
                continue
            color[0] = (color[0] + 1) % 16
            triangles = pyutils._coin_util.get_triangles(child)
            for t in triangles:
                ti = empty((3, 4))
                ti[:, 0:3] = t
                ti[:, 3] = 1
                for n in range(3):
                    ti[n, :] = dot(base, ti[n, :])
                ti = ti[0:3, 0:3]
                f = sdxf.Face(ti, color=color[0])
                into.append(f)
            a = SoCallbackAction()
            def output_line(user_data, action, v1, v2):
                p1 = v1.getPoint()
                p2 = v2.getPoint()
                ti = empty((2, 4))
                ti[0, 0:3] = p1[0], p1[1], p1[2] 
                ti[1, 0:3] = p2[0], p2[1], p2[2]
                ti[:, 3] = 1
                for n in range(2):
                    ti[n, :] = dot(base, ti[n, :])
                ti = ti[0:3, 0:3]
                l = sdxf.Line(ti)
                into.append(l)
                # list(v1.getPoint()), list(v2.getPoint())
            a.addLineSegmentCallback(SoIndexedLineSet.getClassTypeId(), output_line, None)
            a.apply(parent)
        return
        
    traverse(obj, into=d)
    d.saveas(fn)

def get_object_at(M, obj, scale=1, return_transform=False):
    p = SoSeparator()

    

    #T = SoMatrixTransform()
    #T.matrix.setValue(*list(M.transpose().flatten()))

    if scale != 1 or return_transform:
        T = SoTransform()
        T.setMatrix(SbMatrix(*list(M.transpose().flatten())))
        T.scaleFactor.setValue(scale, scale, scale)
    else:
        T = SoMatrixTransform()
        T.matrix = pyutils.inventor.get_ivmatrix(M)

    p.addChild(T)
    p.addChild(obj)
    if return_transform:
        return p, T
    return p
    
def get_line_set(lines):
    p = SoSeparator()
    vertices = SoLineSet()
    myVertexProperty = SoVertexProperty()
    num_vertices = [0] * len(lines)
    all_vertices = []
    for i, line in enumerate(lines):
        num_vertices[i] = len(line)
        all_vertices.extend(line)
    myVertexProperty.vertex.setValues(0, len(all_vertices), all_vertices)
    vertices.vertexProperty = myVertexProperty 
    vertices.numVertices.setValues(0, len(lines), num_vertices)
    p.addChild(vertices)
    return p

def get_matrix(m):
    """
    read values from inventor matrix M and store them correctly
    transposed into a new numpy array
    """
    return array(m.getValue()).transpose()

def get_ivmatrix(m):
    return SbMatrix(*list(array(m, dtype=float, copy=False).transpose().flatten()))
    
def get_path(n, base):
    """
    search the node n starting at node base and return its path
    """
    searchaction = SoSearchAction()
    searchaction.reset()
    searchaction.setNode(n)
    searchaction.apply(base)
    p = searchaction.getPath()
    if p is None:
        raise Exception("path not found!")
    p.ref()
    return p

def get_path_matrix(path, ign_det=False):
    """
    return transform matrix accumulated by traversing path
    """
    getmatrixaction = SoGetMatrixAction(SbViewportRegion())
    getmatrixaction.apply(path)
    m = getmatrixaction.getMatrix() 
    gpm = get_matrix(m)
    if abs(det(gpm) - 1) > 1e-5:
        print("warning: get_path_matrix() returning det of %.7f %.7f -- normlizing..." % (
            det(gpm), m.det3()))
        if not ign_det and abs(det(gpm) - 1) > 1e-2:
            print(gpm)
            raise Exception("determinant far away from 1!")
        gpm = pyutils.matrix.normalize_matrix(gpm)
    return gpm

def get_shape(triangles, M=None, return_transform=False):
    s = SoSeparator()
    s.ref()
    if M is not None:
        t = SoMatrixTransform()
        t.matrix = pyutils.inventor.get_ivmatrix(M)
        s.addChild(t)
    sc = SoCoordinate3()
    s.addChild(sc)
    ifs = SoIndexedFaceSet()
    s.addChild(ifs)
    pyutils._coin_util.get_shape(triangles, sc, ifs)
    if return_transform:
        return s, t
    return s

def get_shape_full(vertices, faces, normals=None, per_vertex_normals=False, color=None, M=None, return_transform=False):
    sep = SoSeparator()
    sep.ref()

    if M is not None:
        t = SoMatrixTransform()
        t.matrix = pyutils.inventor.get_ivmatrix(M)
        sep.addChild(t)
    
    if normals is not None:
        sn = SoNormal()
        for i, n in enumerate(normals):
            sn.vector.set1Value(i, n[0], n[1], n[2])
        sep.addChild(sn)
        
        nb = SoNormalBinding()
        if per_vertex_normals:
            #nb.value = SoNormalBinding.PER_VERTEX
            nb.value = SoNormalBinding.PER_VERTEX_INDEXED
        else:
            nb.value = SoNormalBinding.PER_FACE
            
        #nb.value = SoNormalBinding.PER_FACE_INDEXED
        sep.addChild(nb)
        
    sc = SoCoordinate3()
    sc.point.setValues(vertices)
    sep.addChild(sc)
    if color is not None:
        mat = SoMaterial()
        mat.diffuseColor = color
        sep.addChild(mat)
    
    #sh = SoShapeHints()
    #sh.vertexOrdering = SoShapeHints.UNKNOWN_ORDERING
    #sh.shapeType = SoShapeHints.UNKNOWN_SHAPE_TYPE
    #sh.faceType = SoShapeHints.UNKNOWN_FACE_TYPE
    ##sh.reaseAngle = 360. / 180. * pi
    #sep.addChild(sh)
    
    fs = SoIndexedFaceSet()
    sep.addChild(fs)
    k = 0
    for i, face in enumerate(faces):
        for vid in face:
            vid = int(vid)
            fs.coordIndex.set1Value(k, vid)
            k += 1
        fs.coordIndex.set1Value(k, -1)
        k += 1
    if return_transform and M is not None:
        return sep, t
    return sep

def get_shape_from_points(points, M=None, return_transform=False, color=None, return_coordinates=False, point_size=None):
    s = SoSeparator()
    s.ref()
    if color is not None and len(color) == 3:
        m = SoMaterial()
        m.diffuseColor.setValue(color)
        s.addChild(m)
    elif color is not None and len(color) == len(points):
        m = SoMaterial()
        m.diffuseColor.setValues(color)
        s.addChild(m)
        mb = SoMaterialBinding()
        mb.value = SoMaterialBinding.PER_VERTEX
        s.addChild(mb)
    if M is not None:
        t = SoMatrixTransform()
        t.matrix = pyutils.inventor.get_ivmatrix(M)
        s.addChild(t)
    sc = SoCoordinate3()
    s.addChild(sc)
    if point_size is not None:
        ds = SoDrawStyle()
        s.addChild(ds)
        ds.pointSize = point_size
    ps = SoPointSet()
    s.addChild(ps)
    sc.point.setValues(points)
    if not return_coordinates:
        if return_transform:
            return s, t
        return s
    if return_transform:
        return s, t, sc
    return s, sc

def get_arrow(from_M, to_M, arrow_len=None, color=None):
    if len(from_M.shape) == 1:
        print("from_M shape")
        print(from_M)
        M = eye(4)
        M[0:3, 3] = from_M[0:3]
        from_M = M
    if len(to_M.shape) == 1:
        print("to_M shape")
        print(from_M)
        M = eye(4)
        M[0:3, 3] = to_M[0:3]
        to_M = M
        
    frame = dot(inv(from_M), to_M)
    direction = frame[0:3, 3]
    l = norm(direction)
    
    def get_rotation(v):
        rot = pyutils.matrix.get_any_rotation_from_vector(v)
        m = SbMatrix(*list(rot.transpose().flatten()))
        return SbRotation(m)

    arrow = SoSeparator()
    if color:
        m = SoMaterial()
        m.diffuseColor.setValue(color)
        arrow.addChild(m)
    
    sc = SoCoordinate3()
    sc.point.setValues(0, 2, [(0, 0, 0), direction])
    ifs = SoIndexedLineSet()
    ifs.coordIndex.setValues(0, 2, [0, 1])

    if arrow_len is None:
        arrow_len = 0.3 * l
        f = 0.15
    else:
        f = (arrow_len / l) / 2.
    cone = SoCone()
    cone.height = arrow_len
    cone.bottomRadius = arrow_len / 6.

    t = SoTranslation()
    nl = l - (cone.height.getValue() / 2)
    f = nl / l
    t.translation = direction * f
    r = SoRotation()
    r.rotation.setValue(get_rotation(direction))

    arrow.addChild(sc)
    arrow.addChild(ifs)
    arrow.addChild(t)
    arrow.addChild(r)
    arrow.addChild(cone)

    node, transform = pyutils.inventor.get_object_at(
        from_M, arrow, return_transform=True)
    return node

def get_normal_arrows(normals, M, length, return_transform=False, color=None, show_triangle=False):
    s = SoSeparator()
    s.ref()
    if M is not None:
        t = SoMatrixTransform()
        t.matrix = pyutils.inventor.get_ivmatrix(M)
        s.addChild(t)
    if color:
        m = SoMaterial()
        m.diffuseColor.setValue(color)
        s.addChild(m)
    from_M = eye(4)
    to_M = eye(4)
    for n, p, v in normals:
        from_M[0:3, 3] = p
        to_M[0:3, 3] = p + n * length
        s.addChild(
            get_arrow(from_M, to_M))
        if not show_triangle:
            continue
        to_M[0:3, 3] = v[0]
        s.addChild(
            get_arrow(from_M, to_M))
        from_M[0:3, 3] = v[0]
        to_M[0:3, 3] = v[1]
        s.addChild(
            get_arrow(from_M, to_M))
        from_M[0:3, 3] = v[1]
        to_M[0:3, 3] = v[2]
        s.addChild(
            get_arrow(from_M, to_M))
    if return_transform:
        return s, t
    return s
    

def get_normals_from_triangles(triangles, order=(0, 1, 2)):
    normals = []
    for v in triangles:
        v = list(map(array, v))
        a = v[order[1]] - v[order[0]]
        b = v[order[2]] - v[order[1]]
        n = cross(a, b)
        n = n / norm(n)
        m = (v[0] + v[1] + v[2]) / 3.
        normals.append((n, m, v))
    return normals

def ball_at(M, r, c=None, ic=None):
    s = SoSeparator()
    T = SoTransform()
    T.setMatrix(SbMatrix(*list(M.transpose().flatten())))
    s.addChild(T)
    if c is not None and ic is None:
        s.addChild(get_material(c))
    if ic is not None:
        s.addChild(ic)
    cube = SoSphere()
    cube.radius = r
    s.addChild(cube)
    return s

def convert_rv(data):
    return data.replace("RvObject", "Separator")

def create_gts_surface(vertices, faces, per_face_normals=None, per_vertex_normals=False):
    import gts
    if per_vertex_normals:
        per_face_normals = None

    s = gts.Surface()
    v = [gts.Vertex(*list(v)) for v in vertices]
    
    if per_face_normals is not None:
        for i, (f, n) in enumerate(zip(faces, per_face_normals)):
            edges = []
            v1 = vertices[f[0]]
            v2 = vertices[f[1]]
            v3 = vertices[f[2]]
            for k in range(3):
                edges.append(
                    gts.Edge(v[f[k]], v[f[(k+1)%3]]))
            face = gts.Face(*edges)

            mn = cross(v2-v1, v3-v2)
            mn /= norm(mn)
            d = dot(n, mn)
            if d < 0:
                face.revert()
            s.add(face)
    else:
        for i, f in enumerate(faces):
            edges = []
            v1 = vertices[f[0]]
            v2 = vertices[f[1]]
            v3 = vertices[f[2]]
            for k in range(3):
                edges.append(
                    gts.Edge(v[f[k]], v[f[(k+1)%3]]))
            face = gts.Face(*edges)
            s.add(face)
    return s

def get_shape_from_gts_surface(s, color=None):
    sep = SoSeparator()
    sep.ref()
    vertices = s.vertices()
    sc = SoCoordinate3()
    for i, vtx in enumerate(vertices):
        sc.point.set1Value(i, vtx.x, vtx.y, vtx.z)
    sep.addChild(sc)
    if color is not None:
        mat = SoMaterial()
        mat.diffuseColor = color
        sep.addChild(mat)
    
    fs = SoIndexedFaceSet()
    sep.addChild(fs)
    k = 0
    for i, face in enumerate(s.face_indices(vertices)):
        for vid in face:
            fs.coordIndex.set1Value(k, vid)
            k += 1
        fs.coordIndex.set1Value(k, -1)
        k += 1
    return sep

def create_convex_hull(node, simplify_to_max_edges=None, scale=None, color=None, per_vertex_normals=False):
    """
    expects inventor node or points array
    optionally reduces edge-count
    optionally adds color
    returns inventor node
    """
    if node.__class__.__name__ == "ndarray":
        points = node
    else:
        # get vertices
        points = pyutils.coin_get_points.get_points(node)
    # get convex hull
    import qhull_interface
    hull_vertices, hull_faces, hull_normals = qhull_interface.compute_convex_hull(points, per_vertex_normals=per_vertex_normals)
    if simplify_to_max_edges is not None or scale is not None:
        # create gts surface from hull, simplify and return inventor shape
        # create gts surface    
        s = create_gts_surface(hull_vertices, hull_faces, hull_normals)
        # simplify gts surface
        if simplify_to_max_edges is not None:
            s.coarsen(simplify_to_max_edges)
        if scale is not None:
            s.scale(scale, scale, scale)
        verts = []
        for p in s.vertices():
            verts.append(array([p.x, p.y, p.z]))
        verts = array(verts)
        # re-create convex hull after simpificytion
        hull_vertices, hull_faces, hull_normals = pyutils.qhull_interface.compute_convex_hull(verts, per_vertex_normals=per_vertex_normals)
    sep = get_shape_full(hull_vertices, hull_faces, hull_normals, color=color, per_vertex_normals=per_vertex_normals)
    return sep
