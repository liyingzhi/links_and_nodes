#!/usr/bin/python
"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt
"""


from __future__ import print_function

import os
import traceback

from numpy import *

import gtk # todo: migrate to Gtk3
import gobject
import gtk.gtkgl
import gtk.gdkgl

import OpenGL
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
try:
    from OpenGL.arrays import vbo
    use_vbo = True
    print("have VBO's")
except:
    print("will not use OpenGL.arrays.vbo because they can't be imported.")
    use_vbo = False
use_vbo = False
had_glut_init = False
    
import links_and_nodes as ln

class pyscope(gtk.VBox):
    colors = [
        (0, 0, 1),
        (0, 1, 0),
        (1, 0, 0),
        
        (0, 0.75, 0.75),
        (0.75, 0, 0.75),
        (0.75, 0.75, 0),

        (0.25, 0.25, 0.25),

    ]
    def __init__(self, fps=30, draw_timescale=True):
        gtk.VBox.__init__(self)
        self.have_xml = False

        self.draw_timescale = draw_timescale
        self.draw_legend = False
        self.legends = None

        self.xml = gtk.Builder()
        ui_fn = os.path.join(os.path.dirname(__file__), "pyscope.ui")
        self.xml.add_from_file(ui_fn)
        self.xml.connect_signals(self)
        self.have_xml = True
        
        mbox = self.xml.get_object("main_vbox")
        mbox.reparent(self)
        #first_child = mbox.get_children()[0]
        #first_child.unparent()
        #self.pack_start(first_child, False, False)
        
        self.da = da = gtk.gtkgl.widget.DrawingArea()
        display_mode = gtk.gdkgl.MODE_RGB | gtk.gdkgl.MODE_DOUBLE
        config = gtk.gdkgl.Config(mode=display_mode)
        share_list = None
        direct = True
        render_type = gtk.gdkgl.RGBA_TYPE
        da.set_gl_capability(config, share_list, direct, render_type)
        
        self.main_ev.add(da)
        da.connect("configure_event", self.on_configure)
        da.connect("expose_event", self.on_expose)

        self.seconds = 3
        self.seconds = float(self.seconds)
        self.seconds_adj.set_value(self.seconds)

        self.rate = 100
        self.is_array = False
        self.array_shape = (1, )
        self.n_values = int(ceil(float(self.seconds) * self.rate))
        self.value_storage = zeros((self.n_values, 2), dtype=float)
        self.new_storage = True
        self.storage_cursor = 0
        self.start_ts = None
        self.start_cursor = 0

        self.display_rate = fps
        self.keep_running = True
        self.back_time = 30
        
        self.timer = None
        self.do_auto_scale = True
        self.value_min = -1
        self.value_max = 1
        self.scale_changed()
        self.do_hold = False
        self.seconds_changed()
        self.first_ctx = True
        self.is_scale_changed = False
        self.reset_start_cursor = False
        self.verts = []
        self.expect_array((1,), set_is_array=False)
        
    def expect_array(self, shape, set_is_array=True):
        if len(shape) > 1:
            raise Exception("currently only 1d-arrays are possible! your shape is %r" % (shape, ))
        if set_is_array:
            self.is_array = True
        self.array_shape = shape
        
        self.value_storage = []
        self.storage_full = False
        for i in range(self.array_shape[0]):
            self.value_storage.append(zeros((self.n_values, 2), dtype=float32))
        self.new_storage = True
        self.storage_cursor = 0
        self.start_ts = None
        self.start_cursor = 0

        
    def __getattr__(self, name):
        if not self.have_xml:
            raise AttributeError(name)
        widget = self.xml.get_object(name)
        if widget is None:
            raise AttributeError(name)
        setattr(self, name, widget)
        return widget

    def push_data(self, value, timestamp):
        if self.do_hold:
            return True
        if self.do_auto_scale:
            if self.is_array:
                maxv = max(value)
                minv = min(value)
            else:
                maxv = value
                minv = value
            if self.value_max is None or maxv > self.value_max:
                self.value_max = maxv
                self.is_scale_changed = True
            if self.value_min is None or minv < self.value_min:
                self.value_min = minv
                self.is_scale_changed = True
            
        if self.start_ts is None:
            self.start_ts = timestamp

         # 35%
        ts = timestamp - self.start_ts
        for i in range(self.array_shape[0]):
            self.value_storage[i][self.storage_cursor, 0] = ts
            if self.is_array:
                self.value_storage[i][self.storage_cursor, 1] = value[i]
            else:
                self.value_storage[i][self.storage_cursor, 1] = value
        self.storage_cursor += 1
        if self.storage_cursor == self.n_values:
            self.storage_full = True
            self.storage_cursor = 0
        if self.storage_full and self.storage_cursor == self.start_cursor:
            print("reached start cursor at %4d - buffer too small to hold time range of %.2f seconds. enlarge to %d entries!" % (self.start_cursor, self.seconds, self.n_values * 2))
            # double buffer!
            old_len = self.n_values
            self.n_values *= 2
            before_end = old_len - self.start_cursor
            for i in range(self.array_shape[0]):
                self.value_storage[i] = resize(self.value_storage[i], (self.n_values, 2))
                self.value_storage[i][-before_end:] = self.value_storage[i][self.start_cursor:old_len]
            self.start_cursor = self.n_values - before_end
            self.new_storage = True

        self.request_refresh = True
        return True

    def _dump(self):
        print("start_ts: %f" % self.start_ts)
        print("start_cursor %d" % self.start_cursor)
        print("storage_cursor: %d" % self.storage_cursor)
        print("back_time: %f" % self.back_time)
        print(self.value_storage)

    def on_display_timeout(self):
        if not self.request_refresh:
            return self.keep_running
        self.request_refresh = False
        self.da.window.invalidate_rect(self.da.allocation, False)
        #self.da.window.process_updates(False)
        return self.keep_running

    def on_configure(self, widget, event):
        glcontext = widget.get_gl_context()
        gldrawable = widget.get_gl_drawable()
        if not gldrawable.gl_begin(glcontext):
            return
        self.reshape(widget.allocation.width, widget.allocation.height)
        self.new_storage = True

        gldrawable.gl_end()
        if self.timer is None:
            self.timer = gobject.timeout_add(int(ceil(1000 / self.display_rate)), self.on_display_timeout)

        return True

    def on_expose(self, widget, ev):
        try:
            self.on_real_expose(widget, ev)
        except:
            traceback.print_exc()
            self.keep_running = False
        return True
            
    def on_real_expose(self, widget, ev):
        if self.is_scale_changed:
            self.is_scale_changed = False
            self.scale_changed()

        glcontext = widget.get_gl_context()
        gldrawable = widget.get_gl_drawable()
        if not gldrawable.gl_begin(glcontext):
            return

        glClearColor(0.0, 0.0, 0.0, 0.0)
        glClear(GL_COLOR_BUFFER_BIT)

        glColor3f(0.6, 0.6, 0.6)

        value_ptp = self.value_max - self.value_min


        units_per_pixel = 2. / self.height
        pixel = 5
        border_units = units_per_pixel * pixel * 2.

        units_per_value = (2.-border_units)/value_ptp
        value_scale = units_per_value
            

        if self.first_ctx:
            self.first_ctx = False
            self.dlist = glGenLists(1)
            #self.dlist2 = glGenLists(1)
        
        if self.draw_timescale and self.new_seconds:
            self.new_seconds = False
            glNewList(self.dlist, GL_COMPILE)
            # oversample:
            c = 0.2
            glColor3f(c, c, c)
            glBegin(GL_LINES)
            for start, end in zip(self.seconds_array2[::2, :], self.seconds_array2[1::2, :]):
                glVertex2f(*start)
                glVertex2f(*end)
            glEnd()
            # major step:
            c = 0.4
            glColor3f(c, c, c)
            glBegin(GL_LINES)
            for start, end in zip(self.seconds_array[::2, :], self.seconds_array[1::2, :]):
                glVertex2f(*start)
                glVertex2f(*end)
            glEnd()
            glEndList()
                
        glPushMatrix()

        t = self.value_offset * value_scale
        glTranslate(0, -t, 0)
        
        glBegin(GL_LINES)
        glVertex3f(-1, 0, 0)
        glVertex3f(1, 0, 0)
        glEnd()

        if self.new_storage:
            self.new_storage = False
            N = self.array_shape[0]
            if use_vbo:
                if self.verts:
                    for v in self.verts:
                        v.delete()
                self.verts = []
                for i in range(N):
                    self.verts.append(vbo.VBO(self.value_storage[i], GL_DYNAMIC_DRAW))
                self.last_storage_cursor = 0

        last_ts = self.value_storage[0][self.storage_cursor-1, 0]
        screen_start = last_ts - self.seconds
        if self.reset_start_cursor:
            self.reset_start_cursor = False
            if self.storage_full:
                self.start_cursor = self.storage_cursor
            else:
                self.start_cursor = 0
        while self.value_storage[0][self.start_cursor, 0] < screen_start:
            self.start_cursor += 1
            if self.storage_full and self.start_cursor == self.n_values:
                self.start_cursor = 0

        glPushMatrix()
        start_time = self.value_storage[0][self.start_cursor, 0]

        glScalef(2/self.seconds, value_scale, 1)
        glTranslate(-last_ts + self.seconds/2, 0, 0)

        if self.draw_timescale:
            # draw timescale
            glPushMatrix()
            offset = int(start_time / self.second_step) * self.second_step
            glTranslate(offset, t/value_scale, 0)
            glScalef(1, 1/value_scale, 1)
            glCallList(self.dlist)
            #glCallList(self.dlists)
            glPopMatrix()

        glEnableClientState(GL_VERTEX_ARRAY)
        if True:
            if use_vbo:
                # use vertex buffer objects
                # update buffer object!
                N = self.array_shape[0]
                if not self.storage_full or self.last_storage_cursor < self.storage_cursor:
                    for i in range(N):
                        self.verts[i].bind()
                        glBufferSubData(GL_ARRAY_BUFFER, self.last_storage_cursor * 2 * 4, (self.storage_cursor - self.last_storage_cursor) * 2 * 4, self.value_storage[i][self.last_storage_cursor:self.storage_cursor, :])
                elif self.last_storage_cursor >= self.storage_cursor:
                    for i in range(N):
                        self.verts[i].bind()
                        glBufferSubData(GL_ARRAY_BUFFER, self.last_storage_cursor * 2 * 4, (self.n_values - self.last_storage_cursor) * 2 * 4, self.value_storage[i][self.last_storage_cursor:, :])
                        glBufferSubData(GL_ARRAY_BUFFER, 0, self.storage_cursor * 2 * 4, self.value_storage[i][:self.storage_cursor:, :])
                self.last_storage_cursor = self.storage_cursor
                for i in range(N):
                    glColor3f(*self.colors[i % len(self.colors)])
                    self.verts[i].bind()
                    glVertexPointerf(self.verts[i])
                    if self.start_cursor > self.storage_cursor:
                        glDrawArrays(GL_LINE_STRIP, self.start_cursor, self.n_values - self.start_cursor)
                        glDrawArrays(GL_LINE_STRIP, 0, self.storage_cursor)
                    else:
                        glDrawArrays(GL_LINE_STRIP, self.start_cursor, self.storage_cursor - self.start_cursor)
            else:
                # use vertex array
                N = self.array_shape[0]
                for i in range(N):
                    glColor3f(*self.colors[i % len(self.colors)])
                    glVertexPointerf(self.value_storage[i])
                    if self.start_cursor > self.storage_cursor:
                        glDrawArrays(GL_LINE_STRIP, self.start_cursor, self.n_values - self.start_cursor)
                        glDrawArrays(GL_LINE_STRIP, 0, self.storage_cursor)
                    else:
                        glDrawArrays(GL_LINE_STRIP, self.start_cursor, self.storage_cursor - self.start_cursor)
        glDisableClientState(GL_VERTEX_ARRAY)

        glPopMatrix()        
        glPopMatrix()

        if self.draw_legend:
            self.do_draw_legend()

        # Swap buffers
        if gldrawable.is_double_buffered():
            gldrawable.swap_buffers()
        else:
            glFlush()
        gldrawable.gl_end()
        return True

    def set_legends(self, legends):
        self.legends = legends
    
    def do_draw_legend(self):
        glPushMatrix()
        glLoadIdentity()

        N = self.array_shape[0]
        units_per_pixel = 2. / self.height
        
        if self.legends is None:
            legends = ["signal %d" % i for i in range(N)]
        else:
            legends = self.legends

        font_height = 13
        y_offset = font_height * units_per_pixel
        
        for i, label in enumerate(legends):
            glColor3f(*self.colors[i % len(self.colors)])
            glRasterPos2f(-1, 1 - y_offset)
            self.do_draw_text(label)

            y_offset += font_height * units_per_pixel
        glPopMatrix()

    def call_glut_init(self):
        global had_glut_init
        had_glut_init = True
        glutInit([])
        
    def do_draw_text(self, text, font=None):
        # https://www.opengl.org/resources/libraries/glut/spec3/node76.html#SECTION000111000000000000000
        if font is None:
            font = GLUT_BITMAP_8_BY_13
        if not had_glut_init:
            self.call_glut_init()
        for char in text:
            glutBitmapCharacter(font, ord(char))
        
    def reshape(self, width, height):
        self.height = height
        glViewport(0, 0, width, height)

    def on_auto_scale_cb_toggled(self, btn):
        self.do_auto_scale = btn.get_active()
        if self.do_auto_scale:
            # todo: fix
            self.value_min = self.value_storage[0][0:self.storage_cursor-1, 1:].min()
            self.value_max = self.value_storage[0][0:self.storage_cursor-1, 1:].max()
            self.scale_changed()
        self.max_sb.set_sensitive(not self.do_auto_scale)
        self.offset_sb.set_sensitive(not self.do_auto_scale)
        self.min_sb.set_sensitive(not self.do_auto_scale)

    def on_hold_tb_toggled(self, btn):
        self.do_hold = btn.get_active()

    def on_seconds_btn_value_changed(self, btn):
        v = btn.get_value()
        if v == self.seconds:
            return True
        old_seconds = self.seconds
        self.seconds = v
        if old_seconds < self.seconds:
            self.reset_start_cursor = True
        self.seconds_changed()
        return True

    def scale_changed(self, calc_offset=False):
        if self.do_auto_scale or calc_offset:
            self.value_offset = (self.value_max + self.value_min) / 2.
        self.max_sb.set_value(self.value_max)
        self.offset_sb.set_value(self.value_offset)
        self.min_sb.set_value(self.value_min)

    def on_max_sb_value_changed(self, btn):
        if self.do_auto_scale:
            return
        self.value_max = btn.get_value()
        if self.value_max < self.value_min:
            self.value_max = self.value_min + abs(self.value_min) * 0.01
        self.scale_changed(calc_offset=True)
    def on_offset_sb_value_changed(self, btn):
        if self.do_auto_scale:
            return
        old_offset = self.value_offset
        self.value_offset = btn.get_value()
        to_new = self.value_offset - old_offset
        self.value_max += to_new
        self.value_min += to_new
        self.scale_changed()

    def on_min_sb_value_changed(self, btn):
        if self.do_auto_scale:
            return
        self.value_min = btn.get_value()
        if self.value_min > self.value_max:
            self.value_min = self.value_max - abs(self.value_max) * 0.01
        self.scale_changed(calc_offset=True)

    def seconds_changed(self):
        n_seconds = 10
        second_step = self.seconds / n_seconds
        self.second_step = ceil(second_step)
        self.major_ts_label.set_text("major ts: %.1fs" % self.second_step)
        self.seconds_array = zeros((2 * n_seconds, 2))
        self.seconds_array[::2, 0] = list(range(1, n_seconds+1))
        self.seconds_array[1::2, 0] = self.seconds_array[::2, 0]
        self.seconds_array[:, 0] *= self.second_step
        self.seconds_array[::2, 1] = -1 # self.value_min
        self.seconds_array[1::2, 1] = 1 # self.value_max

        oversample = 10
        self.seconds_array2 = zeros((2 * n_seconds*oversample, 2))
        self.seconds_array2[::2, 0] = list(range(1, n_seconds*oversample+1))
        self.seconds_array2[1::2, 0] = self.seconds_array2[::2, 0]
        self.seconds_array2[:, 0] *= self.second_step/oversample
        self.seconds_array2[::2, 1] = -1 # self.value_min
        self.seconds_array2[1::2, 1] = 1 # self.value_max
        self.new_seconds = True
        self.request_refresh = True

    def on_main_ev_scroll_event(self, evb, ev):
        f = 1.1
        if ev.direction == gtk.gdk.SCROLL_UP:
            new_seconds = self.seconds / f
        else:
            new_seconds = self.seconds * f
        self.seconds_btn.set_value(new_seconds)
        self.seconds_changed()
        return True

    def on_show_legend_clicked(self, btn):
        self.draw_legend = btn.get_active()
    
if __name__ == "__main__":
    main_window = gtk.Window()
    main_window.set_size_request(600, 400)
    s = pyscope("tests.scope", "value")
    main_window.add(s)
    main_window.show_all()
    gtk.main()
