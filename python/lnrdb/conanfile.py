import os
import sys

from conans import ConanFile, tools
from cissy.conantools import get_python_dist_versions, get_python_dist_latest_version, get_python_dist_requirement

class lnrdb_python_conan(ConanFile):
    python_requires = "ln_conan/[~4]@common/unstable"
    python_requires_extend = "ln_conan.Base"
    
    name = "lnrdb_python"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = "lnrdb is an efficient column-oriented database format used by lnrecorder. this is a pure-python binding (2 & 3)"
    settings = None
    exports = [ "../../site_scons*" ]
    exports_sources = [
        # our self
        "../../python*",
        "!python/links_and_nodes*",
        "!python/py*",
        
        # will need base-build-system
        "../../SConstruct",        
    ] + ["!%s" % x for x in tools.Git().excluded_files()]

    options = {
        "python_version": ["2", "3"],
        "python_dist_version": get_python_dist_versions()
    }
    default_options = {
        "python_version": "2",
        "python_dist_version": get_python_dist_latest_version()
    }
            
    def init(self):
        base = self.python_requires["ln_conan"].module.Base
        base.set_attrs(self)
    
    def requirements(self):
        self.requires(get_python_dist_requirement(self.options.python_version.value, self.options.python_dist_version.value))
    
    def build(self):
        self.scons_build("libln python", "--no-manager --no-python-api --no-python-base", with_python_lib=True)

    def package(self):
        install = os.path.join("build", "python", self.install_sandbox, self.prefix[1:])
        self.copy("*", src=install, symlinks=True)
        
    def package_info(self):
        self.env_info.PYTHONPATH.append(os.path.join(self.package_folder, self.python_path))
