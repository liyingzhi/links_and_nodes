import os
import sys
from conans import ConanFile, tools
from cissy.conantools import get_python_dist_versions, get_python_dist_latest_version

class links_and_nodes_manager_conan(ConanFile):
    python_requires = "ln_conan/[~4]@common/unstable"
    python_requires_extend = "ln_conan.Base"
    
    name = "links_and_nodes_manager"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "GPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = None
    exports = [ "../../site_scons*" ]
    exports_sources = [
        # all of python for scons to be able to build python
        "../../python*",
        "../../documentation*",
        "!python/links_and_nodes/*",
        "!python/pyutils*",
        
        "../../share*",
        
        # will need base-build-system
        "../../SConstruct",
    ] + ["!%s" % x for x in tools.Git().excluded_files()]
    
    options = {
        "python_version": ["2", "3"],
        "python_dist_version": get_python_dist_versions()
    }
    default_options = {
        "python_version": "2",
        "python_dist_version": get_python_dist_latest_version()
    }

    def init(self):
        base = self.python_requires["ln_conan"].module.Base
        base.set_attrs(self)
    
    def requirements(self):
        self.requires("links_and_nodes_runtime/%s" % self.same_branch_or("[~2]"))
        self.requires("links_and_nodes_python/%s" % self.same_branch_or("[~2]"))
        self.requires("links_and_nodes_ln_msgdef/%s" % self.same_branch_or("[>=1.2 <3]"))

    def configure(self):
        self.output.write("python_version: %s, python_dist_version: %s\n" % (
            self.options.python_version,
            self.options.python_dist_version))
        self.options["links_and_nodes_python"].python_version = self.options.python_version
        self.options["links_and_nodes_python"].python_dist_version = self.options.python_dist_version
        
    def source(self):
        self.write_version_file("version")
            
    def build(self):
        self.scons_build("python documentation", "--only-manager", with_python_lib=True)

    def package(self):
        self.copy("version", dst=os.path.join(self.python_path, "links_and_nodes_manager")) # todo: this is strange
        install = os.path.join("build", "python", self.install_sandbox, self.prefix[1:])
        self.copy("*", src=install, symlinks=True)
        doc_install = os.path.join("build", "documentation", self.install_sandbox, self.prefix[1:])
        self.copy("*", src=doc_install, symlinks=True)
        
    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.env_info.PYTHONPATH.append(os.path.join(self.package_folder, self.python_path))

        if self.channel == "unstable":
            allow = "-au" # unstable
        elif self.channel != "stable":
            allow = "-as" # snapshot
        else:
            allow = ""
        self.env_info.LN_DAEMON_START = "/opt/rmc-build-tools/cissy run %s -p links_and_nodes_runtime/%s@%s/%s ln_daemon" % (
            allow,
            self.version, self.user, self.channel)
