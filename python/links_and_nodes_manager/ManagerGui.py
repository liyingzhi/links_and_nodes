# This file is part of links and nodes.
#
# links and nodes is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# links and nodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf
gi.require_version('GObject', '2.0')
from gi.repository import GObject
import os
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
import sys
import pprint
import glob

from . import config
import links_and_nodes as ln

from . import Logging
from .CommunicationUtils import *
from .Logging import *
from .ProcessesGui import ProcessesGui
from .TopicsGui import TopicsGui
from .ClientsGui import ClientsGui
from .ServicesGui import ServicesGui
from . import parameters_window
from . import log_window
from . import ManagerConnection

manager_base_dir = os.path.dirname(os.path.realpath(__file__))

class ManagerGui(ManagerConnection.ManagerConnection):
    def __init__(self, manager=None, connect=None):
        super(ManagerGui, self).__init__(manager, connect)
        enable_logging_on_instance(self, "ManagerGui")
        if manager:
            self.manager.gui = self
        
        self.window_sizes = {}
        self._last_pos = None
        self._save_last_pos_source = None

        resources_dir = os.path.join(config.manager_base_dir, "resources")
        icons = []
        fns = glob.glob(os.path.join(resources_dir, "lnm_*.png"))
        fns.sort()
        for fn in fns:
            icons.append(GdkPixbuf.Pixbuf.new_from_file(fn))
        Gtk.Window.set_default_icon_list(icons)
        
        self.dialog = Gtk.Window()
        self.dialog.resize(400, 700)
                
        self.notebook = Gtk.Notebook()
        self._pages = {}
        self.dialog.add(self.notebook)

        self.prefered_host_color = {}         
        
        self.pg = ProcessesGui(self)
        self.cg = ClientsGui(self)
        self.tg = TopicsGui(self)
        self.sg = ServicesGui(self)

        self.pw = parameters_window.parameters_window(self.tg, start_pattern="*", dont_show=True)
        self.pw.vbox1.unparent()
        self.add_notebook_page("parameters", self.pw.vbox1, show=False)

        self.lw = log_window.log_window(self, dont_show=True)
        self.lw.vpaned1.unparent()
        hb = Gtk.HBox()
        hb.pack_start(Gtk.Label("log"), True, True, 0)
        img = self.lw.create_bug_img()
        hb.pack_start(img, False, False, 0)
        hb.show_all()
        self.notebook.append_page(self.lw.vpaned1, hb)
        img.set_property("visible", False)

        self.notebook.connect("switch-page", self.on_switch_page)

        self.g = None
        
    def main(self):
        self.dialog.connect("delete-event", self.on_close)
        self.dialog.connect("configure-event", self.on_resize)

        self.connect_to_manager()
        if self.connect:
            enable_logging_on_instance(self, "ManagerGui")
            self.request(("connection", "enable_log_update"))
            self.DISPLAY = os.getenv("DISPLAY")
            self.request(("connection", "set_member"), "DISPLAY", self.DISPLAY)
            have_display_unix_socket = check_for_display_unix_socket(self.DISPLAY)
            self.request(("connection", "set_member"), "DISPLAY_UNIX_SOCKET", have_display_unix_socket)
            self.XAUTHORITY = get_xauthority()
            if not os.path.isfile(self.XAUTHORITY):
                self.warning("XAUTHORITY file %r does not exist!?" % self.XAUTHORITY)
            self.request(("connection", "set_member"), "XAUTHORITY", self.XAUTHORITY)
            self.xcookie = get_xcookie()
            self.request(("connection", "set_member"), "xcookie", self.xcookie)
            
        self.sysconf = self.manager_proxy(request="get_system_configuration")

        self.pg.connect()
        self.cg.connect()
        self.tg.connect()
        self.sg.connect()
        self.pw.connect()
        self.lw.connect()

        self.restore_window_pos()

        self.pg.vbox3.connect("size-allocate", self.on_resize)
        
        if config.version is None:
            version = ""
        else:
            version = "v%s " % config.version
        self.dialog.set_title("ln_manager %s%s on %s" % (version, self.sysconf.instance_config.name, self.sysconf.instance_config.manager))
        self.dialog.show_all()

        self.pg.hide()

        self.notebook.set_current_page(0) # for debugging

        self.init_gui_plugins()

        if self.manager and self.manager.do_present:
            self.dialog.present()

    def select_page(self, page_name):
        for page_idx, pn in enumerate("processes,clients,topics,services,parameters,log".split(",")):
            if pn == page_name:
                self.notebook.set_current_page(page_idx)
                return
        raise Exception("unknown page name %r" % page_name)
            
    def init_gui_plugins(self):
        self.plugin_packages = {}
        self.plugins = {}
        for pname, plugin in self.sysconf.gui_plugins.items():
            try:
                mod = self.plugin_packages[pname] = import_plugin_package(plugin.plugin_package)
                self.debug("successfully imported module file %r" % plugin.plugin_package)
            except:
                self.error("failed to import gui_plugin %r:\n%s" % (pname, traceback.format_exc()))
                continue
            try:
                self.plugins[pname] = mod.gui_plugin(self, plugin)
            except:
                self.error("failed to initialize gui_plugin %r:\n%s" % (pname, traceback.format_exc()))
                continue

    def add_notebook_page(self, name, widget, show=True):
        ret = self.notebook.append_page(widget, Gtk.Label(name))
        self._pages[name] = ret
        if show:
            widget.show_all()
        return ret

    def switch_to_page(self, name):
        self.notebook.set_current_page(self._pages[name])
        
    def work_on_notifications(self):
        kwargs = self.notifications[0]
        self.notifications.pop(0)

        try:
            self._gui_notification(self, kwargs)
        except:
            self.error("error while processing manager notification:\n%s\n%s" % (
                pprint.pformat(kwargs),
                traceback.format_exc()))
        if self.notifications:
            return True
        self.notification_id = None
        return False

    def on_notification(self, kwargs):
        self.notifications.append(kwargs)
        if self.notification_id is None:
            self.notification_id = GLib.idle_add(self.work_on_notifications)
        return True

    def on_close(self, widget, ev):
        if self.connect is None:
            n_clients = self.cg.clients_model.iter_n_children(None)
            if n_clients > 0:
                dlg = Gtk.MessageDialog(
                    widget, 
                    Gtk.DialogFlags.DESTROY_WITH_PARENT, 
                    Gtk.MessageType.QUESTION,
                    Gtk.ButtonsType.YES_NO,
                    message_format=(
                        "there are still clients connected, "
                        "do you only want to close the gui window or do you want to shutdown "
                        "the ln_manager aswell?\n\n"
                        "shutdown ln_manager?")
                    )
                dlg.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CLOSE)
                dlg.show()
                resp = dlg.run()
                dlg.hide()
                if resp != Gtk.ResponseType.YES:
                    if resp == Gtk.ResponseType.CLOSE:
                        # cancel!
                        return True
                    # just close gui window
                    widget.hide()
                    self.manager.gui = None
                    del widget
                    return True
        if self.connect is None:
            self.manager.exit()
        self.info("Exiting application")
        sys.exit(0)

    def _gui_notification(self, manager_or_gui, msg):
        r = msg.get("request")
        
        if False:
            mkeys = list(msg.keys())
            mkeys.sort()        
            mN = 160
            tmsg = []
            for k in mkeys:
                if k == "request":
                    continue
                line = "  %s: %r" % (k, msg[k])
                if len(line) > mN:
                    line = line[:mN] + "..."
                tmsg.append(line)
            self.debug("gui_notification: %r\n%s" % (r, "\n".join(tmsg)))
        
        if r == "new_config":
            self.sysconf = msg["new_config"]
            if config.version is None:
                version = ""
            else:
                version = "v%s " % config.version
            self.dialog.set_title("ln_manager %s%s on %s" % (version, self.sysconf.instance_config.name, self.sysconf.instance_config.manager))
        elif r == "log_msg":
            for msg in msg["msgs"]:
                #print "received", msg
                Logging.append_log(*msg[1:])
            Logging.trigger_log_update.notify(Logging.log_head)
            return True
        self.call_hook(("notification", r), msg)
        return True

    def _get_window_pos_fn(self):
        display = os.getenv("DISPLAY", "").replace(":", "_")
        fn = "~/.ln_manager_%s_%s" % (self.sysconf.instance_config.name, display)
        if self.connect:
            fn += "_connected"
        fn = os.path.expanduser(fn)
        return fn

    def save_window_size(self):
        self._save_last_pos_source = None
        fn = self._get_window_pos_fn()
        with open(fn, "w") as fp:
            fp.write(repr(self._last_pos))
        return False

    def on_resize(self, widget, ev=None):
        window_size = self.dialog.get_size()
        position = self.dialog.get_position()
        pane_pos = self.pg.vpaned.get_position()
        hpane_pos = self.pg.hpaned.get_position()
        new_pos = (
            (window_size.width, window_size.height),
            (position.root_x, position.root_y),
            pane_pos,
            hpane_pos,
            tuple(self.window_sizes.items())
        )
        if new_pos != self._last_pos:
            self._last_pos = new_pos
            if self._save_last_pos_source:
                GLib.source_remove(self._save_last_pos_source)
            self._save_last_pos_source = GLib.timeout_add(500, self.save_window_size)
        return False

    def restore_window_pos(self):
        fn = self._get_window_pos_fn()
        try:
            with open(fn, "r") as fp:
                self._last_pos = eval(fp.read())
        except:
            return
        if len(self._last_pos) == 3:
            window_size, position, pane_pos = self._last_pos
        elif len(self._last_pos) == 4:
            window_size, position, pane_pos, hpane_pos = self._last_pos
            self.pg.hpaned.set_position(hpane_pos)
        elif len(self._last_pos) == 5:
            window_size, position, pane_pos, hpane_pos, self.window_sizes = self._last_pos
            self.window_sizes = dict(self.window_sizes)
            self.pg.hpaned.set_position(hpane_pos)
        self.dialog.resize(*window_size)
        self.dialog.move(*position)
        self.pg.vpaned.set_position(pane_pos)

    def get_window_size(self, window_id):
        return self.window_sizes.get(window_id)
    
    def store_window_size(self, window_id, size):
        self.window_sizes[window_id] = size
        self.on_resize(None)

    def on_switch_page(self, nb, ptr, page_no):
        if page_no == 5:
            self.lw.queue_log_display(None, False)
        return True
