"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
gi.require_version('Pango', '1.0')
gi.require_version('GObject', '2.0')
from gi.repository import GObject
import pprint
import os
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
import sys
import socket
import math
import re
from gi.repository import Pango
from . import config

from .Logging import *
from .CommunicationUtils import *
from .PortInspector import PortInspector
from . import topic_priorities

import time
import pyutils

manager_base_dir = os.path.dirname(os.path.realpath(__file__))

class TopicsGui(pyutils.hooked_object):
    def __init__(self, gui):
        self.have_xml = False
        self.nb = None
        pyutils.hooked_object.__init__(self)
        enable_logging_on_instance(self, "TopicsGui")
        self.gui = gui
        self.manager_proxy = self.gui.manager_proxy

        # load processes gui
        resources_dir = os.path.join(config.manager_base_dir, "resources")
        self.xml = Gtk.Builder()
        self.xml_fn = os.path.join(resources_dir, "ln_manager_topics.ui")
        self.xml.add_from_file(self.xml_fn)
        self.have_xml = True
        self.xml.connect_signals(self)

        vb = Gtk.VBox()
        self.main_paned.reparent(vb)
        self.gui.add_notebook_page("topics", vb, show=False)

        tv = self.topics_tv        
        tv.set_enable_tree_lines(True)
        tv.set_headers_visible(True)
        self.model = Gtk.ListStore(
            GObject.TYPE_PYOBJECT,
            GObject.TYPE_STRING, # topic
            GObject.TYPE_STRING, # publisher
            GObject.TYPE_INT, # N-subscribers
            GObject.TYPE_STRING, # publisher rate
            GObject.TYPE_STRING, # published packets
            )
        crtext = Gtk.CellRendererText()
        crtext.set_property("ellipsize", Pango.EllipsizeMode.END)
        tv.insert_column_with_attributes(-1, "topic", crtext, text=1)
        col = tv.get_columns()[-1]
        col.set_resizable(True)
        col.set_property("expand", True)
        self.topics_col = col
        def on_topics_tv_configure(tv, alloc, col):
            col.set_property("max-width", int(alloc.width / 4 * 3))
        tv.connect("size-allocate", on_topics_tv_configure, col)
        col.set_sort_column_id(1)
        tv.insert_column_with_attributes(-1, "publisher", Gtk.CellRendererText(), text=2)
        col = tv.get_columns()[-1]
        col.set_sort_column_id(2)
        tv.insert_column_with_attributes(-1, "subscribers", Gtk.CellRendererText(), text=3)
        col = tv.get_columns()[-1]
        col.set_sort_column_id(3)
        tv.insert_column_with_attributes(-1, "publisher rate", Gtk.CellRendererText(), text=4)
        col = tv.get_columns()[-1]
        col.set_sort_column_id(4)
        tv.insert_column_with_attributes(-1, "published pkts", Gtk.CellRendererText(), text=5)
        col = tv.get_columns()[-1]
        col.set_sort_column_id(5)
        tv.set_model(self.model) # cursor-changed-fine

        tv = self.subscribers_tv
        tv.set_enable_tree_lines(True)
        tv.set_headers_visible(True)
        self.subscribers_model = Gtk.ListStore(
            GObject.TYPE_PYOBJECT, # client
            GObject.TYPE_STRING, # client-name
            GObject.TYPE_STRING, # subscriber fps
            GObject.TYPE_PYOBJECT, # topic_subscription
            )
        tv.insert_column_with_attributes(-1, "client", Gtk.CellRendererText(), text=1)
        col = tv.get_columns()[-1]
        col.set_resizable(True)
        tv.insert_column_with_attributes(-1, "rate [Hz]", Gtk.CellRendererText(), text=2)
        tv.set_model(self.subscribers_model) # cursor-changed-fine

        # variables
        self.last_message_def_label = None
        self.last_publisher_label = None

        self.current_topic = None
        self.ports_needing_info = {}
        self.get_info_idle = None
        self.port_info_in_progress = None
        ### disable topic update for now! 
        self.update_topics_timeout = GLib.timeout_add(1000, self._update_topics)
        self.inspectors = {}
        self.topic_priorities = {}
        self.topics_prios_btn.set_sensitive(False)

    def __getattr__(self, name):
        if not self.have_xml:
            raise AttributeError(name)
        widget = self.xml.get_object(name)
        if widget is None:    
            raise AttributeError(name)
        return widget

    def connect(self):
        def notification_wrapper(callback):
            def inner_notification_wrapper(gui, msg):
                if "request" in msg:
                    del msg["request"]
                try:
                    return callback(**msg)
                except:
                    self.error("error in notification callback %r%s:\n%s" % (callback.__name__, msg, traceback.format_exc()))
                    raise
            return inner_notification_wrapper
        self.gui.add_hook(("notification", "add_topic"), notification_wrapper(self.on_add_topic))
        self.gui.add_hook(("notification", "del_topic"), notification_wrapper(self.on_del_topic))
        self.gui.add_hook(("notification", "update_topic"), notification_wrapper(self.on_update_topic))
        #self.gui.add_hook(("notification", "update_obj_requested_state"), notification_wrapper(self._obj_requested_state_changed))

        self.update_topics()

    def _get_publisher(self, topic):
        if topic.publisher:
            return topic.publisher.program_name
        return "<no publisher>"

    def _need_info_for_port(self, port, topic, cb=None):
        if port.is_client_port:
            return # no info for client-only ports!
        if not topic.has_port(port):
            return # skip this, try next

        def got_port_info_answer(msg=None, exception=None):
            if exception is not None:
                self.warning("could not get port info for port %r: %s" % (port, exception))
                return
            port.read_info(msg)
            self.on_update_topic(topic)
            if cb: cb()
        
        self.manager_proxy("call_cb", port.daemon.get_state, got_port_info_answer, port_id=port.get_id())
        
    def _append_topic(self, topic):
        m = self.model
        iter = m.get_iter_first()
        while iter and m.iter_is_valid(iter):
            row = m[iter]
            if row[1] > topic.name:
                # insert before
                new_iter = m.insert_before(iter, self._get_topic_row(topic))
                return new_iter
            iter = m.iter_next(iter)
        return m.append(self._get_topic_row(topic))

    def _get_topic_row(self, topic):
        if topic.source_port and topic.source_port.measured_rate is not None:
            src_rate = "%.1f" % topic.source_port.measured_rate
            src_count = str(topic.source_port.packet_count)
        else:
            src_rate = ""
            src_count = ""
        return topic, topic.name, self._get_publisher(topic), len(topic.subscribers), src_rate, src_count

    def _update_topic(self, iter, topic):
        if topic.source_port and topic.source_port.measured_rate is None:
            self._need_info_for_port(topic.source_port, topic)
        self.model[iter] = self._get_topic_row(topic)
        for port, inspector in self.inspectors.items():
            if (hasattr(inspector, "topic_name") and inspector.topic_name == topic.name) or (inspector.topic is not None and inspector.topic.name == topic.name):
                inspector.topic = topic

    def _update_topics(self):
        if self.gui.notebook.get_current_page() != 2:
            return True
        m = self.model
        iter = m.get_iter_first()
        while iter:
            topic = m[iter][0]
            if topic.source_port:
                self._need_info_for_port(topic.source_port, topic)
            iter = m.iter_next(iter)
        return True
    
    def _find_topic_iter(self, topic):
        m = self.model
        iter = m.get_iter_first()
        while iter:
            if m[iter][0].name == topic.name:
                return iter
            iter = m.iter_next(iter)

    ## manager events
    def on_add_topic(self, topic):
        self._append_topic(topic)
        return True
    def on_del_topic(self, topic):
        if topic == self.current_topic:
            self.topics_prios_btn.set_sensitive(False)
        iter = self._find_topic_iter(topic)
        if iter:
            self.model.remove(iter)
        else:
            self.warning("wanted to remove topic %r but did not find it!" % topic)
        return True
    def on_update_topic(self, topic):
        iter = self._find_topic_iter(topic)
        if not iter:
            self.warning("wanted to update topic %r but did not find it!" % topic)
        else:
            self._update_topic(iter, topic)
            if self.current_topic == topic:
                self.show_topic(topic)
        return True

    def _select_topic(self, topic):
        iter = self._find_topic_iter(topic)
        if not iter:
            return False
        path = self.model.get_path(iter)
        self.topics_tv.set_cursor(path)
        return True

    def _select_subscribed_topic(self, topic, client):
        iter = self._find_topic_iter(topic)
        if not iter:
            return False
        path = self.model.get_path(iter)
        self.topics_tv.set_cursor(path)
        m = self.subscribers_model
        iter = m.get_iter_first()
        while iter:
            row = m[iter]
            if row[0] == client:
                self.subscribers_tv.set_cursor(m.get_path(iter))
                return True
            iter = m.iter_next(iter)
        
        return True

    ## gui callbacks
    def on_topics_tv_cursor_changed(self, tv):
        path, col = tv.get_cursor() # path-none-checked
        if not path:
            return
        topic = self.model[path][0]
        self.show_topic(topic)
        
    def on_label_activate_link(self, label, uri):
        if uri == "goto_publisher" and self.current_topic.publisher:
            self.gui.notebook.set_current_page(1)
            self.gui.cg._select_client_topic(self.current_topic.publisher, self.current_topic)
        if uri == "goto_message_def":
            import links_and_nodes
            fn = links_and_nodes.search_message_definition(self.current_topic.md_name)
            pyutils.system("emacs '%s' &" % fn)
        return True

    def open_inspector_for_port(self, port, topic):
        if port in self.inspectors:
            self.inspectors[port].present()
        else:
            self.inspectors[port] = PortInspector(self, port, topic)

    def on_on_inspect_clicked(self, btn):
        if self.current_topic and self.current_topic.source_port:
            self.open_inspector_for_port(self.current_topic.source_port, self.current_topic)
            
    def on_subscribers_tv_cursor_changed(self, tv):
        path, col = tv.get_cursor() # path-none-checked
        if path:
            sub = self.subscribers_model[path][3]
            self.selected_subscription = sub
            self.on_inspect_subscribed.set_sensitive(sub.port is not None)

    def on_on_inspect_subscribed_clicked(self, btn):
        self.open_inspector_for_port(self.selected_subscription.port, self.current_topic)

    ## worker methods
    def update_topics(self):
        # initially get topics from manager!
        m = self.model
        m.clear()
        self.topics = topics = self.manager_proxy(request="get_topics")
        for topic_name, topic in topics.items():
            self._append_topic(topic)
            if self.current_topic == topic:
                self.show_topic(topic)

    def show_topic(self, topic):
        self.current_topic = topic
        self.topics_prios_btn.set_sensitive(True)
        if not hasattr(self, "last_selected_topic") or self.last_selected_topic != topic.name:
            self.last_selected_topic = topic.name
            self.selected_topic_label.set_markup("selected topic: <b>%s</b>" % topic.name)
        pub = self._get_publisher(topic)
        if pub[0] != "<":
            t = '<a href="goto_publisher">%s</a> on <a href="goto_daemon">%s</a>' % (
                pub, topic.source_port.daemon.host.name)
        else:
            t = escape_markup(pub)
        if t != self.last_publisher_label:
            self.last_publisher_label = t
            self.publisher_label.set_markup(t)

        t = '<a href="goto_message_def">%s</a> %d bytes' % (topic.md_name, topic.message_size)
        if t != self.last_message_def_label:
            self.last_message_def_label = t
            self.message_def_label.set_markup(t)

        subs = topic.subscribers
        if not subs:
            self.on_inspect_subscribed.set_sensitive(False)

        subs.sort(key=lambda s: s.client.program_name)

        m = self.subscribers_model

        i = 0
        iter = m.get_iter_first()

        while iter is not None or i < len(subs):
            if i == len(subs):
                # remove old row
                if not m.remove(iter):
                    iter = None
                continue
            client = subs[i].client
            rate = subs[i].rate

            if iter is None:
                # new row -> append!
                m.append((client, client.program_name, "%.1f" % rate, subs[i]))
                i += 1
                continue

            row = m[iter]
            if row[0].program_name == client.program_name:
                row[2] = "%.1f" % rate
                iter = m.iter_next(iter)
                i += 1
                continue

            if row[1] > client.program_name:
                # insert before
                m.insert_before(iter, (client, client.program_name, "%.1f" % rate, subs[i]),)
                i += 1
                continue
            
            # row[1] < client.program_name
            if not m.remove(iter):
                iter = None

        #for client, rate in topic.subscribers.iteritems():
        #    m.append((client, client.program_name, "%.1f" % rate))

    def on_subscribers_tv_row_activated(self, tv, path, col):
        m = self.subscribers_model
        sub = m[path][3]
        self.open_inspector_for_port(sub.port, self.current_topic)
        return True

    def on_topics_tv_row_activated(self, tv, path, col):
        m = self.model
        topic = m[path][0]
        if topic.publisher:
            #self.gui.notebook.set_current_page(1)
            #self.gui.cg._select_client_topic(topic.publisher, topic)
            self.open_inspector_for_port(topic.source_port, topic)
        return True

    def on_topics_prios_btn_clicked(self, btn):
        global topic_priorities
        #topic_priorities = reload(topic_priorities)
        if self.current_topic not in self.topic_priorities:
            self.topic_priorities[self.current_topic] = topic_priorities.topic_priorities(self, self.current_topic)
        else:
            self.topic_priorities[self.current_topic].present()

    def on_open_console_btn_clicked(self, btn):
        if self.nb is None:
            import pyutils.pygtksvnb.notebook
            notebook_fn = os.path.join(
                self.gui.sysconf.instance_config.default_notebook_path,
                "gui.py")
            content = """% gui
% manager_proxy
% gui.manager
% gui.manager.sysconf
"""
            self.nb = pyutils.pygtksvnb.notebook(
                filename=notebook_fn,
                new_notebook_cell_content=content,
                create_file=True,
                add_to_context=dict(
                    gui=self.gui,
                    tg=self.gui.tg,
                    manager_proxy = self.gui.manager_proxy,
                    )
            )
            self.nb.on_hide = lambda *args: True
            self.nb.show()
        self.nb.present()
    
    def on_profile_outputs_btn_toggled(self, btn):
        self.manager_proxy("enable_process_output_profiling", btn.get_active())

