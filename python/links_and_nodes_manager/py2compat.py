import sys

is_py2 = sys.version_info < (3, 0)

if not is_py2:
    unicode = str
    long = int
    raw_input = input

    def cmp(x, y):
        if x < y:
            return -1
        elif x > y:
            return 1
        else:
            return 0
