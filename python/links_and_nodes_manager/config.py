"""
    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2013 DLR e.V., Florian Schmidt, Maxime Chalon
"""

from __future__ import print_function

DEAMON_PORT = 54376 # reserved fixed port number for our ln_daemon arbiters
SSH_PORT = 22
TELNET_PORT = 23

state_check_interval = 1000
#state_check_interval = 100

import os

ln_arbiter_port_env = os.getenv("LN_ARBITER_PORT")
if ln_arbiter_port_env:
    DEAMON_PORT = int(ln_arbiter_port_env)

ln_debug = os.getenv("LN_DEBUG")
file_dir = os.path.dirname(__file__)
version = "<unknown version>"

source_dir = os.path.dirname(os.path.dirname(os.path.realpath(file_dir)))
source_build_dir = os.path.join(source_dir, os.getenv("LN_BUILD_SUBDIR", "build"))
source_libln_dir = os.path.join(source_dir, "libln")
if ln_debug:
    print("file_dir: %r" % file_dir)
    print("source_dir would be: %r" % source_dir)

if os.path.isdir(source_dir) and os.path.isdir(source_build_dir) and os.path.isdir(source_libln_dir):
    # source tree
    if ln_debug: print("detected source tree!")
    prefix = source_dir
    ln_tree = "source"
    ln_release_version = "src"
    manager_base_dir = os.path.join(source_dir, "python", "links_and_nodes_manager")
    contrib_base_dir = os.path.join(source_dir, "python") # todo: remove
    python_binding_base_dir = os.path.join(source_dir, "python") # todo: remove
    daemon_binary = os.path.join(source_build_dir, "ln_runtime", "ln_daemon", "ln_daemon")
    ld_library_path = os.path.join(source_build_dir, "libln")
    
    daemon_start_script = os.getenv("LN_SRC_TREE_DAEMON_START", ". %(source_dir)s/rmc/conan_source ln_daemon; nohup %(daemon_binary)s") % dict(
        source_dir=source_dir,
        daemon_binary=daemon_binary 
    ) # this default command assumes a shared network filesystem!
    
    # we assume there is no shell capable to execute this script so we call the binary directly on telnet hosts!
    telnet_daemon_start = daemon_binary

    version = "<source tree>"

    ln_share_dir = os.path.join(prefix, "share")
    ln_config_include_dirs = [
        ln_share_dir,
        os.path.join(ln_share_dir, "includes"),
        os.path.join(source_dir, "ln_runtime", "file_services"),
    ]
    
    
else: # assume installed into some prefix
    # <PREFIX>/lib/python2.7/site-packages/links_and_nodes_manager/config.py
    if ln_debug: print("assume release into prefix!")
    prefix = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(file_dir))))
    ln_tree = "install"
    ln_share_dir = os.path.join(prefix, "share", "links_and_nodes_manager")
    ln_release_version = "install-unknown"
    try:
        ptfn = os.path.join(os.path.dirname(__file__), "version")
        with open(ptfn, "r") as fp:
            version = fp.read().strip()
    except:
        version = "<unknown rlease>"
    manager_base_dir = os.path.realpath(file_dir)
    python_binding_base_dir = os.path.dirname(file_dir) # probably <PREFIX>/lib/python2.7/site-packages
    contrib_base_dir = python_binding_base_dir
    daemon_binary = os.path.join(prefix, "bin", "ln_daemon")
    ld_library_path = os.path.join(prefix, "lib")

    daemon_start_script = daemon_binary # will only work if daemon is installed into same prefix as manager
    # conan/cissy users will have to use LN_DAEMON_START environment variable
    
    # we assume there is no shell capable to execute this script so we call the binary directly on telnet hosts!
    telnet_daemon_start = daemon_binary

    ln_config_include_dirs = [os.path.join(ln_share_dir, "includes")]
    if ln_debug: print("ln_config_include_dirs: %r" % ln_config_include_dirs)
    

user_ln_daemon_start = os.getenv("LN_DAEMON_START", None)
if user_ln_daemon_start:
    daemon_start_script = user_ln_daemon_start
elif os.sep != "/": # daemon start script is always considered to be run on a remote unix-like system
    daemon_start_script = daemon_start_script.replace(os.sep, "/")
    
default_gen_msg_defs = os.path.join(os.path.expanduser("~"), "ln_message_definitions", "gen")
configuration_file = None
additional_ssh_arguments = ""

# daemon_private_key_file
# ssh-dsa private key file for authenticated manager-daemon communication
daemon_private_key_file = None

# daemon_public_key_file
# ssh-dsa public key file for authenticated manager-daemon communication
# needs to be readable on target-host!
daemon_public_key_file = None

ln_config_file = os.path.join(os.path.expanduser("~"), ".ln_config")
if os.path.isfile(ln_config_file):
    ln_config_reader = "manager"
    message_definition_dirs = [] # this is simply to be compatible for .ln_config
    exec(open(ln_config_file, "rb").read())

import sys

# import own pyutils
if contrib_base_dir not in sys.path:
    sys.path.insert(0, contrib_base_dir)
import pyutils
import pyutils.line_assembler

# import own links_and_nodes
if python_binding_base_dir not in sys.path:
    sys.path.insert(0, python_binding_base_dir)
import links_and_nodes

mantis_report_url = "https://rmintra01.robotic.dlr.de/mantis/set_project.php?project_id=%s&make_default=no&ref=bug_report_page.php"

ln_issues_url = "https://rmc-github.robotic.dlr.de/schm-fl/links_and_nodes/issues"

