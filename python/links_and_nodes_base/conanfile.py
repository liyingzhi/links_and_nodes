import os

from conans import ConanFile, tools
from cissy.conantools import get_python_dist_versions, get_python_dist_latest_version, get_python_dist_requirement

class links_and_nodes_base_python_conan(ConanFile):
    python_requires = "ln_conan/[~4]@common/unstable"
    python_requires_extend = "ln_conan.Base"
    
    name = "links_and_nodes_base_python"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = None
    exports = [ "../../site_scons*" ]
    exports_sources = [
        # our self
        "../../python*",
        "!python/links_and_nodes_base/*.pyc",
        "!python/links_and_nodes/*",
        "!python/pyutils/*.pyc",
        "!python/pyutils/*.pyx",
        "!python/pyutils/.*",
        
        # will need base-build-system
        "../../SConstruct",        
    ] + ["!%s" % x for x in tools.Git().excluded_files()]

    options = {
        "python_version": ["2", "3"],
        "python_dist_version": get_python_dist_versions()
    }
    default_options = {
        "python_version": "2",
        "python_dist_version": get_python_dist_latest_version()
    }
    
    def init(self):
        base = self.python_requires["ln_conan"].module.Base
        base.set_attrs(self)
        
    def requirements(self):
        self.output.write("python dist requirement: %r\n" % get_python_dist_requirement(self.options.python_version.value, self.options.python_dist_version.value))
        self.requires(get_python_dist_requirement(self.options.python_version.value, self.options.python_dist_version.value))
        self.requires("links_and_nodes_ln_msgdef/%s" % self.same_branch_or("[>=1.2 <3]"))

    def source(self):
        self.write_version_file(os.path.join("libln", "version"))
            
    def build(self):
        self.scons_build("python", "--no-manager --no-python-api --no-python-lnrdb", with_python_lib=True)

    def package(self):
        install = os.path.join("build", "python", self.install_sandbox, self.prefix[1:])
        self.copy("version", src="libln", dst=os.path.join(self.python_path, "links_and_nodes_base")) # todo: this is strange
        self.copy("*", src=install, symlinks=True)
        
    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.env_info.PYTHONPATH.append(os.path.join(self.package_folder, self.python_path))
