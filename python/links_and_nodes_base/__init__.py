"""
    Copyright 2013-2019 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function
import traceback
import sys
import os

ln_debug = os.getenv("LN_DEBUG")

if os.getenv("LN_COVERAGE_RECORD", "0")[:1] in "1yt":
    already_enabled = hasattr(sys, "_ln_coverage_trace_active") and sys._ln_coverage_trace_active
    if not already_enabled:
        if ln_debug: print("ln_base: coverage recording was requested via environment!")
        lntest_path = os.getenv("LN_COVERAGE_RECORD_LNTEST_BASE", None)
        if lntest_path:
            sys.path.insert(0, lntest_path)
        import lntest.coverage_monitor
        sys._ln_coverage_monitor = lntest.coverage_monitor.coverage_monitor()
        coverage_fn = os.path.join(os.getenv("LN_COVERAGE_RECORD_DIR", os.getcwd()), "%s_%s_coverage.py" % (
            os.getenv("LN_COVERAGE_RECORD_NAME", "ln_base"), os.getpid()))
        sys._ln_coverage_monitor.start_recording(coverage_fn)
        if ln_debug: print("ln_base: coverage recording started to %r" % coverage_fn)

additional_message_definition_dirs = []
message_definition_dirs = []

package_base = file_dir = os.path.dirname(os.path.dirname(__file__))
# todo: check what from this is still
if ln_debug: print("libln file_dir: %r" % file_dir)
site_packages_dir = os.path.dirname(os.path.dirname(__file__))

source_dir = os.path.dirname(os.path.realpath(file_dir))
source_build_dir = os.path.join(source_dir, os.getenv("LN_BUILD_SUBDIR", "build"))
source_libln_dir = os.path.join(source_dir, "libln")

if os.path.isdir(source_dir) and os.path.isdir(source_libln_dir):
    # source tree
    if ln_debug: print("libln detected source tree!")
    ln_tree = "source"
    ln_release_version = "src"
    ln_libdir = os.path.join(source_build_dir, "libln")
    ln_manager_dir = os.path.join(source_dir, "python", "links_and_nodes_manager")
    ln_share_dir = os.path.join(source_dir, "share")
    builtin_msg_defs = os.path.join(ln_share_dir, "message_definitions")
    ln_runtime_dir = os.path.join(source_dir, "ln_runtime")
    runtime_msg_defs = [
        os.path.join(ln_runtime_dir, "lnrecorder", "message_definitions"),
        os.path.join(ln_runtime_dir, "file_services", "message_definitions")
    ]

    # import own pyutils
    import pyutils
    import pyutils.line_assembler

else: # assume installed into site_packages
    prefix = os.path.dirname(os.path.dirname(os.path.dirname(file_dir)))
    if ln_debug: print("links_and_nodes_base detected install in prefix %r" % prefix)
    ln_tree = "install"
    ln_share_dir = os.path.join(prefix, "share", "links_and_nodes")
    ln_release_version = "install-unknown"
    ptfn = os.path.join(os.path.dirname(__file__), "version")
    try:
        with open(ptfn, "rb") as fp:
            ln_release_version = fp.read().strip()
    except:
        ln_release_version = "<unknown release>"
    ln_libdir = os.path.join(prefix, "lib")
    ln_manager_dir = os.path.join(site_packages_dir, "links_and_nodes_manager")
    builtin_msg_defs = os.path.join(ln_share_dir, "message_definitions")
    runtime_msg_defs = [
        os.path.join(prefix, "share", "ln_runtime", "message_definitions")
    ]

default_gen_msg_defs = os.path.join(os.path.expanduser("~"), "ln_message_definitions", "gen")

ln_config_file = os.path.expanduser("~/.ln_config")
if os.path.isfile(ln_config_file):
    ln_config_reader = "client"
    exec(open(ln_config_file, "rb").read())

def rescan_message_definition_dirs():
    global message_definition_dirs, additional_message_definition_dirs
    
    message_definition_dirs = list(additional_message_definition_dirs)
    message_definition_dirs.append(builtin_msg_defs)
    message_definition_dirs += runtime_msg_defs
    message_definition_dirs.append(os.path.join(os.path.expanduser("~"), "ln_message_definitions"))

    md = os.getenv("LN_MESSAGE_DEFINITION_DIRS")
    if md is not None:
        message_definition_dirs.extend([os.path.realpath(s.strip()) for s in md.split(os.path.pathsep)])
    
    # search for md.conf files in these dirs!
    while True:
        new_md_dirs = []
        for dir in message_definition_dirs:
            if os.path.isdir(dir):
                fn = os.path.join(dir, "md.conf")
            elif os.path.isfile(dir):
                fn = dir
            else:
                continue
            if not os.path.isfile(fn):
                continue
            with open(fn, "r") as fp:
                for line in fp:
                    line = line.strip()
                    if not line or line[0] == "#":
                        continue
                    line = os.path.expanduser(line)
                    if not os.path.isabs(line):
                        line = os.path.join(os.path.dirname(fn), line)
                    new_dir = os.path.realpath(line)
                    if os.path.exists(new_dir) and new_dir not in message_definition_dirs:
                        new_md_dirs.append(new_dir)
        if not new_md_dirs:
            break # done
        for dir in new_md_dirs:
            if dir not in message_definition_dirs:
                message_definition_dirs.append(dir)

    # remove files from these dirs!
    message_definition_dirs = [dir for dir in message_definition_dirs if os.path.isdir(dir)]

postprocess_md_dirs = rescan_message_definition_dirs
rescan_message_definition_dirs()

from links_and_nodes_base.message_definitions import *
from links_and_nodes_base.lnm_remote import *
from links_and_nodes_base import util
