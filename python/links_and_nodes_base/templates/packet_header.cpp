/* %(mdname)s */
#include "ln/packed.h"
struct LN_PACKED %(sn_no_t)s_struct;
typedef struct %(sn_no_t)s_struct %(sn)s;
struct LN_PACKED %(sn_no_t)s_struct {
%(fields)s
};
#include "ln/endpacked.h"
