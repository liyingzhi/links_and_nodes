#!/usr/bin/env python
"""
    Copyright 2013-2015 DLR e.V., Daniel Leidner, Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

import sys
import os
import inspect
import traceback
import links_and_nodes
import gobject

#=================================================================================
#
#=================================================================================
class lnrpc_server():
    #=================================================================================
    def __init__(self, name, messages_path, *args, **kwargs):    
        self.use_gtk = kwargs.get("use_gtk", False)
        self.services = {}
        self.clnt = links_and_nodes.client("%s Server" %name, *args)
        for m in os.listdir(messages_path):
            if not m.startswith("."):
                svc = self.clnt.get_service_provider("%s.%s" %(name, m), "%s/%s" %(messages_path, m))
                svc.set_handler(self._process_message)
                if self.use_gtk:
                    svc.do_register_gobject()
                else:        
                    svc.do_register()
                self.services[m] = svc          
        # add raw interface
        svc = self.clnt.get_service_provider("%s.pyservice" %(name), "ln/pyservice")
        svc.set_handler(self._process_message)
        if self.use_gtk:
            svc.do_register_gobject()
        else:        
            svc.do_register()
        self.services["pyservice"] = svc

    #===============================================================
    def _process_message(self, svc, req, resp): 
        resp.exception = ""
        resp.result = None
        method_name = svc.name.rsplit(".")[-1]  
        if method_name == "pyservice":
            method = getattr(self, req.method_name, None)
            if method is None:
                resp.exception = "Method %r does not exist. " %(req.method_name,)
                svc.respond()
                return
            args = req.args
            kwargs = req.kwargs
        else:
            args = []
            kwargs = {}
            method = getattr(self, method_name, None)  
            if method is None:
                resp.exception = "No such method %s" % method_name
                svc.respond()
                return
            for key in inspect.getargspec(method).args:
                val = getattr(req, key, "") # think about 15, [], 
                if val != "":
                    kwargs[key] = val

        # calls the server method
        try:
            result = method(*args, **kwargs)
            if result is not None:
                resp.result = result
        except:
            ex = traceback.format_exc()   
            resp.exception = ex
            print(ex)
        svc.respond()

    #===============================================================        
    def run(self):  
        # Waiting for service requests   
        if self.use_gtk:
            loop = gobject.MainLoop()
            return loop.run()
        while self.keep_running:
            self.clnt.wait_and_handle_service_group_requests(None, 1)
