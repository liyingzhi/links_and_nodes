#!/usr/bin/python

"""
file services client python wrapper.

"""

import sys
import pprint
import links_and_nodes as ln

class file_services_client(ln.services_wrapper):
    def __init__(self, clnt, node):
        ln.services_wrapper.__init__(self, clnt, node)

        self.wrap_service("read_from_file", "ln/file_services/read_from_file")
        self.wrap_service("write_file", "ln/file_services/write_file")
        self.wrap_service("get_tree_info", "ln/file_services/get_tree_info")        
        self.wrap_service("chown", "ln/file_services/chown")
        self.wrap_service("clock_settime", "ln/file_services/clock_settime")
