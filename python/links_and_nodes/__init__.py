"""
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import print_function

from links_and_nodes_base import *
import sys
import sysconfig
platform_version = "%s-%s.%s" % (sysconfig.get_platform(), sys.version_info[0], sys.version_info[1])
if ln_tree == "source":
    python_ext_dir = os.path.join(os.path.dirname(__file__), "src-" + platform_version)
else:
    python_ext_dir = os.path.join(os.path.dirname(__file__), platform_version)
if ln_debug:
    print("links_and_nodes adds extension dir %r" % python_ext_dir)
sys.path.insert(0, python_ext_dir)
from . import ln_wrappers
from _ln import *

import os

def _read_path_var(name):
    value = os.getenv(name)
    if value:
        return value.split(os.path.pathsep)
    return []
def _search_lib(libname, add_to):
    for dir in _read_path_var("LD_LIBRARY_PATH") + [ ln_libdir ]:
        p = os.path.join(dir, libname)
        if os.path.isfile(p):
            if dir not in add_to:
                add_to.append(dir)
            return dir

package_base_api = os.path.dirname(os.path.dirname(__file__))

ld_library_path = [ ln_libdir ] # assumes libln (& libboost_python, ...) is installed to same prefix as links_and_nodes_base!
_search_lib("libln.so", ld_library_path)
_search_lib("libboost_python.so", ld_library_path)
# conan users will have to use other means to provide a valid LD_LIBRARY_PATH!
# (e.g. by deriving their processes from cissy_workspace generated templates,
# or by specifying "pass_environment: LD_LIBRARY_PATH" ...)

pythonpath = list(set([package_base_api, package_base]))

from links_and_nodes.ln_wrappers import *
