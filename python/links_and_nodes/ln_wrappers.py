# Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon
#
# This file is part of links and nodes.
#
# links and nodes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# links and nodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

try:
    import cPickle as pickle
except:
    import pickle
import numpy
import sys
import pprint
import threading
import copy
import inspect
import pyutils
import links_and_nodes

from numpy import *
from links_and_nodes import *
from collections import OrderedDict as odict

is_py2 = sys.version_info < (3, 0)
if not is_py2:
    unicode = str

our_repr_options = dict(
    max_line_width=sys.maxsize, separator=",",
    formatter=dict(all=str)
)
this_version = tuple(map(int, numpy.version.version.split(".")[:2]))
numpy_array2string_has_threshold = this_version >= (1, 14)
if numpy_array2string_has_threshold:
    our_repr_options["threshold"] = sys.maxsize

def with_lock(method):
    def with_lock_held(self, *args, **kwargs):
        with self.lock:
            return method(self, *args, **kwargs)
    return with_lock_held

class attr_dict(dict):
    def __getattr__(self, name):
        return self[name]
    def __setattr__(self, name, value):
        self[name] = value
    def __setstate__(self, state):
        self.__dict__.update(state)

def do_stack_fields(data, prefix, out_name=None, transpose=True):
    keys = []
    for key in data.keys():
        if key.startswith(prefix):
            keys.append(key)
    keys.sort()
    if out_name is None:
        out_name = prefix
    out = attr_dict()
    for key in keys:
        for field, field_data in data[key].items():
            if field not in out:
                out[field] = []
            l = out[field]
            l.append(field_data)
    data[out_name] = out
    for field, field_data in out.items():
        out[field] = array(field_data)
        if transpose:
            out[field] = out[field].transpose()
    
# maybe option to get char* as buffer objects instead of strings?!

class ln_packet_part(object):
    def __init__(self, field_name, type_name):
        self._name = field_name
        self._type_name = type_name
        self._fields = []
        self._field_ctors = {} # field-name -> constructor method
        self._field_types = {} # field-name -> type-name

    def dict(self):
        ret = dict()
        for k in self._fields:
            v = getattr(self, k)
            if type(v) == ln_packet_part:
                v = v.dict()
            elif type(v) == list and len(v) and type(v[0]) == ln_packet_part:
                v = map(lambda item: item.dict(), v)
            ret[k] = v
        return ret

    def __str__(self):
        if self._type_name != self._name:
            name_type = "%s %s" % (self._type_name, self._name)
        else:
            name_type = self._name
        return "<%s:\n\t%s\n>" % (name_type, ",\n\t".join(map(lambda f: "%s: %r" % f, self.iteritems())))
    def __repr__(self):
        return str(self)

    def keys(self):
        return self._fields
    def items(self):
        return map(lambda k: (k, getattr(self, k)), self._fields)
    def iteritems(self):
        return self.items()
    def __iter__(self):
        if not is_py2:
            self.items()
        for item in self.items():
            yield item

    def get_field_ctors(self):
        return self._field_ctors
    def get_field_types(self):
        return self._field_types

    def __deepcopy__(self, memo):
        import copy
        new = ln_packet_part(self._name, self._type_name)
        new._fields = self._fields
        new._field_ctors = self._field_ctors
        new._field_types = self._field_types
        for k in self._fields:
            v = getattr(self, k)
            setattr(new, k, copy.deepcopy(v, memo))
        return new
    
class ln_packet(ln_packet_part):
    def __init__(self, md_name, md, size, use_numpy_arrays=True, auto_cast=True):
        ln_packet_part.__init__(self, md_name, md_name)
        self._use_numpy_arrays = use_numpy_arrays
        self._auto_cast = auto_cast
        self._utf8_decode_char_fields = True
        self._md = eval(md)
        self._size = size
        self._data = bytearray(size)
        self._data_buffer = memoryview(self._data)
        self._define_formats = {}
        self._unpacks, size = self._get_struct_format(self._md["fields"])
        self._init()

    def utf8_decode_char_fields(self, do_decode):
        """
        call utf8_decode_char_fields(False) to disable automatic utf-8 decoding
        of char array/poitner fields
        """
        self._utf8_decode_char_fields = do_decode

    def _get_struct_format(self, fields):
        unpacks = []
        size = 0
        for field_type, field_name, field_count in fields:
            is_primitive = field_type in data_type_map

            if is_primitive:
                data_type = data_type_map[field_type]
                size += data_type.size * field_count
                if field_count > 1 and self._use_numpy_arrays and field_type != "char":
                    unpacks.append((field_name, field_count, data_type.npformat, data_type.size, field_type))
                else:
                    unpacks.append((field_name, field_count, data_type.pyformat, data_type.size, field_type))
                continue

            # not a primitive type
            if field_type not in self._define_formats:
                self._define_formats[field_type] = self._get_struct_format(self._md["defines"][field_type])
            try:
                inner_unpacks, inner_size = self._define_formats[field_type]
            except:
                raise Exception("invalid message definition! field %r has unknown field_type %r" % (field_name, field_type))
            unpacks.append((field_name, field_count, inner_unpacks, inner_size, field_type))
            size += inner_size * field_count
        return unpacks, size

    def _unpack(self, offset=0):
        def unpack(obj, unpacks, offset=0):
            #print "unpack part %r offset %d" % (obj._name, offset)
            for field_name, field_count, field_type, field_size, field_type_name in unpacks:
                ft = type(field_type)
                #print "  field %r, count %d, size %d, type %s" % (field_name, field_count, field_size, field_type)
                if ft == list:
                    if field_count == 1:
                        unpack(getattr(obj, field_name), field_type, offset=offset)
                        offset += field_size
                    else:
                        for inner_obj in getattr(obj, field_name):
                            unpack(inner_obj, field_type, offset=offset)
                            #print "field_size", field_size
                            offset += field_size
                    continue
                if ft == str:
                    if field_type == "b":
                        field_type = "B"
                    if field_count == 1:
                        field_value = struct.unpack_from(field_type, self._data_buffer, offset)[0]
                        offset += field_size
                    elif field_type == "B":
                        field_value = self._data_buffer[offset:offset + field_count].tobytes()
                        if self._utf8_decode_char_fields:
                            field_value = field_value.rstrip(b"\0")
                            try:
                                field_value = field_value.decode("utf-8")
                            except:
                                raise Exception("can not decode char-array!")
                        offset += field_count
                    else:
                        field_value = struct.unpack_from(field_type * field_count, self._data_buffer, offset)
                        offset += field_size * field_count
                    #print "  field %r: %r" % (field_name, field_value)
                    setattr(obj, field_name, field_value)
                    continue
                # unpack numpy array
                links_and_nodes.copy2array(getattr(obj, field_name), self._data, offset, field_size * field_count)
                offset += field_size * field_count
        unpack(self, self._unpacks, offset)

    def _init(self):
        def init(obj, unpacks):
            for field_name, field_count, field_type, field_size, field_type_name in unpacks:
                obj._fields.append(field_name)
                ft = type(field_type)
                if ft == list:
                    if field_count == 1:
                        inner_obj = ln_packet_part(field_name, field_type_name)
                        setattr(obj, field_name, inner_obj)
                        init(inner_obj, field_type)
                    else:
                        l = []
                        setattr(obj, field_name, l)
                        for i in range(field_count):
                            l.append(ln_packet_part("%s[%d]" % (field_name, i), field_type_name))
                        for inner_obj in l:
                            init(inner_obj, field_type)
                    continue
                if ft == str:
                    if field_count == 1:
                        field_value = 0
                    else:
                        field_value = [0] * field_count
                    setattr(obj, field_name, field_value)
                    continue
                # unpack numpy array
                field_value = numpy.zeros((field_count, ), dtype=field_type)
                setattr(obj, field_name, field_value)
        init(self, self._unpacks)


    def _pack(self):
        def pack(obj, unpacks, offset=0):
            #print "pack part %r offset %d" % (obj._name, offset)
            for field_name, field_count, field_type, field_size, field_type_name in unpacks:
                field_value = getattr(obj, field_name)
                try:
                    ft = type(field_type)
                    if ft == list: # field type is a list describing a complex data type
                        if field_count == 1:
                            pack(field_value, field_type, offset=offset)
                            offset += field_size
                        else:
                            for inner_obj in field_value:
                                pack(inner_obj, field_type, offset=offset)
                                offset += field_size
                        continue
                    if ft == str: # field_type is a str with the name of a simple data type
                        if field_type == "b" and isinstance(field_value, (str, unicode)):
                            field_type = "B"
                            if not is_py2 or isinstance(field_value, unicode):
                                field_value = field_value.encode("utf-8")
                            self._data[offset:offset + len(field_value)] = field_value
                            offset += field_size * field_count
                            continue
                        if field_count == 1:
                            struct.pack_into(field_type, self._data, offset, field_value)
                            offset += field_size
                        else:
                            struct.pack_into(field_type * field_count, self._data, offset, *field_value)
                            offset += field_size * field_count
                        continue
                    # numpy array type
                    if type(field_value) != numpy.ndarray:
                        if self._auto_cast:
                            field_value = numpy.array(field_value, dtype=field_type).flatten()
                        else:
                            raise Exception("field %r is not a numpy-array: %r" % (field_name, field_value))
                    if field_value.dtype != field_type:
                        if self._auto_cast:
                            field_value = numpy.array(field_value, dtype=field_type).flatten()
                        else:
                            raise Exception("field %r has wrong dtype %r instead of %r" % (field_name, field_value.dtype, field_type))
                    if numpy.size(field_value) != field_count:
                        raise Exception("field %r has wrong size %d shape %r instead of %r" % (field_name, numpy.size(field_value), field_value.shape, field_count))

                    links_and_nodes.copy2buffer(self._data, offset, field_value, field_size * field_count)
                    offset += field_size * field_count
                except:
                    raise Exception("could not pack field %r with value %r for type %r!" % (
                        field_name, field_value, field_type))
        pack(self, self._unpacks)

    def get_dict(self):
        d = {}
        for field_name, field_count, field_type, field_size, field_type_name in self._unpacks:
            value = getattr(self, field_name)
            if type(value) == ln_packet:
                value = value.get_dict()
            elif type(value) == ln_packet_part:
                value = value.dict()
            d[field_name] = value
        return d
        
class port_wrapper(object):
    def __init__(self, md_name, md, size, msg_def_hash, port, client, topic_name, need_reliable_transport=False):
        self.client = client
        self.topic_name = topic_name
        self.msg_def_hash = msg_def_hash
        self.packet = ln_packet(md_name, md, size)
        self.port = port
        self.need_reliable_transport = need_reliable_transport
        self.timestamp = 0

        #self.first = True

    def read(self, blocking_or_timeout=True):
        """
        blocking_or_timeout might be
          bool: True if blocking operation is wanted
        or
          double: timeout in seconds for blocking operation
        """
        new_packet = self.port.read(self.packet._data, blocking_or_timeout)
        if not new_packet:
            return None # no new packet
        self.timestamp = self.port.timestamp
        self.packet._unpack()
        return self.packet

    def write(self, timestamp=None):
        # todo: timestamp!
        self.packet._pack()
        if timestamp is None:
            self.port.write(self.packet._data)
        else:
            self.port.write_ts(self.packet._data, timestamp)

    def has_subscriber(self):
        return self.port.has_subscriber()

    def has_publisher(self):
        return self.port.has_publisher()

    def unblock(self):
        self.port.unblock()

class request_wrapper(object):
    """
    service request wrapper
    """
    def __init__(self, service, request):
        self.service = service
        self.request = request
        self.name = self.service.name
    def respond(self):
        self.service.respond(request=self.request)
    def is_aborted(self):
        return self.request.is_aborted()
    def abort(self):
        return self.request.abort()
class service_wrapper(object):
    def __init__(self, name, interface, definition, signature, svc, clnt, use_numpy_arrays=True, auto_cast=True, is_provider=False, record_call_stacks=False):
        self.name = name
        self.interface = interface
        self.lock = threading.RLock()
        if type(definition) == dict:
            self.definition = definition
        else:
            try:
                self.definition = eval(definition)
            except:
                raise Exception("invalid definition: %r\n%s" % (definition, definition))
        self.signature = signature
        self.svc = svc
        self.clnt = clnt
        self._use_numpy_arrays = use_numpy_arrays
        self._auto_cast = auto_cast
        self._is_provider = is_provider
        self.async_req = None
        self.async_notification_fd = None
        self.async_notification_source = None
        self.on_async_finish_handler = None
        self.call_finished = True
        self._utf8_decode_char_fields = True

        self.defines = self.definition["defines"]

        self.resp = ln_packet_part(self.name + ".resp", interface + "::response")
        self._resp_fields = self.definition["resp_fields"]

        self.req = ln_packet_part(self.name + ".req", interface + "::request")
        self._req_fields = self.definition["fields"]

        if self._is_provider:
            self._prepare_pack_request(req_and_fields=(self.resp, self._resp_fields))
            self._prepare_unpack_response(fields_and_def=(self.req, self._req_fields))
        else:
            self._prepare_pack_request()
            self._prepare_unpack_response()
        
        self.cb = None
        self.fd_handler = None
        self._client_fd_sources = {}
        self.gobject_source_ids = {}

        if struct.pack("H", 0xaabb)[0] == '\xbb':
            self.host_byte_order = "little"
        else:
            self.host_byte_order = "big"
            
        self._need_swap = False
        self.record_call_stacks = record_call_stacks
        self.call_stack = []

        if self.clnt and self._is_provider: # only service providers are automatically "hold" by client!
            if not hasattr(self.clnt, "_services"):
                self.clnt._services = set()
            self.clnt._services.add(self)

    def __del__(self):
        if self.clnt: self.clnt.release_service(self) # will trigger call of _cleanup

    def _cleanup(self):
        """ called by internal API _ln.so/release_service
        """
        if self.gobject_source_ids:
            import gobject
            for fd, sid in self.gobject_source_ids.items():
                gobject.source_remove(sid)
            self.gobject_source_ids = {}
            for fd, source_id in self._client_fd_sources.items():
                gobject.source_remove(source_id)
            self._client_fd_sources = {}
        if self.clnt and hasattr(self.clnt, "_services") and self in self.clnt._services:
            self.clnt._services.remove(self)
        self.__dict__.clear() # release all other references        

    def set_handler(self, cb, *cb_args):
        self.cb = cb, cb_args

    def do_register(self, group_name=None):
        self.svc.do_register(self, group_name)

    def utf8_decode_char_fields(self, do_decode):
        """
        call utf8_decode_char_fields(False) to disable automatic utf-8 decoding
        of char array/poitner fields
        """
        self._utf8_decode_char_fields = do_decode

    def get_fd(self):
        return self.svc.get_fd()
        
    def _fd_handler(self, event, fd):
        if not self.fd_handler:
            return
        try:
            self.fd_handler[0](event, fd, *self.fd_handler[1])
        except:
            print("fd_handler threw exception:\n%s" % traceback.format_exc())
        
    def set_client_fd_handler(self, handler, *args):
        if handler is None:
            self.fd_handler = None
            return
        self.fd_handler = handler, args
        self.svc.set_client_fd_handler(self)

    def do_register_gobject(self, group_name=None):
        import gobject
        self.svc.do_register(self, group_name)
        def on_io(fd, why):
            #print "new on_io %r, %r" % (fd, why)
            self.clnt.wait_and_handle_service_group_requests(group_name, 0)
            return fd in self.gobject_source_ids or fd in self._client_fd_sources
        fd = self.get_fd()
        self.gobject_source_ids[fd] = gobject.io_add_watch(fd, gobject.IO_IN, on_io) # for new provider fds
        def on_client_fd(ev, fd):
            #print "on_client_fd: %r, %r" % (ev, fd)
            if ev == "new":
                self._client_fd_sources[fd] = gobject.io_add_watch(fd, gobject.IO_IN, on_io) # for new requests from existing clients
            elif ev == "remove" and fd in self._client_fd_sources:
                gobject.source_remove(self._client_fd_sources[fd])
                del self._client_fd_sources[fd]
            elif ev == "new_svc_fd":
                self.gobject_source_ids[fd] = gobject.io_add_watch(fd, gobject.IO_IN, on_io) # for new connections
            elif ev == "remove_svc_fd" and fd in self.gobject_source_ids:
                gobject.source_remove(self.gobject_source_ids[fd])
                del self.gobject_source_ids[fd]
            return True
        self.set_client_fd_handler(on_client_fd)
    do_register_gtk = do_register_gobject
        
    @with_lock
    def _handler(self, req):
        if not self.cb:
            print("no handler for service %r!" % self.name)
            return 0

        req_wrap = request_wrapper(self, req)
        try:
            self._unpack_response(fields_and_def=(self.req, self._req_fields))
            ret = self.cb[0](req_wrap, self.req, self.resp, *self.cb[1])
        except:
            print("could not unpack_response():\n%s" % traceback.format_exc())
            return 1
        return ret
    
    def respond(self, request):
        # pack self.resp into iovec structure and send back!
        self._check_request(req_and_fields=(self.resp, self._resp_fields))
        try:
            request.respondv(self)
        except ValueError:
            raise ValueError("_resp_fields:\n%s\n%s" % (pprint.pformat(self._resp_fields), sys.exc_value))

    @with_lock
    def __call__(self, *args, **kwargs):
        if "async" in kwargs:
            is_async = kwargs["async"]
            del kwargs["async"]
        else:
            is_async = False
        for arg, value in kwargs.items():
            setattr(self.req, arg, value)
        self._check_request()
        if not is_async:
            self.async_req = None
            self.svc.call(self)
            return self
        self.async_unpacked_response = False
        if hasattr(self, "async_running") and self.async_running:
            raise Exception("async request for service %r already running!" % self.name)
        self.async_running = True
        self.async_req = self.svc.call_async(self)
        return self

    def call(self, *args, **kwargs):
        """
        blocking call of service
        """
        return self(*args, **kwargs)

    def abort(self):
        """
        abort running blocked call() from another thread
        """
        self.svc.abort()
    
    def call_async(self, *args, **kwargs):
        """
        non-blocking call of service.
        check finish with self.finished()
        or register callback with gobject_on_async_finish
        """
        kwargs["async"] = True
        return self(*args, **kwargs)

    def abort_async(self):
        """
        abort running call_async()
        """
        if self.async_req is None:
            raise Exception("error, there is no async request pending!")
        self.async_running = False
        self.async_req.abort()

    @with_lock
    def call_via_mainloop(self, *args, **kwargs):
        """
        blocking call of service
        but allowing the specified mainloop to run.
        """
        loop = self._mainloop

        if not hasattr(self, "async_running"):
            self.async_running = False

        if self.async_running:
            if not hasattr(self, "async_wait_count"):
                self.async_wait_count = 0
            self.async_wait_count += 1
            if self.record_call_stacks:
                print("service call to %r already running! gobject waiting (wait depth: %d)! stack:\n%s" % (
                    self.name, self.async_wait_count, "".join(traceback.format_stack())))
            else:
                print("service call to %r already running! gobject waiting (wait depth: %d)!" % (
                    self.name, self.async_wait_count))
            while self.async_running:
                loop.iterate(True)
            print("service call to %r waiter %d done" % (
                self.name, self.async_wait_count))
            self.async_wait_count -= 1

        if self.record_call_stacks:
            self.call_stack.append("".join(traceback.format_stack()))
        if "timeout" in kwargs:
            timeout = kwargs["timeout"]
            del kwargs["timeout"]
        else:
            timeout = None
        try:
            self.call_async(*args, **kwargs)
        except:
            if self.record_call_stacks:
                print("have exception on call_async(%r). call stacks:" % self.name)
                for i, stack in enumerate(self.call_stack):
                    print("stack %d:\n%s\n" % (i, stack))
                if len(self.call_stack):
                    self.call_stack.pop()
            raise
        if timeout is not None:
            def on_timeout():
                self.had_timeout = True
                return False
            timeout_id = loop.timeout_add(timeout, on_timeout)
        self.call_finished = False
        self.had_timeout = False
        def on_finish():
            self.call_finished = True
            if self.record_call_stacks and len(self.call_stack):
                self.call_stack.pop()
            return False
        self.mainloop_on_async_finish(loop, on_finish)
        while not self.call_finished:
            loop.iterate(True)
        self.async_running = False
        if timeout is not None:
            loop.source_remove(timeout_id)
        if self.had_exception is not None:
            raise self.had_exception
        if self.had_timeout:
            raise Exception("service call %s(%r, %r) timed out (timeout: %.1fs)" % (self.name, args, kwargs, timeout))

    def mainloop_on_async_finish(self, loop, handler, *args, **kwargs):
        if self.async_req is None:
            raise Exception("error, there is no async request pending!")
        self.on_async_finish_handler = handler, args, kwargs
        self.async_notification_fd = self.async_req.get_finished_notification_fd()
        def on_mainloop_io(fd):
            self.had_exception = None
            try:
                self.finished() # unpack and satisfy notification pipe
            except:
                self.had_exception = sys.exc_value
            loop.source_remove(self.async_notification_source)
            self.async_notification_source = None
            if self.on_async_finish_handler:
                # notify user
                handler, args, kwargs = self.on_async_finish_handler
                self.on_async_finish_handler = None
                try:
                    handler(*args, **kwargs)
                except:
                    print("error in async service request %r handler %r:\n%s" % (
                        self.name, handler, traceback.format_exc()))
            return False
        self.async_notification_source = loop.fd_add(
            self.async_notification_fd, "in", on_mainloop_io)

    @with_lock
    def call_gobject(self, *args, **kwargs): # todo: remove this providing pygtk gobject mainloop wrapper
        """
        blocking call of service 
        but allowing the gobject mainloop to run.
        """

        if not hasattr(self, "async_running"):
            self.async_running = False

        import gtk
        import gobject
        if self.async_running:
            if not hasattr(self, "async_wait_count"):
                self.async_wait_count = 0
            self.async_wait_count += 1
            if self.record_call_stacks:
                print("service call to %r already running! gobject waiting (wait depth: %d)! stack:\n%s" % (
                    self.name, self.async_wait_count, "".join(traceback.format_stack())))
            else:
                print("service call to %r already running! gobject waiting (wait depth: %d)! service return value of already running requests will be lost!" % (
                    self.name, self.async_wait_count))
            while self.async_running:
                gtk.main_iteration(True)
            print("service call to %r waiter %d done" % (
                self.name, self.async_wait_count))
            self.async_wait_count -= 1
            
        if self.record_call_stacks:
            self.call_stack.append("".join(traceback.format_stack()))
        if "timeout" in kwargs:
            timeout = kwargs["timeout"]
            del kwargs["timeout"]
        else:
            timeout = None
        try:
            self.call_async(*args, **kwargs)
        except:
            if self.record_call_stacks:
                print("have exception on call_async(%r). call stacks:" % self.name)
                for i, stack in enumerate(self.call_stack):
                    print("stack %d:\n%s\n" % (i, stack))
                if len(self.call_stack): 
                    self.call_stack.pop()
            raise
        if timeout is not None:
            def on_timeout():
                self.had_timeout = True
                return False
            timeout_id = gobject.timeout_add(int(timeout * 1000), on_timeout)
        self.call_finished = False
        self.had_timeout = False
        def on_finish():
            self.call_finished = True
            if self.record_call_stacks and len(self.call_stack): 
                self.call_stack.pop()
            return False
        self.gobject_on_async_finish(on_finish)
        while not self.call_finished:
            gtk.main_iteration(True)
        self.async_running = False
        if timeout is not None:
            gobject.source_remove(timeout_id)
        if self.had_exception is not None:
            raise self.had_exception
        if self.had_timeout:
            raise Exception("service call %s(%r, %r) timed out (timeout: %.1fs)" % (self.name, args, kwargs, timeout))
        
    def gobject_on_async_finish(self, handler, *args, **kwargs):
        if self.async_req is None:
            raise Exception("error, there is no async request pending!")
        self.on_async_finish_handler = handler, args, kwargs
        self.async_notification_fd = self.async_req.get_finished_notification_fd()
        import gobject
        def on_io(fd, why):
            self.had_exception = None
            try:
                self.finished() # unpack and satisfy notification pipe
            except:
                self.had_exception = sys.exc_value
            gobject.source_remove(self.async_notification_source)
            self.async_notification_source = None
            if self.on_async_finish_handler:
                # notify user
                handler, args, kwargs = self.on_async_finish_handler
                self.on_async_finish_handler = None
                try:
                    handler(*args, **kwargs)
                except:
                    print("error in async service request %r handler %r:\n%s" % (
                        self.name, handler, traceback.format_exc()))
            return False
        self.async_notification_source = gobject.io_add_watch(
            self.async_notification_fd, gobject.IO_IN, on_io) # for new connections

    def finished(self):
        if self.async_req is None:
            raise Exception("error, there is no async request pending!")
        if self.async_unpacked_response:
            return True
        finished = self.async_req.finished()
        if finished:
            self.async_running = False
        if finished and not self.async_unpacked_response:
            self.async_unpacked_response = True
            self.svc.async_unpack_response(self)
        return finished

    def _req_check_len(self, packet, fields):
        for field in fields:
            #print "check len of", field
            field_type, field_name, field_count = field[:3]
            is_pointer = field_type[-1] == "*"
            if is_pointer:
                # set len field of dynamically sized object
                field_value = getattr(packet, field_name, None)
                if field_type[:-1] == "char":
                    if field_value == None:
                        field_value = ""
                        setattr(packet, field_name, field_value)
                    elif not isinstance(field_value, (str, unicode, bytes)):
                        if self._auto_cast:
                            field_value = str(field_value)
                            setattr(packet, field_name, field_value)
                        else:
                            raise Exception("field %r has to be a string! but its type is: %r: %r" % (field_name, type(field_value), field_value))
                if type(field_value) in (str, list, tuple):
                    field_len = len(field_value)
                elif field_value is None:
                    field_len = 0
                else:
                    field_len = numpy.size(field_value)
                setattr(packet, field_name + "_len", field_len)
                #print "field %r type %r, has length %d: %r" % (field_name, field_type, field_len, field_value)
                field_type = field_type[:-1]
                field_count = field_len
            is_primitive = field_type in data_type_map

            if is_primitive:
                continue

            if field_type == "pyobject":
                # check len
                field_value = getattr(packet, field_name)
                if field_count > 1:
                    if field_count != len(field_value):
                        raise Exception("field %r needs length %d you provided length %d!" % (field_name, field_count, len(field_value)))
                continue
            # not a primitive type
            if field_type not in self.defines:
                raise Exception("invalid message definition! field %r has unknown field_type %r" % (field_name, field_type))
            field_value = getattr(packet, field_name)
            if not is_pointer and field_count == 1:
                self._req_check_len(field_value, self.defines[field_type])
                continue
            # pointer or field_count > 1
            if len(field_value) != field_count:
                raise Exception("field %r has only %d entries, while %d are needed: %s" % (
                        field_name, len(field_value), field_count, field_value))
            for i in range(field_count):
                self._req_check_len(field_value[i], self.defines[field_type])
    def _req_check(self, packet, fields):
        if not isinstance(packet, (ln_packet, ln_packet_part)):
            raise Exception("packet has to be of non-primitive ln_packet with these fields:\n%s\nnot %r" % (
                fields, packet))
        for field in fields:
            field_type, field_name, field_count = field[:3]
            field_value = getattr(packet, field_name)

            is_pointer = field_type[-1] == "*"
            if is_pointer:
                field_type = field_type[:-1]
            is_primitive = field_type in data_type_map

            if is_primitive:
                data_type = data_type_map[field_type]
                byte_size = data_type.size * field_count
                if (field_count > 1 or is_pointer) and self._use_numpy_arrays and field_type != "char":
                    # check numpy array!
                    if type(field_value) != numpy.ndarray:
                        if self._auto_cast:
                            if type(field_value) == str:
                                field_value = numpy.fromstring(field_value, data_type.npformat)
                            elif type(field_value) == bytes:
                                field_value = numpy.frombuffer(field_value, data_type.npformat) # py3
                            else:
                                field_value = numpy.array(field_value, dtype=data_type.npformat).flatten()
                            if is_pointer: setattr(packet, field_name + "_len", len(field_value))
                            setattr(packet, field_name, field_value)
                        else:
                            raise Exception("field %r is not a numpy-array: %r" % (field_name, field_value))
                    if field_value.dtype != data_type.npformat:
                        if self._auto_cast:
                            field_value = numpy.array(field_value, dtype=data_type.npformat).flatten()
                            if is_pointer: setattr(packet, field_name + "_len", len(field_value))
                            setattr(packet, field_name, field_value)
                        else:
                            raise Exception("field %r has wrong dtype %r instead of %r" % (field_name, field_value.dtype, data_type.npformat))
                    if not is_pointer and numpy.size(field_value) != field_count:
                        raise Exception("field %r has wrong size %d shape %r instead of %d: %r" % (field_name, numpy.size(field_value), field_value.shape, field_count, field_value))
                    continue
                if data_type.name == "char":
                    # type(field_value) != str is done in _req_check_len!
                    if is_pointer:
                        continue
                    continue
                if data_type.name in ("double", "float") and type(field_value) != float:
                    setattr(packet, field_name, float(field_value))
                continue
            # not a primitive type
            inner_fields = self.defines[field_type]
            if not is_pointer and field_count == 1:
                if field_type == "pyobject":
                    pybject_packet = getattr(packet, field_name + "_packet")
                    pybject_packet.data = pickle.dumps(field_value)
                    pybject_packet.data_len = len(pybject_packet.data)
                    pybject_packet.is_pickle = 1
                    field_value = pybject_packet
                if not isinstance(field_value, (ln_packet, ln_packet_part)):
                    raise Exception("field %r has to be of non-primitive type %r, not %r" % (field_name, field_type, type(field_value)))
                self._req_check(field_value, inner_fields)
                continue
            # either pointer or list or non-primitives
            if field_type != "pyobject":
                for ii, inner_packet in enumerate(field_value):
                    if not isinstance(inner_packet, (ln_packet, ln_packet_part)):
                        raise Exception("%d. item of field %s has to be of non-primitive type %s, not %r\nuse .new_%s_packet() to get a new packet of this type!" % (
                            ii+1, field_name, field_type, inner_packet, field_type))
                    self._req_check(inner_packet, inner_fields)
                continue
            # list of pyobjects
            pyobject_list = getattr(packet, field_name + "_packet")
            for i, inner_packet in enumerate(field_value):
                if i >= len(pyobject_list):
                    pyobject_list.append(getattr(packet, "new_%s_packet" % field_type)())
                pyobject_packet = pyobject_list[i]
                pyobject_packet.data = pickle.dumps(inner_packet)
                pyobject_packet.data_len = len(pyobject_packet.data)
                pyobject_packet.is_pickle = 1
                self._req_check(pyobject_packet, inner_fields)
                
    def _check_request(self, req_and_fields=None):
        if req_and_fields is None: # default is to check request, not response
            req_and_fields = self.req, self._req_fields
        self._req_check_len(*req_and_fields)
        self._req_check(*req_and_fields)

    def __prepare_pack_request(self, packet, fields):
        """
        this function has a deliberate side effect: it modifies/extends the `fields` positional parameter!
        """
        for ii, field in enumerate(fields):
            field_type, field_name, field_count = field[:3]
            packet._fields.append(field_name)
            packet._field_types[field_name] = field_type
            is_pointer = field_type[-1] == "*"
            if is_pointer:
                field_type = field_type[:-1]
            is_primitive = field_type in data_type_map
            first_time = len(field) == 3 # first time this md is examined
            if first_time: 
                fields[ii].extend((field_type, is_pointer, is_primitive))

            if is_primitive:
                data_type = data_type_map[field_type]
                if first_time:
                    fields[ii].append(data_type.name)
                size = data_type.size * field_count
                if (field_count > 1 or is_pointer) and self._use_numpy_arrays and data_type.name != "char":
                    if is_pointer:
                        field_count = 0
                    field_value = numpy.zeros((field_count, ), dtype=data_type.npformat)
                    setattr(packet, field_name, field_value)
                    continue
                if data_type.name == "char":
                    if not is_pointer:
                        setattr(packet, field_name, " " * field_count)
                        continue
                    setattr(packet, field_name, "")
                    continue
                default_value = struct.unpack(data_type.pyformat, b"\0" * data_type.size)[0]
                if not is_pointer and field_count == 1:
                    # single value
                    setattr(packet, field_name, default_value)
                    continue
                # points or multi value
                if is_pointer:
                    inner_list = []
                else:
                    inner_list = [default_value] * field_count
                setattr(packet, field_name, inner_list)
                continue
            # not a primitive type
            if field_type not in self.defines:
                raise Exception("invalid message definition! field %r has unknown field_type %r" % (field_name, field_type))
            if first_time:
                fields[ii].append(self.defines[field_type])
            if not is_pointer and field_count == 1:
                inner_packet = ln_packet_part(field_name, field_type)
                self.__prepare_pack_request(inner_packet, self.defines[field_type])
                if field_type == "pyobject":
                    setattr(packet, field_name, None)
                    setattr(packet, field_name + "_packet", inner_packet)
                else:
                    setattr(packet, field_name, inner_packet)
                continue
            # pointer or field_count > 1
            inner_list = []
            setattr(packet, field_name, inner_list)
            if is_pointer: # leave list empty
                # but create packet constructor!
                def create_ctor(default_name, fields):
                    def create(name=None, **kwargs):
                        if name is None:
                            name = default_name
                        p = ln_packet_part(name, field_type)
                        self.__prepare_pack_request(p, fields)
                        p.__dict__.update(kwargs)
                        return p
                    return create
                ctor_name = "new_%s_packet" % field_type
                packet._field_ctors[field_name] = create_ctor(field_name, self.defines[field_type])
                setattr(packet, ctor_name, packet._field_ctors[field_name])
                if field_type == "pyobject":
                    setattr(packet, field_name + "_packet", [])
                # call it atleast once to support scenarious liek in #198 where the ctor() from above is never called
                self.__prepare_pack_request(ln_packet_part("init", field_type), self.defines[field_type]) # packet itself is thrown away...
                continue
            if field_type == "pyobject":
                pyobject_list = []
                setattr(packet, field_name + "_packet", pyobject_list)
                for i in range(field_count):
                    inner_list.append(None)
                    inner_packet = ln_packet_part("%s[%d]" % (field_name, i), field_type)
                    pyobject_list.append(inner_packet)
                    self.__prepare_pack_request(inner_packet, self.defines[field_type])
                continue
            # prepare list
            for i in range(field_count):
                inner_packet = ln_packet_part("%s[%d]" % (field_name, i), field_type)
                inner_list.append(inner_packet)
                self.__prepare_pack_request(inner_packet, self.defines[field_type])
        
    def _prepare_pack_request(self, req_and_fields=None):
        # only executed once
        if req_and_fields is None:
            req_and_fields = self.req, self._req_fields
        self.__prepare_pack_request(*req_and_fields)
        # managed by _ln:
        self._cast_buffer = bytearray(1)
        self._iovec_buffer = bytearray(1)
        self._iovec_element_lens_buffer = bytearray(1)

    ########## response ###########
    def __prepare_unpack_response(self, packet, fields):
        for field in fields:
            field_type, field_name, field_count = field[:3]
            packet._fields.append(field_name)
            packet._field_types[field_name] = field_type
            is_pointer = field_type[-1] == "*"
            if is_pointer:
                field_type = field_type[:-1]
            is_primitive = field_type in data_type_map

            if is_primitive:
                data_type = data_type_map[field_type]
                size = data_type.size * field_count
                if (field_count > 1 or is_pointer) and self._use_numpy_arrays and field_type != "char":
                    field_value = numpy.empty((field_count, ), dtype=data_type.npformat)
                    setattr(packet, field_name, field_value)
                continue
            # not a primitive type
            if field_type not in self.defines:
                raise Exception("invalid message definition! field %r has unknown field_type %r" % (field_name, field_type))
            if field_type == "pyobject":
                field_name = field_name + "_packet"
            if not is_pointer and field_count == 1:
                inner_packet = ln_packet_part(field_name, field_type)
                setattr(packet, field_name, inner_packet)
                self.__prepare_unpack_response(inner_packet, self.defines[field_type])
                continue
            # pointer or field_count > 1
            inner_list = []
            setattr(packet, field_name, inner_list)
            if is_pointer: # leave list empty
                # but create packet constructor!
                def create_ctor(default_name, fields):
                    def create(name=None, **kwargs):
                        if name is None:
                            name = default_name
                        p = ln_packet_part(name, field_type)
                        self.__prepare_unpack_response(p, fields)
                        p.__dict__.update(kwargs)
                        return p
                    return create
                ctor_name = "new_%s_packet" % field_type
                packet._field_ctors[field_name] = create_ctor(field_name, self.defines[field_type])
                setattr(packet, ctor_name, packet._field_ctors[field_name])
                continue
            # prepare list
            for i in range(field_count):
                inner_packet = ln_packet_part("%s[%d]" % (field_name, i), field_type)
                inner_list.append(inner_packet)
                self.__prepare_unpack_response(inner_packet, self.defines[field_type])
        
    def _prepare_unpack_response(self, fields_and_def=None):
        self._response_buffer = bytearray(1) # dummy size!
        if fields_and_def is None:
            fields_and_def = self.resp, self.definition["resp_fields"]
        self.__prepare_unpack_response(*fields_and_def)

    def __unpack_response(self, packet, fields, buffer, offset, src_endianess):
        for field in fields:
            #print("unpack", field, offset, buffer[offset:], len(buffer[offset:]))
            field_type, field_name, field_count = field[:3]
            is_pointer = field_type[-1] == "*"
            if is_pointer:
                field_type = field_type[:-1]
                field_count = getattr(packet, field_name + "_len")
            is_primitive = field_type in data_type_map

            if is_primitive:
                data_type = data_type_map[field_type]
                size = data_type.size * field_count
                if (field_count > 1 or is_pointer) and self._use_numpy_arrays and field_type != "char":
                    field_value = getattr(packet, field_name)
                    if field_value.shape[0] != field_count:
                        field_value = numpy.empty((field_count, ), dtype=data_type.npformat)
                    links_and_nodes.copy2array(field_value, buffer, offset, size)
                elif field_type == "char":
                    # unpack utf-8 string
                    try:
                        field_value = struct.unpack_from("%ss" % field_count, buffer, offset)[0]
                    except:
                        raise Exception("could not unpack_from(): field_name: %r, field_type: %r, field_count: %r\n%s" % (field_name, field_type, field_count, traceback.format_exc()))
                    if self._utf8_decode_char_fields:
                        if not is_pointer:
                            field_value = field_value.rstrip(b"\0")
                        try:
                            field_value = field_value.decode("utf-8")
                        except:
                            raise Exception("char(-array) does not contain valid utf-8! %s: %r" % (field_name, field_value))
                elif field_count > 1:
                    field_value = struct.unpack_from(src_endianess + (data_type.pyformat * field_count), buffer, offset)
                else:
                    field_value = struct.unpack_from(src_endianess + data_type.pyformat, buffer, offset)[0]
                offset += size
                setattr(packet, field_name, field_value)
                #print("got primitive: %r" % field_value)
                continue

            # not a primitive type
            if not is_pointer and field_count == 1:
                if field_type != "pyobject":
                    field_value = getattr(packet, field_name)
                else:
                    pyobject_field_name = field_name + "_packet"
                    field_value = getattr(packet, pyobject_field_name)
                offset = self.__unpack_response(field_value, self.defines[field_type], buffer, offset, src_endianess)
                if field_type == "pyobject":
                    if not field_value.is_pickle:
                        if field_value.data:
                            pyobject = eval(field_value.data)
                        else:
                            pyobject = None
                    else:
                        pyobject = pickle.loads(field_value.data)
                    setattr(packet, field_name, pyobject)
                continue
            # either pointer or field_count > 1
            if field_type != "pyobject":
                inner_list = getattr(packet, field_name)
            else:
                pyobject_field_name = field_name + "_packet"
                inner_list = getattr(packet, pyobject_field_name)

            while len(inner_list) > field_count:
                del inner_list[-1]
            #i = len(inner_list)
            while len(inner_list) < field_count:
                #inner_packet = ln_packet_part("%s[%d]" % (field_name, i))
                inner_packet = getattr(packet, "new_%s_packet" % field_type)()
                #ln_packet_part("%s[%d]" % (field_name, i))
                #i += 1
                inner_list.append(inner_packet)
                #prepare(inner_packet, self.defines[field_type])
            for i in range(field_count):
                offset = self.__unpack_response(inner_list[i], self.defines[field_type], buffer, offset, src_endianess)
            if field_type == "pyobject":
                # for each list pickle or not
                pyinner_list = []
                setattr(packet, field_name, pyinner_list)
                for i in range(field_count):
                    field_value = inner_list[i]
                    if not field_value.is_pickle:
                        if field_value.data_len > 0:
                            pyobject = eval(field_value.data)
                        else:
                            pyobject = None
                    else:
                        pyobject = pickle.loads(field_value.data)
                    pyinner_list.append(pyobject)
        return offset
        
    def _unpack_response(self, fields_and_def=None):
        """
        used to unpack service-client response AND service-provider request
        """
        if self._need_swap:
            if self.host_byte_order == "little":
                # we are little endian!
                src_endianess = ">" # source must be big!
            else:
                src_endianess = "<" # source must be small!
        else:
            src_endianess = "" # same!
        # unpack self._response_buffer
        if fields_and_def is None:
            fields_and_def = self.resp, self.definition["resp_fields"]
        self.__unpack_response(fields_and_def[0], fields_and_def[1], self._response_buffer, 0, src_endianess)


class topic_cursor_wrapper(object):
    def __init__(self, c):
        self.name = c.name
        self.c = c
        self.topic = c.get_topic()

        if self.topic.md:
            self.packet = ln_packet(self.topic.md_name, self.topic.md, self.topic.sample_size)
        else:
            self.packet = None # no message seen, no message-definition known,
            # no packet wrapper...
        
    def get_sample(self, idx):
        if idx < 0 or idx >= self.topic.n_samples:
            raise IndexError("index %s is out of range" % idx)
        p = self.packet
        self.c.get_sample(idx, p._data)
        p._unpack(offset=2 * 8 + 4)
        p.log_ts, p.src_ts, p.packet_counter = struct.unpack_from("ddI", self.packet._data_buffer)
        return p

    def __len__(self):
        return self.topic.n_samples

class logger_data_wrapper(object):
    def __init__(self, data):
        self.data = data

    def save(self, fn):
        self.data.save(fn)

    def get_dict(self, do_print=False):
        return self.unpack_into(dict(), do_print=do_print)
        
    def unpack_into(self, user_dict, src_ts_name="_packet_source_ts",
                    stack_fields=[], do_print=True):
        shapes = []
        
        class packet_part_wrapper(object):
            def __init__(self, N, pp, own_name, shapes):
                self.own_name = own_name
                self.N = N
                self.pp = pp
                self.dims = {}
                for name in self.pp._fields:
                    value = getattr(self.pp, name)
                    if type(value) == ln_packet_part:
                        v = packet_part_wrapper(N, value, "%s.%s" % (own_name, name), shapes)
                        self.dims[name] = None
                    elif type(value) in (float, int):                    
                        v = zeros((N, ), dtype=type(value))
                        self.dims[name] = 1
                        shapes.append(("%s.%s" % (own_name, name), v.shape))
                    elif isinstance(value, (str, unicode, bytes)):
                        v = zeros((N, ), dtype=object)
                        self.dims[name] = 1
                        shapes.append(("%s.%s" % (own_name, name), v.shape))
                    elif hasattr(value, "shape") and len(value.shape) == 1:
                        v = zeros((N, value.shape[0]), dtype=value.dtype)
                        self.dims[name] = 2
                        shapes.append(("%s.%s" % (own_name, name), v.shape))
                    else:
                        new_shape = [N]
                        new_shape.extend(value.shape)
                        v = zeros(tuple(new_shape), dtype=value.dtype)
                        self.dims[name] = len(v.shape)
                        shapes.append(("%s.%s" % (own_name, name), v.shape))
                    setattr(self, name, v)                
            def set(self, idx, pp):
                for name in self.pp._fields:
                    value = getattr(self, name)
                    this_value = getattr(pp, name)
                    if self.dims[name] == None:
                        value.set(idx, this_value)
                    elif self.dims[name] == 1:
                        value[idx] = this_value
                    else:
                        value[idx, :] = this_value                        
            def keys(self):
                return self.pp._fields        
            def get_fields(self):
                d = attr_dict()
                for name in self.pp._fields:
                    v = getattr(self, name)
                    if self.dims[name] == None:
                        d[name] = v.get_fields()
                    else:
                        d[name] = v
                return d

        def proper_identifier(name):
            return name.strip().replace(".", "_").replace("/", "_").replace(" ", "_")
        for topic in self.topics.keys():
            t = self.topics[topic]
            N = len(t)
            arrays = {}
            dims = {}

            ptopic = proper_identifier(topic)
            target_dict = user_dict[ptopic] = attr_dict()
            topic_prefix_name = ptopic + "."
            
            # packet source ts
            target_dict[src_ts_name] = arrays[src_ts_name] = zeros((N, ), dtype=double)
            dims[src_ts_name] = 1                        
            shapes.append((topic_prefix_name + src_ts_name, (N, )))
            
            # packet counter
            pkt_cnt_name = "_packet_counter"
            target_dict[pkt_cnt_name] = arrays[pkt_cnt_name] = zeros((N, ), dtype=int)
            dims[pkt_cnt_name] = 1                        
            shapes.append((topic_prefix_name + pkt_cnt_name, (N, )))
            
            # packet log ts
            log_ts_name = "_packet_log_ts"
            target_dict[log_ts_name] = arrays[log_ts_name] = zeros((N, ), dtype=int)
            dims[log_ts_name] = 1                        
            shapes.append((topic_prefix_name + log_ts_name, (N, )))

            if not N:
                continue
            
            for name, value in t.get_sample(0).items():
                if type(value) == ln_packet_part:
                    arrays[name] = packet_part_wrapper(N, value, topic_prefix_name + name, shapes)
                    dims[name] = None
                    target_dict[name] = arrays[name].get_fields()
                    continue
                elif type(value) == list:
                    if not len(value):
                        continue
                    arrays[name] = [ packet_part_wrapper(N, value[0], "%s[%d]" % (topic_prefix_name + name, i), shapes) for i in range(len(value)) ]
                    dims[name] = None
                    target_dict[name] = [ a.get_fields() for a in arrays[name] ]
                    continue
                elif type(value) in (float, int):
                    v = zeros((N, ), dtype=type(value))
                    dims[name] = 1
                elif isinstance(value, (str, unicode, bytes)):
                    v = zeros((N, ), dtype=object)
                    dims[name] = 1
                elif hasattr(value, "shape") and len(value.shape) == 1:
                    v = zeros((N, value.shape[0]), dtype=value.dtype)
                    dims[name] = 2
                else:
                    new_shape = [N]
                    new_shape.extend(value.shape)
                    v = zeros(tuple(new_shape), dtype=value.dtype)
                    dims[name] = len(value.shape)
                arrays[name] = v
                target_dict[name] = v
                shapes.append((topic_prefix_name + name, v.shape))
            for i in range(N):
                s = t.get_sample(i)
                arrays[log_ts_name][i] = s.log_ts
                arrays[src_ts_name][i] = s.src_ts
                arrays[pkt_cnt_name][i] = s.packet_counter
                for name, value in s.items():
                    if dims[name] == None:
                        if isinstance(value, list):
                            for k, aval in enumerate(value):
                                arrays[name][k].set(i, aval)
                        else:
                            arrays[name].set(i, value)
                    elif dims[name] == 1:
                        arrays[name][i] = value
                    else:
                        arrays[name][i, :] = value
        if do_print:
            max_name_len = 0
            for name, shape in shapes:
                if len(name) > max_name_len:
                    max_name_len = len(name)
            for name, shape in shapes:
                print("%-*.*s %s" % (max_name_len, max_name_len, name, shape))
        
        for stack in stack_fields:
            if type(stack) == tuple:
                do_stack_fields(user_dict, stack[0], stack[1])
            else:
                do_stack_fields(user_dict, stack)
        
        return user_dict

class parameters_proxy(pyutils.hooked_object):
    _query_sinterface = "ln/parameters/query_dict"
    _override_sinterface = "ln/parameters/override_dict_single"
    _request_topic_sinterface = "ln/parameters/request_parameter_topic"

    def __init__(self, clnt, parameters_name, allow_empty=False, update_on_write=True, do_parameter_scan=True, record_call_stacks=False, parent=None,
                 debug=False):
        
        self._parameters = {}
        self._sub_blocks = {}
        pyutils.hooked_object.__init__(self)

        self.debug = debug
        self.clnt = clnt
        self.allow_empty = allow_empty

        if parent is None:
            self._query_services = {}
            self._override_services = {}
            self._request_topic_services = {}
            self._parameter_topics = {} # parameter_name -> (topic_name, md)
            self._topics = {} # topic_name -> subscribed port
            self._multi_waiter = None
        else:
            self._query_services = parent._query_services
            self._override_services = parent._override_services
            self._request_topic_services = parent._request_topic_services
            self._parameter_topics = parent._parameter_topics
            self._topics = parent._topics
            self._multi_waiter = parent._multi_waiter # might have multiple waiters
            self.debug = parent.debug
        
        self.parameters_name = parameters_name
        self._update_on_write = update_on_write
        self.is_subscribed = False
        self.record_call_stacks = record_call_stacks or (hasattr(self.clnt, "record_call_stacks") and self.clnt.record_call_stacks)

        self._queued_overrides = {}

        self.lock = threading.RLock()
        
        if do_parameter_scan:
            self._parameter_scan()
            if not self.allow_empty and not self._parameters and not self._sub_blocks:
                raise Exception("no parameters matching %r found!" % (self.parameters_name + "*"))

    def on_resource_event(self, ev):
        print("received event: %s, name: %s, client: %s" % (
            ev.event, ev.name, ev.client))

    @with_lock
    def _parameter_scan(self):
        service_names = self.clnt.find_services_with_interface(self._query_sinterface)
        if service_names.strip():
            service_names = set(service_names.split("\n"))
        else:
            service_names = set()
        
        # check for dead service provider
        for sname in set(self._query_services.keys()).difference(service_names):
            # remove all parameters of this provider!
            to_del = []
            for short_name, pvalue in self._parameters.items():
                if pvalue["service"] == sname:
                    del self._parameters[short_name]           
            
            # also remove this provider from query_services
            del self._query_services[sname]

        for sname in service_names:
            # try to use service from pool
            svc = self._query_services.get(sname)
            if svc is None:
                # not in list, ask for new one
                svc = self._query_services[sname] = self.clnt.get_service(sname, self._query_sinterface)
                svc.record_call_stacks = self.record_call_stacks
            
            svc.req.pattern = "%s*" % self.parameters_name
            svc()
            params = eval(svc.resp.data)
            
            # loop over parameters of service provider
            #  find the one that match our parameters-group name
            pnd = "%s." % self.parameters_name
            for pname, pvalue in params.items():
                if self.parameters_name == "": # take all
                    short_name = pname
                elif pname == self.parameters_name: # exact match
                    short_name = "value"
                elif pname.startswith(pnd): # group match
                    short_name = pname[len(pnd):]
                else:
                    continue

                pvalue["service"] = sname
                pvalue["full_name"] = pname

                if short_name in self._parameters:
                    # already known parameter
                    if self._parameters[short_name]["service"] == sname:
                        # same provider -> nothing to do
                        continue
                        
                    # the following case should not occur
                    print(("warning: parameters with duplicate name:\n"
                           "from service %r: %r (already in list)\n"
                           "from service %r: %r (new)\n"
                           ">> using the one we already have!\n") % (
                               self._parameters[short_name]["service"], self._parameters[short_name]["full_name"],
                               sname, pname))
                else:
                    self._add_parameter(short_name, pvalue)

        if not self.allow_empty and not self._parameters and not self._sub_blocks:
            raise Exception("no parameters matching %r found!" % (self.parameters_name + "*"))

    def _add_parameter(self, short_name, value):
        if "." not in short_name:
            self._parameters[short_name] = value
            return
        sub_block, new_short_name = short_name.split(".", 1)
        if sub_block not in self._sub_blocks:
            sub_parameters_name = sub_block
            if len(self.parameters_name) > 0:
                sub_parameters_name = "%s.%s" % (self.parameters_name, sub_block)
            self._sub_blocks[sub_block] = parameters_proxy(
                self.clnt, 
                sub_parameters_name, 
                update_on_write=self._update_on_write,
                do_parameter_scan=False,
                parent=self
            )
        self._sub_blocks[sub_block]._add_parameter(new_short_name, value)

    def _get_update_services_to_call(self, update_services_to_call=None):
        if update_services_to_call is None:
            update_services_to_call = {}
        for (service, pname), new_value in self._queued_overrides.items():
            override_service = service.replace("query_dict", "override_dict_single")
            if override_service not in update_services_to_call:
                update_services_to_call[override_service] = []
            update_services_to_call[override_service].append((pname, new_value))
        self._queued_overrides.clear()
        # collect all sub-blocks as well!
        for block_name, block in self._sub_blocks.items():
            block._get_update_services_to_call(update_services_to_call)
        return update_services_to_call

    def _get_refresh_services_to_call(self, refresh_services_to_call=None, only_short_name=None, return_names=False):
        if refresh_services_to_call is None:
            refresh_services_to_call = {}
        for short_name, pvalue in self._parameters.items():
            if only_short_name is not None and short_name != only_short_name:
                continue
            service_name = pvalue["service"]
            if return_names:
                service = service_name
            else:
                service = self._query_services[service_name]
            if service not in refresh_services_to_call:
                refresh_services_to_call[service] = []
            refresh_services_to_call[service].append((pvalue["full_name"], pvalue))
        # collect all sub-blocks as well!
        for block_name, block in self._sub_blocks.items():
            block._get_refresh_services_to_call(refresh_services_to_call, only_short_name=only_short_name, return_names=return_names)
        return refresh_services_to_call

    # public API:
    def __str__(self):
        return "<ln_parameters_proxy %r\n%s>" % (self.parameters_name, pprint.pformat(self.get_dict()))
    def __repr__(self):
        return "<ln_parameters_proxy@%#x %r\n%s>" % (id(self), self.parameters_name, pprint.pformat(self.get_dict()))
    def __getattr__(self, name):
        #print "get name: %r" % name
        if name != "_parameters" and name != "_sub_blocks":
            value = self._parameters.get(name)
            if value is not None:
                return self.get_output(name, value, refresh=False)
            block = self._sub_blocks.get(name)
            if block is not None:
                return block
        raise AttributeError(name)
        
    def __setattr__(self, name, new_value):
        if name != "_parameters" and name != "_sub_blocks":
            value = self._parameters.get(name)
            if value is not None:
                self.set_override(name, new_value)
                return
        #print "set %r = %r" % (name, new_value)
        object.__setattr__(self, name, new_value)

    def set_override(self, pname, override_value, update_on_write=True, wait_step=False): # set override_value to None to disable override
        pvalue = self._parameters[pname]
        self._queued_overrides[(pvalue["service"], pvalue["full_name"])] = override_value
        if self._update_on_write and update_on_write:
            self.refresh(wait_step=wait_step)

    def get_output(self, pname, value=None, refresh=True):
        if refresh:
            self.refresh()
        if value is None:
            value = self._parameters.get(pname)
        if value["override_enabled"]:
            return value["output"]
        return value["input"]

    def get_input(self, pname, value=None, refresh=True):
        if refresh:
            self.refresh()
        if value is None:
            value = self._parameters.get(pname)
        return value["input"]

    def __getitem__(self, name):
        if name not in self._parameters:
            raise KeyError(name)
        return self._parameters[name]
        
    def __contains__(self, name):
        return name in self._parameters

    def get_dict(self, refresh=True, full=False):
        if refresh:
            self.refresh() # will also refresh sub_blocks
        d = dict()
        for pname, value in self._parameters.items():
            if not full:
                d[pname] = self.get_output(pname, value, refresh=False)
            else:
                d[pname] = self.get_signal_full_dict(pname, value=value)
        for bname, block in self._sub_blocks.items():
            d[bname] = block.get_dict(refresh=False, full=full)
        return d
    
    def get_signal_full_dict(self, signal_name, value=None):
        if value is None:
            value = self._parameters[signal_name]
        return dict([ (key, value[key]) for key in ("input", "output", "override_enabled") ])

    def _our_repr(self, array_like):
        """
        format array_like independent of user's set_printoptions()
        - have threshold & max_line_width disabled
        - separate by "," and use python's str as formatter for all (should be
        fastest and has the least unneeded eyecandy)
        - tested this repr against a random array of size 3000 to be 2.5times
        faster than numpy's standard repr with set_printoptions(threshold=sys.maxsize)
        """
        # numpy 1.12.1 doesn't have threshold
        # numpy 1.14.3 has it!
        if isinstance(array_like, (tuple, list, numpy.ndarray)):
            if not numpy_array2string_has_threshold:
                numpy.set_printoptions(threshold=sys.maxsize)
            return "array(%s)" % numpy.array2string(
                numpy.asarray(array_like), **our_repr_options)
        # assume its a scalar and use standard repr
        return repr(array_like)
    
    @with_lock
    def refresh(self, short_name=None, wait_step=False):
        # first apply queued overrides!
        update_services_to_call = self._get_update_services_to_call()
        for service, parameters in update_services_to_call.items():
            svc = self._override_services.get(service)
            if svc is None:
                svc = self._override_services[service] = self.clnt.get_service(service, self._override_sinterface)
                svc.record_call_stacks = self.record_call_stacks
            for pname, pvalue in parameters:
                svc.req.parameter_name = pname
                if pvalue is None:
                    svc.req.override_data = "" # disable override
                else:
                    svc.req.override_data = self._our_repr(pvalue)
                if wait_step:
                    svc.req.wait_step = 1
                else:
                    svc.req.wait_step = 0
                svc()
                if svc.resp.error_len:
                    raise Exception("error overriding parameter %r in service %r with value %r:\n%s" % (pname, service, pvalue, svc.resp.error))
        refresh_services_to_call = self._get_refresh_services_to_call(only_short_name=short_name)
        for svc, parameters in refresh_services_to_call.items():
            svc.req.pattern = "%s*" % self.parameters_name
            svc()
            params = eval(svc.resp.data)
            for pname, pvalue in parameters:
                pvalue.update(params[pname])
        self.call_hook("on_update")

    # subscribe via topics
    @with_lock
    def subscribe(self, rate=-1):
        # subscribe to all parameters in this basename.
        # possibly to multiple blocks
        self.is_subscribed = True

        # which parameters to publish from which service
        refresh_services_to_call = self._get_refresh_services_to_call(return_names=True)
        self._parameter_topics = {}
        self._parameter_values = {}
        self._parameter_access = {}
        for sname, parameters in refresh_services_to_call.items():
            sname = sname.replace("query_dict", "request_topic")
            svc = self._request_topic_services.get(sname)
            if svc is None:
                svc = self._request_topic_services[sname] = self.clnt.get_service(sname, self._request_topic_sinterface)
                svc.record_call_stacks = self.record_call_stacks
            svc.req.parameters = ", ".join([pname for pname, pvalue in parameters])
            #svc.call()
            svc()
            if svc.resp.error_len:
                raise Exception(svc.resp.error)
            self._parameter_topics.update(eval(svc.resp.parameter_topics))
            for pname, pvalue in parameters:
                self._parameter_values[pname] = pvalue
        # subscribe to all needed topics
        ports = set()
        for pname, (topic, md) in self._parameter_topics.items():
            port = self._topics.get(topic)
            if port is None:
                port = self._topics[topic] = self.clnt.subscribe(topic, md, rate=rate)
            if not hasattr(port, "_parameters"):
                port._parameters = []
            part = port.packet
            item = pname.split(".")
            while len(item) > 1:
                part = getattr(part, item[0])
                item.pop(0)
            self._parameter_access[pname] = part, item[0]
            port._parameters.append(pname)
            ports.add(port)
        self._parameter_ports = ports
        # return list of ports
        return ports

    def subscribe_gtk(self, rate=-1, check_republish=True):
        """
        same as subscribe() but create a gtk_multi_waiter and add ports to that multi-waiter
        """
        import gobject
        ports = self.subscribe(rate)
        if self._multi_waiter is None:
            self._multi_waiter = links_and_nodes.gtk_multi_waiter(self.clnt)
        self.gtk_ports = ports
        if check_republish:
            self.check_republish_timer = gobject.timeout_add(1000, self._on_check_republish)
        for port in ports:
            self._multi_waiter.add_port(port, self._on_data_read)

    @with_lock
    def _on_check_republish(self):
        # are all subscribed ports still published?
        unpublished = []
        for port in self.gtk_ports:
            if not port.has_publisher():
                unpublished.append(port)
        if not unpublished:
            return True
        # get services for those parameters
        parameter_services = {}
        refresh_services_to_call = self._get_refresh_services_to_call(return_names=True)
        for sname, parameters in refresh_services_to_call.items():
            for pname, pvalue in parameters:
                parameter_services[pname] = sname
        try:
            self.call_hook("on_lost_parameters", parameter_services.keys())
        except:
            print(traceback.format_exc())
        missing_services = set()
        for port in unpublished:
            for pname in port._parameters:
                missing_services.add(parameter_services[pname])
        if self.debug: print("missing %d services..." % len(missing_services))
        # are those services avaliable again?
        service_names = self.clnt.find_services_with_interface(self._query_sinterface)
        if service_names.strip():
            service_names = set(service_names.split("\n"))
        else:
            service_names = set()
        avaliable_again = service_names.intersection(missing_services)
        if self.debug: print("%d services are again avaliable" % len(avaliable_again))
        if not avaliable_again:
            return True            
        # try to republish those parameters!
        unpublished_parameters = set()
        for sname, parameters in refresh_services_to_call.items():
            if sname not in avaliable_again:
                continue
            sname = sname.replace("query_dict", "request_topic")
            svc = self._request_topic_services.get(sname)
            if svc is None:
                svc = self._request_topic_services[sname] = self.clnt.get_service(sname, self._request_topic_sinterface)
                svc.record_call_stacks = self.record_call_stacks
            svc.req.parameters = ", ".join([pname for pname, pvalue in parameters])
            #svc.call()
            svc()
            if svc.resp.error_len:
                raise Exception(svc.resp.error)
            self._parameter_topics.update(eval(svc.resp.parameter_topics))
            for pname, pvalue in parameters:
                self._parameter_values[pname] = pvalue        
        return True

    def _call_update_hook_idle(self):
        def on_idle():
            try:
                self.call_hook("on_update")
            except:
                print("exception while calling %s.on_update:\n%s" % (
                    self, traceback.format_exc()))
            self._update_idle = None
            return False
        if not hasattr(self, "_update_idle") or self._update_idle is None:
            import gobject
            self._update_idle = gobject.idle_add(on_idle)
            
    def _on_data_read(self, port):
        port.read()
        # update named parameters
        for pname in port._parameters:
            value = self._parameter_values[pname]
            part, item = self._parameter_access[pname]
            p = getattr(part, item)
            if type(p) == ln_packet_part:
                value.update(p.dict())
            else:
                value["input"] = p
        self._call_update_hook_idle()
        return True

    @with_lock
    def read(self, blocking=True):
        # blocking == True: block on all needed ports
        # blocking == False: nonblocking
        # blocking == parameter_name: block only on port providing this absolute or relative parameter name

        if not self.is_subscribed:
            raise Exception("can only read() on subscribe()'d parameters!")

        if blocking in (True, False):
            for port in self._parameter_ports:
                port.read(blocking=blocking)
        else:
            if blocking in self._parameter_topics:
                topic, md = self._parameter_topics[blocking]
            else:
                topic, md = self._parameter_topics["%s.%s" % (self.parameters_name, blocking)]
            port = self._topics[topic]
            port.read(blocking=True)

        # now update!
        for pname, (topic, md) in self._parameter_topics.items():
            #port = self._topics[topic]
            value = self._parameter_values[pname]
            part, item = self._parameter_access[pname]
            value.update(getattr(part, item).dict())
        self._call_update_hook_idle()

class event_connection_wrapper(object):
    def __init__(self, args, connect_args, definition, signature):
        self.clnt, self.name, self.interface, self.callback = args
        self.connect_args = connect_args
        self.connection = None
        
        self.connect_signature, self.call_signature = signature.split("|")
        self.connect_signature = "%s: %s" % (self.interface, self.connect_signature)
        self.call_signature = "%s: %s" % (self.interface, self.call_signature)
        
        self.helper_svc = service_wrapper(self.name, self.interface, definition, signature, None, None, True, True, False)

        self.event_data = self.helper_svc.resp
        self.event_data.connection = self
    
        for name, value in connect_args.items():
            setattr(self.helper_svc.req, name, value)
        self.helper_svc._check_request()
        if not hasattr(self.clnt, "_event_connections"):
            self.clnt._event_connections = set()
        self.clnt._event_connections.add(self)
        self.clnt._connect_to_event(self)

    def __call__(self):
        try:
            self.callback(self.event_data)
        except:
            print("error in %s callback %s:\n%s" % (
                self, self.callback, traceback.format_exc()))
        
    def __str__(self):
        return "<ln.event_connection %s>" % self.name


class wrapped_service(object):
    """
    internal wrapper object used by services_wrapper.
    not intended for public use
    """
    def __init__(self, clnt, service_name, interface, default_arguments=None,
                 preprocessors={}, postprocessors={}, postprocessor=None, call_method="call"):
        self.svc = clnt.get_service(service_name, interface)
        self.req_fields = self._extract_field(self.svc._req_fields)
        self.resp_fields = self._extract_field(self.svc._resp_fields)
        self.default_arguments = default_arguments
        self.preprocessors = dict()
        for field, pre in preprocessors.items():
            try:
                n_args = len(inspect.getargspec(pre)[0])
            except:
                # assume build-in function expecting one argument!
                n_args = 1
            if inspect.ismethod(pre):
                n_args -= 1
            full_sig = n_args > 1
            # full cb sig (4 args: pyvalue, name, req, old_value)
            # minimal cb sig (1 arg: pyvalue)
            self.preprocessors[field] = pre, full_sig 
        self.postprocessors = postprocessors
        self.postprocessor = postprocessor
        if call_method[0] == "call_via_mainloop":
            self.svc._mainloop = call_method[1]
            self.call = self.svc.call_via_mainloop
        else:
            self.call = getattr(self.svc, call_method)
        self.lock = threading.RLock()

    def _extract_field(self, input_fields):
        fields = odict()
        last_field_name = None
        for field in input_fields:
            ftype, name = field[:2]
            fields[name] = ftype
            if ftype.endswith("*") and len(fields) > 1 and last_field_name == name + "_len":
                del fields[last_field_name]
            last_field_name = name
        return fields
        
    @with_lock
    def __call__(self, *args, **kwargs):
        values = odict()
        for (name, field_type), value in zip(self.req_fields.items(), args):
            is_primitive = field_type.rstrip("*") in data_type_map
            if is_primitive or isinstance(value, (ln_packet, ln_packet_part)) or name in self.preprocessors:
                values[name] = value
                continue
            # maybe its a simple non-primitive with only one member?
            inner_fields = self.svc.defines[field_type]
            if len(inner_fields) == 1:
                inner_field = inner_fields[0]
                values[name] = field = getattr(self.svc.req, name)
                setattr(field, inner_field[1], value)
                continue
            raise Exception("error, field %r is of non-primitive, non-trivial (len %d > 1) type %r, you gave value of type %r:\n%r" % (
                name, len(inner_fields), field_type, type(value), value))
        for name, value in kwargs.items():
            if name in values:
                raise Exception("error, keyword argument %r already used via positional argument %d!" % (
                    name, self.req_fields.keys().index(name)))
            values[name] = value
        if self.default_arguments:
            for name, value in self.default_arguments.items():
                if name not in values:
                    values[name] = value

        for name, value in values.items():
            pre = self.preprocessors.get(name)
            if pre is not None:
                if pre[1]:
                    value = pre[0](value, name, self.svc.req, getattr(self.svc.req, name, value))
                else:
                    value = pre[0](value)
            setattr(self.svc.req, name, value)
        for name in self.req_fields.keys():
            if name in values:
                continue
            if hasattr(self.svc.req, name):
                delattr(self.svc.req, name)
        self.call()
        
        if "error_message" in self.resp_fields:
            if self.svc.resp.error_message_len > 0:
                raise Exception(self.svc.resp.error_message)
        ret = self.svc.resp.dict()
        if "error_message" in self.resp_fields:
            del ret["error_message_len"]
            del ret["error_message"]
        if len(ret) == 0:
            return None
        
        # run postprocessors
        for name in list(ret.keys()):
            post = self.postprocessors.get(name)
            if post is None:
                continue
            ret[name] = post(ret[name])

        had_match = False
        if len(ret) == 2:
            for k in ret.keys():
                if (k + "_len") in ret:
                    ret = ret[k]
                    had_match = True
                    break
            
        if not had_match and len(ret) == 1:
            ret = list(ret.values())[0]

        if self.postprocessor is not None:
            ret = self.postprocessor(ret)
        
        return ret
        
class services_wrapper(object):
    """
    derive from this class to wrap ln-service client calls
    as python method calls.

    this is not about service providers!
    
    call ``self.wrap_service(...)`` in your constructor to define
    which service-methods to make avaliable.

    Example:
      here is an example how to derive from this class, with two dummy service
      interfaces:

      myrobot/set_power::

        service
        request
        uint8_t enable
        response
        char* error_message

      myrobot/move_to::

        service
        request
        double a
        double b
        double speed
        response
        char* error_message
        double time_needed

      and here how your python code could look like::
        
        class myrobot_interface(links_and_nodes.services_wrapper):
            def __init__(self, ln_client, robot_name):
                super(myrobot_interface, self).__init__(ln_client, robot_name)
                self.wrap_service("set_power", "myrobot/set_power")
                self.wrap_service(
                    "move_to", "myrobot/move_to", 
                    default_arguments=dict(speed=1)
                )
        
        clnt = links_and_nodes.client("example", sys.argv)
        bert = myrobot_interface(clnt, "bert")
        bert.set_power(true) # this will call "bert.set_power"
        time_needed = bert.move_to(2, 3) # this will call "bert.move_to" and return time_needed

    """
    def __init__(self, clnt, service_prefix=""):
        self.clnt = clnt
        self.service_prefix = service_prefix
        self.services = {}
        self.default_call_method = "call"

    def wrap_service(self, postfix, interface, default_arguments={},
                     preprocessors={}, postprocessors={}, postprocessor=None,
                     method_name=None, call_method=None):
        """create a wapper method for specified service

        the service name will be ``self.service_prefix + "." + postfix``.
        a new method will be added to `self` with a signature like this:

        ``response_fields = method_name(request_field1, request_field2, ...)``
        
        the type of ``response_fields`` depends on the number of
        non-``error_message`` response fields in that service-interface. if there
        is only one response field that value will be returned.
        in case there are more than one response-fields a dict will be
        returned: ``dict(response_field1_name: response_field1_value, ...)``

        if the service interface includes a ``error_message`` response-field an
        exception will thrown if this field has non-zero-length on arrival of
        the response. the exception text will be the text sent in that field.

        args:
          postfix: the name of the service to call (``self.service_prefix`` will
            be prepended.
          interface: the name of the mssage_definition for that service
          default_arguments (dict): mapping of default values for fields that
            are not specified when calling the generated wrapper function.
          method_name: the name of the method to generate. defaults to
            ``postfix`` but with "."'s replaced by "_"
          call_method: defaults to "call". can also be "call_gobject" which is
            useful when you are calling from your gtk main thread and want gtk
            events to be processed while waiting for the service response

        """
        if call_method is None:
            call_method = self.default_call_method
        if self.service_prefix:
            service_name = self.service_prefix + "." + postfix
        else:
            service_name = postfix
        svc = wrapped_service(self.clnt, service_name, interface, default_arguments,
                              preprocessors, postprocessors, postprocessor, call_method)
        self.services[service_name] = svc
        if method_name is None:
            method_name = postfix.replace(".", "_")
        setattr(self, method_name, svc)
        return svc

class wrapped_service_provider(object):
    """
    internal wrapper object used by service_provider.
    not intended for public use
    """
    def __init__(self, provider, service_name, interface, method,
                 preprocessors={}, postprocessor=None, cleanup=None, group_name=None, register_method="do_register",
                 pass_request=False, utf8_decode_char_fields=True
    ):
        self.provider = provider
        self.clnt = self.provider.clnt
        self.svc = self.clnt.get_service_provider(service_name, interface)
        self.req_fields = self._extract_field(self.svc._req_fields)
        self.resp_fields = self._extract_field(self.svc._resp_fields)
        self.preprocessors = preprocessors
        self.postprocessor = postprocessor
        self.cleanup = cleanup
        self.group_name = group_name
        self.method = method
        self.pass_request = pass_request
        self.svc.utf8_decode_char_fields(utf8_decode_char_fields)
        
        self.has_error_message = "error_message" in self.resp_fields
        
        self.svc.set_handler(self.handler)
        getattr(self.svc, register_method)(self.group_name)
        
    def _extract_field(self, input_fields):
        fields = odict()
        last_field = None
        for field in input_fields:
            ftype, name = field[:2]
            fields[name] = ftype
            if ftype.endswith("*") and len(fields) > 1 and last_field == name + "_len":
                del fields[name + "_len"]
            last_field = name
        return fields
        
    def handler(self, request, req, resp):
        # pass request fields as keyword arguments to method
        kwargs = dict()
        if self.pass_request:
            kwargs["request"] = request
        for field_name, field_type in self.req_fields.items():
            preproc = self.preprocessors.get(field_name)
            value = getattr(req, field_name)
            is_primitive = field_type.rstrip("*") in data_type_map
            if preproc:
                field_name, value = preproc(self, value, field_name)
            elif not is_primitive:
                if not isinstance(value, list):
                    value = value.dict()
            kwargs[field_name] = value
                
        try:
            ret = self.method(**kwargs)
        except:
            print("exception in handler %s of service provider %s:\n%s" % (
                self.method,
                self.svc.name,
                traceback.format_exc()))
            if self.has_error_message:
                ret = dict(error_message=traceback.format_exc())
            else:
                ret = dict()

        if self.postprocessor:
            self.postprocessor(self, resp, ret)
        else:
            def set_field(packet, field_name, value):
                field_type = self.resp_fields[field_name]
                #print "set field %r type %r to %r" % (field_name, field_type, value)
                is_primitive = field_type.rstrip("*") in data_type_map
                if is_primitive or isinstance(value, (ln_packet, ln_packet_part)):
                    setattr(packet, field_name, value)
                    return
                # is trivial?
                inner_fields = self.svc.defines[field_type]
                if len(inner_fields) == 1:
                    # yes
                    inner_packet = getattr(packet, field_name)
                    inner_field = inner_fields[0]
                    setattr(inner_packet, inner_field[1], value)
                    return
                setattr(packet, field_name, value)

            # default action: try to fill response!
            if len(self.resp_fields) == 1:
                single_field = list(self.resp_fields.keys())[0]
                if self.has_error_message:
                    if type(ret) == dict:
                        setattr(resp, single_field, ret.get("error_message", ""))
                    else:
                        setattr(resp, single_field, str(ret))
                else:
                    setattr(resp, single_field, ret)
            elif self.has_error_message and len(self.resp_fields) == 2:
                single_field = filter(lambda key: key != "error_message", self.resp_fields.keys())[0]
                #print "single_field: %r" % single_field
                if type(ret) == dict:
                    setattr(resp, "error_message", ret.get("error_message", ""))
                    set_field(resp, single_field, ret.get(single_field))
                else:
                    setattr(resp, "error_message", "")
                    set_field(resp, single_field, ret)
            else:
                if not isinstance(ret, dict):
                    raise Exception("for now we expect the service handler to return a dict!\nyou returned %s: %r" % (type(ret), ret))

                for name in self.resp_fields.keys():
                    if name in ret:
                        set_field(resp, name, ret[name])
                    else:
                        delattr(resp, name)

        request.respond()
        
        if self.cleanup:
            self.cleanup(self, resp)
        
        return 0

class service_provider(object):
    """
    derive from this class to implement service providers!

    """

    def __init__(self, clnt, service_prefix="", default_register_method="do_register"):
        self.clnt = clnt
        self.service_prefix = service_prefix
        self.services = {}
        self.default_register_method = default_register_method

    def wrap_service_provider(
            self, postfix, interface,
            preprocessors={}, postprocessor=None, cleanup=None,
            method=None, method_name=None,
            group_name=None,
            register_method=None,
            pass_request=False,
            utf8_decode_char_fields=True):
        """
        preprocessors are called for each matching fieldname with (svc_wrap, field_name, field_value) to return tuple of kwarg-name and kwarg-value
        postprocessor is called to interpret returnvalue of method and fill response fields
        cleanup is called after sending the response
        either specify method or method_name. of not specified method_name=postfix is assumed
        """

        if register_method is None:
            register_method = self.default_register_method
        
        if self.service_prefix:
            service_name = self.service_prefix + "." + postfix
        else:
            service_name = postfix
            
        if method is None:
            if method_name is None:
                method_name = postfix.replace(".", "_")
            if not hasattr(self, method_name):
                raise Exception("there is no method named %r in this class! define it or specify method either via method_name or method kwargs!" % method_name)
            method = getattr(self, method_name)
        
        svc = self.services[service_name] = wrapped_service_provider(
            self, service_name, interface, method,
            preprocessors=preprocessors, 
            postprocessor=postprocessor, 
            cleanup=cleanup,
            group_name=group_name,
            register_method=register_method,
            pass_request=pass_request,
            utf8_decode_char_fields=utf8_decode_char_fields)
        return svc

    def handle_service_group_requests(self, group_name=None):
        print("ready")
        self.keep_running = True
        while self.keep_running:
            self.clnt.wait_and_handle_service_group_requests(group_name, 0.5)
