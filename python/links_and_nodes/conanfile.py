import os

from conans import ConanFile, tools
from cissy.conantools import get_python_dist_versions, get_python_dist_latest_version

class links_and_nodes_python_conan(ConanFile):
    python_requires = "ln_conan/[~4]@common/unstable"
    python_requires_extend = "ln_conan.Base"
    
    name = "links_and_nodes_python"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = "os", "compiler", "build_type", "arch"
    exports = [ "../../site_scons*" ]
    exports_sources = [
        # our self
        "../../python*",
        "!python/links_and_nodes_base/*.pyc",
        "!python/links_and_nodes/*.pyc",
        "!python/links_and_nodes/.*",
        "!python/links_and_nodes/src-*",
        "!python/links_and_nodes/linux-*",
        "!python/pyutils/*.pyc",
        "!python/pyutils/*.pyx",
        "!python/pyutils/.*",
        
        "../../share*",
        
        # will need base-build-system
        "../../SConstruct",

        # all of library
        "../../libln*",
        "../../external*",
        # excludes are checked with fnmatch against results of positive patterns!
        "!libln/.*",
        "!libln/build/*",
        "!libln/tests/*",
        "!libln/examples/*",

        "../../ln_runtime*file_services*fs_sync",
    ] + ["!%s" % x for x in tools.Git().excluded_files()]

    options = {
        "python_version": ["2", "3"],
        "python_dist_version": get_python_dist_versions()
    }
    default_options = {
        "python_version": "2",
        "python_dist_version": get_python_dist_latest_version()
    }
    
    def init(self):
        base = self.python_requires["ln_conan"].module.Base
        base.set_attrs(self)

    def configure(self):
        self.options["boost_python"].python_version = self.options.python_version
        self.options["links_and_nodes_base_python"].python_version = self.options.python_version
        
    def requirements(self):
        self.requires("liblinks_and_nodes/%s" % self.same_branch_or("[>=1.2 <3]"))
        self.requires("links_and_nodes_base_python/%s" % self.same_branch_or("[~=2]"))
        self.requires("boost_python/1.66.0@3rdparty/stable")

    def source(self):
        self.write_version_header("LIBLN_VERSION", os.path.join("libln", "include", "ln", "version.h"))
        self.write_version_file(os.path.join("libln", "version"))
            
    def build(self):
        self.scons_build("libln python", "--no-manager --no-python-base --use-private-libtomcrypt", with_python_lib=True)

    def package(self):
        install = os.path.join("build", "python", self.install_sandbox, self.prefix[1:])
        self.copy("version", src="libln", dst=os.path.join(self.python_path, "links_and_nodes")) # todo: this is strange
        self.copy("*", src=install, symlinks=True)
        
    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.env_info.PYTHONPATH.append(os.path.join(self.package_folder, self.python_path))
