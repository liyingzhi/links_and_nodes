from links_and_nodes.mainloop import *

import gi
gi.require_version('GLib', '2.0') 
from gi.repository import GLib

class GLibMainloop(MainloopInterface):
    """
    using a Glib.MainLoop from
    from gi.repository import GLib    

    https://developer.gnome.org/programming-guidelines/stable/main-contexts.html.en
    """
    default = None

    @classmethod
    def get_for_default_context(cls):
        if cls.default is None:
            cls.default = GLibMainloop(None)
        return cls.default
    
    def __init__(self, context=None):
        MainloopInterface.__init__(self)
        self.loop = GLib.MainLoop(context)
        self.ctx = self.loop.get_context()

    def iterate(self, blocking=True):
        self.ctx.iteration(blocking)

    def timeout_add(self, timeout_in_seconds, callback, *args):
        source = GLib.timeout_source_new(int(timeout_in_seconds * 1e3))
        source.set_callback(callback, *args)
        return source.attach(self.ctx)

    def fd_add(self, fd, condition, callback, *args):
        iocondition = None
        def set_or(value, mask):
            if value is None:
                return mask
            return value | mask
        if condition == "in"  or "in"  in condition: iocondition = set_or(iocondition, GLib.IOCondition.IN)
        if condition == "out" or "out" in condition: iocondition = set_or(iocondition, GLib.IOCondition.OUT)
        if condition == "hup" or "hup" in condition: iocondition = set_or(iocondition, GLib.IOCondition.HUP)
        if condition == "err" or "err" in condition: iocondition = set_or(iocondition, GLib.IOCondition.ERR)
        channel =  GLib.IOChannel.unix_new(fd)
        source = GLib.io_create_watch(channel, iocondition)
        source.set_callback(callback, *args)
        return source.attach(self.ctx)
    
    def source_remove(self, source_id):
        GLib.source_remove(source_id)
