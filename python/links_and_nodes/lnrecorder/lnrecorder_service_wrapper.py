import links_and_nodes as ln

class lnrecorder_service_wrapper(ln.services_wrapper):
    def __init__(self, clnt, name="lnrecorder"):
        ln.services_wrapper.__init__(self, clnt, name)

        self.wrap_service("record", "lnrecorder/record",
                          default_arguments=dict(patterns=[]),
                          preprocessors=dict(patterns=self._list_of_strings)) # test empty patterns, test patterns
        self.wrap_service("replay", "lnrecorder/replay",
                          default_arguments=dict(patterns=[]),
                          preprocessors=dict(patterns=self._list_of_strings))
        self.wrap_service("stop", "lnrecorder/stop")

    def _list_of_strings(self, pylist, name, req, old_value):
        ret = []
        for s in pylist:
            p = req.new_string_packet()
            p.string = s
            ret.append(p)
        return ret
