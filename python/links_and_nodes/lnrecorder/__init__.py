from __future__ import print_function

from .lnrecorder_log import *
from .lnrecorder_service_wrapper import *

def pprint_odict(data, indent=0):
    indent_str = " " * indent
    for key, value in data.items():
        if not isinstance(value, odict):
            prefix = "%s%s: " % (indent_str, key)
            s = repr(value)
            s = s.replace("\n", "\n" + (" " * len(prefix)))
            print("%s%s" % (prefix, s))
            continue
        print("%s%s:" % (indent_str, key))
        pprint_odict(value, indent+2)
