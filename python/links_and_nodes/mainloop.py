
class MainloopInterface(object):
    def iterate(self, blocking=True):
        """
        do a single mainloop iteration.
        if blocking is True, wait until there was atleast one event to process
        """
        raise NotImplementedError()
    
    def timeout_add(self, timeout_in_seconds, callback, *args):
        """
        register a callback to be called every given number of seconds.
        returns a unique source_id
        """
        raise NotImplementedError()
    
    def fd_add(self, fd, condition, callback, *args):
        """
        call callback if condition on give file-descriptor is met.
        condition can be "out", "in", "err" or sequence of them.
        returns a unique source_id
        """
        raise NotImplementedError()
