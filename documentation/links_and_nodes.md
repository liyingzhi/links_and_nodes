% links and nodes Manual
% Florian Schmidt
% 2013-10-15



Introduction
============

links_and_nodes (short: ln) is a framework for easy creation and maintenance of
distributed computing networks.

*ln_clients* within the network only need to know the
*ln_manager* TCP-address to communicate with each other.

currently there are two orthogonal communication paradigms provided:

* reliable, efficient, dynamically sized request-/response services
  (think of TCP remote procedure calls)

* realtime, low-latency, fixed sized signal publishing 
  (think of shared-memory+conditions and UDP)
  
processes, arranged in groups, and dependencies between them can be
defined in a human readable config-file.

actions on remote hosts (like process start/stop, signaling, process listing...)
are executed by *ln_daemon*-processes running on these hosts.
  
the *ln_manager*-gui allows to inspect/monitor or influence all those
things.



Terminology
------------

please start reading here to get an overview and a common
understanding of the terms used later in this document.

host
:    a host is a unique computer. it has to have a unique name. it can
     have multiple network-interfaces (each with its own ip-address).

network
:    a network has a unique name. a host is reachable from within a
     network if it has at least one network-interface within that network.

links and nodes instance
:    a links and nodes instance identifies a links and nodes
     configuration. each instance has to have a unique name and exactly
     one ln_manager with a unique tcp-address.

ln_manager
:    the ln_manager is the central information hub within a links and
     nodes instance. the manager knows who is providing what and on
     which host. the manager needs a human-readable config file which
     defines things like the manager tcp-address, what processes to
     start on which hosts and many other details of your system. the
     manager itself is a python program with an optional graphical
     user interface. you can use the user interface to manually
     start-/stop processes, inspect what is published by whom, at
     which rate and many other details.

ln_daemon
:    to start- stop processes on remote hosts the manager needs to
     connect to some kind of server running on those remote
     hosts. because ssh-servers are not common on all supported
     platforms and do not provide sufficient functionality a new
     server was needed.

     on every host there should be one ln_daemon running for your
     user-id. multiple daemons of different users do not interfere
     with each other.
     multiple instances can share the same ln_daemon.

ln_client
:    a ln_client is a program using the links and nodes library to
     communicate with other clients in the same links and nodes instance.
     there are bindings for different languages available: C, C++,
     Python and Java

process
:    a process is a program and a collection of execution units or
     threads running within an operating system on a specific
     host. which process to start on which host and with which
     arguments can be specified in the ln_manager-config file.
     
     processes can also depend on other processes - meaning other
     processes need to be running before this process can be started.

state
:    a binary state living somewhere in your system. you can
     configure shell-commands to set- or unset a state. processes can
     depend on a state being set (= up) or unset (= down). an example
     might be whether or not an additional network interface is
     configured (= has an ip address) or not.




Getting Started
===============



Installation
------------

links_and_nodes is available and ready to be used in the DLR-RM
package repository. 

if you want to make changes to links and nodes, cross-compile to new
targets or want to test the latest bleeding-edge features you can
instead [install the source-tree](#installing-the-source-tree). 

you can get the location of the newest released version by executing:

~~~shell
$ /home/f_soft/bin/pkgtool links_and_nodes --key=PKGROOT
/volume/software/common/packages/links_and_nodes/0.2.0
~~~

in the following you need to know which shell you are using. if your
are not sure type:

~~~shell
$ echo $0
bash
~~~

if you are using the bash-shell its suggested to add these lines to
your `~/.bashrc` file:

~~~shell
export LN_BASE=/volume/software/common/packages/links_and_nodes/0.2.0
source ${LN_BASE}/scripts/bash.rc
~~~

in already running shells you can enter this to read these new
settings:

~~~shell
$ source ~/.bashrc
~~~

if you are using the tcsh-shell (or some other C shell compatible) you
should add these lines to your `~/.cshrc`-file:

~~~shell
setenv LN_BASE /volume/software/common/packages/links_and_nodes/0.2.0
source ${LN_BASE}/scripts/csh.rc
~~~

in already running shells you can enter this to read these new
settings:

~~~shell
$ source ~/.cshrc
~~~

if you're not interested in playing the beta-tester you can skip the
next section and jump right to [starting the manager](#starting-the-manager).



Installing the Source Tree
--------------------------

To create a checkout of the links_and_nodes source svn repository and
to compile binaries for linux, qnx6.5 and vxworks6.9 type:

~~~shell
$ cd ~/foreign_packages
$ svn co https://rmsvn01.robotic.dlr.de/repos/f_soft_src/links_and_nodes/trunk ln_base
$ cd ln_base
$ make TARGETS="qnx65 vxworks6.9"
~~~

this will compile the ln_library, the python binding and the
ln_daemon. if you only want to compile for linux just enter

~~~shell
$ make TARGETS=
~~~

then you should add these lines to your `~/.bashrc`-file:

~~~shell
export LN_BASE=~/foreign_packages/ln_base
source ${LN_BASE}/scripts/bash.rc
~~~

(see the previous section to derive a csh version of this)



Starting the manager
--------------------

The ln_manager is the single central information storage within a
running links and nodes instance. Every time a ln_client wants to use
or provide a new service the ln_manager is needed. Without a running
manager clients can not establish new ln-communication channels.

Each ln_client holds an opened tcp-connection to the ln_manager. As
soon as this connection is closed the manager assumes that the client
has gone away and removes/cleans-up all resources used by this client.

If needed, in an already running system, the manager can be restarted
-- all clients will automatically try to reconnect to the
manager. But, as mentioned before, while the manager is stopped, all
tries from ln_clients to publish or subscribe new services will fail
with an error.

To start the ln_manager you always need to specify a config file. The
most simple config file can look like this `my_config.cfg`:

~~~
instance
name: test_instance
manager: :54414

~~~

This defines the instance name to be **test_instance** and the manager
will listen for client connections at TCP-port number **54414**. You
can freely choose this name and port-number. (see [instance
config](#instance-section))

To start the ln_manager with this config file enter:

~~~shell
$ ln_manager -c my_config.cfg
~~~

A new, pretty empty window like this will be opened:

![empty ln_manager window](img/empty_manager.png)



Starting a daemon
-----------------



Config File Reference
=====================



Syntax
------------------

This section describes the syntax used for the links_and_nodes manager
config file. The used notation is the same as in the [Python Language
Refernce](http://docs.python.org/2/reference/introduction.html#notation)
-- a modified [Backus–Naur
Form](http://en.wikipedia.org/wiki/Backus%E2%80%93Naur_Form) - please
refer to these pages for more information about this notation.

here is the lexical definition:

~~~
config_file ::= config_section*

config_section ::= section_header "\n" key_value_pairs*

section_header ::= section_type [ WS section_name ]
section_type ::= terminal_string
section_name ::= config_string

key_value_pairs ::= [WS] section_key [WS] ": " section_value "\n"
section_key ::= terminal_string
section_value ::= config_string | ( "[" config_string_multiline "]" )

config_string_multiline ::= config_string "\n" [ config_string_multiline ]
config_string ::= ( replacement | [ terminal_string ] )*

replacement ::= "%(" config_string ")"
terminal_string ::= any_text_wo_newline+

any_text_wo_newline ::= <any character except "\n">

WS ::= [ " " | "\t" | "\r" | "\n" ]+
~~~

(note: empty lines and lines starting with "#" are ignored, trailing
whitespace is ignored)



Defines
-------


within the config file simple text-replacements can be defined. (like
variables and their values)

the general syntax of how to use a text-replacement by a `define` looks like this:

~~~
# from the lexical definition:
replacement ::= "%(" config_string ")"

# syntax:
replace_by_define ::= "%(" define_name ")"
define_name ::= config_string
~~~

the values are defined within a `defines`-`config_section`. a config
file can contain multiple `defines`-sections. the syntax
of the defines config section is defined like this:

~~~
# from the lexical definition:
config_section ::= section_header "\n" key_value_pairs*
section_header ::= section_type [ WS section_name ]

# syntax:
section_header ::= "defines"
~~~

each key of the following `key_value_pairs` is the name of a new
define. define-names are of course case-sensitive! if a define of this
name already exists it will be overwritten.
the value of each define is the evaluated `section_value` (might
contain other `%(...)`-constructs).

Example usage:

~~~
defines
PROJECT_BASE_PATH: /home/meier_mu/workspace/projekt_nielpferd
Jager_Version: 1.0
JAEGER_BINARY: %(PROJECT_BASE_PATH)/jaeger/%(Jager_Version)/bin/hunter

process Hunter
command: %(JAEGER_BINARY)
...

defines
OPFER_BINARY: %(PROJECT_BASE_PATH)/opfer/final/bin/nielpferd

process Markus
command: %(OPFER_BINARY)
...

~~~


Predefined Defines
------------------

the following sections describes the predefined config-defines.

`%(CURRENT)`
:    this define evaluates to the expanded real filename of the current
     config-file.

`%(CURDIR)`
:    this define holds the directory name part of `%(CURRENT)`

`%(LN_BASE)`
:    this define evaluates to the currently used links_and_nodes base
     directory. (where the manager is started from)

`%(LN_PYTHONPATH)`
:    this define evaluates to a ':'-separated list of directories that
     contain needed python-packages for ln_clients. you can use it in
     your python process-definitions like this:
~~~
process py_client
node: guihost
environment: PYTHONPATH=%(LN_PYTHONPATH)
command: /usr/bin/python %(ln_base)/library/tests/python/slow_subscribe.py 500
~~~


Avaliable Functions
-------------------

beside simple replacements by defines there are also function-calls which
are replaced by their return values.

the general syntax of a function call looks like this:

~~~
# from the lexical definition:
replacement ::= "%(" config_string ")"

# function call syntax:
function_call ::= "%(" method_name WS method_arguments ")"
method_name ::= config_string
method_arguments ::= config_string
~~~

the `method_name` has to be one of the listed methods in the following:

shell
:    this function executes a shell-command at
     config-file-load-time and returns the command output with
     trailing newlines stripped. it is executed on the host where
     the manager is running -- every time the config file is loaded (at
     startup and when the user presses the reload button).

     Example:

~~~
process pferd
environment: MANAGER_USER_ID=%(shell id -u)
...
~~~

dirname
:    This function expects a path name as argument and returns a directory
	 name of this path name.
	 
	 Example: `%(dirname /home/hello/world)` will get replaced by `/home/hello`

basename
:    This function expects a path name as argument and returns only the
	 last path element.
	 
	 Example: `%(basename /home/hello/world)` will get replaced by `world`

subst
:    This function expects 3 double-quoted, comma-separated strings as
	 input: SEARCH, REPLACE, TEXT. It returns TEXT where every occurrence
	 of SEARCH is replaced by REPLACE.
	 
	 Example: `%(subst "otto", "world", "hello otto!")` will get replaced
	 by `hello world!`

strip
:    This function expects a string as argument and returns that string
	 with leading and trailing whitespaces removed.
	 
	 Example: `%(strip        title   space   )` will get replaced
	 by `title   space`

findstring
:    This function expects 2 double-quoted, comma-separated strings as
	 input: SEARCH, TEXT. It returns SEARCH when SEARCH is found to occur
	 in TEXT and an empty string if not.
	 
	 Example: `%(findstring "Flo", "Hello World")` will get replaced
	 by an empty string.

	 Example: `%(findstring "Flo", "Hello World and Flo")` will get replaced
	 by `Flo`

guess_display
:    this function tries to guess a valid X-display-string suitable for
     a `DISPLAY`-environment variable. 
	 
	 **Consider using the `forward_x11` process flag instead - its
	 designed to be easier and more robust!**
	 	 
	 The guess is based on the
     current value of the managers DISPLAY-environment variable. It
     has no `method_arguments`
     
     When the process is to be started on the same host as the manager
     run's on, `%(guess_display)` will equal to `%(env DISPLAY)`.
     when the process is to be started on a remote host, and the
     managers local `%(env DISPLAY)` contains no host-part the manager-ip
     address (as seen from the remote host) is prepended. if there is a
     host-part the DISPLAY-environment variable is passed unmodified.
     example usage:
~~~
process pyXpferd
node: guihost
environment: PYTHONPATH=%(LN_PYTHONPATH), DISPLAY=%(guess_display)
pass_environment: USER, HOME, XAUTH
command: /usr/bin/python %(ln_base)/library/tests/python/slow_subscribe.py 500
~~~

env
:    This function uses the python function os.getenv to retrieve the value of
     the environment variable specified by the first parameter. The optional
     second parameter is used, if this variable is not existing.
	 
	 Example: `%(env "HOME", "~")` will get replaced by the home directory of
	 the user. IF $HOME is not existing, the expression will be replaced by '~'

ifip
:    This function can be used to retrieve the IP associated with a network
     interface. It makes use of the linux SIOCGIFADDR ioctl. It takes the name
     of the interface as parameter.
	 
	 Example: `%(ifip "eth0")` will get replaced by the IP of eth0.

ifndef
:    This function takes the name of a define as first argument and a default
     value as second argument. If the define is existing, its value is used as
     replacement. If not, the default value is used instead. The function is
     especially useful for included config files. It allows user to define
     values for the included file, while having the option to fall back to
     default values. 
	 
	 Example: `SRC_PATH: %(ifndef "SRC_PATH", "/default/src/path")`
	 Here, SRC_PATH can be defined before this line of code and the value
	 defined there will be used for SRC_PATH. If this is not done, SRC_PATH
	 takes the default value /default/src/path.

ifexists
:    This function checks for a path existence. The path is specified as first
     argument. The second argument is the value to be used as replacement in
     case the path is existing. The third argument is the value which is used
     instead. Internally, the python function os.path.exists is used.
	 
	 Example: `%(ifexists "/my/path", "existing", "not existing")`
	 This will get replaced by "existing", if /my/path exists. Otherwise it 
	 will get replaced by "not existing".

ifeq
:    This function checks for equality (python ==) of the two parameters
     specified first. In case they are equal, the third parameter is returned,
     else th fourth one.
	 
	 Example: `%(ifeq "3.0", "3", "equal", "not equal")`
	 Note that this will get replaced by "not equal", as the parameters can
	 only be string objects.

Include other files
-------------------


Instance Section
----------------


The instance-section describes global settings valid for this
ln-instance. The most-important setting is the manager-address and the
instance-name. Each ln-instance has to have a unique manager-address
and should have a unique name. Reusing the same name in different
instances can cause conflicts within the ln-daemon!

~~~
# from the lexical definition:
config_section ::= section_header "\n" key_value_pairs*
section_header ::= section_type [ WS section_name ]

key_value_pairs ::= [WS] section_key [WS] ": " section_value "\n"
section_key ::= terminal_string
section_value ::= config_string | ( "[" config_string_multiline "]" )

# syntax:
section_header ::= "instance"

section_key ::= <one of the supported instance config-keys listed below>
section_value ::= <valid value for given section_key>
~~~

valid config-keys are:

name
:    *required, string* the name of this links_and_nodes instance. Daemons use this name
     to separate resources from different manager instances.

manager
:    *required, string, HOSTNAME ":" TCP_PORT* the host-name and TCP-port-number of the
     ln-manager for this instance. This has to be unique - i.e. there
     can not be another ln-manager on the same host with the same
     port-number! You should use the real-hostname or IP-address of
     your manager host. (you could also use `%(env HOSTNAME)` or
     `%(shell uname -n)` or `%(shell hostname)` as the hostname-part)

daemon_private_key_file
:    *string, filename to DSA private key in PEM format* To protect daemons from being used by
     unauthorized users/managers they can be started with a public-key
     (see daemon_public_key_file) on their command line (see [starting
     a daemon](#starting-a-daemon)). For those daemons the manager needs to know the matching
     private-key to authenticate itself against the daemon.

     Currently only DSA keys in PEM-format are supported (like the
     ones created with OpenSSH's `ssh-keygen -t dsa`). The manager uses libtomcrypt to
     sign a random message sent from the daemon. The daemon can then
     check the validity of this signature with the specified
     public-key.

     This is considered to provide a state-of-the-art authentication
     mechanism. But beware: it all depends on how secret your private
     key really is! Make sure only authorized users can read that
     key-file!

     (This does not cause the daemon-manager connection to be
     encrypted! It's only use is to protect daemons from being used by
     unauthorized users. Reading/sniffing the cleartext TCP connection
     does not provide reasonable information about the private key!)

daemon_public_key_file
:    *string, filename to DSA public key in OpenSSH format* When the
     manager needs to start a new daemon and this config-key is
     specified, the manager passes this filename via an command line
     option to the newly started daemon. Daemons started with a public
     key require managers to authenticate themselves with a matching
     private key. See previous config-option.

max_log_items
:    *integer, number of messages, default: 5000* This is used by the manager-GUI to
     limit the number of log-messages to keep.

log_file
:    *string, filename* When specified all manager generated
     log-messages will be appended to this file.

ssh_binary
:    *string, filename of ssh-program, default: /usr/bin/ssh* The
     manager tries to use this ssh-program when a new daemon process
     has to be started on a remote machine. (you want to adjust this
     settings especially when the manager runs on a windows host)

additional_ssh_arguments
:    *string, command line arguments* With this setting you can pass
     additional command line arguments to the ssh-command line (see
     above and next paragraph).

     The ssh-command line will look like this:
     `<ssh_binary> -T -a -k -x -o 'StrictHostKeyChecking=no' <additional_ssh_arguments> TARGET '<daemon_start_script>'`

daemon_start_script
:    *string, command line* When starting a new local or remote daemon
     this command-line will be executed.

     The default is: `nohup %(LN_BASE)/scripts/ssh_daemon_start.sh`

     This shell-script tries to identify the host-architecture its
     running on (by executing `uname -s`) and then starts a matching daemon process.

vte_font
:    *string, pango font description, default: Monospace 9* This font
     description will be used to set the font of all
     VTE-Terminal-Widgets (see process config flag `use_vte`). 

     The format is: `[FAMILY-LIST] [STYLE-OPTIONS] [SIZE]`
     
     See [pango FontDescription reference](https://developer.gnome.org/pango/stable/pango-Fonts.html#pango-font-description-from-string)
     for more information.

vte_color_style
:    *string, default: holo_flo* This setting selects a color-profile
     to use with VTE-Terminal-Widgets. You can choose one of [ "linux" "rxvt" "tango" "xterm" "zenburn"
     "solarized_light" "solarized_dark" "holo" "holo_flo" ]


documentation
:    *string, PDF filename or URL* When specified, a button will be
     shown in the manager-GUI which tries to open the referred
     document when pressed.

monitor_path
:    *string, pathname* A path pointing to the `bin`-directory within
     a monitor-release. This path will be used to start an
     `mrelay`-process on every daemon-host. This can be set to be the
     empty string to avoid the automatic mrelay start.

     Default: `/volume/software/common/packages/monitor/1.4.12/bin`

gen_message_definition_path
:    *string, pathname* LN-clients can automatically generate possibly
     dynamic message-definitions. Those message-definitions need to be
     stored on a filesystem. 

     Default: `~/ln_message_definitions/gen`

scope_process_template
:    *string, process_template-name* The manager tries to start a
     scope-GUI-process when the user double-clicks on a topic-field
     in the port-inspector window. This process is derived from an
     internal process_template. With this option the user can provide
     an own process_template (with two arguments: topicname and
     fieldname) to start a different program or to start the scope on
     a different host.

     Example:

~~~
instance
name: test_instance
manager: localhost:54414
scope_process_template: my_scope_template

process_template my_scope_template(topic_name, value_name)
command: echo display scope from topic %(topic_name) on field %(value_name)
flags: start_in_shell
node: localhost
~~~




Process Section
---------------


The process-section describes a new process object. Each process
object has to have a unique `process_name`, a `command` line and an
assigned `node` to be started on. The given node-name needs to resolve
to a single host.

~~~
# from the lexical definition:
config_section ::= section_header "\n" key_value_pairs*
section_header ::= section_type [ WS section_name ]

key_value_pairs ::= [WS] section_key [WS] ": " section_value "\n"
section_key ::= terminal_string
section_value ::= config_string | ( "[" config_string_multiline "]" )

# syntax:
section_header ::= section_type WS section_name
section_type ::= "process"
section_name ::= process_name

section_key ::= <one of the supported process config-keys listed below>
section_value ::= <valid value for given section_key>
~~~

valid configuration-keys are:

command
:    *required, string, command line*

node
:    *required, string* node_name where this process shall be started
     on. this can also be a host_name. an ip-address is a valid
     host_name.

term_signal
:    *string of ["SIG"] [ "INT" "TERM"  ...] or int signal, default:
     SIGTERM*

term_timeout
:    *float, time in seconds*

ready_regex
:    *string, regular expression*

ready_time
:    *float, time in seconds*

policy
:    *string of [ "SCHED_FIFO" "SCHED_RR" ... ]* scheduling policy. supported
     values depend on the node's operating system.

priority
:    *int* scheduling priority. supported values depend on selected
     policy and the node's operating system.

affinity
:    *list of int* list of cpu-id's (zero based) where this process is
     allowed to run on.

warning_regex
:    *string, regular expression*

warning_msg
:    *string*

documentation
:    *string*

derive_from
:    *string*

change_directory
:    *string, directory*

environment
:    *list of strings*

pass_environment
:    *list of strings, names of environment variables* The environment
     variables listed here are read from the managers environment and
     passed unmodified to the environment of the process.

flags
:    *list of strings*
     valid process flags:

	 forward_x11
	 :	  This option establishes tcp-tunnels and sets necessary
	 	  environment variables to allow this process to connect to the
	 	  currently running X-server.	 
		  
		  This is equivalent to ssh's ForwardX11 option but without
		  encryption!

		  Environment variables overwritten are: DISPLAY and
		  XAUTHORITY. This is all that is needed for a simple X11
		  connection.

     start_in_shell
     :    the most efficient way to start a process is via operating
     	  system-specific system calls. this also guarantees the most
     	  exact control about the environment the process is started
     	  in. 

     	  but sometimes you want to execute the command-line via a
     	  shell. this is done when this flags is specified. the
     	  default system shell will be used to execute the command-line
     	  (`/bin/sh`).
	  
     	  this allows to execute internal shell-commands which have no
     	  own program-binary to start (like bash's `ulimit`, `if`, `;`...)

     no_pty
     :    when this flag is specified, no pseudo-terminal will be allocated for this
     	  process -- which is more resource efficient. **BUT**: typical
     	  standard-C libraries change their behaviour depending on
     	  whether they are used within a terminal or not. for example:
     	  they switch their stdout-buffering from line-based to
     	  page-based - which means that the process-output will only
     	  be visible when a complete memory-page is filled (~4kB). On
     	  windows there is nothing comparable to a pseudo-terminal
     	  (the win32-console window is the only available terminal).
     
     use_vte
     :    when this flag is given a full-featured
          terminal-emulator-widget is used to display the process
          output within the ln_manager-gui. this will only have an
          effect when the python-vte package is available on the
          manager-gui-host.
          this terminal widget allows processes to display colors and
          advanced console-based user interfaces (like emacs or vim).
     
     remove_color_codes
     :    you can remove color-escape-sequences from process output if
          you are not interested in having colored process-output
          and/or you don't use the `use_vte` flag.

     no_state_led
     :    this flag avoids the display of a square (red, yellow,
          green) indicator box for this process.
     
     no_warning_window
     :    as soon as you specify a `warning_regex` a dialog-window will
          pop-up every time that `warning_regex` matches a
          process-output line. by using this flag the display of the
          warning-dialog can be disabled.

     no_error_on_stop
     :    when a process terminates on its own - without the user
          requesting it - its interpreted as an error if this flag is
          not specified
     
     disable_stop
     :    this flag disabled the the stop-button in the manager gui.

depends_on
:    *list of strings, process- or state names*

depends_on_restart
:    *list of strings, process- or state names*

start_on_ready
:    *list of strings, process- or state names* the processes or
     states listed here will be started as soon as this process
     reaches its ready state. They do not need to
     keep running for the whole lifespan of this process and are not
     needed for this process to be in a valid state. 

     Use-cases include starting small shell-scripts or other helper
     programs every time when this process is started. It is advisable
     to specify the `no_error_on_stop`-flag for processes listed here!
     
     *since version 0.2.5*

provides
:    *list or strings*

output_encoding
:    *string, default: utf-8*

max_output_lines
:    *int, number of lines*

max_output_frequency
:    *float, frequency in Hz, default: 10Hz*

auto_restart_interval
:    *float*



Library Reference
=================

the library is available for different programming
languages. the next sections shall describe how to use the
specific language-binding. thereafter all available function are
described. 

#### using the C/C++ binding
for C/C++ programs you need to include the "ln.h" header-file:

~~~c
#include <ln.h>
~~~

and you need to link against the libln-library. for this to work you
need to tell your compiler and linker where to find those. in case
of gcc/g++ it could look like this:

~~~shell
# compiling:
$ g++ -o my_program.o -c my_program.cpp -I${LN_BASE}/include
# linking:
$ g++ -o my_program my_program.o -L${LN_BASE}/lib/sled11-x86-gcc4.x -lln
~~~

#### using the Python binding

For python programs you need to import the links_and_nodes package:

~~~python
import links_and_nodes as ln
~~~

I suggest to alias the `links_and_nodes`-package as `ln` for easier
typing.



ln_client constructor
---------------------

~~~c
/* C */
int ln_init(ln_client* clnt, const char* client_name, 
    	    int argc, char** argv);
int ln_init_to_manager(ln_client* clnt, const char* client_name, 
                       const char* ln_manager);

// C++
ln::client(std::string client_name);
ln::client(std::string client_name, int argc, char* argv[]);
ln::client(std::string client_name, std::string ln_manager);
ln::client(ln_client clnt);
~~~

~~~python
# python
class ln.client(client_name, args)
class ln.client(client_name, ln_manager=None)
~~~



Simulink Binding
================


Appendix
========



The LN-Build System
-------------------



### how to add new architectures
for cross compiling


## left overs

the daemon should be started by system startup-scripts or
     manually. as kind of a last-resort the manager tries to start the
     ln_daemon by itself via ssh or telnet if available.

  
  daemons and client library are available for 
    - linux on x86, x86_64, armv7
    - qnx 6.3 and 6.5 on x86
    - vxworks 6.8 and 6.9 on x86, vxworks 6.9 on armv7
    - win32 on x86
  (recommended full featured platforms: linux, qnx, vxworks 6.8 on x86)

  client-library has bindings to
    - C (native API)
    - C++ (thin wrapper)
    - python (boost-python binding ontop C++ wrapper)
    - java/android
  
  how to get started:
  - add this to your ~/.bashrc:
      export LN_BASE=/home/schm_fl/workspace/ln_base/install/links_and_nodes/0.0.1
  - for python also add:
      export PYTHONPATH=$LN_BASE/share/python
  - a cross-compiling Makefile for c/c++ can be found here:
      $LN_BASE/share/Makefile.example
      (copy to your project dir as Makefile)
  - write a links-and-nodes config file and then start the ln-manager:
      $LN_BASE/scripts/ln_manager -c my_config.lnc
