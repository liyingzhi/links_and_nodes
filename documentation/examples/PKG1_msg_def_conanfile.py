import os
from conans import tools, ConanFile

class PKG1_ln_msgdef(ConanFile):
    name = "PKG1_ln_msgdef"
    url = ...
    ...
    description = "links_and_nodes message definitions for PKG1"
    settings = None
    exports_sources = [
        "PKG1_major/*"
    ] + ["!%s" % x for x in tools.Git().excluded_files()]

    def package(self):
        self.copy("PKG1_major/*", "ln_msg_defs")
    
    def package_info(self):
        self.env_info.LN_MESSAGE_DEFINITION_DIRS.append(
            os.path.join(self.package_folder, "ln_msg_defs"))
