.. https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html
.. https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
.. https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html
.. https://pygments.org/docs/lexers/
   
Welcome to links_and_nodes documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   config_file
   message_definition_usage
   migrations
   python3_migration


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
