migrations between different versions
*************************************

towards LN 2.x
==============

python binding
--------------
the python binding can now be used with python3. see :ref:`python3_migration-label`.

deprecated API that was removed
-------------------------------
.. contents::
   :local:

``ln_wait_for_service_requests()`` and ``ln_handle_service_requests()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
.. code-block:: C

  // old, removed API:
  while(keep_running) {		
      if(ln_wait_for_service_requests(clnt, 1))
          ln_handle_service_requests(clnt); // handles all pending service requests
  }  
  // new:
  while(keep_running)
      ln_wait_and_handle_service_group_requests(clnt, NULL, 1); // every second we check `keep_running`


C++ binding:
  
.. code-block:: C++

  // old, removed API:
  while(true)
      if(clnt.wait_for_service_requests())
          clnt.handle_service_requests();
  // new:
  while(true)
      clnt.wait_and_handle_service_group_requests(std::nullptr);


python binding:

.. code-block:: python

  # old, removed API:
  while True:
      if self.clnt.wait_for_service_requests(): # (block until request)
          self.clnt.handle_service_requests()
  # new:
  while True:
      self.clnt.wait_and_handle_service_group_requests()
      # or: self.clnt.wait_and_handle_service_group_requests(None)
      # or: self.clnt.wait_and_handle_service_group_requests(None, -1)

``ln_enable_async_service_handling()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
(C++ binding: ``ln::client::enable_async_service_handling()``)

.. code-block:: C

  // old, removed API:
  ln_enable_async_service_handling(clnt, 1);
  // new:
  ln_handle_service_group_in_thread_pool(clnt, NULL, "main thread pool");


``ln::client::unregister_service_provider()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
(python binding: ``ln.client.unregister_service_provider()``)

.. code-block:: C++

  // old, removed API:
  clnt.unregister_service_provider(time_service);
  // new:
  clnt.release_service(time_service);

the old function was used in ln_generate-generated header files. but it was
always discoraged to distribute those generated headers. users should
generate those headers with their buildsystem for whatever messages they
want to use.

``WITH_DEPRECATED_GET_TIME``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
user of ``ln.h`` could define that macro to get a ``get_time()`` function.
instead use `ln_get_time()` for wall time or `ln_get_monotonic_time()` for
a monotonic time source.


towards LN 1.2.3
================

API changes
-----------

flat-md
^^^^^^^
there is a new ``ln_get_message_definition_v21()`` which provides the
"flat-md"-representation that is used by
``links_and_nodes_simulink/ln_publish_subscribe_sfun``.

C++ programs can simply use the new member-overload
``ln::client::get_message_definition()`` with 5 arguments.

python ``ln.client.get_message_definition()`` now returns the 4-tuple
``md, msg_size, hash, flat_md``.
