``instance`` section
====================

usually the first entry in your config file.

example:

.. code-block:: lnc

  instance
  name: %(env USER)'s first LN setup
  manager: :%(get_port_from_string %(instance_name))
  enable_auto_groups: true

name
----
required name of this instance. it needs to be globally unique to avoid conflicts with other LN instances.

manager
-------
required manager TCP port number of this instance. expected to be in format ``IP:PORT`` where IP is ip-address of one of
the managers network interfaces to listen for incoming connection on -- can be left empty. PORT is a TCP/IP port number
where LN clients communicate with the LN manager.

example: ``manager: :54412``


check_all_states_after_daemon_connect
-------------------------------------
if set to ``true`` will cause all configured states of a host to be *checked* as soon as a connection is established.
(default: ``false`` since version 0.13.11)

.. versionadded:: 0.13.0

flags
-----

* ``use_old_arch_names``
* ``disable_all_vte``
* ``no_shmv3``
* ``log_store_stack``
  append a stack-print to each log-message

enable_auto_groups
------------------
when set to ``true`` will interpret ``/``-character in process & state names as delimiters between groups and
display-names.
groups will be automatically created.

.. versionadded:: 0.8.5


log_file
--------
.. note::
   todo

log_file_size_limit
-------------------
.. note::
   todo

log_file_keep_count
-------------------
.. note::
   todo

groups_opened_default
---------------------
.. note::
   todo

groups_sorted_default
---------------------
.. note::
   todo

default_notebook_path
---------------------
.. note::
   todo

daemon_private_key_file
-----------------------
.. note::
   todo

daemon_public_key_file
----------------------
.. note::
   todo

max_log_items
-------------
.. note::
   todo

additional_ssh_arguments
------------------------
.. note::
   todo

scope_process_template
----------------------
.. note::
   todo

notebook_process_template
-------------------------
.. note::
   todo

ssh_binary
----------
.. note::
   todo

daemon_start_script
-------------------
.. note::
   todo

vte_font
--------
.. note::
   todo

vte_color_style
---------------
.. note::
   todo

gen_message_definition_path
---------------------------
.. note::
   todo

documentation
-------------
.. note::
   todo

default_change_user_to
----------------------
.. note::
   todo

default_expected_user
---------------------
.. note::
   todo

allow_x11_tcp_connect
---------------------
.. note::
   todo

open_scope_on_parameter_double_click
------------------------------------
.. note::
   todo

do_not_try_uids_of_clients
--------------------------
.. note::
   todo

do_lock_daemons_with_unexpected_uids
------------------------------------
.. note::
   todo

default_resource_limits
-----------------------
.. note::
   todo

