``hosts`` section
====================

``hosts``-sections can be used to specify per-host settings. these include ip-addresses, ln_daemon-start-commands,
authentication settings and more, as described below.

it is not needed to always have a ``hosts``-section, ln_manager will guess reasonable default values and will use the
systems name-resolver to get ip-addresses when trying to contact remote hosts.

syntax::

  HOSTS_SECTION := "hosts\n" + HOST_SPEC*
  HOST_SPEC := <HOST-NAME> + ": " + HOST_SETTINGS + "\n"
  HOST_SETTINGS := HOST_SETTING + [ ", " + HOST_SETTING ]*
  HOST_SETTING := <SETTING-NAME> + ":" + <SETTING-VALUE>

example:
  
.. code-block:: lnc

  hosts
  localhost: process_start_rate_limit:5
  rmc-mobilproxy: ens3:129.247.189.41, ens10:192.168.128.1

in this example, at most 5 processes per second will be started on localhost. for ``rmc-mobilproxy`` we specify two
ip-addresses with assigned interface-names ens3 and ens10.

an ln-config file can have multiple ``hosts``-sections. they will be merged -- later ones can overwrite settings or add
more interfaces.

valid settings:

<interface-name>
----------------
any ``<SETTING-NAME>`` that does not match one of the below listed, known settings, will be assumed to be an
interface-name. ``<SETTING-VALUE>`` in this case has to be a valid IPv4-address in dotted-decimal form.
example::

  en0:129.247.189.41

the interface name is only used to refer to that interface. it doesn't have to match the actual interface name.

process_start_rate_limit
------------------------
this setting can be used to limit how many processes are started per second at most on this host. without this setting
there is no limit. this can lead to an unresponsive system if you try to start many processes "at once".
example::

  process_start_rate_limit:2

will only allow 2 processes to be started within the same second on this host.

daemon_start_script
-------------------
in case ln_manager can not contact an already running daemon, it will try to start a new one. this setting can be used
to specify the command that should be used.

default is::

  <LN_PREFIX>/bin/ln_daemon

where ``<LN_PREFIX>`` depends on how LN was installed on your system.

this default can be overwritten by environment variable ``LN_DAEMON_START`` to set a new site-local start-command
(e.g. via conan).
  
it can be set to special value ``none`` to NOT have the manager try to start a daemon here.

additional_ssh_arguments
------------------------
additional argument to append to the ssh-command line when ln_manager tries to start an ln_daemon on a remote host.
default is empty. they will be appended to this ssh-command::

  <SSH-BINARY> -l <USERNAME> -T -v -a -x -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o
  PasswordAuthentication=no -o PreferredAuthentications=gssapi-with-mic,publickey -K <HOST-NAME-OR-IP> <CMD>

ssh_binary
----------
this setting can be used to cause ln_manager to use a different ssh-binary than ``ssh``.
 
arch
----
this can be an arbitrary string that will be passed to newly started ln_daemons. daemons will report this arch when
ln_manager connects them. this can be used in ``process`-sections as runtime-variable ``$(ARCH)`` e.g. in the
``command:``-field.

daemon_private_key_file
-----------------------
private key to use for user-authentication towards daemons. unlocked daemons will be locked with this private key on
connection.

daemon_public_key_file
----------------------
public key to use for user-authentication towards daemons. ln_manager will use this public key to sign
authentication-challenges from locked ln_daemons to authenticate itself.

preferred_network
-----------------
if a host is reachable via multiple networks, this setting can be used force the use of the named network.

expected_user
-------------
when ln_manager is  contacting ln_daemons, it defaults to expect the daemon to be running as the same user.
use this setting if you want to override this, e.g. if the remote host only has some few local users.

expected_user_id
----------------
similar as ``expected_user`` this can be used to require a specify user-id from the remote ln_daemon.

arbiter_port
------------
ln_daemons will always start an `arbiter`-process per host if there is not already one running. this arbiter is usually
expected to run behind port-number ``54376``.

default_change_user_to
----------------------
if the ln_daemon itself is running with root-rights, this setting can be used to switch to a different user when
starting processes where no specific ``change_user_to``-setting was specified.

requires_tunnel
---------------
this setting is useful if some other state or process has to be UP or ready before ln_manager is allowed to try to
contact this host.
this can be used to bring up a new interface (possibly a vpn) or start other kind of tunnels.
``<SETTING-VALUE>`` should be the name of a process or state in your config.

filesystems
-----------
a list of file-system names this host has access to.

.. note::
   todo: i think this is not yet used for anything?

