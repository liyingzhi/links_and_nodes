#!/usr/bin/python

import lntest

test = lntest.new_test()

test.short_description = "test ln_manager gui connection"
test.description = """
start lnm with/without gui with simple lnc file
start test-lnm-gui-client which tests connection and protocol
wait until test-client finishes
"""

"""
implicitely:

stop all processes at end of test.
collect all generated reports
classify reports into OK, WARNING, ERROR
"""

lnc = test.new_template("test.lnc.in")
lnc.generate("test.lnc")

def without_gui(*args):
    if test.get_parameter("ln_manager_without_gui", True):
        args = list(args)
        args.insert(0, "--without_gui")
    return args

lnm = test.new_python_process(
    "lnm", 
    pyscript=test.get_parameter("ln_manager_pyscript", "ln_manager.py"),
    arguments=without_gui("-c", "test.lnc")
)

lnm.wait_for_line_with_regex("listening on .* for ln-clients!") # with some default timeout

test_client = test.new_python_process(
    "test_client", 
    pyscript="test_client.py",
    arguments=["localhost:%d" % test.get_parameter("ln_manager_port", 51234)],
    
    # todo: env
)

test_client.wait() # expect to terminate with status 0!

# when we get here we assume the test went fine up to now. 
# now inspect generated reports to decide the result of this test

lnm_out = lnm.get_output()
lnm_out_lower = lnm_out.lower()
if "error" in lnm_out_lower:
    test.fail("there are error messages in lnm output!")
if "warning" in lnm_out_lower:
    test.warning("there are warning messages in lnm output!")

test.ok()
