#!/bin/bash

SCRIPT=$(readlink ${BASH_SOURCE[0]} || echo ${BASH_SOURCE[0]})
RELPATH=$(dirname ${SCRIPT})
pushd ${RELPATH} > /dev/null
TESTS_DIR=$(pwd -P)
popd > /dev/null


grep -v "^#" $TESTS_DIR/known_good_tests | while read test; do
    rm $TESTS_DIR/$test/*_coverage.py 2>/dev/null
done

$TESTS_DIR/do_test.sh --check-coverage "$@"
