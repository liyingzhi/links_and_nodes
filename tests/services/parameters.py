short_description = "test ln_services"

description = """
start lnm with/without gui with simple lnc file
start provider & client processes that test service calls.
wait until both test-clients finish
"""

provides = [
    "use ln python binding",
    "add env vars", # todo?
    "use local mds", # todo?
    "use ready regex" # todo?
]

#enable_coverage_monitor = True
enable_coverage_monitor = False

old_ln_base = "/volume/software/common/packages/links_and_nodes/0.10.1"
