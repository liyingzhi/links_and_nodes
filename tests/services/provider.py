#!/usr/bin/python

from __future__ import print_function

import sys
import time
import links_and_nodes as ln
import random

random.seed(0)

class test_provider(ln.service_provider):
    def __init__(self):
        self.clnt = ln.client("test_provider", sys.argv)
        ln.service_provider.__init__(self, self.clnt, "test")

        self.wrap_service_provider("add", "test/add",
                                   #utf8_decode_char_fields=False
        )

    def add(self, a, b, fixed_text, dyn_text):
        res = a + b
        print("a %s + b %s is %s" % (a, b, res))
        print("fixed: %r, dyn: %r" % (fixed_text, dyn_text))
        if isinstance(fixed_text, bytes): # we decode them
            fixed_text = fixed_text.decode("utf-8")
            dyn_text = dyn_text.decode("utf-8")
        time.sleep(random.random() * 0.05)
        print("return")
        return dict(c=res, fixed_text=fixed_text.upper() + "UUU", dyn_text=dyn_text.upper().encode("utf-8"))

    def run(self):
        self.handle_service_group_requests()
        
if __name__ == "__main__":
    p = test_provider()
    p.run()
