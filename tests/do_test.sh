#!/bin/bash

SCRIPT=$(readlink ${BASH_SOURCE[0]} || echo ${BASH_SOURCE[0]})
RELPATH=$(dirname ${SCRIPT})
pushd ${RELPATH} > /dev/null
TESTS_DIR=$(pwd -P)
popd > /dev/null

grep -v "^#" $TESTS_DIR/known_good_tests | while read test; do
    if ! python3 $TESTS_DIR/$test/test.py "$@"; then
	echo "ERROR: test '$test' did not succeed!"
	exit 1
    fi
done || exit 1

echo "all tests OK"

