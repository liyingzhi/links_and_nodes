# lntest package init
# provides user-interface to test.py scripts

from . import test_class

def new_test(*args, **kwargs):
    return test_class.test_class(*args, **kwargs)
