from string import Template

class test_parameter_provider(object):
      def __init__(self, test):
            self.test = test

      def __getitem__(self, name):
            return self.test.get_parameter(name, "<undefined parameter %s>" % name)

class file_template(object):
      def __init__(self, test, input_file):
            self.test = test
            self.input_file = input_file
            self.provider = test_parameter_provider(self.test)

      def generate(self, output_file):
            input_data = open(self.input_file, "r").read()
            ignores = [
                  "$(ARCH)",
            ]
            def rep(ignore):
                  return "####%s####" % ignore[1:]
            had_ignores = False
            for ignore in ignores:
                    if ignore not in input_data:
                            continue
                    input_data = input_data.replace(ignore, rep(ignore))
                    had_ignores = True
            s = Template(input_data)
            output_data = s.substitute(self.provider)
            if had_ignores:
                    for ignore in ignores:
                            output_data = output_data.replace(rep(ignore), ignore)
            open(output_file, "w").write(output_data)
            self.test.report_file(output_file)
