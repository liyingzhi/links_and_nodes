#!/usr/bin/python

import os
import sys; sys.path.insert(0, "..")
import lntest
import testlib

test = lntest.new_test()
lnm = testlib.get_lnm(test, extra_args=["--console"])
lnm.wait_for_line_with_regex("ln_manager console.")
lnm.write("start main\n")
lnm.wait_for_line_with_regex("all/main: started", timeout=4, hint="wait for main start")
lnm.wait_for_line_with_regex("all/on_ready: started", timeout=4, hint="wait for on_ready start")
lnm.wait_for_line_with_regex("all/main: stopped", timeout=4, hint="wait for main stop")
lnm.wait_for_line_with_regex("all/after: started", timeout=4, hint="wait for after start")
lnm.wait_for_line_with_regex("all/after: stopped", timeout=4, hint="wait for after stop")
lnm.write("quit\n")

test.ok()

