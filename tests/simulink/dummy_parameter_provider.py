import sys

import links_and_nodes

clnt = links_and_nodes.client("dummy_parameter_provider", sys.argv)

def params(**kwargs):
    ret = dict()
    for key, value in kwargs.items():
        ret[key] = dict(
            input=value,
            output=value,
            override_enabled=False
        )
    return ret

class Provider(links_and_nodes.service_provider):
    def __init__(self, clnt):
        links_and_nodes.service_provider.__init__(self, clnt, "test")
        self.wrap_service_provider("query_dict", "ln/parameters/query_dict")

        self.params = params(
            param1=42
        )
        
    def query_dict(self, pattern):
        print("query dict for pattern %r" % pattern)
        ret = repr(self.params)
        print("return: %r" % ret)
        return ret

provider = Provider(clnt)
print("ready")
while True:
    clnt.wait_and_handle_service_group_requests(None, -1)
