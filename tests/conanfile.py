import os
import sys
from six import StringIO
from conans import ConanFile, tools

class test_links_and_nodes_manager(ConanFile):
    def test(self):
        print("pwd: %r" % os.getcwd())
        tests_folder = os.path.dirname(os.path.dirname(os.getcwd()))

        mybuf = StringIO()
        self.run("which ln_manager", output=mybuf, run_environment=True)
        ln_manager_pyscript = mybuf.getvalue().strip()
        self.output.warn("ln_manager location: %r" % ln_manager_pyscript)
        env = dict(
            LNTEST_RESULTS_BASE=os.getcwd(),
            LNTEST_LN_MANAGER_PYSCRIPT=ln_manager_pyscript,
            LN_MANAGER_CONDITIONS_CHECK_FOR_LONG_WAITERS="n", # disable display of traceback for long waiters
            #LN_TERM_DEBUG="y",
        )
        with tools.environment_append(env):
            self.run("./do_test.sh -v", cwd=tests_folder, run_environment=True)
