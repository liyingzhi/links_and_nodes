#!/usr/bin/python

import sys
import time
import links_and_nodes as ln

class test_provider(ln.service_provider):
    def __init__(self):
        self.clnt = ln.client("test_provider", sys.argv)
        ln.service_provider.__init__(self, self.clnt, "test")

        self.wrap_service_provider("add", "test/add")

    def add(self, a, b):
        res = a + b
        sys.stdout.write("a %s + b %s is %s\n" % (a, b, res))
        time.sleep(0.05)
        sys.stdout.write("return\n")
        return res

    def run(self):
        self.handle_service_group_requests()
        
if __name__ == "__main__":
    p = test_provider()
    p.run()
