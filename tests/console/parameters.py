short_description = "test ln_manager console"

description = """
start lnm with/without gui with simple lnc file
issue some start / stop commands via ln-console and check for expected output.
"""

provides = [
    "use ln python binding",
    "add env vars", # todo?
    "use local mds", # todo?
    "use ready regex" # todo?
]
