#!/usr/bin/env python3

import os
import re
tests_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
import sys; sys.path.insert(0, tests_dir)
import lntest
import numpy as np
import pprint

rate_error_threshold = 30

test = lntest.new_test()

test.debug("pythonpath:")
for path in sorted(sys.path):
    test.debug("  %r" % path)

lnc = test.new_template(os.path.join(test.src_dir, "test.lnc.in"))
lnc.generate(os.path.join(test.src_dir, "test.lnc"))

import testlib

lnm = testlib.get_lnm(test, extra_args=["--mi-console"])

lnm.console.exec("log enable")

lnm.console.start_wait_ready("lnrecorder script")
out = lnm.console.exec("tail 'lnrecorder'", expect_response=True)

lnm.console.start_wait_ready("publish")

resp = lnm.console.exec("show_topic topic1", expect_response=True)
match = re.search("daemon-state:.*?rate: (.*?)\n", resp, re.M | re.S)
test.debug("found publisher rate string %r" % (match.group(1)))
measured_rate = float(match.group(1))
expected_rate = test.get_parameter("publish_rate")
error = abs(measured_rate - expected_rate) / expected_rate * 100
test.debug("publisher rate %.1fHz error of %.1f%%" % (measured_rate, error))
if error > rate_error_threshold:
    raise Exception("more than %s%% error of publishing rate (msr: %.1f, expected: %.1f, error: %.1f%%)" % (
        rate_error_threshold, measured_rate, expected_rate, error))

lnm.console.start_wait_ready("daemon logger script")

# now start subscriber process
lnm.console.start_wait_ready("subscribe")
lnm.console.exec("sleep 2")

# inspect topic sched info
sinfo = lnm.console.exec("get_topic_scheduling_info topic1", expect_response=True)
sinfo_parsed = []
for line in sinfo.split("\n"):
    if line[:1] != "[":
        continue
    sinfo_parsed.append(eval(line))
sinfo = sinfo_parsed
test.debug("got scheduling info:\n%s\n" % pprint.pformat(sinfo))
if len(sinfo) != 3: raise Exception("we expect three lines in sched-info, got:\n%s" % pprint.pformat(sinfo))

resp = lnm.console.exec("show_topic topic1", expect_response=True)
# todo: sth to check in resp?

# stop subscriber and check its output
resp = lnm.console.stop_wait("subscribe")
subscriber_output = lnm.console.exec("tail subscribe", expect_response=True)
rates = None
md_info = None
for line in subscriber_output.split("\n"):
    #test.debug("subscriber output line: %r" % line)
    if rates is None:
        if md_info is None:
            if line.startswith("md:"):
                md_info = {}
            continue
        if line.startswith("  "):
            mdfield, mdvalue = line[2:].split(": ", 1)
            md_info[mdfield] = eval(mdvalue)
            continue
        rates = []
        continue
    if line.startswith("rate:"):
        rate = line.split(":", 1)[1].split(",", 1)[0]
        rates.append(float(rate))
test.debug("subscriber md_info:\n%s" % (pprint.pformat(md_info)))
if md_info == "":
    raise Exception("md_info provides no flat-md!")
test.debug("subscriber rates: %r" % rates)
if not rates:
    raise Exception("subscriber did not report any rates!")
measured_rate = mean_rate = np.mean(rates)
test.debug("mean subscriber rate: %.1f" % mean_rate)
error = abs(measured_rate - expected_rate) / expected_rate * 100
test.debug("subscriber rate %.1fHz error of %.1f%%" % (measured_rate, error))
if error > rate_error_threshold:
    raise Exception("more than %s%% error of subscriber rate (msr: %.1f, expected: %.1f, error: %.1f%%)" % (
        rate_error_threshold, measured_rate, expected_rate, error))

output = lnm.console.exec("tail -n 1000 'daemon logger script'", expect_response=True)
test.debug("\ndaemon logger script output:\n%s\n--" % output)
if "terminated with retval: 0" not in output:
    print("daemon logger script output:\n%s\n--" % output)
    raise Exception("daemon recorder script output did not terminate as expected!")


lnm.console.stop_wait("publish")
lnm.console.stop_wait("lnrecorder script")
out = lnm.console.exec("tail -n 1000 'lnrecorder script'", expect_response=True)
test.debug("lnrecorder test script output:\n%s\n" % out)

lnm.write("quit\n")
lnm.wait()

# when we get here we assume the test went fine up to now.
# now inspect generated reports to decide the result of this test
testlib.check_lnm_output(test, lnm, removes=[
    "\[WARNING\]:",
    "warning.*still waiting for answer of"
])

test.ok()
