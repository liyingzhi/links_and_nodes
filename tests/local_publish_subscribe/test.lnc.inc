instance
name: test instance
manager: %(shell hostname):51234
enable_auto_groups: true

process all/test process
command: echo "test process started!"; sleep 0.1; echo "test process stopped!"
flags: start_in_shell, no_error_on_successful_stop
node: localhost