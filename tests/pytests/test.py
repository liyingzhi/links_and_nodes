#!/usr/bin/env python3

import os
tests_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
import sys; sys.path.insert(0, tests_dir)
import lntest
import re
import testlib

test = lntest.new_test()

lnm = testlib.get_lnm(test, extra_args=["--mi-console"], config_file="tests/test_conf.lnc")
lnm.console.exec("log enable")

#pub = test.new_python_process("publisher", "publisher.py")

pytest = test.new_python_process(
    "pytest", library_module="pytest",
    arguments=["-v", test.relative_to_test("./tests/")],
    cwd=test.src_dir
)
pytest.wait()
# we got returncode == 0! (no error)
out = pytest.get_output()

match = re.search("===+.*? ([0-9]+) warning.*?===", out)
if match:
    n_warnings = int(match.group(1))
else:
    n_warnings = 0

if test.mainloop.runtime.log_level == "debug": # -v
    # in successfull case, also print pytest output:
    print(out)

# on osl151 with conan 1.22.2 and /opt/python/osl15-x86_64/python3/stable/1.3.3/bin/python3
# i get a warning
#
# pytest-catchlog plugin has been merged into the core, please remove it from your requirements.
# -- Docs: https://docs.pytest.org/en/latest/warnings.html
#
# which i can not avoid.
#
#if n_warnings:
#    test.warning(msg="pytest reported %d warnings" % n_warnings)
#else:

lnm.write("quit\n")
lnm.wait()

test.ok()
