import time

import links_and_nodes as ln

TOPIC_NAME  = 'test_topic'
N_PUBLISHED = 100000
MESSAGE_TYPE = 'uint32_t'

# Connect to ln manager
ln_client = ln.client("publisher")
rate = 100 # 100 Hz

# Publish to a given topic with a buffer of size 10
port = ln_client.publish(TOPIC_NAME, MESSAGE_TYPE, buffers=10)

port.packet.data = 0
for _ in range(N_PUBLISHED):
    # Update data sent (increment the counter)
    port.packet.data += 1
    # Send data
    port.write()
    time.sleep(1 / float(rate))
