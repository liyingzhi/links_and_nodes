import subprocess
import time

import pytest

import links_and_nodes as ln

LN_MANAGER = 'localhost:51234'

def pytest_addoption(parser):
    parser.addoption(
        "--with-manager", action="store_true", help="Launch a ln_manager"
    )

@pytest.fixture(scope="module")
def ln_manager(request):
    """
    Start ln_manager process for the tests
    """
    # LN manager already running
    if not request.config.getoption("--with-manager"):
        yield True
        return

    ln_start = ['ln_manager', '-c', 'tests/test_conf.lnc', '--without_gui']
    # Use bash to have access to aliases
    command = ['/bin/bash', '-i', '-c', ' '.join(ln_start)]

    print("Launching ln_manager...")
    print(command)
    process = subprocess.Popen(command, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE, shell=False)
    # Wait for process to start
    time.sleep(1.0)

    yield process  # provide the fixture value

    # Killing ln_manager
    process.kill()


@pytest.fixture(scope="module")
def ln_client(ln_manager):
    client = ln.client("ln_client")
    yield client
