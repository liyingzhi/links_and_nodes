import links_and_nodes as ln

LN_MANAGER = 'localhost:51234'
TOPIC_NAME  = 'test_topic'
N_PUBLISHED = 100
MESSAGE_TYPE = 'uint32_t'

# Connect to ln manager
ln_client = ln.client("subscriber", LN_MANAGER)
# Subscribe to a topic
port = ln_client.subscribe(topic_name=TOPIC_NAME, message_definition_name=MESSAGE_TYPE)

timeout = 0.1 # O.1s
messages = []
for _ in range(N_PUBLISHED):
    # wait for messages
    have_packet = port.read(timeout) is not None
    if have_packet:
        # read message
        messages.append(port.packet.data)

ln_client.unsubscribe(port)
