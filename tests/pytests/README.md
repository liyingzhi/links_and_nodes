# LN with Python3 Demo

## Getting Started

Run the manager:
```
cissy run
```

Create env using [cpy](https://rmc-github.robotic.dlr.de/raff-an/cissy-python-tools)
```
cpy env create --name ln -i conanfile.txt
```

Activate the env and test the script:
```
cpy activate ln

python3 publisher.py
```

## Run automated tests

Start test ln manager
```
./scripts/start_ln_test.sh
```

Run automated tests:
```
cpy activate ln
./scripts/run_tests.sh
```

which does:
```
python3 -m pytest -v tests/
```

with python2:
```
cpy env create --name py2 -i tests/conanfile_python2.txt
```
and
```
python2 -m pytest -s tests/ -v
```
Note you may need to remove `tests/__pycache__/`
