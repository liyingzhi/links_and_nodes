#ifndef LN_SERVICE
#define LN_SERVICE

#include "os.h"
#include "aux_mem.h"
#include "iovec_mgr.h"

struct service_;
typedef struct service_ service;

struct service_client_;
typedef struct service_client_ service_client;

struct service_group_;
typedef struct service_group_ service_group;

struct service_request_;
typedef struct service_request_ service_request;

struct log_service_;
typedef struct log_service_ log_service;

struct service_request_header_;
typedef struct service_request_header_ service_request_header;

struct _ln_service_field {
	uint32_t field_size; /*
			       datasize if primitive
			       single-data-size if pointer to primitive!
			       count if non primitive!
			       host-size-struct-mem-needs if pointer to non-primitive
			     */
	uint8_t ptr_len_size; // nonzero if pointer!
	uint8_t is_primitive;
	// only if non-primitive:
	unsigned int n_inner_fields;
	unsigned int max_inner_fields;
	struct _ln_service_field* inner_fields;
};

#include "os.h"
#include "client.h"
#include "thread_pool.h"

enum endianess_t {
	BO_LITTLE_ENDIAN,
	BO_BIG_ENDIAN
};

#include "ln/packed.h"
struct service_request_header_ {
	uint16_t version;
	uint32_t client_id;
	uint32_t request_id;
} LN_PACKED;
#include "ln/endpacked.h"

struct service_client_ {
	service* svc;
	int fd;
	struct sockaddr* client_addr;
	os_socklen_t client_addr_len;
	int is_pending;
	char* buffer;
	uint32_t buffer_size;
	uint32_t buffer_len;
	int did_respond;
	int request_error;
	char* service_data;
	struct _ln_iovec_mgr mgr;
	struct _ln_aux_mem_holder aux_mem;
	int is_fake;
	unsigned int fake_request_id;
	int is_busy;
	int handler_return;
	pool_thread* thread;
	char* peer_address;
	double request_time;
	service_request_header* request_hdr;
	char* user_buffer;
	uint32_t user_buffer_len;
};

struct service_group_ {
	client* clnt;
	char* name;
	thread_pool* pool;
	pool_thread* thread;
	ln_pipe_t notification_pipe;
	
	DEFINE_LIST_DATA_TYPE_NAME(service, provider);
};

struct log_service_ {
	service* svc;
	ln_mutex_handle_t mutex;
	struct _ln_iovec_mgr mgr;
	uint8_t* iov_element_lens;
	unsigned int iov_element_lens_size;
};

struct service_ {
	client* clnt;
	char* name;
	char* interface_name;
	char* signature;

// common
	int is_provider;
	int fd; // -1 if not connected
	unsigned int request_len; // FIXED! size of request part in service_data in bytes
	unsigned int hostside_response_len; // FIXED! size of response part in service_data in bytes

	struct _ln_aux_mem_holder aux_mem;
	DEFINE_LIST_DATA(log_service);

	int send_request_id;
	ln_mutex_handle_t mutex;
// user
	uint32_t request_id;
	// uint32_t n_bytes; // n bytes to send (sum of msg.msg_iov.iov_len's)
	// struct msghdr msg; // request message, .msg_iovlen hold real length
	// unsigned int msg_max_len; // max len for allocated mem of .msg_iov
	struct _ln_iovec_mgr mgr;

	// unsigned int n_fields;
	// struct _ln_service_field* fields;
	struct _ln_service_field req;

	// unsigned int n_resp_fields;
	// struct _ln_service_field* resp_fields;
	struct _ln_service_field resp;

	char* response_buffer;
	uint32_t response_len;
	uint32_t response_buffer_len;

	int is_aborted;
	
	// async requests:
	void* async_user_data;
	int async_req_is_v;
	struct iovec* async_user_data_iov;
	unsigned int async_user_data_iov_len;
	uint8_t* async_user_data_iov_element_lens;
	int async_req_finished;
	int async_req_retval;
	int async_req_running;
	int async_did_connect;
	int async_did_read_user_data;
	int async_did_send;
	ssize_t async_n_to_send;
	ssize_t async_n_sent;	
	int async_request_wait_for_write;
	int async_request_wait_for_read;
	int async_did_recv_size;
	char* async_response_buffer;
	ssize_t async_n_recvd;
	ln_pipe_t async_request_notification_pipe;
	int async_request_notification_pipe_was_signaled;
	struct _ln_iovec_mgr* async_backup_mgr;
	double async_completion_time;	

	enum endianess_t endianess;
	uint8_t needs_endianess_swap;

	char* peer_address;
// provider
	int service_port;
	int is_listening;
	int is_client_pending;
	ln_service_handler handler;
	void* handler_user_data;
	ln_service_fd_handler fd_handler;
	void* fd_handler_user_data;
	service_group* group;
	DEFINE_LIST_DATA(service_client);
	DEFINE_LIST_DATA_TYPE_NAME(service_client, fake_service_clients);	
	char* unix_socket_name;
	int unix_fd;
	int is_client_pending_unix;
	ln_pipe_t new_provider_fd_notification_pipe;
	int notified_tcp_fd;
	int notified_unix_fd;
	char* service_name_patterns_to_log;
};

DECLARE_LIST_METHODS(service, service_client);
DECLARE_LIST_METHODS_TYPE_NAME(service, service_client, fake_service_clients);

int _ln_service_destroy(service* s);
int _ln_service_client_destroy(service_client* c);
int _ln_fake_service_clients_destroy(service_client* c);
// internally needed:
int _ln_service_connect(service* s, int* did_connect);
int _ln_service_accept_client(service* s);
int _ln_service_client_handle_request(service_client* c);
ssize_t full_recv(client* clnt, int fd, void* buffer_in, ssize_t len);
int _ln_service_call_only(service* s, int* did_connect);
int _ln_service_process_async_service_call_before_select(service* s);
int _ln_async_service_call_destroy(service* c);
void _ln_service_add_client_fd(service_client* c);
void _ln_service_remove_client_fd(service_client* c);
int _ln_handle_service_group_requests(service_group* sg);

int _wait_for_service_requests_top(service_group*, fd_set* read_fds, int max_fd);
int _wait_for_service_requests_bottom(service_group*, fd_set* read_fds);

// utility:
char* _ln_parse_signature(struct _ln_service_field* packet, char* signature);
int _ln_fill_structure(struct _ln_aux_mem_holder* s, struct _ln_service_field* packet, char** host_data, char** buffer, unsigned int* left_in_buffer, unsigned int needs_endianess_swap);
void _ln_read_structure(_ln_iovec_mgr* mgr, struct _ln_service_field* packet, char** host_data, struct _ln_service_field* parent_packet);
void _ln_service_fields_free(struct _ln_service_field* packet);

// service groups
service_group* _ln_add_service_group(client* clnt, const char* group_name);
service_group* find_service_group(client* clnt, const char* group_name);
int _ln_service_group_destroy(service_group*);
DECLARE_LIST_METHODS_TYPE_NAME(service_group, service, provider);
int _ln_service_group_add(const char* group_name, service* s);
int _ln_service_group_remove(service* s);

// delayed socket allocation
int _ln_service_request_tcp_port(service* s);
void _ln_service_add_provider_fd(service* s, int fd);
void _ln_service_remove_provider_fd(service* s, int fd);
int _ln_service_accept_client_tcp(service* s);
int _ln_service_accept_client_unix(service* s);
int _ln_service_request_unix_socket(service* s);

// service logging
DECLARE_LIST_METHODS(service, log_service);
int _ln_service_add_log_service(service* s, log_service* ls);
int _ln_service_del_log_service(service* s, log_service* ls);

typedef enum {
	LOG_SERVICE_REQUEST = 0,
	LOG_SERVICE_RESPONSE
} log_service_item_type_t;
int _ln_log_service_event(service* s,
			  log_service_item_type_t item_type,
			  double completion_time,
			  double request_time,
			  const char* peer_address,
			  uint32_t client_id,
			  uint32_t request_id,
			  struct _ln_iovec_mgr* data);



#endif // LN_SERVICE
