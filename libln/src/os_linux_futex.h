#ifndef OS_LINUX_FUTEX_H
#define OS_LINUX_FUTEX_H

#include <stdint.h>
#include <sys/syscall.h>

#if defined(SYS_futex)
#define HAVE_SYS_FUTEX

void memory_barrier();

long futex_wake_all(uint32_t* uaddr);
long futex_wait(uint32_t* uaddr, uint32_t expected);
long futex_timedwait(uint32_t* uaddr, uint32_t expected, double timeout);

#endif

#endif /* OS_LINUX_FUTEX_H */

