#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "client.h"

typedef void (*adder_t)(client* clnt, char* src, char* dst);

static void __add_topic_mapping(client* clnt, char* src, char* dst)
{
	topic_mapping_t* mapping = (topic_mapping_t*)malloc(sizeof(topic_mapping_t));
	mapping->src = src;
	mapping->dst = dst;
	_ln_client_append_topic_mapping(clnt, mapping);
}

static void __add_service_mapping(client* clnt, char* src, char* dst)
{
	service_mapping_t* mapping = (service_mapping_t*)malloc(sizeof(service_mapping_t));
	mapping->src = src;
	mapping->dst = dst;
	_ln_client_append_service_mapping(clnt, mapping);
}

static void __read_mappings(client* clnt, const char* input, const char* upper, const char* lower, adder_t adder)
{
	if(!input)
		return;
	
	const char* cp;
	while(input && *input && (cp = strchr(input, '|'))) {
		char* src = strndup(input, cp - input);
		input = cp + 1;
		cp = strchr(input, '|');
		if(!cp)
			cp = strchr(input, '\0');
		char* dst = strndup(input, cp - input);
		input = cp + 1;
		
		adder(clnt, src, dst);			
		ln_debug(clnt, "added %s mapping from env: '%s' to '%s'\n", lower, src, dst);
	}
}

void _read_name_mappings(client* clnt, const char* map_topics, const char* map_services)
{
	__read_mappings(clnt, map_topics,   "TOPICS",   "topic",   __add_topic_mapping);
	__read_mappings(clnt, map_services, "SERVICES", "service", __add_service_mapping);
}

static const char* strnstrn(const char* haystack, unsigned int haystack_len, const char* needle, unsigned int needle_len)
{
	if(haystack_len < needle_len)
		return NULL;
	
	for(unsigned int i = 0; i < haystack_len - needle_len + 1; i++) {
		if(memcmp(haystack + i, needle, needle_len) == 0)
			return haystack + i;
	}
	
	return NULL;
}

static void _check_replacement(client* clnt, const char* src, const char* dst, const char* name, const char* what, const char* hint, char** repl)
{
	char* ret;
	int src_len = strlen(src);
	int name_len = strlen(name);
	int dst_len = strlen(dst);
	const char* cp;

	// only overwrite *repl if there is a match!
	
	ln_debug(clnt, "check whether pattern '%s' matches '%s'\n", src, name);

	if(src[0] == '^' && src[src_len - 1] == '$') {
		if(!strncmp(src + 1, name, src_len - 2)) {
			ln_debug(clnt, "%s %s '%s' will be mapped to '%s' instead.\n", hint, what, name, dst);
			if(*repl) free(*repl);
			*repl = strdup(dst);
		}
		return;
	}
	
	if(src[src_len - 1] == '$' && !strncmp(src, name + name_len - src_len + 1, src_len - 1)) {
		
		// someEND with "END$ => end" results in someend
		ret = (char*)calloc(1, name_len - (src_len - 1) + dst_len + 1);
		memcpy(ret, name, name_len - (src_len - 1));
		memcpy(ret + name_len - (src_len - 1), dst, dst_len + 1); // copy trailing \0 from name into *repl!
		ln_debug(clnt, "%s %s '%s' will be $-mapped to '%s' instead.\n", hint, what, name, ret);
		if(*repl) free(*repl);
		*repl = ret;
		
	} else if(name_len >= (src_len - 1) && src[0] == '^' && !strncmp(src + 1, name, src_len - 1)) {
		
		// FINDsome with "^some => end" results in FINDend		
		ret = (char*)calloc(1, dst_len + name_len - (src_len - 1) + 1);
		memcpy(ret, dst, dst_len);
		memcpy(ret + dst_len, name + src_len - 1, name_len - (src_len - 1) + 1);
		ln_debug(clnt, "%s %s '%s' will be ^-mapped to '%s' instead.\n", hint, what, name, ret);
		if(*repl) free(*repl);
		*repl = ret;

	} else if(name_len >= src_len && (cp = strnstrn(name, name_len, src, src_len))) {
		// FINDsomeEND with "some => __" results in FIND__END
		// middle match! OK
		unsigned int middle_offset = cp - name;
		int dst_len = strlen(dst);
		int name_len = strlen(name);
		unsigned int first_part_len = middle_offset;
		unsigned int middle_part_len = dst_len;
		unsigned int end_part_len = name_len - first_part_len - src_len;
		ret = (char*)calloc(1, first_part_len + middle_part_len + end_part_len + 1);
		memcpy(ret, name, first_part_len);
		memcpy(ret + first_part_len, dst, middle_part_len);
		memcpy(ret + first_part_len + middle_part_len, name + first_part_len + src_len, end_part_len);
		ln_debug(clnt, "%s %s '%s' will be middle-mapped to '%s' instead.\n", hint, what, name, ret);
		if(*repl) free(*repl);
		*repl = ret;
	}
}


#define CREATE_ITERATOR(kind)												\
	char* _get_ ## kind ## _mapping(client* clnt, const char* name, const char* hint)				\
	{														\
		char* replacement = NULL;										\
		const char* src = name;											\
		bool had_match = false;											\
		for(unsigned int i = 0; i < clnt->n_ ## kind ## _mappings; i++) {					\
			kind ## _mapping_t* mapping = clnt->kind ##_mappings[i];					\
			_check_replacement(clnt, mapping->src, mapping->dst, src, hint, #kind, &replacement);		\
			if(replacement) {										\
				src = replacement;									\
				had_match = true;									\
			}												\
		}													\
		if(had_match)												\
			return (char*)src; /* now points to new memory */						\
		return strdup(name);											\
	}

CREATE_ITERATOR(topic)
CREATE_ITERATOR(service)
