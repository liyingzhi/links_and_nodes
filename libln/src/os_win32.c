/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef __WIN32__

#include "os.h"

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include "ln/ln.h"


int ln_shared_mem_create(ln_shared_mem_handle_t* data, const char* name, unsigned int size) {
	if(strlen(name) > MAX_SHARED_MEM_LEN)
		return -LNE_SHM_NAME_TOO_LONG;
	strncpy(data->name, name, MAX_SHARED_MEM_LEN);
	data->name[MAX_SHARED_MEM_LEN] = 0;
	data->size = size;

	data->file_mapping = CreateFileMappingA(
		INVALID_HANDLE_VALUE,    // use paging file
		NULL,                    // default security 
		PAGE_READWRITE,          // read/write access
		0,                       // maximum object size (high-order DWORD) 
		data->size + sizeof(uint32_t), // maximum object size (low-order DWORD)  
		data->name);               // name of mapping object
	if(data->file_mapping == NULL)
		return -LNE_CHECK_ERRNO;
	data->real_mem = MapViewOfFile(
		data->file_mapping,   // handle to map object
		FILE_MAP_ALL_ACCESS, // read/write permission
		0,                   // offset high
		0,                   // offset low
		0);
	
	if(data->real_mem == NULL) { 
		fprintf(stderr, "MapViewOfFile() returned error: %ld 0x%lx\n", GetLastError(), GetLastError());
		CloseHandle(data->file_mapping);
		return -LNE_CHECK_ERRNO;
	}

	// win32 has no way to transfer the size of a mapped object -> so we need to explicitely remember it!
	uint32_t* shm_size = (uint32_t*)data->real_mem;
	*shm_size = data->size;
	data->mem = shm_size + 1;

	return 0;
}

int ln_shared_mem_open(ln_shared_mem_handle_t* data, const char* name) {
	if(strlen(name) > MAX_SHARED_MEM_LEN)
		return -LNE_SHM_NAME_TOO_LONG;
	strncpy(data->name, name, MAX_SHARED_MEM_LEN);

	data->file_mapping = OpenFileMappingA(
		FILE_MAP_ALL_ACCESS,
		FALSE,
		data->name);               // name of mapping object
	if(data->file_mapping == NULL)
		return -LNE_CHECK_ERRNO;

	data->real_mem = MapViewOfFile(
		data->file_mapping,   // handle to map object
		FILE_MAP_ALL_ACCESS, // read/write permission
		0,                   // offset high
		0,                   // offset low
		0);
	
	if(data->real_mem == NULL) { 
		fprintf(stderr, "MapViewOfFile() returned error: %ld 0x%lx\n", GetLastError(), GetLastError());
		CloseHandle(data->file_mapping);
		return -LNE_CHECK_ERRNO;
	}
	
	uint32_t* shm_size = (uint32_t*)data->real_mem;
	data->size = *shm_size;
	data->mem = shm_size + 1;

	return 0;
}

int ln_shared_mem_close(ln_shared_mem_handle_t* data, int destroy) {
	if(data->real_mem)
		UnmapViewOfFile(data->real_mem);
	if(data->file_mapping)
		CloseHandle(data->file_mapping);
	return 0;
}

int ln_shared_mem_resize(ln_shared_mem_handle_t* data, unsigned int new_size) {
	if(new_size == 1) {
		printf("ln_shared_mem_resize() not supported on win32!\n");
		return -1;
	}

	// unmap
	UnmapViewOfFile(data->real_mem);
	CloseHandle(data->file_mapping);

	// open with current size of shm!
	data->file_mapping = OpenFileMappingA(
		PAGE_READWRITE,
		FALSE,
		data->name);               // name of mapping object
	if(data->file_mapping == NULL)
		return -LNE_CHECK_ERRNO;
	
	data->real_mem = MapViewOfFile(
		data->file_mapping,   // handle to map object
		FILE_MAP_ALL_ACCESS, // read/write permission
		0,                   // offset high
		0,                   // offset low
		0);
	
	uint32_t* shm_size = (uint32_t*)data->real_mem;
	data->size = *shm_size;
	data->mem = shm_size + 1;

	return 0;
}

// named semaphores:
int ln_semaphore_create(ln_semaphore_handle_t* handle, const char* name, int create) {
	char* sn = NULL;
	char sem_name[1024];
	if(name) {
		snprintf(sem_name, 1024, "%s_sem", name);
		sn = sem_name;
	}
		
	if(create) {
		*handle = CreateSemaphoreA(
			NULL, // __in_opt  LPSECURITY_ATTRIBUTES lpSemaphoreAttributes,
			0,    // __in      LONG lInitialCount,
			1,    // __in      LONG lMaximumCount,
			sn
			);
	} else {
		*handle = OpenSemaphoreA(
			SEMAPHORE_ALL_ACCESS, // __in  DWORD dwDesiredAccess,
			0,                    // __in  BOOL bInheritHandle,
			sn              //__in  LPCTSTR lpName
			);
	}
	if(!*handle)
		return -1;
	return 0;
}
int ln_semaphore_close(ln_semaphore_handle_t handle) {
	CloseHandle(handle);
	return 0;
}
int ln_semaphore_unlink(const char* name) {
	// not needed
	return 0;
}
// unnamed semaphores:
int ln_semaphore_init(ln_semaphore_handle_t* handle, unsigned int value) {
	*handle = CreateSemaphore(
		NULL, // __in_opt  LPSECURITY_ATTRIBUTES lpSemaphoreAttributes,
		0,    // __in      LONG lInitialCount,
		1,    // __in      LONG lMaximumCount,
		NULL // name
		);
	if(!*handle)
		return -1;
	return 0;
}
int ln_semaphore_destroy(ln_semaphore_handle_t sem) {
	CloseHandle(sem);
	return 0;
}

// common semaphores:
int ln_semaphore_wait(ln_semaphore_handle_t handle) {
	DWORD ret = WaitForSingleObject(handle, INFINITE);
	if(ret == WAIT_FAILED)
		return -1;
	return 0; // success
}
int ln_semaphore_trywait(ln_semaphore_handle_t handle) {
	DWORD ret = WaitForSingleObject(handle, 0);
	if(ret == WAIT_OBJECT_0)
		return 0; // success
	return -1;

}
int ln_semaphore_timedwait(ln_semaphore_handle_t handle, double seconds) {
	DWORD ret = WaitForSingleObject(handle, (DWORD)(seconds * 1e3));
	if(ret == WAIT_OBJECT_0)
		return 0; // success
	return -1;
}
int ln_semaphore_timedwait_abs(ln_semaphore_handle_t handle, double abs_timeout) {
	return ln_semaphore_timedwait(handle, abs_timeout - ln_get_time());
}
int ln_semaphore_post(ln_semaphore_handle_t handle) {
	ReleaseSemaphore(
		handle, // __in       HANDLE hSemaphore,
		1,      // __in       LONG lReleaseCount,
		NULL    // __out_opt  LPLONG lpPreviousCount
		);
	return 0;
}
int ln_semaphore_getvalue(ln_semaphore_handle_t handle) {
	// no idea...but we are an binary semaphore anyway...

	return 0;
}

int ln_mutex_create(ln_mutex_handle_t* mutex_handle) {
	CRITICAL_SECTION* s = (CRITICAL_SECTION*)malloc(sizeof(CRITICAL_SECTION));
	InitializeCriticalSection(s);
	*mutex_handle = s;
	return 0;
}
int ln_mutex_destroy(ln_mutex_handle_t* mutex_handle) {
	DeleteCriticalSection(*mutex_handle);
	free(*mutex_handle);
	return 0;
}
int ln_mutex_lock(ln_mutex_handle_t* mutex_handle) {
	EnterCriticalSection(*mutex_handle);
	return 0;
}
int ln_mutex_trylock(ln_mutex_handle_t* mutex_handle) {
	BOOL ret = TryEnterCriticalSection(*mutex_handle);
	// return 0 if lock acquired!
	if(ret)
		return 0;
	// return -1 if lock is not acquired!
	return -1;
}
int ln_mutex_unlock(ln_mutex_handle_t* mutex_handle) {
	LeaveCriticalSection(*mutex_handle);
	return 0;
}


int ln_thread_create(ln_thread_handle_t* thread_handle, thread_func_type thread, void* data) {
	return ln_thread_create_with_prio(thread_handle, thread, data, -1, -1, NULL);
}

int ln_thread_create_with_prio(ln_thread_handle_t* thread_handle, thread_func_type thread, void* data, int policy, int priority, const char* name) {
	return ln_thread_create_with_params(thread_handle, thread, data, policy, priority, 0, name);
}

int ln_thread_create_with_params(ln_thread_handle_t* thread_handle, thread_func_type* thread, void* data, int policy, int priority, unsigned int affinity_mask, const char* name) {
	HANDLE ret = CreateThread(
		NULL,
		0,      // __in       SIZE_T dwStackSize,
		thread, // __in       LPTHREAD_START_ROUTINE lpStartAddress,
		data,   // __in_opt   LPVOID lpParameter,
		0,      // __in       DWORD dwCreationFlags,
		NULL    // __out_opt  LPDWORD lpThreadId
		);

	if(!ret)
		return -1;

	// todo: set policy, prio, affinity and thread name
	
	// if successfull:
	struct win32_thread_handle_and_cancel_flag* r = (struct win32_thread_handle_and_cancel_flag*)calloc(1, sizeof(struct win32_thread_handle_and_cancel_flag));
	r->thread = ret;
	
	*thread_handle = r;
	return 0;
}

int ln_thread_setschedparam(ln_thread_handle_t thread_handle, int policy, int priority, unsigned int affinity_mask) {
	// todo: set policy, prio and affinity of thread
	return 0;
}

int ln_thread_test_cancel(ln_thread_handle_t handle) {
	if(handle->cancel_requested) {
		ExitThread(0);
	}
	return 0;
}

void ln_thread_cancel(ln_thread_handle_t handle) {
	handle->cancel_requested = 1;
	TerminateThread(handle->thread, 0);
}

int ln_thread_join(ln_thread_handle_t t, void** retval) {
	DWORD ret = WaitForSingleObject(
		t->thread, // __in  HANDLE hHandle,
		INFINITE          // __in  DWORD dwMilliseconds
		);
	if(ret == WAIT_FAILED)
		return -1;
	return 0; // success
}

char *strndup(const char *s, size_t n) {
	const char* cp = s;
	size_t i = 0;
	while(*cp) {
		i++;
		if(i >= n)
			break; // enough chars
		cp++;
	}
	i ++;
	char* result = (char*)malloc(i);
	memcpy(result, s, i);
	result[i - 1] = 0;
	return result;
}

void ln_sleep_seconds(double s) {
	Sleep((DWORD)(s * 1e3));
}

double ln_get_time() {
	SYSTEMTIME t;
	GetSystemTime(&t);
	time_t time_offset = time(NULL);
	return (double)time_offset + (double)t.wMilliseconds / 1e3;
}
double ln_get_time_sys() {
	return ln_get_time();
}

double ln_get_monotonic_time() {
	// the unit of GetTickCounts is milliseconds
	// (since system was started)
	return GetTickCount64() * 0.001;
}

void thread_cleanup_push(void (*fn)(void*), void* arg) {

}

void thread_cleanup_pop(int exec) {

}

int inet_aton(const char *cp, struct in_addr *inp) {
	inp->s_addr = inet_addr(cp);
	if(inp->s_addr == INADDR_NONE)
		return 0; // invalid
	return 1; // valid
}

#ifndef MSVC
const char* _ln_winxp_inet_ntop(int family, void* addr, char* out, size_t max_out) {
	if(max_out < 16)
		return NULL;
	strcpy(out, inet_ntoa(*(struct in_addr*)addr));
	return out;
}
#endif

ssize_t sendmsg(int socket, const struct msghdr *msg, int flags) {
	/*
	  this WSASendMsg() is only avaliable from >= Vista -- we want >= WinXP.
	  and it only works for datagram or raw sockets!
	  http://msdn.microsoft.com/en-us/library/windows/desktop/ms741692%28v=vs.85%29.aspx

	DWORD NumberOfBytesSent;
	return WSASendMsg(
		socket,
		msg,
		flags,
		&NumberOfBytesSent,
		NULL,
		NULL);
	*/

	if(flags & __WIN32__DGRAM_SEND) {
		// we are only allowed to do a single send() call, so we need to collect the message into a big continuous buffer!
		unsigned int needed_size = 0;
		for(unsigned int i = 0; i < msg->msg_iovlen; i++)
			needed_size += msg->msg_iov[i].iov_len;
		static char* send_buffer = NULL;
		static unsigned int send_buffer_len = 0;
		if(!send_buffer) {
			send_buffer_len = needed_size * 2;
			send_buffer = (char*)malloc(send_buffer_len);
		} else if(needed_size > send_buffer_len) {
			send_buffer_len = needed_size;
			void* new_mem = realloc(send_buffer, send_buffer_len);
			if(!new_mem)
			  return -1;
			send_buffer = (char*)new_mem;
		}
		// now collect data
		char* dp = send_buffer;
		for(unsigned int i = 0; i < msg->msg_iovlen; i++) {
			memcpy(dp, msg->msg_iov[i].iov_base, msg->msg_iov[i].iov_len);
			dp += msg->msg_iov[i].iov_len;
		}
		unsigned int n = send(socket, send_buffer, needed_size, 0);
		if(n < 0) {
			// error
			return -1;
		}
		return n;
	}

	// simulate behaviour
	unsigned int sent = 0;
	for(unsigned int i = 0; i < msg->msg_iovlen; i++) {
		unsigned int already_sent = 0;
		while(already_sent < msg->msg_iov[i].iov_len) {
			unsigned int n = send(socket, msg->msg_iov[i].iov_base + already_sent, msg->msg_iov[i].iov_len - already_sent, 0);
			if(n < 0) {
				// error
				return -1;
			}
			already_sent += n;
		}
		sent += already_sent;
	}
	return sent;
}

int ln_pipe_init(ln_pipe_t* p) {
	BOOL ret = CreatePipe(&p->input_fd, &p->output_fd, NULL, 1);
	if(!ret)
		return -LNE_CHECK_ERRNO;
	return 0;
}

int ln_pipe_close(ln_pipe_t* pipe) {
	CloseHandle(pipe->input_fd);
	if (pipe->output_fd != ln_pipe_invalid)
		CloseHandle(pipe->output_fd);
	return 0;
}

void ln_pipe_send_notification(ln_pipe_t pipe) {
	DWORD n = 0;
	WriteFile(pipe.output_fd, "\n", 1, &n, NULL);
}

void ln_pipe_read_notification(ln_pipe_t pipe) {
	char n = '\n';
	DWORD nbytes = 0;
	ReadFile(pipe.input_fd, &n, 1, &nbytes, NULL);
}

// F*cking win32 has no condition variables!

struct win32_condition {
	ln_mutex_handle_t mutex;
	unsigned int n_blocked_threads;
	ln_semaphore_handle_t sem;
	unsigned int in_destruct;
};

int ln_cond_create(ln_cond_handle_t* cond_) {
	if(!cond_)
		return -1;

	*cond_ = calloc(1, sizeof(struct win32_condition));
	if(!*cond_)
		return -2;

	struct win32_condition* cond = (struct win32_condition*)*cond_;

	int ret;
	
	ret = ln_mutex_create(&cond->mutex);
	if(ret != 0)
		return ret;

	ret = ln_semaphore_init(&cond->sem, 0);
	if(ret != 0)
		return ret;
	
	return 0;
}
int ln_cond_destroy(ln_cond_handle_t* cond_) {
	if(!cond_)
		return -1;
	struct win32_condition* cond = (struct win32_condition*)*cond_;

	cond->in_destruct = 1;
	ln_cond_broadcast(cond_);
	
	ln_semaphore_destroy(&cond->sem);
	ln_mutex_destroy(&cond->mutex);

	free(cond);
	return 0;
}
int ln_cond_wait(ln_cond_handle_t* cond_, ln_mutex_handle_t* mutex) {
	if(!cond_)
		return -1;
	struct win32_condition* cond = (struct win32_condition*)*cond_;
	if(cond->in_destruct)
		return -2;
	
	ln_mutex_lock(&cond->mutex);
	cond->n_blocked_threads ++;
	ln_mutex_unlock(&cond->mutex);

	ln_mutex_unlock(mutex);

	ln_semaphore_wait(cond->sem);

	ln_mutex_lock(mutex);
	
	return 0;
}
int ln_cond_timedwait(ln_cond_handle_t* cond_, ln_mutex_handle_t* mutex, double abs_timeout) {
	if(!cond_)
		return -1;
	struct win32_condition* cond = (struct win32_condition*)*cond_;
	if(cond->in_destruct)
		return -2;

	ln_mutex_lock(&cond->mutex);
	cond->n_blocked_threads ++;
	ln_mutex_unlock(&cond->mutex);

	ln_mutex_unlock(mutex);
	
	int ret = ln_semaphore_timedwait(cond->sem, abs_timeout - ln_get_monotonic_time());

	ln_mutex_lock(mutex);
	
	return ret;
}

int ln_cond_broadcast(ln_cond_handle_t* cond_) {
	if(!cond_)
		return -1;
	struct win32_condition* cond = (struct win32_condition*)*cond_;
	
	ln_mutex_lock(&cond->mutex);
	while(cond->n_blocked_threads) {
		ln_semaphore_post(cond->sem);
		cond->n_blocked_threads --;		
	}
	ln_mutex_unlock(&cond->mutex);
	return 0;
}

int ln_cond_signal(ln_cond_handle_t* cond_) {
	if(!cond_)
		return -1;
	struct win32_condition* cond = (struct win32_condition*)*cond_;
	
	ln_mutex_lock(&cond->mutex);
	if(cond->n_blocked_threads) {
		ln_semaphore_post(cond->sem);
		cond->n_blocked_threads --;		
	}
	ln_mutex_unlock(&cond->mutex);
	return 0;
}

void ln_thread_setname(const char* new_name) {
	// not on win32?
}

void _ln_close_socket(_ln_socket_t fd) {
	closesocket(fd);
}

#ifdef _MSC_VER

int c99_vsnprintf(char* str, size_t size, const char* format, va_list ap)
{
	int count = -1;

	if (size != 0)
		count = _vsnprintf_s(str, size, _TRUNCATE, format, ap);
	if (count == -1)
		count = _vscprintf(format, ap);

	return count;
}

int c99_snprintf(char* str, size_t size, const char* format, ...)
{
	int count;
	va_list ap;

	va_start(ap, format);
	count = c99_vsnprintf(str, size, format, ap);
	va_end(ap);

	return count;
}
#else
char *strdup(const char *s) {
	return strndup(s, strlen(s));
}

#endif

#endif // __WIN32__

