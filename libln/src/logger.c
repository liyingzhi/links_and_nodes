/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logger.h"

#include "request.h"
#include "repr.h"

int ln_client_get_logger(ln_client clnt_, ln_logger* logger_, const char* logger_name) {
	client* clnt = (client*)clnt_;
	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	// search for existing logger
	for(unsigned int i = 0; i < clnt->n_loggers; i++) {
		struct logger_* l = clnt->loggers[i];
		if(!strcmp(l->name, logger_name)) {
			*logger_ = l;
			return 0;
		}
	}

	// create new logger:
	logger* l = (logger*)calloc(1, sizeof(logger));
	l->clnt = clnt;
	l->name = strdup(logger_name);
	l->only_ts = 0;
	
	lock_client(clnt);
	_ln_client_append_logger(clnt, l);
	unlock_client(clnt);
	
	*logger_ = l;
	return 0;
}

int _ln_logger_destroy(logger* l) {
	DESTRUCT_LIST(l, logger_topic);
	free(l->name);
	free(l);

	return 0;
}

DEFINE_LIST_METHODS(logger, logger_topic);

int ln_logger_add_topic(ln_logger logger_, const char* topic_name, unsigned int log_size, unsigned int divisor) {
	logger* l = (logger*)logger_;

	// does this topic already exist0
	logger_topic* t = NULL;
	for(unsigned int i = 0; i < l->n_logger_topics; i++) {
		logger_topic* tt = l->logger_topics[i];
		if(!strcmp(tt->name, topic_name)) {
		  t = tt;
		  break;
		}
	}
	
	if(t == NULL) {
		// spawn new client!
		t = (logger_topic*)calloc(1, sizeof(logger_topic));
		t->name = strdup(topic_name);
		_ln_logger_append_logger_topic(l, t);
	}
	t->log_size = log_size;
	t->divisor = divisor;

	return 0;
}
int ln_logger_clear_topics(ln_logger logger_) {
	logger* l = (logger*)logger_;
	
	DESTRUCT_LIST(l, logger_topic);

	return 0;
}

int ln_logger_set_only_ts(ln_logger logger_, int only_ts) {
	logger* l = (logger*)logger_;
	l->only_ts = only_ts;
	return 0;
}

int _ln_logger_topic_destroy(logger_topic* t) {
	free(t->name);
	free(t->md_name);
	free(t->md);
	free(t->samples);
	free(t);
	return 0;
}

#define GB_ADD(name, value) growing_buffer_append(&name, &name ## _cp, &name ## _size, value)

int ln_logger_enable(ln_logger logger_) {
	logger* l = (logger*)logger_;

	if(!l->n_logger_topics)
		return -LNE_NO_LOGGER_TOPICS;

	char* topics = NULL;
	unsigned int topics_size = 0;
	char* topics_cp = topics;

	char* repr_value = NULL;
	unsigned int repr_value_size = 0;

	GB_ADD(topics, "[");
	for(unsigned int i = 0; i < l->n_logger_topics; i++) {
		struct logger_topic_* t = l->logger_topics[i];
		if(i > 0) {
			GB_ADD(topics, ", (");
		} else {
			GB_ADD(topics, "(");
		}
		strrepr_(&repr_value, &repr_value_size, t->name);
		GB_ADD(topics, repr_value);
		if(l->clnt->manager_library_version < 8) {
			if(t->divisor != 1)
				ln_debug(l->clnt, "warning: manager version %d is too old to accept log-divisor != 1 for topic %s",
				      l->clnt->manager_library_version, t->name);
			strformat_(&repr_value, &repr_value_size, ", %d)", t->log_size);
		} else {
			// with divisor
			strformat_(&repr_value, &repr_value_size, ", %d, %d)", t->log_size, t->divisor);
		}
		GB_ADD(topics, repr_value);
	}
	GB_ADD(topics, "]");
	*topics_cp = 0;

	request_t r;
	int ret = _ln_init_request(
		l->clnt, &r,
		"method", "r", "logger_enable",
		"name", "r", l->name,
		"topics", "O", topics,
		"only_ts", "d", l->only_ts,
		NULL);

	free(repr_value);
	free(topics);

	if(ret)
		return ret;

	ln_mutex_lock(&l->clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&l->clnt->communication_mutex);

	_ln_destroy_request(&r);

	return ret;
}

int ln_logger_disable(ln_logger logger_) {
	logger* l = (logger*)logger_;

	request_t r;
	int ret = _ln_init_request(
		l->clnt, &r,
		"method", "r", "logger_disable",
		"name", "r", l->name,
		NULL);

	if(ret)
		return ret;

	ln_mutex_lock(&l->clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&l->clnt->communication_mutex);

	_ln_destroy_request(&r);

	return ret;
}

int ln_logger_download(ln_logger logger_, ln_logger_data* data_) {
	logger* l = (logger*)logger_;

	request_t r;
	int ret = _ln_init_request(
		l->clnt, &r,
		"method", "r", "logger_download",
		"name", "r", l->name,
		NULL);

	if(ret)
		return ret;

	ln_mutex_lock(&l->clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&l->clnt->communication_mutex);

	if(ret) {
		_ln_destroy_request(&r);
		return ret;
	}

	// get data from response

	/* expected response format:
	   topic_name_0: 
	   n_samples_0:
	   sample_size_0:
	   md_name_0:
	   md_0:
	   data_0: binary_data,
	   ...
	 */
	
	// for each defined topic extract data from response
	for(unsigned int i = 0; i < l->n_logger_topics; i++) {
		logger_topic* t = l->logger_topics[i];
		// now search index in response!
		unsigned int idx;
		char field_name[128];
		char field_value[1024];
		int found = 0;
		for(idx = 0; idx < l->n_logger_topics; idx++) {
			snprintf(field_name, 128, "topic_name_%d", idx);
			ret = _ln_get_from_request(&r, field_name, "s1024", field_value);
			if(ret == 0 && !strcmp(t->name, field_value)) {
				found = 1;
				break;
			}
		}
		if(!found) {
			_ln_set_error(l->clnt, "missing topic '%s' in logger_download response!\n", t->name);
			return -LNE_FIELD_NOT_IN_HEADER;
		}
		// free already existing data
		if(t->md_name) {
			free(t->md_name);
			free(t->md);
			free(t->samples);
		}

		snprintf(field_name, 128, "n_samples_%d", idx);
		ret = _ln_get_from_request(&r, field_name, "u", &t->n_samples);
		if(ret)
			return ret;
		snprintf(field_name, 128, "sample_size_%d", idx);
		ret = _ln_get_from_request(&r, field_name, "u", &t->sample_size);
		if(ret)
			return ret;
		snprintf(field_name, 128, "md_name_%d", idx);
		ret = _ln_get_from_request(&r, field_name, "s1024", field_value);
		if(ret)
			return ret;
		t->md_name = strdup(field_value);
		
		request_line_t* field;
		snprintf(field_name, 128, "md_%d", idx);
		ret = _ln_get_from_request_raw_named(&r, field_name, &field);
		if(ret)
			return ret;

		unsigned int max_len = strlen(field->value);
		t->md = (char*)malloc(max_len + 1);
		evalstr(t->md, max_len, field->value);
		
		snprintf(field_name, 128, "data_%d", idx);
		ret = _ln_get_from_request_raw_named(&r, field_name, &field);
		if(ret)
			return ret;
		// overtake binary data from request_line!
		if(field->binary_data_len != (t->n_samples * t->sample_size)) {
			_ln_set_error(l->clnt, "internal error. topic '%s' n_samples: %d, sample_size: %d, field->binary_data_len: %d != %d!\n", 
				      t->name,
				      t->n_samples,
				      t->sample_size,
				      field->binary_data_len,
				      t->n_samples * t->sample_size
				);
			return -LNE_INTERNAL_ERROR;
		}
		t->samples = (logger_sample*)field->value;
		// take ownership
		field->value = NULL;
	}

	_ln_destroy_request(&r);

	// stand alone data repr for logger data but belonging to this logger!
	l->own_data.topics = l->logger_topics;
	l->own_data.n_topics = l->n_logger_topics;
	*data_ = &l->own_data;

	return 0;
}

int ln_logger_manager_save(ln_logger logger_, const char* filename, const char* format) {
	logger* l = (logger*)logger_;

	request_t r;
	int ret = _ln_init_request(
		l->clnt, &r,
		"method", "r", "logger_manager_save",
		"name", "r", l->name,
		"filename", "r", filename,
		"format", "r", format,
		NULL);

	if(ret)
		return ret;

	ln_mutex_lock(&l->clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&l->clnt->communication_mutex);

	_ln_destroy_request(&r);

	return ret;
}


int ln_logger_data_get_info(ln_logger_data logger_data_, unsigned int* n_topics) {
	logger_data* data = (logger_data*)logger_data_;
	*n_topics = data->n_topics;
	return 0;
}

int ln_logger_data_get_topic(ln_logger_data logger_data_, unsigned int topic_id, logger_topic** topic) {
	logger_data* data = (logger_data*)logger_data_;
	if(topic_id >= data->n_topics)
		return -LNE_TOPIC_ID_OUT_OF_RANGE;
	*topic = data->topics[topic_id];
	return 0;
}

static int _write_string(FILE* fp, const char* str) {
	uint32_t str_len = strlen(str);
	int ret = fwrite(&str_len, sizeof(uint32_t), 1, fp);
	if(ret != 1)
		return ret;
	ret = fwrite(str, str_len, 1, fp);
	if(ret != 1)
		return ret;
	return 0;
}

static int _read_string(FILE* fp, char** str) {
	uint32_t str_len;
	int ret = fread(&str_len, sizeof(uint32_t), 1, fp);
	if(ret != 1)
		return ret;
	*str = (char*)malloc(str_len + 1);
	ret = fread(*str, str_len, 1, fp);
	if(ret != 1)
		return ret;
	(*str)[str_len] = 0; // terminate with \0 for lazy users
	return 0;
}

int ln_logger_data_save(ln_logger_data logger_data_, const char* filename) {
	logger_data* data = (logger_data*)logger_data_;
	
	FILE* fp = fopen(filename, "wb");
	if(!fp)
		return -LNE_CHECK_ERRNO;


	uint16_t byte_order = 0x0420;
	fwrite(&byte_order, sizeof(uint16_t), 1, fp);

	uint32_t file_version = 2;
	fwrite(&file_version, sizeof(uint32_t), 1, fp);

	_write_string(fp, "\r# this is a links & nodes binary log file. read with ln::logger_data::read()\n");

	fwrite(&data->n_topics, sizeof(uint32_t), 1, fp);
	for(unsigned int i = 0; i < data->n_topics; i++) {
		logger_topic* t = data->topics[i];
		_write_string(fp, t->name);
		fwrite(&t->log_size, sizeof(uint32_t), 1, fp);
		fwrite(&t->divisor, sizeof(uint32_t), 1, fp);
		fwrite(&t->n_samples, sizeof(uint32_t), 1, fp);
		fwrite(&t->sample_size, sizeof(uint32_t), 1, fp);
		_write_string(fp, t->md_name);
		_write_string(fp, t->md);
		fwrite(t->samples, t->n_samples * t->sample_size, 1, fp);
	}

	fclose(fp);
	return 0;
}

int ln_logger_data_load(ln_logger_data* logger_data_, const char* filename) {
	FILE* fp = fopen(filename, "rb");
	if(!fp)
		return -LNE_CHECK_ERRNO;

	uint16_t byte_order;
	fread(&byte_order, sizeof(uint16_t), 1, fp);
	if(byte_order != 0x0420) {
		fprintf(stderr, "this log file uses a different byte order! have 0x%04x, expected 0x%04x!\n", 
			byte_order, 0x0420);
		fclose(fp);
		return -LNE_WRONG_BYTE_ORDER;
	}

	uint32_t file_version = 0;
	fread(&file_version, sizeof(uint32_t), 1, fp);

	if(file_version > 2) {
		fprintf(stderr, "this log file uses version %d which is not supported by this library. please update!\n", 
			file_version);
		fclose(fp);
		return -LNE_LOG_FILE_TOO_NEW;
	}

	char* comment = NULL;
	_read_string(fp, &comment);
	// printf("file comment:\n%s\n", comment);
	free(comment);

	logger_data* data = (logger_data*)calloc(1, sizeof(logger_data));
	//data->own_by_user = 1;
	fread(&data->n_topics, sizeof(uint32_t), 1, fp);
	data->topics = (logger_topic**)calloc(1, sizeof(logger_topic*) * data->n_topics);
	for(unsigned int i = 0; i < data->n_topics; i++) {
		logger_topic* t = (logger_topic*)calloc(1, sizeof(logger_topic));;
		_read_string(fp, &t->name);
		fread(&t->log_size, sizeof(uint32_t), 1, fp);
		if(file_version >= 2)
			fread(&t->divisor, sizeof(uint32_t), 1, fp);
		else
			t->divisor = 1;
		fread(&t->n_samples, sizeof(uint32_t), 1, fp);
		fread(&t->sample_size, sizeof(uint32_t), 1, fp);
		_read_string(fp, &t->md_name);
		_read_string(fp, &t->md);
		t->samples = (logger_sample*)malloc(t->n_samples * t->sample_size);
		fread(t->samples, t->n_samples * t->sample_size, 1, fp);
		data->topics[i] = t;
	}

	fclose(fp);

	*logger_data_ = data;
	return 0;
}
int ln_logger_data_deinit(ln_logger_data* logger_data_) {
	logger_data* data = *(logger_data**)logger_data_;
	// if(!data->own_by_user)
	// 	return 0;
	for(unsigned int i = 0; i < data->n_topics; i++) {
		logger_topic* t = data->topics[i];
		_ln_logger_topic_destroy(t);
	}
	free(data->topics);
	free(data);
	*logger_data_ = NULL;
	return 0;
}

static int recv_all(int fd, void* data_, unsigned int len) {
	unsigned int received = 0;
	uint8_t* data = (uint8_t*)data_;
	
	while(len > 0) {
		int ret;
		UNTIL_RET_NOT_EINTR(ret, recv(fd, data, len, 0));
		if(ret == -1 || ret == 0)
			return ret;
		len -= ret;
		data += ret;
		received += ret;
	}
	return received;
}

static int _ln_log_direct_download(logger* l, const char* ip, unsigned int port, const char* token, logger_sample** _samples, unsigned int* _downloaded_size)
{
	uint8_t* data = NULL;
	uint32_t downloaded_size = 0;

	// tcp connect to ip/port
	int fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(fd == -1) {
		ln_debug(l->clnt, "failed to create log-connection socket\n");
		return -LNE_CHECK_ERRNO;
	}

	struct sockaddr_in daemon_address;
	memset(&daemon_address, 0, sizeof(daemon_address));
	daemon_address.sin_port = htons(port);

	int ret;
	if((ret = resolve_hostname(ip, &daemon_address))) {
		ln_debug(l->clnt, "failed to resolve ip '%s' for log-connection to daemon\n", ip);
		return -LNE_CHECK_ERRNO;
	}

	if((ret = connect(fd, (struct sockaddr*)&daemon_address, sizeof(daemon_address)))) {
		ln_debug(l->clnt, "failed to connect to daemon at %s:%d for direct log download!\n", ip, port);
		_ln_close_socket(fd);
		return -LNE_CHECK_ERRNO;
	}

	// send token
	UNTIL_RET_NOT_EINTR(ret, send(fd, token, strlen(token), 0));
	if(ret != (signed)strlen(token)) {
		_ln_set_error(l->clnt, "failed to send token of length %d '%s': %d!", strlen(token), token, ret);
		_ln_close_socket(fd);
		return -LNE_SVC_RECV_RESP;
	}
	
	// recv uint32_t downloaded_size
	ret = recv_all(fd, &downloaded_size, sizeof(downloaded_size)); // todo: byte ordering information!
	if(ret == -1) {
		_ln_set_error(l->clnt, "failed to receive log-data-size: %d", ret);
		_ln_close_socket(fd);
		return -LNE_SVC_RECV_RESP;
	}
		
	// allocate downloaded_size bytes for data
	data = (uint8_t*)malloc(downloaded_size);
	if(!data) {
		_ln_set_error(l->clnt, "failed to allocate log-data-size of %d bytes!", downloaded_size);
		_ln_close_socket(fd);
		return -LNE_NO_MEM;
	}

	// recv downloaded_size bytes
	ret = recv_all(fd, data, downloaded_size);
	if(ret != (signed)downloaded_size) { // okay, only 31bit
		_ln_set_error(l->clnt, "failed to receive %d bytes of log-data: %d!", downloaded_size, ret);
		_ln_close_socket(fd);
		return -LNE_SVC_RECV_RESP;
	}

	// close connection
	_ln_close_socket(fd);

	*_samples = (logger_sample*)data;
	*_downloaded_size = downloaded_size;
	return 0;
}

int ln_logger_direct_download(ln_logger logger_, ln_logger_data* data_) {
	logger* l = (logger*)logger_;

	request_t r;
	int ret = _ln_init_request(
		l->clnt, &r,
		"method", "r", "logger_direct_download",
		"name", "r", l->name,
		NULL);

	if(ret)
		return ret;

	ln_mutex_lock(&l->clnt->communication_mutex);
	ret = _ln_request_and_wait(&r);
	ln_mutex_unlock(&l->clnt->communication_mutex);

	if(ret) {
		_ln_destroy_request(&r);
		return ret;
	}

	// get data from response

	/* expected response format:
	   topic_name_0: 
	   n_samples_0:
	   sample_size_0:
	   md_name_0:
	   md_0:
	   
	   ip_port_0:
	   token_0:
	   
	   ...
	 */
	
	// for each defined topic:
	//   extract connection info from response
	//   connect daemon,
	//   download data
	//   disconnect daemon
	for(unsigned int i = 0; i < l->n_logger_topics; i++) {
		logger_topic* t = l->logger_topics[i];
		// now search index in response!
		unsigned int idx;
		char field_name[128];
		char field_value[1024];
		int found = 0;
		for(idx = 0; idx < l->n_logger_topics; idx++) {
			snprintf(field_name, 128, "topic_name_%d", idx);
			ret = _ln_get_from_request(&r, field_name, "s1024", field_value);
			if(ret == 0 && !strcmp(t->name, field_value)) {
				found = 1;
				break;
			}
		}
		if(!found) {
			_ln_set_error(l->clnt, "missing topic '%s' in logger_download response!\n", t->name);
			return -LNE_FIELD_NOT_IN_HEADER;
		}
		// free already existing data
		if(t->md_name) {
			free(t->md_name);
			free(t->md);
			free(t->samples);
		}

		snprintf(field_name, 128, "n_samples_%d", idx);
		ret = _ln_get_from_request(&r, field_name, "u", &t->n_samples);
		if(ret)
			return ret;
		snprintf(field_name, 128, "sample_size_%d", idx);
		ret = _ln_get_from_request(&r, field_name, "u", &t->sample_size);
		if(ret)
			return ret;
		snprintf(field_name, 128, "md_name_%d", idx);
		ret = _ln_get_from_request(&r, field_name, "s1024", field_value);
		if(ret)
			return ret;
		t->md_name = strdup(field_value);
		
		request_line_t* field;
		snprintf(field_name, 128, "md_%d", idx);
		ret = _ln_get_from_request_raw_named(&r, field_name, &field);
		if(ret)
			return ret;

		unsigned int max_len = strlen(field->value);
		t->md = (char*)malloc(max_len + 1);
		evalstr(t->md, max_len, field->value);


		char token[128];
		snprintf(field_name, 128, "token_%d", idx);
		ret = _ln_get_from_request(&r, field_name, "s128", token);
		if(ret)
			return ret;
		
		snprintf(field_name, 128, "ip_port_%d", idx);
		ret = _ln_get_from_request(&r, field_name, "s1024", field_value);
		if(ret)
			return ret;
		const char* ip = strtok(field_value, ":");
		unsigned int port = atoi(strtok(NULL, ":"));

		unsigned int downloaded_size = 0;
		ret = _ln_log_direct_download(l, ip, port, token, &t->samples, &downloaded_size);
		if(ret)
			return ret;
		
		// overtake binary data from request_line!
		if(downloaded_size != (t->n_samples * t->sample_size)) {
			_ln_set_error(l->clnt, "internal error. topic '%s' n_samples: %d, sample_size: %d, downloaded_size: %d != %d!\n", 
				      t->name,
				      t->n_samples,
				      t->sample_size,
				      downloaded_size,
				      t->n_samples * t->sample_size
				);
			return -LNE_INTERNAL_ERROR;
		}
	}
	_ln_destroy_request(&r);

	// stand alone data repr for logger data but belonging to this logger!
	l->own_data.topics = l->logger_topics;
	l->own_data.n_topics = l->n_logger_topics;
	*data_ = &l->own_data;

	return 0;
}
