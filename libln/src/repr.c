/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

#include "ln/ln.h"
#include "repr.h"

char* strrepr(char* target, unsigned int max_len, const char* source) {
	return strnrepr(target, max_len, source, strlen(source));
}

char* strnrepr(char* target, unsigned int max_len, const char* input, unsigned int input_len) {
	char* ss = target;
	int rem = max_len;

#define append(ch) 	 do { *(ss++) = (ch); rem --; } while(0)
#define appendn(str, n) do { const char* cp = str; unsigned int it = (n); while(*cp && it) { *(ss++) = (*cp++); rem --; it --;} } while(0)
#define appends(str) do { const char* cp = str; while(*cp) { *(ss++) = (*cp++); rem --; } } while(0)

	append('\'');
	for(const char* cp = input; cp != input + input_len; cp++) {
		if(rem == 0)
			break;
			
		if((strchr(" ,.:;/_-+=[](){}^!?", *cp) || isdigit(*cp) || isalpha(*cp)) && (unsigned char)*cp <= 127 && *cp) {
			//log("printable char: %d %c", *cp, *cp);
			append(*cp);
			continue;
		}
		if(*cp == '<') append('<');
		else if(*cp == '>') append('>');
		else if(*cp == '"') append('"');
		else {
			// check for needed space!
			if(rem < 2) {
				// target too small! terminate with \0
				append(0);
				return target;
			}
			if(*cp == '\n') appends("\\n");
			else if(*cp == '\r') appends("\\r");
			else if(*cp == '\t') appends("\\t");
			else if(*cp == '\\') appends("\\\\");
			else if(*cp == '\'') appends("\\'");
			else {
				char format[16];
				snprintf(format, 16, "\\x%02x", (unsigned char)*cp);
				if(rem < (signed)strlen(format)) {
					// target too small! terminate with \0
					append(0);
					return target;
				}
				appends(format);
			}
		}
	}
	if(rem == 0) {
		// target too small! terminate with \0
		target[max_len - 1] = 0;
		return target;
	}
	append('\'');
	if(rem == 0) {
		// target too small! terminate with \0
		target[max_len - 1] = 0;
		return target;
	}
	append(0);

#undef append
#undef appendn
#undef appends

	return target;
}

int growing_buffer_append(char** base, char** cp, unsigned int* size, const char* source) {
	return growing_buffer_appendn(base, cp, size, source, strlen(source));
}

int growing_buffer_appendn(char** base, char** user_cp, unsigned int* size, const char* source, unsigned int source_len) {
	char* _cp;
	char** cp = user_cp;

	if(!cp) {
		_cp = *base;
		cp = &_cp;
	}

	if(!*base) {
		*size = source_len * 2;
		if ((*base =  (char*)malloc(*size)) == NULL)
			return -LNE_NO_MEM;
		*cp = *base;
	}
	
	unsigned int in_buffer = *cp - *base;
	unsigned int remaining = *size - in_buffer;
	
	char* np;
	if(source_len > remaining) {
		*size += source_len * 2;
		if ((np = (char*)realloc(*base, *size)) == NULL)
			return -LNE_NO_MEM;
		*base = np;
		*cp = *base + in_buffer;
	}
	memcpy(*cp, source, source_len);
	*cp += source_len;
	return 0;
}


char* strrepr_(char** target, unsigned int* max_len, const char* source) {
	return strnrepr_(target, max_len, source, strlen(source));
}

char* strnrepr_(char** target, unsigned int* max_len, const char* input, unsigned int input_len) {
	char* target_cp = *target;

#define append(ch) 	    do { char ch_ = (ch); growing_buffer_appendn(target, &target_cp, max_len, &ch_, 1); } while(0)
#define appendn(str, n) growing_buffer_appendn(target, &target_cp, max_len, str, n)
#define appends(str)    growing_buffer_append(target, &target_cp, max_len, str)

	append('\'');
	for(const char* cp = input; cp != input + input_len; cp++) {
		if((strchr(" ,.:;/_-+=[](){}^!?", *cp) || isdigit(*cp) || isalpha(*cp)) && (unsigned char)*cp <= 127 && *cp) {
			//log("printable char: %d %c", *cp, *cp);
			append(*cp);
			continue;
		}
		if(*cp == '<') append('<');
		else if(*cp == '>') append('>');
		else if(*cp == '"') append('"');
		else {
			// check for needed space!
			if(*cp == '\n') appends("\\n");
			else if(*cp == '\r') appends("\\r");
			else if(*cp == '\t') appends("\\t");
			else if(*cp == '\\') appends("\\\\");
			else if(*cp == '\'') appends("\\'");
			else {
				char format[16];
				snprintf(format, 16, "\\x%02x", (unsigned char)*cp);
				appends(format);
			}
		}
	}
	append('\'');
	append(0);
#undef append
#undef appendn
#undef appends
	return *target;
}


char* strformat(char* target, unsigned int max_len, const char* format, ...) {
	va_list ap;
	va_start(ap, format);
	int n = vsnprintf(target, max_len, format, ap);
	if (!(n > -1 && (unsigned)n < max_len))
		target[max_len - 1] = 0;
	va_end(ap);

	return target;
}

char* strformat_(char** target, unsigned int* max_len, const char* format, ...) {
	if(!*target) {
		*max_len = strlen(format) * 2;
		if ((*target =  (char*)malloc(*max_len)) == NULL)
			return NULL; // throw std::bad_alloc();
	}
	va_list ap;
	char* np;
	int n;
	while(1) {
		va_start(ap, format);
		n = vsnprintf(*target, *max_len, format, ap);
		va_end(ap);
		if (n > -1 && (unsigned)n < *max_len)
			break;
		if (n > -1)    /* glibc 2.1 */
			*max_len = n + 1; /* precisely what is needed */
		else           /* glibc 2.0 */
			*max_len *= 2;  /* twice the old size */
		if ((np = (char*)realloc(*target, *max_len)) == NULL)
			return NULL;
		*target = np;
	}

	return *target;
}

char* evalstr(char* target, unsigned int max_len, const char* value) {
	if(!value)
		return NULL;

	char* ss = target;
	char* end = target + max_len - 1;
	*end = 0;
	int rem = max_len;

#define append(ch) 	 do { if(ss < end) { *(ss++) = (ch);}; rem --; } while(0)
#define appendn(str, n) do { const char* cp = str; unsigned int it = (n); while(*cp && it) { if(ss < end) { *(ss++) = (*cp++); } else { cp++; }; rem --; it --;} } while(0)
#define appends(str) do { char* cp = str; while(*cp) { if(ss < end) { *(ss++) = (*cp++) } else { cp++; }; rem --; } } while(0)

	// string::size_type p = start;
	// string::size_type tp;

	const char* p = value;
	const char* tp;
	while(*p && strchr(" \r\n\t", *p))
		p++;
	if(!*p) {
		// only whitespaces found!
		append(0);
		return target;
	}

	char delim = *p;

	if(delim != '\'' && delim != '"') {
		// no delim!
		// search next non identifier char
		// tp = value.find_first_of(" \r\n\t,:'\"}{[]()", p);
		tp = p;
		while(*tp && strchr(" \r\n\t,:'}\"}{[]()", *tp))
			tp++;
		// output = value.substr(p, tp - p);
		appendn(p, tp - p);
		append(0);
		return target;
	}
	p++;
	while(*p) { // -1!
		// find next backslash
		// string::size_type np = value.find('\\', p);
		const char* np = strchr(p, '\\');
		// if(np == string::npos) {
		if(!np) {
			// no further backslashes!
			// see where this string terminates!
			// tp = value.find(delim, p);
			tp = strchr(p, delim);
			//if(tp == string::npos)
			if(!tp) {
				// throw str_exception_tb("error: terminating ' not found in %s!", repr(value).c_str());			
				tp = p + strlen(p);
			}
			// ss << value.substr(p, tp - p);
			appendn(p, tp - p);
			break;
		}
		// found a backslash. 

		// check if there is a terminating '
		// tp = value.find(delim, p);
		tp = strchr(p, delim);
		//if(tp < np && tp != string::npos) {
		if(tp < np && tp) {
			// found terminating tick before backslash! - finish
			// printf("finish before next backslash\n");
			// ss << value.substr(p, tp - p - 1);
			appendn(p, tp - p - 1);
			break;
		}
			
		// ss << value.substr(p, np - p);
		appendn(p, np - p);

		// look at next char
		char nc = *(np + 1);
		if(nc == 'r') append('\r');
		else if(nc == 'n') append('\n');
		else if(nc == '0') append('\0');
		else if(nc == 't') append('\t');
		else if(nc == '\\') append('\\');
		else if(nc == '\'') append('\'');
		else if(nc == '"') append('"');
		else if(nc == 'x') {
			// 2 digit hex number
			// ss << (char)hex2dec(value.substr(np + 2, 2));
			char hex[3];
			strncpy(hex, np + 2, 2);
			append((unsigned char)strtol(hex, NULL, 16));
			np += 2; // skip these two chars
		} else {
			// throw str_exception_tb("unknown excape sequence starting from %s", repr(value.substr(p)).c_str());
			append('\\');
			np -= 1;
		}
		p = np + 2;
	}
	// output = ss.str();
	append(0);
#undef append
#undef appendn
#undef appends

	return target;
}


char* evalstr_(char** target, unsigned int* max_len, const char* value) {
	if(!value)
		return NULL;

	char* target_cp = *target;
#define append(ch) 	    do { char ch_ = (ch); growing_buffer_appendn(target, &target_cp, max_len, &ch_, 1); } while(0)
#define appendn(str, n) growing_buffer_appendn(target, &target_cp, max_len, str, n)
#define appends(str)    growing_buffer_append(target, &target_cp, max_len, str)

	const char* p = value;
	const char* tp;
	while(*p && strchr(" \r\n\t", *p))
		p++;
	if(!*p) {
		// only whitespaces found!
		append(0);
		return *target;
	}

	char delim = *p;

	if(delim != '\'' && delim != '"') {
		// no delim!
		// search next non identifier char
		// tp = value.find_first_of(" \r\n\t,:'\"}{[]()", p);
		tp = p;
		while(*tp && strchr(" \r\n\t,:'}\"}{[]()", *tp))
			tp++;
		// output = value.substr(p, tp - p);
		appendn(p, tp - p);
		append(0);
		return *target;
	}
	p++;
	while(*p) { // -1!
		// find next backslash
		// string::size_type np = value.find('\\', p);
		const char* np = strchr(p, '\\');
		// if(np == string::npos) {
		if(!np) {
			// no further backslashes!
			// see where this string terminates!
			// tp = value.find(delim, p);
			tp = strchr(p, delim);
			//if(tp == string::npos)
			if(!tp) {
				// throw str_exception_tb("error: terminating ' not found in %s!", repr(value).c_str());			
				tp = p + strlen(p);
			}
			// ss << value.substr(p, tp - p);
			appendn(p, tp - p);
			break;
		}
		// found a backslash. 

		// check if there is a terminating '
		// tp = value.find(delim, p);
		tp = strchr(p, delim);
		//if(tp < np && tp != string::npos) {
		if(tp < np && tp) {
			// found terminating tick before backslash! - finish
			// printf("finish before next backslash\n");
			// ss << value.substr(p, tp - p - 1);
			appendn(p, tp - p - 1);
			break;
		}
			
		// ss << value.substr(p, np - p);
		appendn(p, np - p);

		// look at next char
		char nc = *(np + 1);
		if(nc == 'r') append('\r');
		else if(nc == 'n') append('\n');
		else if(nc == '0') append('\0');
		else if(nc == 't') append('\t');
		else if(nc == '\\') append('\\');
		else if(nc == '\'') append('\'');
		else if(nc == '"') append('"');
		else if(nc == 'x') {
			// 2 digit hex number
			// ss << (char)hex2dec(value.substr(np + 2, 2));
			char hex[3];
			strncpy(hex, np + 2, 2);
			append((unsigned char)strtol(hex, NULL, 16));
			np += 2; // skip these two chars
		} else {
			// throw str_exception_tb("unknown excape sequence starting from %s", repr(value.substr(p)).c_str());
			append('\\');
			np -= 1;
		}
		p = np + 2;
	}
	// output = ss.str();
	append(0);
#undef append
#undef appendn
#undef appends

	return *target;
}
