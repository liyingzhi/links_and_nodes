#ifndef OS_LINUX_H
#define OS_LINUX_H

#include <pthread.h>
#include <sys/select.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>
#include <features.h>

#if __GLIBC_PREREQ(2, 25)
#  define GLIBC_ATLEAST_2_25 1
/*
  2.25 has new pthread_condvar implementation that is no longer robust.
  https://sourceware.org/bugzilla/show_bug.cgi?id=21422
 */
#endif

#include "os_linux_futex.h"

// switches
#define USE_POSIX_SHM

struct ln_pipe {
	int input_fd;
	int output_fd;
};
#define ln_pipe_invalid -1

typedef char* iov_base_t;
typedef socklen_t os_socklen_t;
#define _ln_socket_t int

#define MAX_SHARED_MEM_LEN 1023
// shared memory type
typedef struct {
	char name[MAX_SHARED_MEM_LEN + 1];
	unsigned int size;
	void* mem;
} ln_shared_mem_handle_t;

// semaphore type
typedef sem_t* ln_semaphore_handle_t;

// mutex type
typedef pthread_mutex_t ln_mutex_handle_t;
typedef pthread_cond_t ln_cond_handle_t;

// thread types
typedef pthread_t ln_thread_handle_t;
typedef void *(thread_func_type)(void*);
typedef void* thread_return_t;
#define THREAD_RETURN_NULL NULL

pid_t gettid();
typedef pid_t tid_t;
#define get_thread_id() gettid()
  
#ifndef USE_OWN_PROCESS_SHARED_IPC
 typedef pthread_mutex_t ln_process_shared_mutex_t;
 typedef pthread_cond_t ln_process_shared_cond_t;
#endif

#define thread_cleanup_push pthread_cleanup_push
#define thread_cleanup_pop pthread_cleanup_pop

#define LOW_PRIO 0
#define LOW_PRIO_POLICY SCHED_OTHER

#define HAVE_AF_UNIX

#endif // OS_LINUX_H
