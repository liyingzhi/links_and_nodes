#ifndef LINE_ASSEMBLER_H
#define LINE_ASSEMBLER_H

typedef int (*on_complete_line_cb_t)(
	void* data, char* line, unsigned int line_len, 
	void* remaining_data, unsigned int remaining_data_len, 
	unsigned int* skip_bytes);

typedef struct {
	int next_finished_line;

	unsigned int n_finished_lines;
	unsigned int* finished_lines;
	unsigned int finished_lines_size;

	char* finished_lines_buffer;
	char* finished_lines_buffer_cp;
	unsigned int finished_lines_buffer_size;

	char* incomplete_line_buffer;
	char* incomplete_line_buffer_cp;
	unsigned int incomplete_line_buffer_size;

	on_complete_line_cb_t on_complete_line_cb;
	void* on_complete_line_cb_data;

} line_assembler_t;

int init_line_assembler(line_assembler_t** la_, on_complete_line_cb_t cb, void* cb_data);
int destroy_line_assembler(line_assembler_t* la);
char* get_next_from_line_assembler(line_assembler_t* la);
int write_to_line_assembler(line_assembler_t* la, char* data, unsigned int len);
int get_n_finished_lines(line_assembler_t* la);

#endif // LINE_ASSEMBLER_H
