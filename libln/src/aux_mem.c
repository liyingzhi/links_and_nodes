/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include "aux_mem.h"

char* _ln_aux_mem_get(struct _ln_aux_mem_holder* s, unsigned int len) {
	int have_close = 0;
	unsigned int close_i;
	unsigned int how_close = 0; // just to silence warning
	for(unsigned int i = 0; i < s->n_aux_mems; i++) {
		if(s->aux_mems[i].used)
			continue;
		if(s->aux_mems[i].len == len) {
			// exact match
			s->aux_mems[i].used = 1;
			return s->aux_mems[i].mem;
		}
		// how close?
		unsigned int hc;
		if(s->aux_mems[i].len > len)
			hc = s->aux_mems[i].len - len;
		else
			hc = len - s->aux_mems[i].len;
		if(!have_close || hc < how_close) {
			have_close = 1;
			how_close = hc;
			close_i = i;
		}
	}
	if(have_close) {
		// realloc!
		s->aux_mems[close_i].mem = (char*)realloc(s->aux_mems[close_i].mem, len);
		s->aux_mems[close_i].len = len;
		s->aux_mems[close_i].used = 1;
		return s->aux_mems[close_i].mem;
	}
	// malloc!
	if(s->n_aux_mems == s->max_aux_mems) {
		// enlargen!
		s->max_aux_mems += 32;
		s->aux_mems = (struct _ln_aux_mem*)realloc(s->aux_mems, sizeof(struct _ln_aux_mem) * s->max_aux_mems);
	}
	unsigned int i = s->n_aux_mems;
	s->n_aux_mems ++;
	s->aux_mems[i].mem = (char*)malloc(len);
	s->aux_mems[i].len = len;
	s->aux_mems[i].used = 1;
	return s->aux_mems[i].mem;
}

void _ln_aux_mem_clear(struct _ln_aux_mem_holder* s) {
	for(unsigned int i = 0; i < s->n_aux_mems; i++)
		s->aux_mems[i].used = 0;
}

void _ln_aux_mem_init(struct _ln_aux_mem_holder* s) {
	s->aux_mems = NULL;
	s->n_aux_mems = 0;
	s->max_aux_mems = 0;
}

void _ln_aux_mem_free(struct _ln_aux_mem_holder* s) {
	if(!s->max_aux_mems)
		return;
	for(unsigned int i = 0; i < s->n_aux_mems; i++)
		free(s->aux_mems[i].mem);
	free(s->aux_mems);
	s->max_aux_mems = 0;
	s->n_aux_mems = 0;
}

