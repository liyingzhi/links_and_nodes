#ifndef LN_AUX_MEM_H
#define LN_AUX_MEM_H

struct _ln_aux_mem {
	char* mem;
	int used;
	unsigned int len;
};

struct _ln_aux_mem_holder {
	struct _ln_aux_mem* aux_mems;
	unsigned int n_aux_mems;
	unsigned int max_aux_mems;
};

void _ln_aux_mem_init(struct _ln_aux_mem_holder* s);
void _ln_aux_mem_clear(struct _ln_aux_mem_holder* s);
char* _ln_aux_mem_get(struct _ln_aux_mem_holder* s, unsigned int len);
void _ln_aux_mem_free(struct _ln_aux_mem_holder* s);


#endif // LN_AUX_MEM_H
