/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdlib.h>
#include <string.h>

#include "ln/ln.h"

#include "line_assembler.h"

int init_line_assembler(line_assembler_t** la_, on_complete_line_cb_t cb, void* cb_data) {
	line_assembler_t* la = (line_assembler_t*)malloc(sizeof(line_assembler_t));
	if(!la)
		return -LNE_NO_MEM;
	memset(la, 0, sizeof(line_assembler_t));

	la->on_complete_line_cb = cb;
	la->on_complete_line_cb_data = cb_data;

	*la_ = la;
	return 0;
}
int destroy_line_assembler(line_assembler_t* la) {
	if(la) {
		if(la->incomplete_line_buffer)
			free(la->incomplete_line_buffer);
		if(la->finished_lines_buffer)
			free(la->finished_lines_buffer);
		if(la->finished_lines)
			free(la->finished_lines);
		free(la); 
	}
	return 0;
}
int _append_to_incomplete_line(line_assembler_t* la, char* data, unsigned int n) {
	if(la->incomplete_line_buffer_size == 0) {
		// initial allocate memory
		la->incomplete_line_buffer_size = n * 2;
		la->incomplete_line_buffer = (char*) malloc(la->incomplete_line_buffer_size);
		if(!la->incomplete_line_buffer)
			return -LNE_NO_MEM;
		la->incomplete_line_buffer_cp = la->incomplete_line_buffer;
	} else {
		// enlargen memory
		int in_buffer = la->incomplete_line_buffer_cp - la->incomplete_line_buffer;
		int remaining = la->incomplete_line_buffer_size - in_buffer;
		if(remaining < (signed)n) {
			la->incomplete_line_buffer_size *= 2;
			remaining = la->incomplete_line_buffer_size - in_buffer;
			if(remaining < (signed)n)
				la->incomplete_line_buffer_size += n; // should be n - remaining
			la->incomplete_line_buffer = (char*) realloc(la->incomplete_line_buffer, la->incomplete_line_buffer_size);
			if(!la->incomplete_line_buffer)
				return -LNE_NO_MEM;
			la->incomplete_line_buffer_cp = la->incomplete_line_buffer + in_buffer;
		}
	}
	// copy incomplete line to mem
	memcpy(la->incomplete_line_buffer_cp, data, n);
	la->incomplete_line_buffer_cp += n;
	return 0;
}
int _append_to_finished_lines(line_assembler_t* la, char* line, unsigned int len) {
	// and provide a terminating \0 ;)
	if(!la->finished_lines_size) {
		la->finished_lines_size = 10; // initial guess of 10 lines
		la->finished_lines = (unsigned int*)malloc(sizeof(*la->finished_lines) * la->finished_lines_size);
		la->n_finished_lines = 0;
	}
	if(la->n_finished_lines >= la->finished_lines_size) {
		// expand lines buffer
		la->finished_lines_size *= 2;
		la->finished_lines = (unsigned int*)realloc(la->finished_lines, sizeof(*la->finished_lines) * la->finished_lines_size);
	}
	unsigned int in_buffer = la->finished_lines_buffer_cp - la->finished_lines_buffer;
	unsigned int remaining = la->finished_lines_buffer_size - in_buffer;
	if(remaining < (len + 1)) { // don't foget the new \0
		la->finished_lines_buffer_size += len + 1;
		la->finished_lines_buffer = (char*)realloc(la->finished_lines_buffer, la->finished_lines_buffer_size);
		if(!la->finished_lines_buffer)
			return -LNE_NO_MEM;
		la->finished_lines_buffer_cp = la->finished_lines_buffer + in_buffer;
	}
	// remember line start
	la->finished_lines[la->n_finished_lines++] = la->finished_lines_buffer_cp - la->finished_lines_buffer;
	memcpy(la->finished_lines_buffer_cp, line, len);
	la->finished_lines_buffer_cp += len;
	*(la->finished_lines_buffer_cp++) = 0; // add trailing \0
	return 0;
}

int get_n_finished_lines(line_assembler_t* la) {
	int n = la->n_finished_lines;
	n -= la->next_finished_line;
	if(n < 0)
		n = 0;
	return n;
}

char* get_next_from_line_assembler(line_assembler_t* la) {
	if(la->next_finished_line >= (signed)la->n_finished_lines)
		return NULL;
	return la->finished_lines_buffer + la->finished_lines[la->next_finished_line++];
}

int write_to_line_assembler(line_assembler_t* la, char* data, unsigned int len) {
	int ret;
	char* p = data;
	// expect all finished lines to be read
	la->next_finished_line = 0;
	la->n_finished_lines = 0;
	la->finished_lines_buffer_cp = la->finished_lines_buffer;

	while (p < data + len) {
		char* np = (char*)memchr(p, '\n', (data + len) - p);
		if (!np) {
			// this line is not completed                                                                             
			// incomplete_line.append(p, len - (p - data));
			if((ret = _append_to_incomplete_line(la, p, len - (p - data))))
				return ret;
			return 0; // wait for more data                                                                              
		}
		// got complete line from p to np
		// string line = incomplete_line;
		// line.append(p, np - p);
		if((ret = _append_to_incomplete_line(la, p, np - p)))
			return ret;
	  
		// callback(instance, line);
		// append now complete incomplete_line to finished_lines
		char* complete_line = la->incomplete_line_buffer;
		unsigned int complete_line_len = la->incomplete_line_buffer_cp - la->incomplete_line_buffer;

		// incomplete_line = "";
		la->incomplete_line_buffer_cp = la->incomplete_line_buffer;
		p = np + 1;

		int do_append = 1;
		if(la->on_complete_line_cb) {
			unsigned int skip_bytes = 0;
			do_append = la->on_complete_line_cb(
				la->on_complete_line_cb_data,
				complete_line, complete_line_len, // finished line
				p, // remaining data,
				(data + len) - p, // remaining data_len
				&skip_bytes);
			p += skip_bytes;
		}

		if(do_append && (ret = _append_to_finished_lines(la, complete_line, complete_line_len)))
			return ret;

	}
	return 0;
}
