#ifndef MULTI_WAITER_H
#define MULTI_WAITER_H

#include "lists.h"

struct multi_waiter_;
struct multi_waiter_port_;
typedef struct multi_waiter_ multi_waiter;
typedef struct multi_waiter_port_ multi_waiter_port;

struct multi_waiter_port_ {
	ln_thread_handle_t thread;
	int keep_running;
	multi_waiter* waiter;
	inport* port;
};
#ifdef __WIN32__
thread_return_t WINAPI _ln_multi_waiter_thread(void* data);
#else
thread_return_t _ln_multi_waiter_thread(void* data);
#endif


int _ln_multi_waiter_port_destroy(multi_waiter_port* mwp);

struct multi_waiter_ {
	client* clnt;
	DEFINE_LIST_DATA(multi_waiter_port);
	ln_semaphore_handle_t waiter_wait;

	ln_thread_handle_t pipe_thread;
	ln_semaphore_handle_t pipe_thread_wait;
	ln_pipe_t pipe;
	int pipe_thread_running;
	int pipe_notified; 
};

DECLARE_LIST_METHODS(multi_waiter, multi_waiter_port);
int _ln_multi_waiter_destroy(multi_waiter* w);

#endif // MULTI_WAITER_H
