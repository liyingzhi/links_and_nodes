#ifndef LN_IOVEC_MGR_H
#define LN_IOVEC_MGR_H

#include "os.h"

struct _ln_iovec_mgr {
	struct msghdr msg; // response message!
	unsigned int msg_max_len; // todo init
	uint32_t n_bytes;

	struct _ln_aux_mem_holder* swap_aux_mem;
};
typedef struct _ln_iovec_mgr _ln_iovec_mgr;

void _ln_iovec_mgr_init(struct _ln_iovec_mgr* mgr);
void _ln_iovec_mgr_destroy(struct _ln_iovec_mgr* mgr);
void _ln_iovec_mgr_reset(struct _ln_iovec_mgr* mgr, struct _ln_aux_mem_holder* swap_aux_mem); // incase swap_aux_mem is given do a byte-swap with each mgr_add!
struct iovec* _ln_iovec_mgr_next(struct _ln_iovec_mgr* mgr);
void _ln_iovec_mgr_add(struct _ln_iovec_mgr* mgr, void* base, unsigned int len, unsigned int element_size);
void* _ln_iovec_mgr_get_flat(struct _ln_iovec_mgr* mgr);
struct _ln_iovec_mgr* _ln_iovec_mgr_clone(struct _ln_iovec_mgr* mgr);

void _ln_swap(unsigned int what, void* dst_, void* src_);
void _ln_memcpy_swap(void* dst_, void* src_, unsigned int len, unsigned int element_size);
void _ln_memcpy_swap_hetero(void* dst_, void* src_, unsigned int len, uint8_t* swap_instr);

#endif // LN_IOVEC_MGR_H
