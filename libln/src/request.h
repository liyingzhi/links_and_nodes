#ifndef REQUEST_H
#define REQUEST_H


#ifndef CLIENT_H
struct client_;
typedef struct client_ client;
#endif

#define RECEIVER_BUFFER_SIZE 2048

typedef struct {
	char* header;
	char* value;
	uint32_t binary_data_len;
	int do_free;
} request_line_t;

typedef struct {
	client* clnt;

	request_line_t* lines;
	unsigned int n_lines;
	unsigned int lines_size;

	char* format_buffer;
	char* format_buffer_cp;
	unsigned int format_buffer_size;

	char* binary_data_field;
	unsigned int binary_data_len;
	uint8_t* binary_data;
	unsigned int binary_data_pos;

	char need_success;
} request_t;

#ifdef __cplusplus
extern "C" {
#endif
int _ln_init_request(client* clnt, request_t* req, ...);
int _ln_reset_request(request_t* req);
int _ln_destroy_request(request_t* req);
int _ln_add_to_request_raw(request_t* req, char* header, void* value, unsigned int data_len, int do_free);
int _ln_add_to_request(request_t* req, const char* header, const char* value);
int _ln_format_request(request_t* req, char** bp, unsigned int* bp_len, int indent, int pretty);
int _ln_print_request(request_t* req);
int _ln_send_request(request_t* r);
int _ln_wait_response(request_t* resp, int async);
int _ln_is_in_request(request_t* req, const char* field_name);
int _ln_get_from_request(request_t* req, const char* field_name, const char* field_format, void* target);
int _ln_get_from_request_raw_named(request_t* req, const char* field_name, request_line_t** field);
int _ln_request_and_wait(request_t* r);
int _ln_process_manager_request(request_t* r);
int _ln_send_all(int fd, char* data, unsigned int len);
	
#ifdef __cplusplus
}
#endif
#endif // REQUEST_H
