/**
   apt-get install  libtomcrypt-dev libtommath-dev

   openssl asn1parse -inform pem -in id_dsa_ln_daemons
   openssl dsa -in id_dsa_ln_daemons -text

   https://www.thedigitalcatonline.com/blog/2018/04/25/rsa-keys/
   https://datatracker.ietf.org/doc/html/rfc4253

 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>

#include <ln/ln.h>

#ifdef NO_DAEMON_AUTH
int ln_dsa_dss1_sign_message(
	const char* pem_private_key_filename,
	const uint8_t* message, unsigned int message_len,
	uint8_t** signature_out, unsigned int* signature_out_len
)
{
	ln_debug(NULL, "library is compiled with NO_DAEMON_AUTH!\n");
	return -100;
}

int ln_dsa_dss1_verify_message_signature(
	const char* openssh_public_key_filename,
	const uint8_t* message, unsigned int message_len,
	const uint8_t* signature, unsigned int signature_len
)
{
	ln_debug(NULL, "library is compiled with NO_DAEMON_AUTH!\n");
	return -100;
}

int ln_dsa_dss1_verify_message_signature_memkey(
	const char* openssh_public_key,
	const uint8_t* message, unsigned int message_len,
	const uint8_t* signature, unsigned int signature_len
)
{
	ln_debug(NULL, "library is compiled with NO_DAEMON_AUTH!\n");
	return -100;
}

#else

#include <tomcrypt.h>

#include "uuencode.h"

/**
   sha1_out has to point to 20 bytes of memory!
 */
static void get_sha1(const void* message, unsigned int message_len, uint8_t* sha1_out)
{
	hash_state md;
	sha1_init(&md);
	sha1_process(&md, (const unsigned char*)message, message_len);
	sha1_done(&md, sha1_out);
}

/**
   data_out will have to be free'd by caller if return == 0
 */
static int read_file(const char* fn, uint8_t** data_out, unsigned int* data_out_len)
{
	int ret;

	FILE* fp = fopen(fn, "rb");
	if(!fp) {
		ln_debug(NULL, "could not open file '%s': %s\n", fn, strerror(errno));
		return -1;
	}
	struct stat sbuf;
	ret = fstat(fileno(fp), &sbuf);
	if(ret == -1) {
		ln_debug(NULL, "could not fstat file '%s': %s\n", fn, strerror(errno));
		return -2;
	}
	*data_out = calloc(1, sbuf.st_size + 1);
	if(!*data_out) {
		ln_debug(NULL, "could not calloc %lu bytes for file '%s': %s\n", sbuf.st_size, fn, strerror(errno));
		return -3;
	}
	*data_out_len = sbuf.st_size;
	(*data_out)[sbuf.st_size] = 0; /* just a safety measure for people who
					  assume text-only contents and do
					  strlen(data_on), ignoring data_out_len
				       */
	ret = fread(*data_out, sbuf.st_size, 1, fp);
	if(ret != 1) {
		ln_debug(NULL, "could not read %lu bytes for file '%s': %s\n", sbuf.st_size, fn, strerror(errno));
		ret = -4;
		goto lbl_free_data_out;
	}
	fclose(fp);
	return 0;

lbl_free_data_out:
	free(*data_out);
	return ret;
}

static int _init_mp()
{
	static bool did_init_mp = false;
	if(did_init_mp)
		return 0;

#ifdef HAVE_CRYPT_MP_INIT
	int ret;
	if((ret = crypt_mp_init("LTM")) != CRYPT_OK) {
		ln_debug(NULL, "could not init LTM mp math: %s\n", error_to_string(ret));
		return -1;
	}
#else
	init_LTM();
#endif

	did_init_mp = true;
	return 0;
}

static int read_openssh_private_key_file(const char* fn, dsa_key* key)
{
	int ret;

	memset(key, 0, sizeof(*key));

	uint8_t* data;
	unsigned int data_len;
	if((ret = read_file(fn, &data, &data_len)) < 0) {
		ln_debug(NULL, "could not read private key '%s'!\n", fn);
		return -1;
	}
	const char* search = "----\n";
	uint8_t* cp = (uint8_t*)strstr((const char*)data, search);
	if(!cp) {
		ln_debug(NULL, "invalid private key '%s': missing end of first comment line\n", fn);
		ret = -2;
		goto lbl_free_data;
	}
	unsigned int p1 = cp - data + strlen(search);

	search = "\n----";
	cp = (uint8_t*)strstr((const char*)(data + p1), search);
	if(!cp) {
		ln_debug(NULL, "invalid private key '%s': could not find last comment line in private key!\n", fn);
		ret = -3;
		goto lbl_free_data;
	}
	unsigned int p2 = cp - data;
	data[p2] = 0;
	uint8_t* encoded = data + p1;

	uint8_t decoded[1024];
	unsigned int decoded_len = _ln_uudecode((const char*)encoded, decoded, sizeof(decoded));

	if((ret = _init_mp()) != 0) {
		ln_debug(NULL, "could not init mp: %s\n", ret);
		ret = -4;
		goto lbl_free_data;
	}

	void* key_type = NULL;
	if ((ret = ltc_init_multi(&key_type, &key->x, &key->y, &key->p, &key->q, &key->g, NULL)) != CRYPT_OK) {
		ln_debug(NULL, "could not mp_init_multi: %s\n", error_to_string(ret));
		ret = -5;
		goto lbl_free_data;
	}
	ret = der_decode_sequence_multi(
		decoded, decoded_len,
		LTC_ASN1_INTEGER, 1UL, key_type,
		LTC_ASN1_INTEGER, 1UL, key->p,
		LTC_ASN1_INTEGER, 1UL, key->q,
		LTC_ASN1_INTEGER, 1UL, key->g,
		LTC_ASN1_INTEGER, 1UL, key->y,
		LTC_ASN1_INTEGER, 1UL, key->x,
		LTC_ASN1_EOL,     0UL, NULL);
	ltc_cleanup_multi(&key_type, NULL);
	if(ret != CRYPT_OK) {
		ln_debug(NULL, "could not der-parse private key: %s\n", error_to_string(ret));
		ret = -6;
		goto lbl_free_key;
	}

	key->type = PK_PRIVATE;
	key->qord = ltm_desc.unsigned_size(key->q);

	int stat = 0;
	if((ret = dsa_verify_key(key, &stat)) != CRYPT_OK) {
		ln_debug(NULL, "could not verify dsa key: %s\n", error_to_string(ret));
		ret = -7;
		goto lbl_free_key;

	}
	if(stat != 1) {
		ln_debug(NULL, "dsa key did not verify as valid: %s\n", error_to_string(ret));
		ret = -8;
		goto lbl_free_key;
	}

	ret = 0;
	goto lbl_free_data;
lbl_free_key:
	dsa_free(key);
lbl_free_data:
	free(data);
	return ret;
}

static int _read_openssh_public_key(uint8_t* data, unsigned int data_len, dsa_key* key)
{
	memset(key, 0, sizeof(*key));
	int ret = 0;
	// read until first ' ' char
	uint8_t* cp = data;
	while(*cp != ' ')
		cp++;
	uint8_t decoded[1024];
	unsigned int decoded_len = _ln_uudecode((const char*)cp, decoded, sizeof(decoded));

	unsigned int part = 0;
	uint8_t* p; unsigned int p_len = 0;
	uint8_t* q; unsigned int q_len = 0;
	uint8_t* g; unsigned int g_len = 0;
	uint8_t* y; unsigned int y_len = 0;
	cp = decoded;
	while(cp < (decoded + decoded_len)) {
		uint32_t len = ntohl(*(uint32_t*)cp);
		cp += 4;
		if(part == 0 && strncmp((const char*)cp, "ssh-dss", len)) {
			ln_debug(NULL, "could not read openssh public key: expect 'ssh-dss', got type '%-*.*s'!\n",
				 len, len, cp);
			ret = -2;
			goto lbl_free_key;
		} else if(part == 1) {
			p = cp; p_len = len;
		} else if(part == 2) {
			q = cp; q_len = len;
		} else if(part == 3) {
			g = cp; g_len = len;
		} else if(part == 4) {
			y = cp; y_len = len;
		}
		cp += len;
		part ++;
	}
	if(part != 5) {
		ln_debug(NULL, "could not read openssh public key: expect 5 parts, got only %d!\n",
			 part);
		ret = -3;
		goto lbl_free_key;
	}
	if((ret = _init_mp()) != 0) {
		ln_debug(NULL, "could not init mp: %s\n", ret);
		ret = -4;
		goto lbl_free_key;
	}

	key->type = PK_PUBLIC;
	if((ret = dsa_set_pqg(p, p_len,  q, q_len,  g, g_len,  key)) != CRYPT_OK) {
		ln_debug(NULL, "could not read openssh public key: could not set p %d, q %d, g %d: %s\n",
			 p_len, q_len, g_len);
		ret = -5;
		goto lbl_free_key;
	}
	ltm_desc.unsigned_read(key->y, y, y_len);
	return 0;
lbl_free_key:
	dsa_free(key);
	return ret;
}

static int read_openssh_public_key_file(const char* fn, dsa_key* key)
{
	uint8_t* data;
	unsigned int data_len;
	int ret;
	if((ret = read_file(fn, &data, &data_len)) < 0)
		goto lbl_exit;
	ret = _read_openssh_public_key(data, data_len, key);
	free(data);
lbl_exit:
	if(ret)
		ln_debug(NULL, "could not read public key '%s': %d!\n", fn, ret);
	return ret;
}

static int read_openssh_public_key(const char* data_, dsa_key* key)
{
	return _read_openssh_public_key((uint8_t*)data_, strlen(data_), key);
}

static int init_prngs(prng_state* prngs)
{
	static bool did_init_prngs = false;
	if(!did_init_prngs) {
		did_init_prngs = true;
		register_all_prngs();
	}
	int wprng = 0;
	struct ltc_prng_descriptor* prng_desc = &prng_descriptor[wprng];

	int ret;
	if((ret = prng_desc->start(prngs)) != CRYPT_OK) {
		ln_debug(NULL, "prng_start: %s\n", error_to_string(ret));
		return -1;
	}
	char seed[27];
	strncpy(seed, "may the source be with you", sizeof(seed));
	*(double*)(&seed[0]) = ln_get_time();
	if((ret = prng_desc->add_entropy((const unsigned char*)seed, sizeof(seed), prngs)) != CRYPT_OK) {
		ln_debug(NULL, "prng_add: %s\n", error_to_string(ret));
		return -2;
	}
	if((ret = prng_desc->ready(prngs)) != CRYPT_OK) {
		ln_debug(NULL, "prng_ready: %s\n", error_to_string(ret));
		return -3;
	}
	return wprng;
}

int ln_dsa_dss1_sign_message(
	const char* pem_private_key_filename,
	const uint8_t* message, unsigned int message_len,
	uint8_t** signature_out, unsigned int* signature_out_len
)
{
	int err = 0;

	dsa_key key;
	if((err = read_openssh_private_key_file(pem_private_key_filename, &key)) < 0) {
		ln_debug(NULL, "could not read openssh private key file '%s': error %d\n", pem_private_key_filename, err);
		return -1;
	}
	prng_state prngs;
	int wprng;
	if((wprng = init_prngs(&prngs)) < 0)  {
		ln_debug(NULL, "could not init prng: error %d\n", wprng);
		err = -2;
		goto lbl_free_key;
	}

	uint8_t sha1[20];
	get_sha1(message, message_len, sha1);

	unsigned char sign[1024];
	unsigned long sign_len = sizeof(sign);
	if((err = dsa_sign_hash(
		    sha1, sizeof(sha1),
		    sign, &sign_len,
		    &prngs, wprng,
		    &key)) != CRYPT_OK) {
		ln_debug(NULL, "dsa_sign_hash: %s\n", error_to_string(err));
		err = -3;
		goto lbl_free_key;
	}

	// success
	*signature_out_len = sign_len;
	*signature_out = calloc(1, sign_len);
	memcpy(*signature_out, sign, sign_len);

lbl_free_key:
	dsa_free(&key);

	return err;
}


static int _ln_dsa_dss1_verify_message_signature(
	dsa_key* key,
	const uint8_t* message, unsigned int message_len,
	const uint8_t* signature, unsigned int signature_len
)
{
	uint8_t sha1[20];
	get_sha1(message, message_len, sha1);

	int stat = 0;
	int err;
	if((err = dsa_verify_hash(
		    signature, signature_len,
		    (const unsigned char*)sha1, sizeof(sha1),
		    &stat,
		    key)) != CRYPT_OK) {
		ln_debug(NULL, "could not read openssh public key: error %d\n", err);
		return -2;
	}

	if(stat == 0)
		return -LNE_INVALID_DSA_SIGNATURE;

	return 0;
}

int ln_dsa_dss1_verify_message_signature(
	const char* openssh_public_key_filename,
	const uint8_t* message, unsigned int message_len,
	const uint8_t* signature, unsigned int signature_len
)
{
	dsa_key key;
	int err;
	if((err = read_openssh_public_key_file(openssh_public_key_filename, &key)) < 0) {
		ln_debug(NULL, "could not read openssh public key file '%s': error %d\n", openssh_public_key_filename, err);
		return -1;
	}

	err = _ln_dsa_dss1_verify_message_signature(&key, message, message_len, signature, signature_len);
	dsa_free(&key);

	return err;
}

int ln_dsa_dss1_verify_message_signature_memkey(
	const char* openssh_public_key_contents,
	const uint8_t* message, unsigned int message_len,
	const uint8_t* signature, unsigned int signature_len
)
{
	dsa_key key;
	int err;
	if((err = read_openssh_public_key(openssh_public_key_contents, &key)) < 0) {
		ln_debug(NULL, "could not read openssh public key from memory: error %d\n", err);
		return -1;
	}

	err = _ln_dsa_dss1_verify_message_signature(&key, message, message_len, signature, signature_len);
	dsa_free(&key);

	return err;
}

#endif
