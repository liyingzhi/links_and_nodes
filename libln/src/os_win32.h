#ifndef OS_WIN32_H
#define OS_WIN32_H

// #define WINVER 0x0400 // Win95 / WinNT
// #define WINVER 0x0501 // since Win2k
#ifndef MSVC
#define WINVER 0x0600 // since WinXp
#endif
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#define NO_OLDNAMES
// needed for ssize_t stuff #define _NO_OLDNAMES
// #define _UWIN

#include <windows.h>
#include <winsock2.h>
#include <errno.h>
#include <sys/types.h>
#include <process.h>
#include <ws2tcpip.h>
#ifndef MSVC
#include <mswsock.h>
#endif
#include <string.h>

#ifdef I586_MINGW32MSVC

struct tcp_keepalive {
    u_long  onoff;
    u_long  keepalivetime;
    u_long  keepaliveinterval;
};
#define SIO_KEEPALIVE_VALS 0x98000004
#else
#include <mstcpip.h>
#endif

// switches
#define USE_OWN_PROCESS_SHARED_IPC // todo: does windows need this?

// shared memory tyoe
#define MAX_SHARED_MEM_LEN 1023
typedef struct {
	char name[MAX_SHARED_MEM_LEN+1];
	unsigned int size;
	void* mem;
	void* real_mem;

	HANDLE file_mapping;
} ln_shared_mem_handle_t;

// semaphore type
typedef HANDLE ln_semaphore_handle_t;
int ln_semaphore_timedwait_abs(ln_semaphore_handle_t handle, double abs_timeout);

// mutex type
typedef CRITICAL_SECTION* ln_mutex_handle_t;
typedef void* ln_cond_handle_t;


// thread types
struct win32_thread_handle_and_cancel_flag {
	HANDLE thread;
	int cancel_requested;
};
typedef struct win32_thread_handle_and_cancel_flag* ln_thread_handle_t;
#ifndef MSVC
#define thread_return_t DWORD __stdcall
typedef thread_return_t WINAPI (thread_func_type)(void*);
#else

#define thread_return_t DWORD
// typedef thread_return_t (WINAPI *thread_func_type)(void*);
#define thread_func_type PTHREAD_START_ROUTINE
#endif
#define THREAD_RETURN_NULL 0

//todo: memleak. destroy thread or the like is needed!

// util
char *strndup(const char *s, size_t n);

#define MAP_LOCKED 0
#define MAP_POPULATE 0
#define MAP_FAILED NULL
#define SEM_FAILED NULL

void thread_cleanup_push(void (*)(void*), void* arg);
void thread_cleanup_pop(int exec);

#ifdef I586_MINGW32MSVC
typedef int pid_t;
#endif

#define getpid _getpid
typedef DWORD tid_t;
#define get_thread_id() GetCurrentThreadId()
#define gettid() get_thread_id()

#define msghdr _WSAMSG 
#define msg_name name
#define msg_namelen namelen
#define msg_iov lpBuffers
#define msg_iovlen dwBufferCount
#define msg_control Control
#define msg_flags dwFlags

#define iovec _WSABUF
#define iov_base buf
#define iov_len len
typedef char* iov_base_t;

int inet_aton(const char *cp, struct in_addr *inp);

#ifndef HAVE_LN_PIPE_FD
#define HAVE_LN_PIPE_FD
typedef HANDLE ln_pipe_fd_t;
#endif
struct ln_pipe {
	ln_pipe_fd_t input_fd;
	ln_pipe_fd_t output_fd;
};
#define ln_pipe_invalid NULL

#define __WIN32__DGRAM_SEND 1
#ifdef MSVC
#define ssize_t int
#endif
ssize_t sendmsg(int socket, const struct msghdr *message, int flags);

typedef socklen_t os_socklen_t;
#define _ln_socket_t int

// not implemented:
#define LOW_PRIO 0
#define LOW_PRIO_POLICY 0

#ifndef ETIMEDOUT
#define ETIMEDOUT EAGAIN
#define EWOULDBLOCK EAGAIN
#endif
#define MSG_DONTWAIT 0 // not avaliable on windows

#define SHUT_RDWR SD_BOTH

#ifndef IOV_MAX
#define IOV_MAX 64
#endif


#ifdef _MSC_VER
#include <stdio.h>

#define snprintf c99_snprintf
#define vsnprintf c99_vsnprintf
int c99_vsnprintf(char* str, size_t size, const char* format, va_list ap);
int c99_snprintf(char* str, size_t size, const char* format, ...);

#pragma warning(push)
#pragma warning(disable: 4996) // strdup name
// #pragma warning(pop)
#else
char *strdup(const char *s);

#endif

#ifdef MSVC
#define __func__ __FUNCTION__
#else
#define inet_ntop _ln_winxp_inet_ntop
#endif

#endif // OS_WIN32_H
