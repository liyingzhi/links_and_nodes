/*
    Copyright 2013-2015 DLR e.V., Florian Schmidt, Maxime Chalon

    This file is part of links and nodes.

    links and nodes is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    links and nodes is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with links and nodes.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "os.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ln/ln.h"
#include "client.h"

#include <errno.h>

int ln_multi_waiter_init(ln_client clnt_, ln_multi_waiter* waiter) {
	client* clnt = (client*)clnt_;

	if(!client_initialized(clnt))
		return -LNE_CLIENT_NOT_INITIALIZED;

	multi_waiter* w = (multi_waiter*)calloc(1, sizeof(multi_waiter));
	w->clnt = clnt;
	w->pipe_thread_running = false;
	w->pipe.input_fd = ln_pipe_invalid;

	lock_client(clnt);
	_ln_client_append_multi_waiter(clnt, w);
	unlock_client(clnt);
	
	ln_semaphore_init(&w->waiter_wait, 0);
	ln_semaphore_init(&w->pipe_thread_wait, 0);

	*waiter = w;
	return 0;
}

int ln_multi_waiter_deinit(ln_multi_waiter* waiter) {
	multi_waiter* w = (multi_waiter*)*waiter;

	lock_client(w->clnt);
	_ln_client_remove_multi_waiter(w->clnt, w);
	unlock_client(w->clnt);
	_ln_multi_waiter_destroy(w);

	*waiter = NULL;
	return 0;
}

int _ln_multi_waiter_destroy(multi_waiter* w) {
	DESTRUCT_LIST(w, multi_waiter_port);

	if(w->pipe_thread_running) {
		ln_semaphore_post(w->pipe_thread_wait);
		w->pipe_thread_running = false;
		ln_thread_join(w->pipe_thread, NULL);
	}
	if(w->pipe.input_fd != ln_pipe_invalid)
		ln_pipe_close(&w->pipe);

	ln_semaphore_destroy(w->waiter_wait);
	ln_semaphore_destroy(w->pipe_thread_wait);

	free(w);
	return 0;
}

int _ln_multi_waiter_port_destroy(multi_waiter_port* mwp) {	
	mwp->keep_running = false;
	// ln_thread_cancel(mwp->thread);
	ln_thread_join(mwp->thread, NULL);
	free(mwp);
	return 0;
}

int ln_multi_waiter_add_port(ln_multi_waiter waiter, ln_inport p_) { // returns waiter_id
	multi_waiter* w = (multi_waiter*)waiter;
	inport* p = (inport*)p_;

	multi_waiter_port* mwp = (multi_waiter_port*)calloc(1, sizeof(multi_waiter_port));
	mwp->waiter = w;
	mwp->port = p;
	p->can_read = 0;
	mwp->keep_running = 1;
	ln_thread_create(&mwp->thread, _ln_multi_waiter_thread, mwp);

	_ln_multi_waiter_append_multi_waiter_port(w, mwp);
	return 0;
}

int ln_multi_waiter_remove_port(ln_multi_waiter waiter, ln_inport p_) {
	multi_waiter* w = (multi_waiter*)waiter;
	inport* p = (inport*)p_;

	for(unsigned int i = 0; i < w->n_multi_waiter_ports; i++) {
		multi_waiter_port* mwp = w->multi_waiter_ports[i];
		if(mwp->port != p)
			continue;
		_ln_multi_waiter_port_destroy(mwp);
		_ln_multi_waiter_remove_multi_waiter_port(w, mwp); // CSA: mwp will not be dereferenced. its fine.
		return 0;
	}
	return -LNE_PORT_NOT_FOUND;
}

int ln_multi_waiter_wait(ln_multi_waiter waiter, double timeout) {
	multi_waiter* w = (multi_waiter*)waiter;

	// if(!w->n_multi_waiter_ports)
	// 	return -LNE_PORT_NOT_FOUND; // no ports!

	if(timeout < 0)
		ln_semaphore_wait(w->waiter_wait); // wait until at least one waiter thread
	else
		if(ln_semaphore_timedwait(w->waiter_wait, timeout)) // wait until at least one waiter thread
			return 0; // probably timeout

	return 1; // there might be at least one port to read from!
}

int ln_multi_waiter_can_read(ln_multi_waiter waiter, ln_inport p_) { // 0 - can not read, 1 - can read without blocking
	inport* p = (inport*)p_;
	return p->can_read;
}

#ifdef __WIN32__
thread_return_t WINAPI _ln_multi_waiter_thread(void* data) {
#else
thread_return_t _ln_multi_waiter_thread(void* data) {
#endif
	multi_waiter_port* mwp = (multi_waiter_port*) data;
	uint32_t last_packet_counter = mwp->port->hdr.packet_counter;
	while(mwp->keep_running) {
		// block on port
		if(!_ln_block_only(mwp->port, &last_packet_counter, 0.1)) // todo: timeout of 0.1 is ugly but we have problems with process shared conditions on linux and pthread_cancel
			continue; // timeout
		mwp->port->can_read = 1;

		// notify waiter!
		if(ln_semaphore_getvalue(mwp->waiter->waiter_wait) <= 0)
			ln_semaphore_post(mwp->waiter->waiter_wait);
	}
	return 0;
}

DEFINE_LIST_METHODS(multi_waiter, multi_waiter_port);

#ifdef __WIN32__
thread_return_t WINAPI _ln_multi_waiter_pipe_thread(void* data) {
#else
thread_return_t _ln_multi_waiter_pipe_thread(void* data) {
#endif
	multi_waiter* w = (multi_waiter*) data;
	double timeout = 0.5;

	w->pipe_thread_running = true;
	// printf("notifier thread started.\n");
	while(w->pipe_thread_running) {
		// printf("notifier thread running\n");
		if(ln_semaphore_timedwait(w->waiter_wait, timeout)) { // wait until at least one waiter thread
			if(errno == EAGAIN || errno == ETIMEDOUT || errno == EINTR) {
				// printf("timeout %.3f\n", ln_get_time());
			} else {
				printf("_ln_multi_waiter_pipe_thread ln_semaphore_timedwait: %d %s\n", errno, strerror(errno));
				ln_sleep_seconds(1);
			}
			continue;
		}

		// notify via pipe!
		// printf("pipe notifiing!\n");
		w->pipe_notified ++;
#ifdef __WIN32__
		DWORD n = 0;
		WriteFile(w->pipe.output_fd, "\n", 1, &n, NULL);
#else		
		int n;
		UNTIL_RET_NOT_EINTR(n, write(w->pipe.output_fd, "\n", 1));
#endif
		// if(n != 1)
		// 	fprintf(stderr, "pipe notifier write: %d - %s\n", n, strerror(errno));

		// printf("pipe waiting for ack!\n");
		ln_semaphore_wait(w->pipe_thread_wait);
		// printf("pipe got ack!\n");
	}
	w->pipe_thread_running = false;
	return 0;
}

int ln_multi_waiter_start_pipe_notifier_thread(ln_multi_waiter waiter, ln_pipe_fd_t* fd) {
	multi_waiter* w = (multi_waiter*)waiter;
	if(w->pipe_thread_running) {
		*fd = w->pipe.input_fd;
		return 0;
	}
	int ret;

	if(w->pipe.input_fd == ln_pipe_invalid) {
		ret = ln_pipe_init(&w->pipe);
		if(ret)
			return ret;
	}

	w->pipe_notified = 0;
	ret = ln_thread_create(&w->pipe_thread, _ln_multi_waiter_pipe_thread, w);
	if(ret)
		return ret;
	*fd = w->pipe.input_fd;
	return 0;
}

int ln_multi_waiter_ack_pipe_notification(ln_multi_waiter waiter) {
	multi_waiter* w = (multi_waiter*)waiter;
	w->pipe_notified --;
	char buffer;
#ifndef __WIN32__
	read(w->pipe.input_fd, &buffer, 1);
#else
	DWORD n = 0;
	ReadFile(w->pipe.input_fd, &buffer, 1, &n, NULL);
#endif
	ln_semaphore_post(w->pipe_thread_wait);
	//printf("sent pipe ack!\n");
	return 0;
}

