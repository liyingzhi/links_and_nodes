#ifndef OS_VXWORKS_H
#define OS_VXWORKS_H

#ifdef __cplusplus
extern "C" {
#endif
#ifdef EMACS_IS_STILL_DUMB
}
#endif

#include <vxWorks.h>
#include <version.h>
#include <pthread.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#ifdef __cplusplus // hide the fact that we are c++ compiling for this header
#  define REMIND_Cplusplus
#  undef __cplusplus
#endif
#include <semaphore.h>
#include <string.h>
#ifdef REMIND_Cplusplus // undo
#  define __cplusplus
#endif

#include <inetLib.h>
#include <sockLib.h>
#include <hostLib.h>

#if (_WRS_VXWORKS_MAJOR < 7)
#  include <net/uio.h> // struct iovec
#  define bzero(buffer, nbytes) memset(buffer, 0, nbytes)
#  include <selectLib.h>
#  include <rtpLib.h>
#else
#  include <sys/uio.h> // struct iovec
#endif

#include <sys/stat.h>

// switches
#define USE_OWN_PROCESS_SHARED_IPC
#define USE_POSIX_SHM

typedef char* iov_base_t;
#if (_WRS_VXWORKS_MAJOR == 6 && _WRS_VXWORKS_MINOR <= 8)
typedef int os_socklen_t;
#else
typedef socklen_t os_socklen_t;
#endif
#define _ln_socket_t int

struct ln_pipe {
	int input_fd;
	int output_fd;
};
#define ln_pipe_invalid -1

#define MAX_SHARED_MEM_LEN 1023
// shared memory type
typedef struct {
	char name[MAX_SHARED_MEM_LEN + 1];
	unsigned int size;
	void* mem;
} ln_shared_mem_handle_t;

// semaphore type
typedef sem_t* ln_semaphore_handle_t;

// mutex type
typedef pthread_mutex_t ln_mutex_handle_t;
typedef pthread_cond_t ln_cond_handle_t;

// thread types
typedef pthread_t ln_thread_handle_t;
typedef void *(thread_func_type)(void*);
typedef void* thread_return_t;
#define THREAD_RETURN_NULL NULL

/*
typedef pthread_t tid_t;
#define gettid() pthread_self()
*/
int my_vxworks_tid();
typedef int tid_t;
#define gettid() my_vxworks_tid()
#define get_thread_id() gettid()
	
// no such thing in vxworks
char *strndup(const char *s, size_t n);
#define MAP_LOCKED 0
#define MAP_POPULATE 0

#define thread_cleanup_push pthread_cleanup_push
#define thread_cleanup_pop pthread_cleanup_pop

#define LOW_PRIO 35
#define LOW_PRIO_POLICY SCHED_OTHER

int ln_semaphore_timedwait_abs(ln_semaphore_handle_t handle, double abs_timeout);

#if !defined(IOV_MAX) || IOV_MAX == 0
#undef IOV_MAX
#define IOV_MAX 64
#endif

#ifdef __cplusplus
}
#endif

#endif // OS_VXWORKS_H
