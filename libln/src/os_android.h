#ifndef OS_ANDROID_H
#define OS_ANDROID_H

#include <pthread.h>
#include <sys/select.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>

#include "os_linux_futex.h"

// switches
#define USE_POSIX_SHM

typedef int ln_pipe_fd_t;

struct ln_pipe {
	ln_pipe_fd_t input_fd;
	ln_pipe_fd_t output_fd;
};
#define ln_pipe_invalid -1

typedef char* iov_base_t;
typedef socklen_t os_socklen_t;
#define _ln_socket_t int

#define MAX_SHARED_MEM_LEN 1022
// shared memory type
typedef struct {
	char name[MAX_SHARED_MEM_LEN + 1];
	unsigned int size;
	void* mem;
	int fd;
} ln_shared_mem_handle_t;

// semaphore type
typedef sem_t* ln_semaphore_handle_t;

// mutex type
typedef pthread_mutex_t ln_mutex_handle_t;
typedef pthread_cond_t ln_cond_handle_t;

// thread types
typedef struct {
	pthread_t handle;
	int cancel;
	pid_t tid;
} android_ln_thread_handle_t;
typedef android_ln_thread_handle_t* ln_thread_handle_t;
typedef void *(thread_func_type)(void*);
typedef void* thread_return_t;
#define THREAD_RETURN_NULL NULL

pid_t gettid();
typedef pid_t tid_t;
#define get_thread_id() gettid()

#ifndef USE_OWN_PROCESS_SHARED_IPC
 typedef pthread_mutex_t ln_process_shared_mutex_t;
 typedef pthread_cond_t ln_process_shared_cond_t;
#endif

#define thread_cleanup_push pthread_cleanup_push
#define thread_cleanup_pop pthread_cleanup_pop

#define LOW_PRIO 0
#define LOW_PRIO_POLICY SCHED_OTHER


#include <android/log.h>
//void ln_debug(const char* format, ...);

#endif // OS_ANDROID_H
