#ifndef SHAREDLNCLIENT_H
#define SHAREDLNCLIENT_H

#include "ln.h"
#include <string>

int sharedlnclient_init(std::string client_name, std::string ln_manager, ln_client* clnt, void* user_ptr, const char* hint);
int sharedlnclient_deinit(std::string client_name, void* user_ptr, const char* hint);

#endif // SHAREDLNCLIENT_H
