#include "ln.h"
#include "ln_messages.h"

#ifdef __WIN32__
#include <windows.h>
#else
#include <unistd.h>
#endif

void crossSleep(int sleepMs){
#ifdef __WIN32__
	Sleep(sleepMs);
#else
	usleep(sleepMs * 1000);
#endif
}

class app_class {
	ln::client clnt;
	ln::service* get_image_service;

public:
	app_class(int argc, char* argv[]) : clnt("beispiel_img_client", argc, argv) {
		get_image_service = clnt.get_service("beispiel.get_image", "beispiel/get_image", beispiel_get_image_signature);

		// clnt.needs_provider("beispiel.get_image", 10);
	}
  
	int run() {
		while(1) {
			crossSleep(2000);
			save_image();
		}
		return 0;
	}

	void save_image() {
		beispiel_get_image_t svc;
    
		svc.req.cam_id = 3;

		get_image_service->call(&svc);

		printf("got image with timestamp: %f\n", svc.resp.ts);
		printf("got %d bytes of images data. width: %d, height: %d\n", svc.resp.img_data_len, svc.resp.img_width, svc.resp.img_height);
	}
};

int main(int argc, char* argv[]) {
	app_class app(argc, argv);
	return app.run();
}
