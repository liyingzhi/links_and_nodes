#include <ln.h>
#include "ln_messages.h"

class app_class : public beispiel::get_image_base {
	ln::client clnt;
	
public:
	app_class(int argc, char *argv[]) : clnt("beispiel_img_provider", argc, argv) {
		register_get_image(&clnt, "beispiel.get_image");
	}
	
	virtual int on_get_image(ln::service_request &req, beispiel::get_image_t& data) {
		printf("user wants to have an image of cam_id %d\n", data.req.cam_id);

		// processing....
		data.resp.img_width = 300;
		data.resp.img_height = 400;
		uint8_t *img = (uint8_t *) malloc(data.resp.img_width * data.resp.img_height);
		// befuellen....

		// alle daten sind da -> fill resp
		data.resp.ts = (double) time(NULL);
		data.resp.img_data_len = data.resp.img_width * data.resp.img_height;
		data.resp.img_data = img;

		req.respond();
		free(img);
		return 0;
	}

	int run() {
		while (1) {
			/// processing...
			clnt.wait_and_handle_service_group_requests(NULL, -1); // -1 blocking, 0 non-blocking, >0 timeout in sekunden
		}
	}
};

int main(int argc, char *argv[]) {
	app_class app(argc, argv);
	return app.run();
}
