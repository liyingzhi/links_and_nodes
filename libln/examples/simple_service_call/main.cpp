#include <stdio.h>
#include <string>

#include <ln.h>
#include "ln_messages.h"

using namespace std;
using namespace robotkernel::canopen_protocol;

int main(int argc, char* argv[]) {
	ln::client clnt("simple_service_call", argc, argv);

	ln::service* read_element = clnt.get_service(
		"robotkernel.ethercat.slave_6.canopen_protocol.read_element",
		"canopen_protocol/read_element",
		robotkernel_canopen_protocol_read_element_signature);

	while(true) {
		read_element_t data;

		data.req.index = 0;
		data.req.sub_index = 0;
		
		read_element->call(&data);

		printf("state: 0x%04x\n", data.resp.state);
		
		string name(data.resp.name, data.resp.name_len);
		printf("name: %s\n", name.c_str());
		
		string value(data.resp.value, data.resp.value_len);
		printf("value: %s\n", value.c_str());
		// ...


		sleep(2);
	}
}
