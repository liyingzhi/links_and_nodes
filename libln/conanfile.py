import os
import sys
from conans import ConanFile, tools

class liblinks_and_nodes_conan(ConanFile):
    python_requires = "ln_conan/[~4]@common/unstable"
    python_requires_extend = "ln_conan.Base"

    name = "liblinks_and_nodes"
    homepage = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
    license = "LGPLv3"
    description = "links_and_nodes is a framework for easy creation and maintanance of distributed computing networks."
    settings = "os", "compiler", "build_type", "arch"
    exports = [ "../site_scons*" ]
    exports_sources = [
        "../SConstruct",
        
        "../libln*",
        # excludes are checked with fnmatch against results of positive patterns!
        "!libln/.*",
        "!libln/build/*",
        "!libln/tests/*",
        "!libln/examples/*",
            
        "../share*",
        
        "../python*",
        "../external*",
        "!python/links_and_nodes/*",
        "!python/links_and_nodes_manager/*",
        "!python/pyparsing/*",
        "!python/pyutils/*.pyc",
    ] + ["!%s" % x for x in tools.Git().excluded_files()]

    resdir = "share/libln"

    def init(self):
        base = self.python_requires["ln_conan"].module.Base
        base.set_attrs(self)
    
    def source(self):
        self.write_version_header("LIBLN_VERSION", os.path.join("libln", "include", "ln", "version.h"))
        self.write_version_file(os.path.join("libln", "version"))

    def build(self):
        self.scons_build("libln", opts="--use-private-libtomcrypt", verbose=True)

    def package(self):
        self.copy("version", src="libln", dst=self.resdir)
        install = os.path.join("build", "libln", "install", self.prefix[1:])
        self.copy("*", src=install, symlinks=True)
        
    def package_info(self):
        self.cpp_info.libs = ["ln", "rt"]
        self.cpp_info.includedirs = ["include" ]
        self.cpp_info.resdirs = [ self.resdir ]
        self.cpp_info.libdirs = ['lib']
        self.env_info.LD_LIBRARY_PATH.append(os.path.join(self.package_folder, "lib"))
