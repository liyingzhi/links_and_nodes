#include "ln_tests.h"
#include <vector>

struct test_case {
	const char* topic_name;
	const char* patterns;
	const char* expected;
	const char* description;
};


bool test(client* clnt) {
	const char* topic_name = "robot.topic.MARKER";

	std::vector<test_case> tests = {
		{ topic_name,
		  "^robot.t|It|c.MARKER$|cI|topic|TopiC|^ITopiCI$|topic1", "topic1",
		  "multiple: prefix, postfix, middle, full"
		},
		{ topic_name,
		  "robot.topic.MARKER|matched", "matched",
		  "full match"
		},
		{ topic_name,
		  "^robot.topic.MARKER$|matched", "matched",
		  "full match with marker"
		},
		{ topic_name,
		  "topic|TOPIC", "robot.TOPIC.MARKER",
		  "middle"
		},
		{ "mironame.some.miroSimSuffix",
		  ".miroSimSuffix$||^mironame.|gerd.", "gerd.some",
		  "multiple: postfix, prefix"
		},		
		{ "mironame_some.miroSimSuffix", 
		  "some.|", "mironame_miroSimSuffix",
		  "empty replace text"
		},
		{ topic_name,
		  ".MARKER$|", "robot.topic",
		  "postfix with marker"
		},
	};

	for(unsigned int i = 0; i < tests.size(); i++) {
		auto&& test = tests[i];
		char test_name[512];
		snprintf(test_name, 512, "test%d: %s", i, test.description);
		printf("\ntesting: %s\n", test_name);
		_read_name_mappings(clnt, test.patterns, NULL);	
		char* output = _get_topic_mapping(clnt, test.topic_name, test_name);
		if(!output || strcmp(output, test.expected)) {
			printf("ERROR: expected '%s' but got '%s' for input '%s' with patterns '%s'\n",
			       test.expected, output, test.topic_name, test.patterns);
			return false;
		}
		printf("ok\n");
		free(output);
		DESTRUCT_LIST_T(clnt, topic_mapping);
	}
	
	return true;
}
int main(int argc, char* argv[]) {
	client* clnt = NULL;
	_init_clnt_struct(&clnt, "test");
	clnt->debug = 1;
	
	bool success = test(clnt);
	
	ln_deinit((ln_client*)&clnt);
	
	if(!success)
		return 1;
	return 0;
}
