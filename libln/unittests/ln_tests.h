#ifndef LN_TESTS_H
#define LN_TESTS_H

#include <stdio.h>
#include <stdint.h>

#include "client.h"

#define EXPECT(expr, msg, ...) do if(!(expr)) { printf("ERROR: %s:%d: " msg "\n", __FILE__, __LINE__, ## __VA_ARGS__); return false; } while(0)
#define EXPECT_NON_NULL(expr, msg) EXPECT((expr) != NULL, msg)
#define EXPECT_CSTR(var, needed) EXPECT((var) && strcmp(var, needed) == 0, "expected '" # var "' to be '" needed "', got '%s'", output)

#endif /* LN_TESTS_H */

