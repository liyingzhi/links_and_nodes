package de.dlr.robotic.android_ln_test;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log; 
import de.dlr.robotic.ln.*;
import android.widget.TextView;

public class android_ln_test extends Activity
{
    private static final String LOGTAG = "android_ln_test"; 
    Thread inport_thread;
    Thread outport_thread;

    client clnt;

    public static void my_sleep(int ms) {
	try {
	    Thread.sleep(ms);
	}
	catch(Exception e) {
	    
	}
    }

    /** Called when the activity is first created. */
    @Override
	public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final TextView text = new TextView(getApplicationContext());
        setContentView(text);
	Log.d(LOGTAG, "onCreate!");

        boolean test_subscribe = false;
        boolean test_publish = false;
	boolean test_service = true;

	String default_manager = "192.168.192.1:46055";
	Log.d(LOGTAG, "client default_manager: " + default_manager);
	clnt = new client(LOGTAG, default_manager); // 10.0.2.2 is the emulator host //1
	Log.d(LOGTAG, "client connected.");
        
	if(test_service) { // service client test!
	    Log.d(LOGTAG, "test_service start");
	    //clnt.needs_provider("die eine zeit!");
	    ln_service_tests_time_service p = new ln_service_tests_time_service();
	    service time_svc = clnt.get_service("tests.time_service", "tests/time_service", p.signature);
	    System.out.println("before call service");

	    // now call the service
	    p.req.count = 1;
	    while(p.req.count < 10) {
		time_svc.call(p);
		p.req.count += 1;
		
		/*
		  p.req.shorts = new short[(int)p.req.count];
		  for(int i = 0; i < p.req.shorts.length; i++) {
		  p.req.shorts[i] = (short)(i * 2);
		  }
		*/

		
		Log.d(LOGTAG, "service response: time: " + p.resp.time + " count: " + p.resp.count);
		my_sleep(500);
		/*
		  Log.d(LOGTAG, "  shorts_len " + p.resp.shorts_len + " shorts.size: " + p.resp.shorts.length);
		  for(int i = 0; i < p.resp.shorts.length; i++) {
		  Log.d(LOGTAG, " short[" + i + "] = " + p.resp.shorts[i]);
		  }
		*/		    
		
	    }
	}
        if (test_subscribe) {
	    inport_thread = new Thread() {
		    public void run() {
	                inport inp = clnt.subscribe("tests.counters", "tests/counters", 10);
	                Log.d(LOGTAG, "have inport!");
			ln_packet_tests_counters p = new ln_packet_tests_counters();
			int counter = 0;
			while(counter < 100) {
			    counter++;
			    Log.d(LOGTAG, "will block on new packet!");
			    inp.read(p, true);
			    p.interpret(); // now unmarshal packet
			    //Log.d(LOGTAG, "got packet: counter: " + p.counter + " all_joints[0] " + p.all_joints[0] + " commanded_position.left_arm[0] " + p.commanded_position.left_arm[0]);
			}
			Log.d(LOGTAG, "going to unsubscribed topic...");
			clnt.unsubscribe(inp);
		    }
	        };
            try {
                inport_thread.start();
                inport_thread.join();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
	if (test_publish) {
	    outport_thread = new Thread() {
		    public void run() {
			outport oup = clnt.publish("tests.counters", "tests/counters");
			Log.d(LOGTAG, "have outport!");
			ln_packet_tests_counters p = new ln_packet_tests_counters();
			int counter = 0;
			while(counter < 10) {
			    counter++;
			    p.counter++;
			    p.pack();
			    Log.d(LOGTAG, "will write new packet!");
			    oup.write(p, System.currentTimeMillis());
			    //Log.d(LOGTAG, "published packet: counter: " + p.counter + " all_joints[0] " + p.all_joints[0] + " commanded_position.left_arm[0] " + p.commanded_position.left_arm[0]);
			}
			Log.d(LOGTAG, "going to unpublish topic...");
			clnt.unpublish(oup); //hangs here!
		    }
		};
	    try {
		outport_thread.start();
		outport_thread.join();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}


	Log.d(LOGTAG, "sleeping");
	my_sleep(50000);
	Log.d(LOGTAG, "going to delete client...");
	clnt.delete();
	Log.d(LOGTAG, "really deleted client");
	
    }//end onCreate()




    //		ln::service* time_service = clnt.get_service("tests.time_service", "tests/time_service", ln_service_tests_time_service_signature);
    //
    //		// int max = 10;
    //		// while(keep_running && max--) {
    //		ln_service_tests_time_service svc;
    //		svc.req.count = 0;
    //		while(keep_running) {
    //			svc.req.count ++;
    //			time_service->call(&svc);
    //
    //			printf("time: %.6f, request: %lld, response: %lld\n", svc.resp.time, svc.req.count, svc.resp.count);
    //			sleep(1);
    //		}




}
