#include <vector>
#include <ln.h>

#include "os.h"

using namespace std;

cond_handle_t cond;
mutex_handle_t mutex;

thread_return_t call_member(void*);

class thread {
public:
	unsigned int id;
	thread_handle_t thread_handle;
	
	thread(unsigned int id) : id(id) {
		int ret;
		ret = create_thread(&thread_handle, call_member, this);
		if(ret)
			printf("failed to create thread: %d\n", ret);
	}

	void run() {
		while(true) {
			lock_mutex(&mutex);
			printf("thread %d waiting\n", id);
			cond_wait(&cond, &mutex);
			printf("thread %d woke up!\n", id);
			unlock_mutex(&mutex);
						
			sleep_seconds(1);
		}
	}
};

thread_return_t call_member(void* data) {
	thread* t = (thread*)data;
	t->run();
	
	return 0;
}

int main(int argc, char* argv[]) {
	int ret;

	ret = create_cond(&cond);
	if(ret) {
		printf("failed to create cond: %d\n", ret);
		return -1;
	}

	ret = create_mutex(&mutex);
	if(ret) {
		printf("failed to create mutex: %d\n", ret);
		return -1;
	}

	// spawn threads
	unsigned int N = 5;
	vector<thread*> threads;
	for(unsigned int i = 0; i < N; i++)
		threads.push_back(new thread(i));


	for(unsigned int i = 0; i < 10; i++) {
		sleep_seconds(0.15);
		printf("signal!\n");
		cond_signal(&cond);
	}
	
	printf("main exiting\n");
	return 0;
}
