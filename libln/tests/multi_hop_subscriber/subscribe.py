#!/usr/bin/python

import time
import sys
import links_and_nodes as ln

topic_name = sys.argv[1]
clnt = ln.client("subscriber", sys.argv)

port = clnt.subscribe(topic_name, "test/topic")

print "ready"

while True:
    block_start = time.time()
    port.read(7)
    dt = time.time() - block_start
    print "%s received %d after %.2fs" % (port.topic_name, port.packet.count, dt)
