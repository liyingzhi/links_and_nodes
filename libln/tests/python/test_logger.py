#!/usr/bin/python
# -*- mode: python -*-

import sys
import time
import links_and_nodes as ln

from numpy import *

def print_data(data):
    print "have %d topics in log:" % len(data.topics)
    for topic_name, tc in data.topics.iteritems():
        print "  topic %r with %d samples of size %d each:" % (
            topic_name, tc.topic.n_samples, tc.topic.sample_size)

        for i in xrange(tc.topic.n_samples):
            s = tc.get_sample(i)
            print "    cnt: %d, src_ts: %.3f, log_ts: %.3fs :: duration: %f, counter: %d" % (
                s.packet_counter,
                s.src_ts,
                s.log_ts,

                s.duration,
                s.counter)

def main(args):
    clnt = ln.client("log test", args)
    
    #mode = "create"
    mode = "load"

    if mode == "create":
        logger = clnt.get_logger("test logger")
        print "logger.name", logger.name
        logger.add_topic("tests.counters", 100000)
        logger.add_topic("tests.slow_counters", 100000)

        print "now enable logging"
        logger.enable()

        print "logging..."
        time.sleep(0.1)

        print "stop logging"
        logger.disable()

        print "saving file at mangers"
        #logger.manager_save("/tmp/test.pickle")

        print "downloading data..."
        data = logger.download()
        print "done."
        
        print "saving on client side"
        data.save("/tmp/testpy")
    elif mode == "load":
        data = ln.logger_data.load("/tmp/testpy")
    else:
        raise Exception("wrong mode")

    print_data(data)

if __name__ == "__main__":
   main(sys.argv)
