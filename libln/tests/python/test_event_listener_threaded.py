#!/usr/bin/python

import sys
import os
import gtk
import glib
import time
import Queue
import links_and_nodes as ln

# http://www.pygtk.org/reference.html

class gui(object):
    def __init__(self):
        self.clnt = ln.client("events test", sys.argv)
        self.connection = self.clnt.connect_to_event(
            "ln.resource_event", "ln/resource_event", self.on_ln_resource_event,
            event_pattern="topic*",
            name_pattern="*",
            md_pattern="*",
            client_pattern="*"
        )
        # https://docs.python.org/2/library/queue.html
        self.new_topics = Queue.Queue() # only this is accessed from multiple threads!
        self.lost_topics = Queue.Queue() # only this is accessed from multiple threads!
        self.clnt.handle_service_group_in_thread_pool(None, "main pool");
        
        # only accessed from gui-thread:
        self.known_topics = []
        
        # init gui...
        self.window = gtk.Window()
        self.label = gtk.Label()
        self.label.set_text("no update yet!")
        self.window.add(self.label)
        self.window.show_all()
        glib.timeout_add(int(1000/10.), self.check_new_events)

    def run(self):
        # start gui main loop
        #while True:
        #    time.sleep(2)
        #    print "idle"
        glib.threads_init()
        gtk.main()

    def check_new_events(self):
        # wird haben GIL!
        # wird aus dem GUI thread aufgerufen!

        if self.new_topics.empty() and self.lost_topics.empty():
            # nothing to update
            return True

        # fetch all new topics and update gui!
        text = []
        while True:
            try:
                name, md = self.new_topics.get(False) # non blocking
            except Queue.Empty:
                break # no more new topics
            self.known_topics.append((name, md))
            text.append("new topic %r: %s" % (name, md))        
        self.label.set_text("new topics: %s" % ("\n".join(text), ))

        # remove topics from self.known_topics(/or other gui elements) that disappeared
        while True:
            try:
                rm_name = self.lost_topics.get(False)
            except Queue.Empty:
                break
            # just flush read queue for now...
            for i, (name, md) in enumerate(self.known_topics):
                if name == rm_name:
                    del self.known_topics[i]
                    break
        return True
        
    def on_ln_resource_event(self, ev):
        # wir haben den GIL!
        # wird aus einem NICHT-GUI thread aufgerufen!!
        # boese: self.label.set_text("12")
        # das ist ok:
        print "received event: %s, name: %s, md: %s, client: %s" % (
            ev.event,
            ev.name,
            ev.md,
            ev.client)
        if ev.event == "topic_new_publisher":
            self.new_topics.put((ev.name, ev.md))
        elif ev.event == "topic_del_publisher":
            self.lost_topics.put(ev.name)
            

if __name__ == "__main__":
    g = gui()
    g.run()
        
