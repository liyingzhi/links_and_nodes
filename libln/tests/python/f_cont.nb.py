## nb properties ##
{'current_cell': 3, 'window_position': (2235, -4), 'window_size': (751, 1055)}
## nb properties end ##
## cell 0 input ##
import scipy.io


## cell 0 end ##
## cell 1 input ##
m = random.random((10, 2)) * 100
% m.shape, m.dtype
% m.flags
scipy.io.savemat("test.mat", dict(m=m))
org_m = m
## cell 1 output ##
# m.shape, m.dtype: ((10, 2), dtype('float64'))
# m.flags:   C_CONTIGUOUS : True
#   F_CONTIGUOUS : False
#   OWNDATA : True
#   WRITEABLE : True
#   ALIGNED : True
#   UPDATEIFCOPY : False
# 
## cell 1 end ##
## cell 2 input ##
m = scipy.io.loadmat("test.mat")["m"]
% m.shape, m.dtype
% m.flags
## cell 2 output ##
# m.shape, m.dtype: ((10, 2), dtype('<f8'))
# m.flags:   C_CONTIGUOUS : False
#   F_CONTIGUOUS : True
#   OWNDATA : False
#   WRITEABLE : True
#   ALIGNED : False
#   UPDATEIFCOPY : False
# 
## cell 2 end ##
## cell 3 input ##
md_name = clnt.put_message_definition("test_fcont", """
double row[2]
""")
%% md_name
port = clnt.publish("fcont", md_name)
p = port.packet
## cell 3 output ##
# md_name: 'gen/test_fcont'
# 
## cell 3 end ##
## cell 4 input ##
import sys
% sys.getrefcount(org_m)
## cell 4 output ##
# sys.getrefcount(org_m): 2
# 
## cell 4 end ##
## cell 5 input ##
# here be dragons!!!

#p.row = org_m[1]
p.row = m[1]
%% p.row
port.write()
## cell 5 output ##
# p.row: 
# array([ 68.66343107,  17.91988787])
# write data: bytearray(b'0\x01\x92\xa7u*Q@M\xa1o\xc5}\xeb1@')
# 
## cell 5 end ##
