#!/usr/bin/python

import os
import sys
import time
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)

logger = clnt.get_logger("flo test logger")

print "now stopping log!"
logger.disable()

fn = os.path.join(os.getcwd(), "out.mat")
print "now ask manager to save to mat file %r!" % fn
logger.manager_save(fn)

print "load mat file (assuming we're on the same filesystem as the manager):"
import scipy.io
data = scipy.io.loadmat(fn)
print "key's in log-data:"
k = data.keys()
k.sort()
for entry in k:
    if entry.startswith("_"): continue
    print "  %s %s" % (entry, data[entry].shape)
