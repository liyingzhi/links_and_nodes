#!/usr/bin/python

import os
import sys
import time
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)

logger = clnt.get_logger("flo test logger")
logger.add_topic("log1", 5000)

print "now starting log!"
logger.enable()
