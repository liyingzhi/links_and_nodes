#!/usr/bin/python

import os
import sys
import time
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)

logger = clnt.get_logger("flo test logger")
logger.add_topic("log1", 5000)
logger.add_topic("log2", 5000)

print "now starting log!"
logger.enable()

print "waiting..."
time.sleep(10)

print "now stopping log!"
logger.disable()

if False:
    # save as mat file
    fn = os.path.join(os.getcwd(), "out.mat")
    print "now ask manager to save to mat file %r!" % fn
    logger.manager_save(fn)

    print "load mat file (assuming we're on the same filesystem as the manager):"
    import scipy.io
    data = scipy.io.loadmat(fn)
    print "key's in log-data:"
    k = data.keys()
    k.sort()
    for entry in k:
        if entry.startswith("_"): continue
        print "  %s %s" % (entry, data[entry].shape)
else:
    print "downloading data..."
    data = logger.download()
    
    fn = os.path.join(os.getcwd(), "out.lnlog")
    print "now save to ln-log file %r!" % fn
    data.save(fn)

