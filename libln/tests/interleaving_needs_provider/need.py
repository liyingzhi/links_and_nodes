#!/usr/bin/python

import time
import sys
import links_and_nodes as ln

what, when = sys.argv[1:]

clnt = ln.client("need", sys.argv)

print "ready"
time.sleep(float(when))

print "need provider", what
clnt.needs_provider(what, -1)
print "got provider", what
