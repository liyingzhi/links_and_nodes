#include <stdio.h>
#include <unistd.h>

#include <string>

#include "ln.h"
#include "ln_messages.h"

using namespace std;

void print_data(ln::logger_data* data) {
	printf("have %d topics in log:\n", (unsigned int)data->topics.size());
	// now do something with data
	for(ln::logger_data::topic_iterator i = data->topics.begin(); i != data->topics.end(); ++i) {
		ln::logger_data::topic_cursor* c = *i;
		printf("  topic %s with %d samples of size %d each (divisor %d):\n", 
		       c->name.c_str(), 
		       c->topic->n_samples,
		       c->topic->sample_size,
		       c->topic->divisor);

		for(unsigned int k = 0; k < c->topic->n_samples; k++) {
			logger_sample* s = c->get_sample(k);
			printf("    cnt: %d, src_ts: %.3f, log_ts: %.3fs :: ",
			       s->packet_counter,
			       s->src_ts,
			       s->log_ts);

			// now read contents of packet:
			tests_counters_t* p = (tests_counters_t*)&s->data;
			printf("duration: %f, counter: %d\n",
			       p->duration, p->counter);
		}
	}
}

int main(int argc, char* argv[]) {
	ln::client clnt("test_logger", argc, argv);

	vector<string> args = clnt.get_remaining_args();
	//string mode = "load"; 
	//string mode = "create";
	string mode = "create";

	if(args.size() > 1)
		mode = args[1];
	printf("mode: %s\n", mode.c_str());
	
	ln::logger_data* data;

	if(mode == "create") { 
		ln::logger* l = clnt.get_logger("test logger");
		unsigned int N = 100000;
		l->add_topic("tests.counters", N);
		l->add_topic("tests.slow_counters", N);
		
		printf("will now enable logging\n");
		l->enable();
		
		printf("logging in progress\n");
		// usleep(200000);
		sleep(10);
		
		printf("will now disable logging\n");
		l->disable();
		
		// printf("saving file on manager\n");
		// l->manager_save("/tmp/test.mat", "mat4");
		
		printf("direct_downloading data\n");
		data = l->direct_download();

		printf("saving file on client\n");
		data->save("testlog");
		

	} else if(mode == "load") {
		data = ln::logger_data::load("testlog");
		
	}

	print_data(data);
	
	if(mode == "load")
		delete data;

	/*
	data = ln::logger_data::load("testlog");
	print_data(data);
	delete data;
	*/
	
	return 0;
}
