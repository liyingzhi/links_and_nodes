#include <stdio.h>
#include <signal.h>

#include "ln.h"
#include "ln_messages.h"

#include <vector>

using namespace std;

vector<char> data;

unsigned int sigint_count = 0;
void on_sigint(int signo) {
	sigint_count += 1;
}

int on_intr_test_call(ln::client& clnt, ln::service_request& req, void* user_data) {
	ln_service_tests_intr_test_call svc;
	req.set_data(&svc, ln_service_tests_intr_test_call_signature);

	printf("call_counter: %d, sigint count: %d\n", svc.req.call_counter, sigint_count);
	svc.resp.response_counter = svc.req.call_counter;
	svc.resp.pid = getpid();
	svc.resp.data = &data[0];
	svc.resp.data_len = data.size();
	req.respond();
	return 0;
}

int main(int argc, char* argv[]) {
	unsigned int response_size = 1024;
	
	ln::client clnt("provider", argc, argv);
	
	list<string> args = clnt.get_remaining_args();
	bool skip = false;
	for(list<string>::iterator i = args.begin(); ; ) {
		if(!skip)
			++i;
		else
			skip = false;
		if(i == args.end())
			break;
		string& arg = *i;
		if(arg == "-size") {
			i++;
			if(i == args.end())
				throw ln::exception("number after -size argument missing!");
			response_size = atoi(i->c_str());
		}
	}
	
	printf("response size: %d\n", response_size);
	data.resize(response_size);

	// register sigint handler
	signal(SIGINT, on_sigint);
	
	ln::service* svc = clnt.get_service_provider("intr_test_call", "tests/intr_test_call", ln_service_tests_intr_test_call_signature);
	svc->set_handler(on_intr_test_call, NULL);
	svc->do_register();
	
	printf("ready...\n");
	while(true)
		clnt.wait_and_handle_service_group_requests(std::nullptr);

	return 0;
}
