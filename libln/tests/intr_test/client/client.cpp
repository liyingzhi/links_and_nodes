#include <stdio.h>
#include <signal.h>

#include "ln.h"
#include "ln_messages.h"

#include <vector>

using namespace std;

unsigned int sigint_count = 0;
void on_sigint(int signo) {
	sigint_count += 1;
}

int main(int argc, char* argv[]) {
	unsigned int request_size = 1024;
	
	ln::client clnt("client", argc, argv);
	
	list<string> args = clnt.get_remaining_args();
	bool skip = false;
	for(list<string>::iterator i = args.begin(); ; ) {
		if(!skip)
			++i;
		else
			skip = false;
		if(i == args.end())
			break;
		string& arg = *i;
		if(arg == "-size") {
			i++;
			if(i == args.end())
				throw ln::exception("number after -size argument missing!");
			request_size = atoi(i->c_str());
		}
	}
	
	printf("request size: %d\n", request_size);
	vector<char> data;
	data.resize(request_size);

	// register sigint handler
	signal(SIGINT, on_sigint);

	ln::service* intr_test_call = clnt.get_service("intr_test_call", "tests/intr_test_call", ln_service_tests_intr_test_call_signature);
	ln_service_tests_intr_test_call svc;
	svc.req.call_counter = 0;
	svc.req.data = &data[0];
	svc.req.data_len = data.size();
	
	printf("ready...\n");
	unsigned int last_sigint_count = sigint_count;
	while(true) {
		svc.req.call_counter += 1;
		intr_test_call->call(&svc);
		if(svc.req.call_counter != svc.resp.response_counter)
			printf("error, sent counter %d received counter %d - delta: %d\n",
			       svc.req.call_counter, svc.resp.response_counter, svc.resp.response_counter - svc.req.call_counter);
		if(last_sigint_count != sigint_count) {
			last_sigint_count = sigint_count;
			printf("had %d sigints!\n", last_sigint_count);
		}
	}
	
	return 0;
}
