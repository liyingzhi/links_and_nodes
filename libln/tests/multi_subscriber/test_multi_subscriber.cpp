
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "ln.h"
#include "ln_messages.h"

bool keep_running = true;

ln_client clnt;

void stop_handler(int signo) {
	keep_running = false;
	printf("subscriber exiting by signal!\n");
	ln_deinit(&clnt);
	exit(0);
}

int main(int argc, char* argv[]) {
	int ret;
	double rate = 100;
	unsigned int i = 0;
	unsigned int n = 10;

	for(i = 1; i < (unsigned)argc; i++)
		if(!strcmp(argv[i], "-rate") && (i + 1) < (unsigned)argc)
			rate = atof(argv[++i]);
			
	if((ret = ln_init(&clnt, "test multi subscriber", argc, argv))) {
		printf("could not init ln_client. returned error %d: %s\n", -ret, ln_format_error(ret));
		if(clnt)
			printf("dynamic error: %s\n", ln_get_error_message(clnt));
		return -1;
	}

	// register publish/output-port
	ln_inport port[3];
	if((ret = ln_subscribe(clnt, "tests.counters", "tests/counters", &port[0], rate, 0))) {
		printf("ln_subscribe error %d: %s\n", -ret, ln_format_error(ret));
		goto error_exit;
	}
	if((ret = ln_subscribe(clnt, "fast_counters", "tests/counters", &port[1], rate, 0))) {
		printf("ln_subscribe error %d: %s\n", -ret, ln_format_error(ret));
		goto error_exit;
	}
	if((ret = ln_subscribe(clnt, "cpp_counters", "tests/counters", &port[2], rate, 0))) {
		printf("ln_subscribe error %d: %s\n", -ret, ln_format_error(ret));
		goto error_exit;
	}

	ln_packet_tests_counters p;

	if(ln_get_message_size(port[0]) != sizeof(p)) 
		printf("warning: sizeof(ln_packet_tests_counters): %d, size from manager: %d\n", sizeof(p), ln_get_message_size(port[0]));

	ln_multi_waiter waiter;
	if((ret = ln_multi_waiter_init(clnt, &waiter))) {
		printf("ln_multi_waiter_init error %d: %s\n", -ret, ln_format_error(ret));
		goto error_exit;
	}
	// registers ports to wait on with this multi waiter
	for(unsigned int i = 0; i < 3; i++) {
		if((ret = ln_multi_waiter_add_port(waiter, port[i])) < 0) {
			printf("ln_multi_waiter_add_port %d error %d: %s\n", i, -ret, ln_format_error(ret));
			goto error_exit;
		}
	}

	while(n--) {
		printf("\nwait!\n");
		if((ret = ln_multi_waiter_wait(waiter, -1)) < 0) {
			printf("ln_multi_waiter_wait error %d: %s\n", -ret, ln_format_error(ret));
			goto error_exit;
		}

		for(unsigned int i = 0; i < 3; i++) {
			ln_inport inport = port[i];
			if(!ln_multi_waiter_can_read(waiter, inport)) {
				printf("port %d has nothing to read...\n", i);
				continue;
			}
			// this inport can read without blocking!
			
			double ts;
			ret = ln_read(inport, &p, sizeof(p), &ts, 1); // unblocking read here!! (blocking read might also work)
			if(ret < 0) {
				printf("ln_read port %d error %d: %s\n", i, -ret, ln_format_error(ret));
				goto error_exit;
			}
			if(ret == 0) { // no new packet!
				printf("port %d: no new packet!?\n", i);
				continue;
			}
				
			printf("port %d: ret: %d, counter: %d, timestmap: %f, message: '%s'\n", i, ret, p.counter, ts, p.message);
		}
	}
	goto exit;

error_exit:
	if(ln_get_error_message(clnt))
		printf("%s\n", ln_get_error_message(clnt));

exit:

	printf("subscriber exiting\n");
	ln_deinit(&clnt);

	return 0;
}

