#!/usr/bin/python

import sys
import time
import links_and_nodes as ln
from math import *

clnt = ln.client(sys.argv[0], sys.argv)
args = clnt.get_remaining_args()

if len(args) > 1:
    topic = args[1]
else:
    topic = "from_simulink"

if len(args) > 2:
    md = args[2]
else:
    md = "test_msr_simulink"

port = clnt.subscribe(topic, md)
print "ready"
print port.packet
print port.msg_def_hash

while True:
    port.read()
    print port.packet
