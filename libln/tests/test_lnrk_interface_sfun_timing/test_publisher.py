#!/usr/bin/python

import sys
import time
import links_and_nodes as ln
from math import *

clnt = ln.client(sys.argv[0], sys.argv)
args = clnt.get_remaining_args()
if len(args) > 1:
    topic = args[1]
else:
    topic = "test_msr"

port = clnt.publish(topic, "test_msr")
n_slaves = 3
scnt = 0
print "ready"
while True:
    scnt += 1
    for s in range(n_slaves):
        slv = port.packet.slaves[s]
        slv.state = scnt;
        slv.pos = sin(float(slv.state) / (s + 1) / 100)
    port.write()
    time.sleep(0.001)
