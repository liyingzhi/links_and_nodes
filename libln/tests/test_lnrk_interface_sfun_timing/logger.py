#!/usr/bin/python

import links_and_nodes as ln
import sys

clnt = ln.client("logger", sys.argv)

logger = clnt.get_logger("test logger")

if sys.argv[1] == "start":
    N = 1000
    logger.add_topic("test_msr", N, 1)
    logger.add_topic("test_cmd", N, 1)
    print "enable"
    logger.enable()

elif sys.argv[1] == "stop":
    print "disable"
    logger.disable()
    logger.manager_save(sys.argv[2])

print "done"
