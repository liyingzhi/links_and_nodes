#include <stdio.h>
#include <ln.h>
#include <string_util/string_util.h>

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "ln_messages.h"

#include <list>
#include <string>

using namespace std;
using namespace string_util;

#define retval_check(function, args...) {						\
		int ret = function(args);						\
		if(ret)									\
			throw retval_exception_tb(ret, #function "(" #args ")");        \
	}



class worker {
	ln_client clnt;
	ln_service svc;
	pthread_t tid;
	
	bool keep_running = false;
	bool started = false;
	int i = 0;
public:
	worker(ln_client clnt, unsigned int sid) : clnt(clnt) {
		retval_check(ln_service_init, clnt, &svc, format_string("test%d", sid).c_str(), "test_md", test_md_signature);
	}
	~worker() {
		printf("worker %p did %d calls\n", this, i);
		stop();		
		retval_check(ln_service_deinit, &svc);
	}

	void start() {
		keep_running = true;
		pthread_create(&tid, NULL, worker::worker_thread, this);
		started = true; // race
	}
	unsigned int stop() {
		if(started) {
			started = false; // race
			keep_running = false;
			pthread_join(tid, NULL);
		}
		return i;
	}
		
	static void* worker_thread(void* data) {
		((worker*)data)->run();
		return NULL;
	}

	void run() {
		i = 0;
		while(keep_running) {
			random_sleep(1000); // 1ms
			i ++;
			
			// call1();
			call2();
		}
	}

	unsigned int n_busy_loop = 0;
	void random_sleep(unsigned int us) {
		if(n_busy_loop) {
			n_busy_loop --;
			return;
		}
		struct timespec ts = { 0, ((rand() % us) * 1000) };
		if(ts.tv_nsec < (us / 100)) {
			n_busy_loop = us % 10;
			return;
		}
		nanosleep(&ts, NULL);
	}

	string random_string() {
		//unsigned int len = (rand() % 32) + 1; // 1..32
		unsigned int len = (rand() % 3200) + 1; // 1..32
		string ret(len, ' ');
		for(unsigned int i = 0; i < len; i++)
			ret[i] = rand() % (90-65) + 65;
		return ret;
	}

	void call1() {
		test_md_t data;
		memset(&data, 0, sizeof(data));

		string s1 = format_string("s1: %s ", random_string().c_str());
		string s2 = format_string("s2: %s ", random_string().c_str());
		string s3 = format_string("s3: %s ", random_string().c_str());

		// explicitely do another dynamic mem alloc here:
		data.req.s1 = strdup(s1.c_str()); data.req.s1_len = strlen(data.req.s1);
		data.req.s2 = strdup(s2.c_str()); data.req.s2_len = strlen(data.req.s2);
		data.req.s3 = strdup(s3.c_str()); data.req.s3_len = strlen(data.req.s3);

		data.req.a = i;
		data.req.b = i * i;
		data.req.c = i * i * i;

		int ret = ln_service_call(svc, &data);

		if(ret < 0) { // LNE_* error
			string err(ln_get_error_message(clnt));
			throw str_exception_tb("ln error %d in call: %s", ret, err.c_str());
		}
		else if(ret > 0) // custom / user error
			throw str_exception_tb("user defined error %d in call!", ret);

		free(data.req.s1);
		free(data.req.s2);
		free(data.req.s3);

		// check result
		if(data.resp.error_message_len) {
			string error(data.resp.error_message, data.resp.error_message_len);
			throw str_exception_tb("have error message:\n%s", error.c_str());
		}
		stringstream expected;
		expected << s1;
		expected << s2;
		expected << s3;
		expected << format_string(" %d %d %d", i, i * i, i * i * i);
		string resp_data(data.resp.data, data.resp.data_len);
		if(resp_data != expected.str())
			throw str_exception_tb("expected this string:\n%s\ngot this:\n%s",
					       repr(expected.str()).c_str(),
					       repr(resp_data).c_str());
		int expected_d = i;
		expected_d += i * i;
		expected_d += i * i * i;
		if(expected_d != data.resp.d)
			throw str_exception_tb("expected this d: %d, got %d", expected_d, data.resp.d);		
	}
	void call2() {
		test_md_t data;
		memset(&data, 0, sizeof(data));

		string s1 = format_string("s1: %s ", random_string().c_str());
		string s2 = format_string("s2: %s ", random_string().c_str());
		string s3 = format_string("s3: %s ", random_string().c_str());

		// explicitely do another dynamic mem alloc here:
		data.req.s1 = strdup(s1.c_str()); data.req.s1_len = strlen(data.req.s1);
		data.req.s2 = strdup(s2.c_str()); data.req.s2_len = strlen(data.req.s2);
		data.req.s3 = strdup(s3.c_str()); data.req.s3_len = strlen(data.req.s3);

		data.req.a = i;
		data.req.b = i * i;
		data.req.c = i * i * i;

		vector<struct iovec> iov;
		vector<uint8_t> element_lens;
		iov.push_back({ &data.req.s1_len, sizeof(data.req.s1_len) }); element_lens.push_back(sizeof(data.req.s1_len));
		iov.push_back({ data.req.s1, data.req.s1_len }); element_lens.push_back(1);
		
		iov.push_back({ &data.req.s2_len, sizeof(data.req.s2_len) }); element_lens.push_back(sizeof(data.req.s2_len));
		iov.push_back({ data.req.s2, data.req.s2_len }); element_lens.push_back(1);
		
		iov.push_back({ &data.req.s3_len, sizeof(data.req.s3_len) }); element_lens.push_back(sizeof(data.req.s3_len));
		iov.push_back({ data.req.s3, data.req.s3_len }); element_lens.push_back(1);

		iov.push_back({ &data.req.a, sizeof(data.req.a) }); element_lens.push_back(sizeof(data.req.a));
		iov.push_back({ &data.req.b, sizeof(data.req.b) }); element_lens.push_back(sizeof(data.req.b));
		iov.push_back({ &data.req.c, sizeof(data.req.c) }); element_lens.push_back(sizeof(data.req.c));

		char* response_buffer = NULL;
		uint32_t response_len = 0;
		uint8_t need_swap = 0;
		int ret = ln_service_callv(svc, &iov[0], iov.size(), &element_lens[0], &response_buffer, &response_len, &need_swap);
		
		if(ret < 0) { // LNE_* error
			string err(ln_get_error_message(clnt));
			throw str_exception_tb("ln error %d in call: %s", ret, err.c_str());
		}
		else if(ret > 0) // custom / user error
			throw str_exception_tb("user defined error %d in call!", ret);

		free(data.req.s1);
		free(data.req.s2);
		free(data.req.s3);

		// check result
		char* rp = response_buffer;
		data.resp.error_message_len = *(uint32_t*)rp; rp += sizeof(uint32_t);
		data.resp.error_message = (char*)rp; rp += data.resp.error_message_len;

		data.resp.data_len = *(uint32_t*)rp; rp += sizeof(uint32_t);
		data.resp.data = (char*)rp; rp += data.resp.data_len;
		
		data.resp.d = *(int32_t*)rp; rp += sizeof(int32_t);
		
		if(data.resp.error_message_len) {
			string error(data.resp.error_message, data.resp.error_message_len);
			throw str_exception_tb("have error message:\n%s", error.c_str());
		}
		stringstream expected;
		expected << s1;
		expected << s2;
		expected << s3;
		expected << format_string(" %d %d %d", i, i * i, i * i * i);
		string resp_data(data.resp.data, data.resp.data_len);
		if(resp_data != expected.str())
			throw str_exception_tb("expected this string:\n%s\ngot this:\n%s",
					       repr(expected.str()).c_str(),
					       repr(resp_data).c_str());
		
		int expected_d = i;
		expected_d += i * i;
		expected_d += i * i * i;
		if(expected_d != data.resp.d)
			throw str_exception_tb("expected this d: %d, got %d", expected_d, data.resp.d);		
	}
};

int main(int argc, char* argv[]) {
	ln_client clnt;

	retval_check(ln_init, &clnt, "ich", argc, argv);

	unsigned int n_worker = 64;
	unsigned int n_services = 8;

	if(argc > 1)
		n_worker = atoi(argv[1]);
	if(argc > 2)
		n_services = atoi(argv[2]);
	
	list<worker*> workers;
	
	for(unsigned int i = 0; i < n_worker; i++) {
		worker* w = new worker(clnt, i % n_services);
		workers.push_back(w);
	}

	double start_time = ln_get_time();
	for(auto& i : workers)
		i->start();	
	
	printf("press enter to stop workers...\n");
	getchar();

	unsigned int total_calls = 0;
	for(auto& i : workers) {
		total_calls += i->stop();
		delete i;
	}
	double duration = ln_get_time() - start_time;
	printf("had %.1fk calls per second\n", total_calls / duration / 1000.);
	
	return 0;
}
