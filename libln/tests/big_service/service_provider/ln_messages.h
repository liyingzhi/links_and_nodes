#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/*
  /volume/USERSTORE/schm_fl/ln_base/message_definitions/ln/string
  string 8 bytes
*/
typedef struct __attribute__((packed)) {
	uint32_t string_len;
	char* string;
} _large_call_string;

/*
  /volume/USERSTORE/schm_fl/ln_base/library/tests/big_service/message_definition/large_call
  large_call
*/
typedef struct __attribute__((packed)) {
	struct __attribute__((packed)) {
		uint32_t names_len;
		_large_call_string* names;
	} req;
	struct __attribute__((packed)) {
		uint32_t values_len;
		_large_call_string* values;
	} resp;
} ln_service_large_call;
#define ln_service_large_call_signature "uint32_t 4 1,[uint32_t 4 1,char* 1 1]* 8 1|uint32_t 4 1,[uint32_t 4 1,char* 1 1]* 8 1"

#ifdef __cplusplus
class ln_service_large_call_base {
public:
	virtual ~ln_service_large_call_base() {
#ifdef LN_UNREGISTER_SERVICE_IN_BASE_DETOR            
		unregister_large_call();
#endif
	}
private:
	static int large_call_cb(ln::client&, ln::service_request& req, void* user_data) {
		ln_service_large_call_base* self = (ln_service_large_call_base*)user_data;
		ln_service_large_call svc;
		req.set_data(&svc, ln_service_large_call_signature);
		memset(&svc.resp, 0, sizeof(svc.resp));
		return self->on_large_call(req, svc);
	}
protected:
	ln::service* large_call_service;
	ln_service_large_call_base() : large_call_service(NULL) {};
	void unregister_large_call() {
		if(large_call_service) {
			large_call_service->clnt->unregister_service_provider(large_call_service);
			large_call_service = NULL;
		}
	}
	void register_large_call(ln::client* clnt, const std::string service_name) {
		large_call_service = clnt->get_service_provider(
			service_name,
			"large_call", 
			ln_service_large_call_signature);
		large_call_service->set_handler(&large_call_cb, this);
		large_call_service->do_register();
	}
	virtual int on_large_call(ln::service_request&/* req*/, ln_service_large_call&/* svc*/) {
		fprintf(stderr, "ERROR: no virtual int on_large_call() handler overloaded for service large_call!\n");
		return 1;
	}
};
#endif
#endif // LN_MESSAGES_H
