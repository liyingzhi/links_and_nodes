

import sys
import time
import links_and_nodes as ln

class my_provider(object):
    def __init__(self, clnt, name, group=None):
        self.svc = clnt.get_service_provider(name, "large_call")
        self.svc.set_handler(self.handler)
        self.svc.do_register(group_name=group)

    def handler(self, request, req, resp):
        print "have %d names:" % len(req.names)
        for i, name in enumerate(req.names):
            print " %d: %r" % (i, name)


        resp.values = []
        for i in xrange(10):
            sp = resp.new_string_packet()
            sp.string = "value %d\r\n" % i
            resp.values.append(sp)
        request.respond()
        return 0
        
clnt = ln.client("service_provider", sys.argv)
p = my_provider(clnt, "large_service", group="my group")

print "ready!"

handle_synchronous = False

if handle_synchronous:
    while True:
        print "iterate..."
        clnt.wait_and_handle_service_group_requests("my group", 1)
else:
    clnt.handle_service_group_in_thread_pool("my group", "pool1");
    clnt.set_max_threads("pool1", 10)

    # dummy mainloop instead of something real...
    import time
    while True:
        print "iterate..."
        time.sleep(1)
