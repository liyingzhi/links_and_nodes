#!/usr/bin/pathon

import sys
import time
import numpy as np
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)
clnt.set_float_parameter("reconnect_interval", 1)
port = clnt.publish("test_vector", "tests/vector14")

port.packet.vector[:] = 0

f = 0.1
start = time.time()
while True:
    t = time.time() - start
    for i in xrange(port.packet.vector.shape[0]):
        port.packet.vector[i] = np.sin(t*(i+1)*f*np.pi) / (port.packet.vector.shape[0] - i)
    port.packet.scalar = np.sin(t * f * np.pi)
    
    port.write()
    time.sleep(0.01)

