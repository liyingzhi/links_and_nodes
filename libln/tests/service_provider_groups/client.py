#!/usr/bin/python

import sys
import time
import pprint
import random
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)
service1 = clnt.get_service("service1", "service_provider_groups_test/service1")
service2 = clnt.get_service("service2", "service_provider_groups_test/service1")

max_ever = 0

def do_call():
    global max_ever
    a = time.time()
    service2.call()
    b = (time.time() - a) * 1e3
    if b > max_ever:
        max_ever = b
    print "%.3fms: %s (max_ever: %.3fms)" % (b, service2.resp.dict(), max_ever)

while True:
#for i in xrange(3):
    do_call()
    #time.sleep(0.1)
    #break
    time.sleep(random.random()*0.15)
