#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/* service_provider_groups_test/service1 */
#define service_provider_groups_test_service1_signature "int32_t 4 1|uint32_t 4 1,int32_t 4 1,int32_t 4 1"
#include "ln_packed.h"
typedef struct LN_PACKED {
	struct LN_PACKED service_provider_groups_test_service1_request_t {
		int32_t req;
	} req;
	struct LN_PACKED service_provider_groups_test_service1_response_t {
		uint32_t thread_id;
		int32_t seq;
		int32_t resp;
	} resp;
#ifdef __cplusplus
	typedef service_provider_groups_test_service1_request_t request_t;
	typedef service_provider_groups_test_service1_response_t response_t;
#endif

} service_provider_groups_test_service1_t;
#include "ln_endpacked.h"

#ifdef __cplusplus
namespace service_provider_groups_test {

typedef service_provider_groups_test_service1_t service1_t;

class service1_base {
public:
	virtual ~service1_base() {
		unregister_service1();
	}
private:
	static int cb(ln::client&, ln::service_request& req, void* user_data) {
		service1_base* self = (service1_base*)user_data;
		service1_t data;
		req.set_data(&data, service_provider_groups_test_service1_signature);
		memset(&data.resp, 0, sizeof(data.resp));
		return self->on_service1(req, data);
	}
protected:
	ln::service* svc;
	
	service1_base() : svc(NULL) {};
	
	void unregister_service1() {
		if(svc) {
			svc->clnt->unregister_service_provider(svc);
			svc = NULL;
		}
	}
	void register_service1(ln::client* clnt, const std::string service_name, const char* group_name=NULL) {
		svc = clnt->get_service_provider(
			service_name,
			"service_provider_groups_test/service1", 
			service_provider_groups_test_service1_signature);
		svc->set_handler(&cb, this);
		svc->do_register(group_name);
	}
	virtual int on_service1(ln::service_request& req, service1_t& data) {
		fprintf(stderr, "ERROR: no virtual int on_service_provider_groups_test_service1() handler overloaded for service service1!\\n");
		return 1;
	}
};
} // namespace service_provider_groups_test
#endif
#endif // LN_MESSAGES_H
