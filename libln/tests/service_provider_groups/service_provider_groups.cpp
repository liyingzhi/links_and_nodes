#include <time.h>
#include <pthread.h>
#include <ln.h>

#include "ln_messages.h"

#define ln_check_error(fcn, ...) { int ret = fcn(__VA_ARGS__); if(ret != 0) { printf(#fcn " failed: %d (%s) %s\n", ret, ln_format_error(ret), ln_get_error_message(clnt)); return -1; } }

void my_sleep(double t) {
	struct timespec ts = {(unsigned long)t, 0};
	ts.tv_nsec = (unsigned long)(1e9 * (t - ts.tv_sec));
	nanosleep(&ts, 0);
}

struct context {
	int32_t seq;
	
} ctx = { 0 };

bool keep_running = true;

// #define TEST_CPP

#ifdef TEST_CPP
#include <string>
#include <set>

using namespace std;

class service_handler : public ln_service_service1_base {
public:
	string name;

	set<unsigned int> threads_seen;
	pthread_mutex_t threads_seen_lock;
	
	void register_service(ln::client* clnt, string name, const char* group_name=NULL) {
		this->name = name;
		pthread_mutex_init(&threads_seen_lock, NULL);
		register_service1(clnt, name, group_name);
	}
	virtual int on_service1(ln::service_request& req, service_provider_groups_test_service1_t& svc) {

		unsigned int n_threads_seen;
		pthread_mutex_lock(&threads_seen_lock);
		threads_seen.insert((unsigned int)pthread_self());
		n_threads_seen = threads_seen.size();
		pthread_mutex_unlock(&threads_seen_lock);
		
		printf("%s: handle in %p (%d threads seen)\n", name.c_str(), (void*)pthread_self(), n_threads_seen);
		
		if(name == "service1")
			my_sleep(4);
		else
			my_sleep(0.1);
		
		svc.resp.thread_id = (uint32_t)pthread_self();
		svc.resp.resp = svc.req.req;
		svc.resp.seq = ++ctx.seq;
	
		req.respond();

		if(svc.req.req == 44)
			keep_running = false;
		if(ctx.seq >= 500)
			keep_running = false;

		// printf("%s: done in %p\n", name.c_str(), (void*)pthread_self());
		return 0;
	}
};

class app {
public:
	ln::client* clnt;
	service_handler service1;
	service_handler service2;
	
	app(ln::client* clnt) : clnt(clnt) {
		clnt->set_thread_safe(true);

		service1.register_service(clnt, "service1");
		service2.register_service(clnt, "service2", "flo1");
		clnt->handle_service_group_in_thread_pool(NULL, "pool0");
		clnt->handle_service_group_in_thread_pool("flo1", "pool1");
		clnt->set_max_threads("pool1", 8);
	}
	void run() {
		keep_running = true;
		while(keep_running) {
			printf("daeumchen drehen\n");
			my_sleep(1);
		}
		printf("main exiting...\n");
	}
};

int main(int argc, char* argv[]) {
	ln::client clnt(argv[0], argc, argv);
	app a(&clnt);
	a.run();
	return 0;
}
#else
int service1_handler(ln_client clnt, ln_service_request req, void* user_data) {
	service_provider_groups_test_service1_t svc;
	
	ln_check_error(ln_service_request_set_data, req, &svc, service_provider_groups_test_service1_signature);
	
	printf("handle in %p\n", (void*)pthread_self());
	my_sleep(4);
	svc.resp.thread_id = (uint32_t)pthread_self();
	svc.resp.resp = svc.req.req;
	svc.resp.seq = ++ctx.seq;
	ln_check_error(ln_service_request_respond, req);

	if(svc.req.req == 44)
		keep_running = false;

	return 0;
}
int service2_handler(ln_client clnt, ln_service_request req, void* user_data) {
	service_provider_groups_test_service1_t svc;
	
	ln_check_error(ln_service_request_set_data, req, &svc, service_provider_groups_test_service1_signature);
	
	printf("handle in %p\n", (void*)pthread_self());
	// my_sleep(1);
	svc.resp.thread_id = (uint32_t)pthread_self();
	svc.resp.resp = svc.req.req;
	svc.resp.seq = ++ctx.seq;
	ln_check_error(ln_service_request_respond, req);

	if(svc.req.req == 44)
		keep_running = false;

	return 0;
}

int main(int argc, char* argv[]) {
	int ret;
	
	ln_client clnt;
	ret = ln_init(&clnt, argv[0], argc, argv);
	if(ret != 0) {
		printf("ln_init failed: %d\n", ret);
		return -1;
	}

	ln_check_error(ln_client_set_thread_safe, clnt, 1);

	ln_service service1;
	ln_check_error(ln_service_init, clnt, &service1, "service1",  "service_provider_groups_test/service1", service_provider_groups_test_service1_signature);	
	ln_check_error(ln_service_provider_set_handler, service1, service1_handler, NULL);

	ln_service service2;
	ln_check_error(ln_service_init, clnt, &service2, "service2",  "service_provider_groups_test/service1", service_provider_groups_test_service1_signature);	
	ln_check_error(ln_service_provider_set_handler, service2, service2_handler, NULL);

	ln_check_error(ln_service_provider_register_in_group, clnt, service1, "grp1");
	ln_check_error(ln_service_provider_register_in_group, clnt, service2, "grp2");
	/*
	while(keep_running) {
		ln_check_error(ln_wait_and_handle_service_group_requests, clnt, "grp1", 0);
		ln_check_error(ln_wait_and_handle_service_group_requests, clnt, "grp2", 0);
		// printf("  !!main loop!!\n");
	}
	*/

	ln_check_error(ln_handle_service_group_in_thread_pool, clnt, "grp1", "pool1");
	ln_check_error(ln_set_max_threads, clnt, "pool1", 2);
#if LN_LIBRARY_VERSION > 13	
	ln_check_error(ln_thread_pool_setschedparam, clnt, "pool1", 1, 2, 0x2);
#endif
	ln_check_error(ln_handle_service_group_in_thread_pool, clnt, "grp2", "pool1");

	ln_check_error(ln_set_max_threads, clnt, "pool1", 1);
	ln_check_error(ln_set_max_threads, clnt, "pool2", 1);
	
	while(keep_running) {
		printf("main is at %p\n", pthread_self());
		my_sleep(1);
	}
	
	ln_check_error(ln_service_deinit, &service1);
	ln_check_error(ln_service_deinit, &service2);
	
	ln_check_error(ln_deinit, &clnt);
	
	return 0;
}
#endif // TEST_CPP
