#!/usr/bin/python

import sys
import time
import links_and_nodes as ln

import thread

class app(object):
    def __init__(self):
        self.clnt = ln.client(sys.argv[0], sys.argv)

        self.ctx_seq = 0
        self.service1 = self.clnt.get_service_provider("service1", "service_provider_groups_test/service1")
        self.service1.set_handler(self.handle_service)
        self.service2 = self.clnt.get_service_provider("service2", "service_provider_groups_test/service1")
        self.service2.set_handler(self.handle_service)

        self.service1.do_register("grp1")
        self.service2.do_register("grp1")

    def handle_service(self, service, req, resp):
        print "%s: handle in %r" % (service.name, thread.get_ident())
		
        if service.name == "service1":
            time.sleep(4)
		
        resp.thread_id = thread.get_ident()
        resp.resp = req.req
        self.ctx_seq += 1
        resp.seq = self.ctx_seq
	
        service.respond()

        if req.req == 44:
            self.keep_running = False

        print "%s: done in %r\n" % (service.name, thread.get_ident())
        return 0

    def run(self):
        self.keep_running = True
        
        self.clnt.set_max_threads("pool1", 10)
        FIFO = 1
        self.clnt.handle_service_group_in_thread_pool("grp1", "pool1")
        self.clnt.thread_pool_setschedparam("pool1", FIFO, 2, 0x2)

        while self.keep_running:
            print "daeumchen drehen"
            time.sleep(1)

if __name__ == "__main__":
    a = app()
    a.run()
