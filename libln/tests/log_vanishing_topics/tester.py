#!/usr/bin/python

import os
import unittest
import sys
import links_and_nodes as ln
import threading
import time
import cPickle

class publisher(threading.Thread):
    def __init__(self, parent, topic, rate=10):
        self.parent = parent
        self.topic = topic
        self.rate = rate
        self.port = self.parent.clnt.publish(topic, "test_md")
        threading.Thread.__init__(self)
        self.keep_running = True
        self.start()

    def unpublish(self):
        if self.port is None:
            raise Exception("already unpublished!")
        self.keep_running = False
        self.join(1./self.rate*10)
        if self.isAlive():
            raise Exception("thread won't die!")
        self.parent.clnt.unpublish(self.port)
        self.port = None
        
    def run(self):
        self.port.packet.cnt = 0
        while self.keep_running:
            self.port.packet.cnt += 1
            #print "publish %s: %d" % (self.topic, self.port.packet.cnt)
            self.port.write()
            time.sleep(1./self.rate)

class tester(unittest.TestCase):
    clnt = ln.client("tester", sys.argv)
    published = {}
    loggers = {}

    def setUp(self):
        self.clnt = tester.clnt
        self.published = tester.published
        self.loggers = tester.loggers

    def create_and_start_logger(self, name, topics, N=1000, div=1):
        if name in self.loggers:
            l = self.loggers[name]
            l.clear_topics()
        else:
            l = self.clnt.get_logger(name)
            self.loggers[name] = l
        for topic in topics:
            l.add_topic(topic, N, div)
        l.enable()

    def save_log(self, name, fn):
        if name not in self.loggers:
            raise Exception("logger %r is not known!" % name)
        l = self.loggers[name]
        l.disable()
        fn = os.path.join(os.path.dirname(__file__), fn)
        l.manager_save(fn)
        l.clear_topics()
        return cPickle.load(file(fn, "rb"))

    def publish(self, topic):
        if topic in self.published:
            raise Exception("topic %r already published!" % topic)
        self.published[topic] = publisher(self, topic)
        time.sleep(0.5)

    def unpublish(self, topic):
        if topic not in self.published:
            raise Exception("topic %r not published!" % topic)
        self.published[topic].unpublish()
        del self.published[topic]
        time.sleep(1.5)
        
    ## tests

    def test_normal_usecase(self):
        self.publish("a")
        self.create_and_start_logger("my_logger", ["a"])
        time.sleep(0.5)
        data = self.save_log("my_logger", "test1")
        self.unpublish("a")
        self.assertTrue(data["a"]["cnt"].shape[0] > 0)

    def test_early_unpublish(self):
        self.publish("a")
        self.create_and_start_logger("my_logger", ["a"])
        time.sleep(0.5)
        self.unpublish("a")
        data = self.save_log("my_logger", "test1")
        self.assertTrue(data["a"]["cnt"].shape[0] > 0)

    def test_late_publish(self):
        self.create_and_start_logger("my_logger", ["b"])
        self.publish("b")
        self.unpublish("b")
        data = self.save_log("my_logger", "test2")
        if data["b"]["cnt"].shape[0] < 4:
            raise Exception("got log shape %r" % (data["b"]["cnt"].shape, ))

    def test_late_publish_twice(self):
        self.create_and_start_logger("my_logger", ["c"])
        self.publish("c")
        self.unpublish("c")

        self.publish("c")
        self.unpublish("c")
        data = self.save_log("my_logger", "test3")
        if data["c"]["cnt"].shape[0] < 4:
            raise Exception("got log shape %r" % (data["c"]["cnt"].shape, ))

if __name__ == '__main__':
    unittest.main()
