#!/usr/bin/python

import sys
import time
import links_and_nodes as ln

class provider(ln.service_provider):
    def __init__(self):
        self.clnt = ln.client("provider", sys.argv)
        ln.service_provider.__init__(self, self.clnt)

        self.wrap_service_provider(
            "slow_service", "sync_service_call_abort/slow_service",
            group_name="main group",
            pass_request=True
        )

    def slow_service(self, request, what):
        print "client requested %r" % what
        for i in xrange(5):
            print "sleep", i
            time.sleep(1)
            print "is_aborted: %d" % request.is_aborted()
            if request.is_aborted():
                print "service call was aborted..."
                return
        print "finish!"
        return dict(data="this is the data")
        
    def run(self):
        self.handle_service_group_requests("main group")
        
if __name__ == "__main__":
    p = provider()
    p.run()
