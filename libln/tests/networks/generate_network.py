#!/usr/bin/python

import re
import sys

net_file = sys.argv[1]

networks = {}
networks_gw_prios = {} # name -> [ (prio, host) ]
host_interfaces = {} # name -> interfaces( { network -> ifname } )

hostips = {} # netip -> last_hostip
def get_ip_in_network(netip):
    hostip = hostips.get(netip, 1)
    hostips[netip] = hostip + 1
    return hostip

for line in open(net_file):
    line = line.strip().replace(" ", "").replace("\t", "")
    if not line or line[0] == "#":
        continue
    
    nethosts = re.findall("\w[0-9]*", line)
    nethost_prios = [nh[1:] for nh in nethosts]
    nethosts = [nh[0] for nh in nethosts]
    
    netname = "".join(nethosts)
    netip = len(networks)
    
    networks[netname] = []
    networks_gw_prios[netname] = []
    for host, prio in zip(nethosts, nethost_prios):
        if host not in host_interfaces:
            host_interfaces[host] = dict() # no interfaces yet
        if netname not in host_interfaces[host]:
            # no interface yet for this network, add one!
            ifname = "en%d" % len(host_interfaces[host])
            ifip = "192.168.%s.%s" % (netip, get_ip_in_network(netip))
            host_interfaces[host][netname] = ifname, ifip
            networks[netname].append((host, ifname))
            if prio: networks_gw_prios[netname].append((int(prio), host))

print "hosts"
for host in sorted(host_interfaces.keys()):
    interfaces = host_interfaces[host]
    print "%s: %s" % (host, ", ".join(["%s:%s" % interfaces[k] for k in sorted(interfaces.keys())]))
print
for network in sorted(networks.keys()):
    print "network %s" % network
    
    members = [ "%s/%s" % p for p in sorted(networks[network]) ]
    print "member_interfaces: %s" % (", ".join(members))

    prios = sorted(networks_gw_prios[network])
    if prios:
        preferred = ", ".join([h for p, h in prios])
        print "preferred_gateways: %s" % preferred
