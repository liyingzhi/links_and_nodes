#include <stdio.h>

#include "ln.h"
#include <string_util/string_util.h>

using namespace std;

int main(int argc, char* argv[]) {
	double rate = 10;
	unsigned int topic_size = 1024;
	bool reliable = false;
	
	ln::client clnt("subscriber", argc, argv);
	
	list<string> args = clnt.get_remaining_args();
	bool skip = false;
	for(list<string>::iterator i = args.begin(); ; ) {
		if(!skip)
			++i;
		else
			skip = false;
		if(i == args.end())
			break;
		string& arg = *i;
		if(arg == "-size") {
			i++;
			if(i == args.end())
				throw ln::exception("number after -size argument missing!");
			topic_size = atoi(i->c_str());
		}
		if(arg == "-rate") {
			i++;
			if(i == args.end())
				throw ln::exception("number after -rate argument missing!");
			rate = atof(i->c_str());
		}
		if(arg == "-reliable")
			reliable = true;		
	}
	
	printf("topic size: %d\n", topic_size);
	string md_name = format_string("data_blob_%d", topic_size);
	ln::inport* port = clnt.subscribe("big_topic", "gen/" + md_name, rate, 1, reliable);

	vector<uint8_t> data(topic_size);
	unsigned int& data_counter1 = *((unsigned int*)&data[0]);
	unsigned int& data_counter2 = *((unsigned int*)(&data[0] + data.size()) - 1);	

	unsigned int last_data_counter = 0;
	unsigned int total_loss = 0;
	unsigned int total_errors = 0;
	bool first = true;
	double last_print = ln_get_time();
	while(true) {
		port->read(&data[0]);
		
		if(data_counter1 != data_counter2) {
			printf("error: counter1: %d, counter2: %d -- error: %d\n", data_counter1, data_counter2, data_counter2 - data_counter1);
			total_errors ++;

			// don't count as loss
			data_counter1 = data_counter2;
			last_data_counter = data_counter1 - 1;
		}
		
		if(!first && data_counter1 != last_data_counter + 1) {			
			printf("lost %d packets!\n", data_counter1 - last_data_counter - 1);
			total_loss += data_counter1 - last_data_counter - 1;
		} else
			first = false;
		
		last_data_counter = data_counter1;

		double now = ln_get_time();
		if((now - last_print) > 1) {
			last_print = now;
			printf("counter1: %d, total loss: %d, errornous packets: %d\n", data_counter1, total_loss, total_errors);
		}
	}
	
	return 0;
}
