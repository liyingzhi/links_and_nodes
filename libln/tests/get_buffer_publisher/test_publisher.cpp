#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "ln.h"

#include "ln_messages.h"

#ifdef __WIN32__
#include <windows.h>
struct timespec {
	time_t tv_sec;
	long tv_nsec;
};
void nanosleep(struct timespec* ts, struct timespec* tsout) {
	double ms = ts->tv_sec * 1e3 + ts->tv_nsec / 1e6;
	Sleep(ms);
}
#endif

int main(int argc, char* argv[]) {
	int ret;
	double rate = 100;
	double start_time;
	unsigned int i;

	for(int i = 1; i < argc; i++)
		if(!strcmp(argv[i], "-rate") && (i + 1) < argc)
			rate = atof(argv[++i]);

	double seconds = 1. / rate;
	struct timespec ts = {(int)seconds, (long int)((seconds - (int)seconds) * 1e9)};
	
	ln_client clnt;
	if((ret = ln_init(&clnt, "test publisher md", argc, argv))) {
		printf("could not init ln_client. returned error %d: %s\n", -ret, ln_format_error(ret));
		if(clnt)
			printf("dynamic error: %s\n", ln_get_error_message(clnt));
				
		return -1;
	}

	// register publish/output-port
	ln_outport counters_port;
	if((ret = ln_publish_with_buffers(clnt, "tests.counters", "tests/counters", 100, &counters_port))) {
		printf("ln_publish error %d: %s\n", -ret, ln_format_error(ret));
		if(ln_get_error_message(clnt)) printf("%s\n", ln_get_error_message(clnt));
		goto error_exit;
	}

	printf("sizeof ln_packet_tests_counters: %d bytes\n", sizeof(ln_packet_tests_counters));

	// publish/write to port
	start_time = ln_get_time();
	i = 0;
	while(true) {
		// instead of filling a local copy of the struct, directly write into the shm!
		ln_packet_tests_counters* p;
		unsigned int buffer_len;
		if((ret = ln_outport_get_next_buffer(counters_port, (void**)&p, &buffer_len))) {
			printf("ln_outport_get_next_buffer error %d: %s\n", -ret, ln_format_error(ret));
			if(ln_get_error_message(clnt)) printf("%s\n", ln_get_error_message(clnt));
			goto error_exit;
		}
		// got shm buffer, fill!
		printf("got shm buffer %p of length %d\n", p, buffer_len);
		
		if(buffer_len != sizeof(ln_packet_tests_counters)) {
			printf("error, got shm buffer_len of %d but local generated struct has %d bytes!\n",
			       buffer_len, sizeof(ln_packet_tests_counters));
			goto error_exit;
		}

		p->duration = ln_get_time() - start_time;
		p->counter = i++;
		for(unsigned int k = 0; k < 16; k++)
			p->frame[k] = k * i;
		
		if((p->counter % 10) == 0) {
			printf("counter: %d\n", p->counter);
			fflush(stdout);
		}
		
		// trigger subscribers, without providing data pointer!
		if((ret = ln_write(counters_port, NULL, sizeof(p), NULL))) {
			printf("ln_write error %d: %s\n", -ret, ln_format_error(ret));
			if(ln_get_error_message(clnt)) printf("%s\n", ln_get_error_message(clnt));
			goto error_exit;
		}
		
		nanosleep(&ts, NULL);
	}

	if((ret = ln_unpublish(&counters_port))) {
		printf("ln_unpublish error %d: %s\n", -ret, ln_format_error(ret));
		if(ln_get_error_message(clnt))
			printf("dynamic error: %s\n", ln_get_error_message(clnt));
	}

error_exit:
	printf("lnc exiting\n");
	ln_deinit(&clnt);

	return 0;
}
