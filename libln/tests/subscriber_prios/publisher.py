#!/usr/bin/python

import time
import links_and_nodes as ln
import sys

clnt = ln.client("logger", sys.argv)

port = clnt.publish("test", "test/topic")
port.packet.count = 0

print "ready"
while True:
    port.packet.count += 1
    port.write()
    time.sleep(0.001)
