#include <stdio.h>
#include <unistd.h>

#include <ros/ros.h>
#include <std_msgs/Int32.h>

class my_subscriber {
	ros::NodeHandle handle;
	ros::Subscriber sub;
public:
	my_subscriber() {
		sub = handle.subscribe("my_topic", 1, &my_subscriber::callback, this);
	}

	void callback(const std_msgs::Int32::ConstPtr& message) {
		printf("my_subscriber: callback: %d\n", message->data);
	}
	
	void run() {
		ros::SingleThreadedSpinner sp;
		sp.spin();
	}
};

int main(int argc, char* argv[]) {
	ros::init(argc, argv, "ros_subscriber");
	my_subscriber s;
	s.run();
	return 0;
}
