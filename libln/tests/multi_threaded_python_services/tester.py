#!/usr/bin/python

import subprocess
import sys
import time

if __name__ == "__main__":
    if len(sys.argv) > 1:
        N = int(sys.argv[1])
    else:
        N = 1000
    
    for i in xrange(N):
        p = subprocess.Popen("python test_process.py", shell=True)
        ret = p.wait()
        if ret != 0:
            if ret == -11:
                ret = "SIGSEGV"
            raise Exception("test failed after %d tries: ret=%s!" % (i + 1, ret))
        time.sleep(0.15)
        sys.stdout.flush()
        sys.stdout.write("\033[2J\033[H")
