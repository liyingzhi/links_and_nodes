#include <stdio.h>
#include <unistd.h>
#include "ln.h"

#include <vector>

using namespace std;

int main(int argc, char* argv[]) {
	ln::client* clnt = new ln::client("publisher", argc, argv);

	ln::outport* port = clnt->publish("test", "test_md", 1);
	unsigned int N = port->message_size / sizeof(uint32_t);
	vector<uint32_t> data(N);
	
	uint32_t o = 0;
	while(true) {
		for(unsigned int i = 0; i < N; i++) {
			data[i] = ((o & 0xffff) << 16) | (i & 0xffff);

			
		}
		// printf("tick %d\n", o);
		port->write(&data[0]);
		// usleep((unsigned int)1000e3);
		// usleep((unsigned int)10e3);
		// usleep((unsigned int)100);

		o ++;
	}
	
	return 0;
}
