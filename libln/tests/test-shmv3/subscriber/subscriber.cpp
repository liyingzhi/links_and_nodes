#include <stdio.h>
#include "ln.h"

#include <vector>

using namespace std;

int main(int argc, char* argv[]) {
	ln::client* clnt = new ln::client("subscriber", argc, argv);

	ln::inport* port = clnt->subscribe("test", "test_md");
	unsigned int N = port->message_size / sizeof(uint32_t);
	vector<uint32_t> data(N);

	double start_time = ln_get_time();
	double last_time = start_time;
	unsigned int tot_n_errors = 0;
	unsigned int tot_n_ok = 0;
	unsigned int expected_start = 0;
	while(true) {
		bool err = false;
		port->read(&data[0]);

		unsigned int start = data[0] >> 16;

		/*
		  // check for lost packages
		if(start != expected_start) {
			err = true;
			printf("expected packet start %d, got %d, diff: %d\n", expected_start, start, start - expected_start);
		}
		expected_start = start + 1;
		*/
		
		
		unsigned int n_errors = 0;
		for(unsigned int i = 0; i < N; i++) {
			unsigned int exp_i = i & 0xffff;
			
			unsigned int data_o = (data[i] >> 16);
			unsigned int data_i = (data[i] & 0xffff);
			
			if(data_o != start || data_i != exp_i) {
				// if(err == false) // show only first err
					printf("expected o %d, %d got o %d, %d\n",
					       start, exp_i,
					       data_o, data_i);
				err = true;
				
				n_errors ++;
			}
		}

		if(n_errors > 0) {
			printf("%d word errors!\n", n_errors);
			tot_n_errors ++;
		} else {
			tot_n_ok ++;

			double now = ln_get_time();
			if(now - last_time > 1) {
				last_time = now;
				double d = now - start_time;
			
				//printf(".");
				printf(" ok: %d (%.0f/s), errors: %d\r", tot_n_ok, tot_n_ok / d, tot_n_errors);
				fflush(stdout);
			}
		}
		
		/*
		if(err)
			printf("\n");
		*/
	}
	
	return 0;
}

