#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string>
#include <signal.h>
#include <unistd.h>
#include <inttypes.h>

#include <pthread.h>

#include "ln.h"
#include "ln_messages.h"

#include <string_util/string_util.h>

using namespace std;

char* text_source = "check errno for error description, out of memory, no -ln_manager HOST:PORT command line argument and no LN_MANAGER environment var, specified ln_manager address is not in format HOST_OR_IP:PORT_NUMBER, invalid hostname specified - has to be a hostname or an ip, could not resolve hostname, host resolved to wrong network family - should be IPv4 - AF_INET, client library received an invalid formatted response from ln_manager, ln_manager denied register-request";
bool keep_running = true;

int on_query_dict_cb(ln::client& clnt, ln::service_request& req, void* user_data) {
	ln_parameters_query_dict_t svc;
	req.set_data(&svc, ln_parameters_query_dict_signature);

	string pattern(svc.req.pattern, svc.req.pattern_len);
	printf("pattern: %s\n", repr(pattern).c_str());

	req.respond();
		
	return 0; // success
}


int time_service_cb(ln::client& clnt, ln::service_request& req, void* user_data) {
	tests_time_service_t svc;
	req.set_data(&svc, tests_time_service_signature);

	printf("count: %d\n", (int)svc.req.count);
	svc.resp.count = svc.req.count + 1;

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	svc.resp.time = (double)ts.tv_sec + ts.tv_nsec * 1e-9;

	// sleep(3);

	/*
	svc.resp.shorts_len = svc.req.count;
	svc.resp.shorts = new short[svc.resp.shorts_len];
	for(unsigned int i = 0; i < svc.resp.shorts_len; i++)
		svc.resp.shorts[i] = (i + 1) * 3.14;
	*/

	/*
	printf("shorts_len: %d\n", svc.req.shorts_len);
	for(unsigned int i = 0 ; i < svc.req.shorts_len; i++) {
		printf(" short[%d] = %d\n", i, svc.req.shorts[i]);
	}
	*/

	printf("time service responding: %f, %d!\n", svc.resp.time, (unsigned int)svc.resp.count);
	req.respond();

	// delete[] svc.resp.shorts;

	if(svc.req.count == 42) {
		printf("req.count reached 42 -- exiting!\n");
		keep_running = false;
	}
		
	return 0; // success
}

int present_window_cb(ln::client& clnt, ln::service_request& req, void* user_data) {
	printf("present - set keep running to false! %p\n", (void*)pthread_self());
	keep_running = false;
	return 0;
}

/*
int read_from_file_cb(ln::client& clnt, ln::service_request& req, void* user_data) {
	ln_service_tests_read_from_file svc;
	req.set_data(&svc, ln_service_tests_read_from_file_signature);

	string filename(svc.req.filename, svc.req.filename_len);
	printf("read %d bytes from %d of %s\n", (int)svc.req.len, (int)svc.req.offset, filename.c_str());

	int ret = 1; // error

	svc.resp.data = new char[svc.req.len];	
	if(svc.resp.data) {
		FILE* fp = fopen(filename.c_str(), "rb");
		if(fp) {
			fseek(fp, svc.req.offset, SEEK_SET);
			svc.resp.data_len = fread(svc.resp.data, 1, svc.req.len, fp);
			fclose(fp);

			req.respond();
			ret = 0; // success
		} else
			ret = 2;

		delete[] svc.resp.data;
	} else 
		ret = 3;
	return ret;
	return 0;
}
*/

/*
int write_file_cb(ln::client& clnt, ln::service_request& req, void* user_data) {
	tests_write_file_t svc;
	req.set_data(&svc, tests_write_file_signature);
	svc.resp.error_message_len = 0;

	string filename(svc.req.filename, svc.req.filename_len);
	printf("write %d bytes to %s\n", (int)svc.req.data_len, filename.c_str());

	FILE* fp = fopen(filename.c_str(), "wb");
	if(fp) {
		int written = fwrite(svc.req.data, 1, svc.req.data_len, fp);
		fclose(fp);
		printf("wrote %d bytes\n", written);
	} else {
		char errmsg[1024];
		int len = snprintf(errmsg, 1024, "could not open file: %s\n", strerror(errno));
		svc.resp.error_message = errmsg;
		svc.resp.error_message_len = len;
		printf("%s", errmsg);
	}
	req.respond();
	return 0;
}
*/

/*
int time_service2_cb(ln::client& clnt, ln::service_request& req, void* user_data) {
	ln_service_tests_time_service_complex svc;
	req.set_data(&svc, ln_service_tests_time_service_complex_signature);

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	svc.resp.time = (double)ts.tv_sec + ts.tv_nsec * 1e-9;
	svc.resp.count = svc.req.count;
	
	printf("count: %d, frame: ", (int)svc.req.count);
	for(unsigned int i = 0; i < 16; i++) {
		printf("%.3f ", svc.req.frame[i]);
	}
	printf("\n");
	printf("var_in %d: ", svc.req.var_in_len);
	for(unsigned int i = 0; i < svc.req.var_in_len; i++) {
		printf("%.3f ", svc.req.var_in[i]);
	}
	// printf("filename %d: %*.*s\n", svc.req.filename_len, svc.req.filename_len, svc.req.filename_len, svc.req.filename);
	printf("filename %s\n", svc.req.filename);

	printf("n1[0].int_value: %d ", svc.req.n1[0].int_value);
	printf("n1[1].int_value: %d\n", svc.req.n1[1].int_value);

	printf("n2 count: %d ", svc.req.n2_len);
	for(unsigned int i = 0; i < svc.req.n2_len; i++) {
		printf("[%d].int_value: %d ", i, svc.req.n2[i].int_value);
	}
	printf("\n");

	svc.resp.vardata_len = rand() % 128;
	printf("svc.resp.vardata_len: %d\n", svc.resp.vardata_len);
	svc.resp.vardata = (double*) malloc(sizeof(double) * svc.resp.vardata_len);
	for(unsigned int i = 0; i < svc.resp.vardata_len; i++)
		svc.resp.vardata[i] = i / 100.;

	svc.resp.text_len = rand() % strlen(text_source);
	svc.resp.text = text_source;

	req.respond();

	free(svc.resp.vardata);

	//  return value is interpreted like this:
	//
	//  0    request successfull
	//  < 0  request failed. value is one of the links and nodes error constants LNE_*
	//  > 0  request failed. value is passed uninterpreted to requesting client
	
	// return 13; // user defined error 13
	// return -LNE_PORT_NOT_FOUND;
	return 0; // success
}
*/

void on_signal(int signo) {
	if(signo == SIGTERM) {
		printf("\ngot SIGTERM, terminating...\n");
		keep_running = false;
	} else if(signo == SIGINT) {
		printf("\ngot SIGINT, terminating...\n");
		keep_running = false;
	}
}

int main(int argc, char* argv[]) {
	double sleep_time = 0.1;
	string mode = "publish";
	int skip = 0;
	for(unsigned int i = 1; i < (unsigned)argc; i++) {
		if(skip) {
			skip --;
			continue;
		}
		if(!strcmp(argv[i], "-rate") && i + 1 < (unsigned)argc) {
			skip = 1;
			sleep_time = 1.0 / atof(argv[i + 1]);
		}
		else if(!strcmp(argv[i], "-mode") && i + 1 < (unsigned)argc) {
			skip = 1;
			mode = argv[i + 1];
		}
	}
	struct timespec ts = {
		(int)sleep_time, 
		(long int)((sleep_time - (int)sleep_time) * 1e9)
	};

	// register SIGTERM handler for clean shutdown
	signal(SIGTERM, on_signal);
	signal(SIGINT, on_signal);
	
	ln::client clnt("cpp test", argc, argv);

	/*
	char* argv[] = {"something", "else", "arguments"};
	int argc = sizeof(argv) / sizeof(char*);
	ln::client clnt("cpp test", argc, argv);
	*/

	//ln::client clnt("cpp test", 0, NULL);
	//ln::client clnt("cpp test");

	//ln::client clnt("cpp test", "localhost:54414");

	if(mode == "publish") {

		ln::outport* counters_port = clnt.publish("tests.counters", "tests/counters");

		// sleep(5);
		printf("now continue!\n");

		tests_counters_t p;
		//for(unsigned int i = 0; i < 1000; i++) {
		unsigned int i = 0;
		while(keep_running) { i++;
			p.counter = i;
			// p.measured_frames.right_arm_tcp[0] = (double)(time(NULL) - start_time);
			
			printf("sending counter %d\n", p.counter);
			counters_port->write(&p);
			
			nanosleep(&ts, NULL);
		}		
		printf("finishing...\n");
		clnt.unpublish(counters_port);

	/* msg def no longer exists
	} else if(mode == "publish_scope") {
		ln::outport* port = clnt.publish("tests.scope", "tests/scope");
		printf("ready!\n");
		ln_packet_tests_scope p;
		double start_time = ln_get_time();
		double f = 5;
		double f2 = .25;
		double f3 = .125;
		double f4 = 1 / 30.;

		double t;
		while(keep_running) { 
			t = ln_get_time() - start_time;
			p.single_freqs[0] = sin(t * f * 2 * M_PI);
			p.single_freqs[1] = cos(t * f2 * 2 * M_PI);
			p.single_freqs[2] = cos(t * f3 * 2 * M_PI) * 4;
			p.single_freqs[3] = sin(t * f4 * 2 * M_PI) * 6;
				
			p.value = p.single_freqs[0] * p.single_freqs[1] * p.single_freqs[2] + p.single_freqs[3] + 2;
			
			port->write(&p);
			nanosleep(&ts, NULL);
		}		
		printf("finishing...\n");
		clnt.unpublish(port);
	*/
	} else if(mode == "present_window") {
		ln::service* service = clnt.get_service_provider(
			"ln.scopes.tests.scope.single_freqs.present_window",
			"ln/present_window", ln_present_window_signature);
		service->set_handler(&present_window_cb);
		service->do_register();

		printf("waiting for service requests, %p\n", (void*)pthread_self());
		while(keep_running) {
			clnt.wait_and_handle_service_group_requests(std::nullptr, 10); // blocking
			printf("handle done: keep running: %d\n", keep_running);
		}

	} else if (mode == "subscribe") {
		ln::inport* counters_port = clnt.subscribe("tests.counters", "tests/counters");
		tests_counters_t p;
		//for(unsigned int i = 0; i < 500; i++) {
		printf("ready\n");
		while(keep_running) {
			counters_port->read(&p);
			printf("received counter %d\n", p.counter);
		}
		printf("finishing...\n");
		clnt.unsubscribe(counters_port);

	} else if (mode == "multi_subscribe") {
		// register publish/output-port
		ln::inport* port[3];
		port[0] = clnt.subscribe("tests.counters", "tests/counters");
		port[1] = clnt.subscribe("fast_counters", "tests/counters");
		port[2] = clnt.subscribe("cpp_counters", "tests/counters");
		tests_counters_t p;

		ln::multi_waiter* waiter = clnt.get_multi_waiter();

		// registers ports to wait on with this multi waiter
		for(unsigned int i = 0; i < 3; i++)
			waiter->add_port(port[i]);

		while(keep_running) {
			printf("\nwait "); fflush(stdout);
			double start = ln_get_time();
			waiter->wait();
			double block_time = ln_get_time() - start;
			printf("blocked for %4.0fms\n", block_time * 1e3);

			for(unsigned int i = 0; i < 3; i++) {
				ln::inport* inport = port[i];
				if(!waiter->can_read(inport)) {
					printf("port %d has nothing to read...\n", i);
					continue;
				}
				// this inport can read
				if(!inport->read(&p, false)) {
					printf("error port %d: no new packet!?\n", i);
					continue;
				}				
				printf("port %d: counter: %d, timestmap: %f\n", i, p.counter, inport->timestamp);
			}
			// sleep(1);
		}

	} else if (mode == "service_provider") {

		ln::service* time_service = clnt.get_service_provider("tests.time_service", "tests/time_service", tests_time_service_signature);
		time_service->set_handler(&time_service_cb);
		time_service->do_register();

		/* msg_def no longer exists
		   ln::service* time_service2 = clnt.get_service_provider("tests.time_service_complex", "tests/time_service_complex", ln_service_tests_time_service_complex_signature);
		   time_service2->set_handler(&time_service2_cb);
		   time_service2->do_register();
		*/

		/*
		ln::service* read_from_file_service = clnt.get_service_provider("tests.read_from_file", "tests/read_from_file", tests_read_from_file_signature);
		read_from_file_service->set_handler(&read_from_file_cb);
		read_from_file_service->do_register();
		*/

		/*
		for(unsigned int i = 0; i < 20; i++) {
			char name[1024];
			snprintf(name, 1024, "tests.write_file.%d", i);
			ln::service* write_file_service = clnt.get_service_provider(name, "tests/write_file", tests_write_file_signature);
			write_file_service->set_handler(&write_file_cb);
			write_file_service->do_register();
		}
		*/
		
		printf("waiting for service requests\n");
		// int max = 15;
		// while(keep_running && max--) {
		while(keep_running)
			clnt.wait_and_handle_service_group_requests(std::nullptr); // blocking
		clnt.release_service(time_service);
		// clnt.release_service(read_from_file_service);
		printf("waiting before exiting...\n");
		sleep(2);

	} else if (mode == "parameter_provider") {

		ln::service* parameter_service = clnt.get_service_provider("ln.parameters.test", "ln/parameters/query_dict", ln_parameters_query_dict_signature);
		parameter_service->set_handler(&on_query_dict_cb);
		parameter_service->do_register();

		printf("waiting for service requests\n");
		// int max = 15;
		// while(keep_running && max--) {
		while(keep_running)
			clnt.wait_and_handle_service_group_requests(std::nullptr); // blocking
		printf("waiting before exiting...\n");
		sleep(2);

	} else if (mode == "service_provider_time2") {

		ln::service* time_service = clnt.get_service_provider("tests.time_service2", "tests/time_service", tests_time_service_signature);
		time_service->set_handler(&time_service_cb);
		time_service->do_register();

		printf("waiting for time_service2 requests\n");
		// int max = 15;
		// while(keep_running && max--) {
		while(keep_running)
			clnt.wait_and_handle_service_group_requests(std::nullptr); // blocking
		clnt.release_service(time_service);

	} else if (mode == "service_client") {

		ln::service* time_service = clnt.get_service("tests.time_service", "tests/time_service", tests_time_service_signature);

		// int max = 10;
		// while(keep_running && max--) {
		tests_time_service_t svc;
		svc.req.count = 0;
		while(keep_running) {
			svc.req.count ++;
			time_service->call(&svc);

			printf("time: %.6f, request: %" PRIu64 ", response: %" PRIu64 "\n", svc.resp.time, svc.req.count, svc.resp.count);
			sleep(1);
		}

	} else if (mode == "async_service_client") {

		ln::service* time_service = clnt.get_service("tests.time_service", "tests/time_service", tests_time_service_signature);

		tests_time_service_t svc;
		svc.req.count = 0;
		while(keep_running) {
			svc.req.count ++;
			ln::service_request* req = time_service->call_async(&svc);
			double start = ln_get_time();
			while(!req->finished() && keep_running) {
				printf("service request not yet finished... %.3fs\n", ln_get_time() - start);
				nanosleep(&ts, NULL);
			}
			if(!keep_running)
				break;
			double d = ln_get_time() - start;
			printf("time: %.6f, request: %" PRIu64 ", response: %" PRIu64 " after %.3fs\n", svc.resp.time, svc.req.count, svc.resp.count, d);
			sleep(1);
		}

		/*
	} else if (mode == "async_service_client2") {

		ln::service* file_service = clnt.get_service("tests.read_from_file", "tests/read_from_file", tests_read_from_file_signature);

		//int max = 10;
	        tests_read_from_file_t svc;
		svc.req.filename = "/home/flo/workspace/ln_base/library/bigfile";
		svc.req.filename_len = strlen(svc.req.filename);
		svc.req.offset = 0;
		svc.req.len = 200 * 1024 * 1024;

		while(keep_running) {
			ln::service_request* req = file_service->call_async(&svc);
			double start = ln_get_time();
			while(!req->finished() && keep_running) {
				printf("service request not yet finished... %.3fs\n", ln_get_time() - start);
				nanosleep(&ts, NULL);
			}
			if(!keep_running)
				break;
			double d = ln_get_time() - start;
			printf("after %.3fs, %d bytes received\n", d, (int)svc.resp.data_len);
			break; // sleep(1);
		}

	}
		*/
/*else if (mode == "async_service_client3") {
		
		ln::service* file_service = clnt.get_service("tests.write_file", "tests/write_file", ln_service_tests_write_file_signature);

		ln_service_tests_write_file svc;
		svc.req.filename = "/tmp/bigfile.out";
		svc.req.filename_len = strlen(svc.req.filename);
		
		svc.req.data_len = 102 * 1024 * 1024;
		svc.req.data = (char*)malloc(svc.req.data_len);
		char* filename = "/home/that/sd/externals/ln_base/library/bigfile";
		FILE* fp = fopen(filename, "rb");
		svc.req.data_len = fread(svc.req.data, 1, svc.req.data_len, fp);
		fclose(fp);
		printf("will send %d bytes of data\n", (int)svc.req.data_len);


		ln::service* time_service = clnt.get_service("tests.time_service2", "tests/time_service", ln_service_tests_time_service_signature);
		ln_service_tests_time_service svc2;
		svc2.req.count = 0;
		ln::service_request* req2 = time_service->call_async(&svc2);

		ln::service_request* req = file_service->call_async(&svc);
		double start = ln_get_time();
		while(!req->finished() && keep_running) {
			printf("service request not yet finished... %.3fs\n", ln_get_time() - start);

			if(req2->finished()) {
				printf("time service finished: %d\n", (int)svc2.resp.count);
				svc2.req.count ++;
				req2 = time_service->call_async(&svc2);
			}

			nanosleep(&ts, NULL);
		}
		double d = ln_get_time() - start;
		free(svc.req.data);
		printf("finished after %.3fs\n", d);

		}*/
	/* msg_def no longer exits
	   else if (mode == "service_client2") {

		ln::service* file_service = clnt.get_service("read_from_file", "tests/read_from_file", ln_service_tests_read_from_file_signature);

		ln_service_tests_read_from_file svc;
		svc.req.filename = "/home/schm_fl/workspace/ln_base/library/test_cpp_wrapper/cpp_test.cpp";
		svc.req.filename = "/home/schm_fl/data/video/JHV Sequenz 01.avi";
		svc.req.filename_len = strlen(svc.req.filename);
		svc.req.offset = 0;
		
		// while(keep_running && max--) {
		uint64_t data_len = 0;
		while(keep_running) {
			svc.req.len = 100 * 1000 *  1000;
			file_service->call(&svc);
			
			//printf("data:\n%*.*s\n", (int)svc.resp.data_len, (int)svc.resp.data_len, svc.resp.data);
			data_len += svc.resp.data_len;
			printf("received %d bytes of data! - total: %lld\n", (unsigned int)svc.resp.data_len, data_len);

			if(svc.resp.data_len == 0)
				break;

			svc.req.offset += svc.resp.data_len;

		}

        */
	/* msg_def no longer exists
	   
	} else if (mode == "service_client_user_db") {

		ln::service* find_users = clnt.get_service("user_db/find_users", "tests/find_users", ln_service_tests_find_users_signature);

		ln_service_tests_find_users svc;
		svc.req.query = "schmidt";
		svc.req.query_len = strlen(svc.req.query);
		
		find_users->call(&svc);
			
		printf("have %d matches!\n", svc.resp.users_len);
		for(unsigned int i = 0; i < svc.resp.users_len; i++) {
		  printf("\nname   : %*.*s\n", svc.resp.users[i].name_len, svc.resp.users[i].name_len, svc.resp.users[i].name);
		  printf("height   : %.2fm\n", svc.resp.users[i].height);
		  printf("sex      : %c\n", svc.resp.users[i].sex);
		  printf("has image: %d\n", svc.resp.users[i].image_len);
		  if(svc.resp.users[i].image_len) {
			  printf("image    : width %d, height %d, channels: %d\n", 
				 svc.resp.users[i].image->width,
				 svc.resp.users[i].image->height,
				 svc.resp.users[i].image->channels);
			  string fn(svc.resp.users[i].name, svc.resp.users[i].name_len);
			  fn += ".img.raw";
			  FILE* fp = fopen(fn.c_str(), "wb");
			  fwrite(svc.resp.users[i].image->data, svc.resp.users[i].image->data_len, 1, fp);
			  fclose(fp);
		  }
		}
	*/
	} else if(mode == "busy_loop") {
		printf("ready! sleep 2 before busy loop...\n");
		sleep(2);
		printf("will now busy loop...\n");
		unsigned int cnt = 0;
		while(true) {
			printf("cnt: %d\n", cnt);
			cnt++;
		}
	} else {
		printf("unknown mode: '%s'\n", mode.c_str());
	}

	return 0;
}
