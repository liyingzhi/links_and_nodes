#include <stdio.h>
#include <string>

#include <string_util/string_util.h>
#include "ln_messages.h"

using namespace std;

class time_provider : public ln_service_get_time_base {
	ln::client clnt;
	class clock_t {
	public:
		string name;
		clockid_t id;
		double offset;
		
		clock_t(string name, clockid_t id) : name(name), id(id), offset(0) {}
		double get_time() {
			struct timespec ts;
			clock_gettime(id, &ts);
			return offset + ts.tv_sec + ts.tv_nsec/1e9;
		}
	};
	typedef map<string, clock_t*> clocks_t;
	clocks_t clocks;
public:
	time_provider(int argc, char* argv[]) : clnt("time_provider", argc, argv) {

		clocks["monotonic"] = new clock_t("monotonic", CLOCK_MONOTONIC);
		clocks["realtime"] = new clock_t("realtime", CLOCK_REALTIME);
		
		register_get_time(&clnt, "get_time", "main");		
	}
	~time_provider() {
		for(clocks_t::iterator i = clocks.begin(); i != clocks.end(); ++i)
			delete i->second;
	}
	void run() {
		printf("ready\n");
		while(true)
			clnt.wait_and_handle_service_group_requests("main");
	}
	virtual int on_get_time(ln::service_request& req, ln_service_time_provider_get_time& svc) {
		svc.resp.error_message_len = 0;
		try {
			string clock(svc.req.clock, svc.req.clock_len);
			
			clock_t* clk = clocks[clock];
			if(!clk)
				throw str_exception_tb("invalid/unknown clock name: %s", repr(clock).c_str());

			svc.resp.time = clk->get_time();
			
			req.respond();
		}
		catch(const exception& e) {
			printf("error in on_get_time:\n%s", e.what());
			ln::string_buffer errmsg(&svc.resp.error_message, e.what());
			req.respond();
		}
		return 0;
	}
};

int main(int argc, char* argv[]) {
	try {
		time_provider app(argc, argv);
		app.run();
	}
	catch(const exception& e) {
		printf("main caught exception:\n%s", e.what());
	}
	return 0;
}
