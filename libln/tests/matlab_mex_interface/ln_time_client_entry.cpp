#include <stdio.h>
#include <string.h>
#include <string>

#include <ln.h>
#include <string_util/string_util.h>

#include "ln_time_client_messages.h"

using namespace std;

//#define debug(...) printf(__VA_ARGS__);
#define debug(...)

class my_service_client {
	ln::client* clnt;
	ln::service* get_time_svc;
public:
	string name;
	unsigned int use_count;
	
	my_service_client(string name, string manager) {
		clnt = NULL;
		get_time_svc = NULL;
		this->name = name;
		use_count = 1;
		
		if(manager.size())
			clnt = new ln::client(name, manager);
		else
			clnt = new ln::client(name);
	}
	~my_service_client() {
		delete clnt;
	}
	
	double get_time(string clock_name) {
		if(!get_time_svc)
			get_time_svc = clnt->get_service("get_time", "time_provider/get_time", ln_service_time_provider_get_time_signature);
		
		ln_service_time_provider_get_time get_time_data;
		ln::string_buffer clock_name_holder(&get_time_data.req.clock, clock_name);
		get_time_svc->call(&get_time_data);
		if(get_time_data.resp.error_message_len)
			throw str_exception_tb("get_time() returned error: %.*s\n", get_time_data.resp.error_message_len, get_time_data.resp.error_message);
		return get_time_data.resp.time;
	}
};
typedef map<string, my_service_client*> clients_t;
clients_t clients;
bool first_call = true;


// here comes matlab fuckers........	
#include "mex.h"
#define max_parameter_strlen 1024
static string get_string(const mxArray* v) {
	if(!mxIsChar(v)) {
		mexErrMsgTxt("error, argument is expected to be text!"); 
		return "";
	} 
	char tmp[max_parameter_strlen];
	mxGetString(v, tmp, max_parameter_strlen);
	string ret = tmp;
	return ret;
}
void delete_clients() {
	for(clients_t::iterator i = clients.begin(); i != clients.end(); i++)
		delete i->second;
	clients.clear();
}
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
	if(first_call) {
		first_call = false;
		mexAtExit(delete_clients);
	}
	debug("nrhs: %d\n", nrhs);
	if (nrhs < 1)
		mexErrMsgTxt("ln_time_client_entry needs atleast 1 arguments: method_name");
	if (nlhs < 1)
		mexErrMsgTxt("ln_time_client_entry needs atleast 1 output argument!");
		
	string method_name = get_string(prhs[0]);
	debug("method_name: %s\n", method_name.c_str());

	try {
		if(method_name == "new_client") {
			string client_name = get_string(prhs[1]);
			debug("client_name: %s\n", client_name.c_str());
			my_service_client* clnt = clients[client_name];

			if(!clnt) {
				debug("need to create new client!\n");
				string manager;
				if(nrhs > 2)
					manager = get_string(prhs[2]);
				debug("manager: %s\n", manager.c_str());
				clnt = clients[client_name] = new my_service_client(client_name, manager);
			} else {
				clnt->use_count ++;
			}
			
			mwSize dims = 1;
			plhs[0] = mxCreateNumericArray(1, &dims, mxINT64_CLASS, mxREAL);
			*(void**)mxGetPr(plhs[0]) = (void*)clnt;
			return;
			
		} else if(method_name == "get_time") {
			my_service_client* clnt = *(my_service_client**)mxGetPr(prhs[1]);
			if(nrhs < 3) {
				mexErrMsgTxt("ln_time_client_entry: get_time needs 3 parameters: method_name, clnt_handle, clock_name!");
			}
			string clock_name = get_string(prhs[2]);
			plhs[0] = mxCreateDoubleScalar(clnt->get_time(clock_name));
			return;
			
		} else if(method_name == "del_client") {
			my_service_client* clnt = *(my_service_client**)mxGetPr(prhs[1]);
			clnt->use_count --;
			if(clnt->use_count == 0) {
				clients.erase(clnt->name);
				delete clnt;
			}
			return;
		}
		
		mexErrMsgTxt("ln_time_client_entry: invalid method name specified!");
		plhs[0] = mxCreateDoubleScalar(-1);
		
	} catch(const exception& e) {
		mexErrMsgTxt(e.what());
	}
}
