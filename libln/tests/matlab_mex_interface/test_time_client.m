t = ln_time_client();

clock_name = 'monotonic'; % 'realtime'
N = 10;

data = zeros(N, 1);
for i = 1:N
    data(i) = t.get_time(clock_name);
end

data_diff = diff(data);

disp(sprintf('max  diff = %.3fus', max(data_diff)*1e6));
disp(sprintf('min  diff = %.3fus', min(data_diff)*1e6));
disp(sprintf('mean diff = %.3fus', mean(data_diff)*1e6));
