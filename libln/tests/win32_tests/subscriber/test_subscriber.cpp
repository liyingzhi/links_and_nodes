
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <ln.h>
#include "ln_messages.h"

#ifdef __WIN32__
#include <windows.h>
#endif

bool keep_running = true;

ln_client clnt;

int main(int argc, char* argv[]) {
	printf("subscriber starting....\n");
	fflush(stdout);

	while(true) {
		ln::client* clnt = new ln::client("test subscriber md", argc, argv);

		// register publish/output-port
		ln::inport* port = clnt->subscribe("tests.counters", "tests/counters");
		ln_packet_tests_counters p;

		printf("subscriber ready\n");
		fflush(stdout);

		unsigned int max_runs = 20;
	
		while(max_runs--) {
			port->read(&p);
			printf("counter: %d\n", p.counter);
			fflush(stdout);
		}

		printf("subscriber exiting\n");
		fflush(stdout);

		clnt->unsubscribe(port);

		delete clnt;

#ifdef WIN32
		Sleep(100);
#endif
		break;
	}
	
	return 0;
}

