#!/usr/bin/env python
import os
import sys
import time
import traceback

if False:
    # ugly hack to force using another _ln python extension:
    new_ln_ext = "/volume/USERSTORE/schm_fl/workspace/ln_base/library/bindings/python/links_and_nodes/obj/osl42-x86_64_py2.7"
    if new_ln_ext not in sys.path:
        sys.path.insert(0, new_ln_ext)
    # cause old released ext to be further back in path:
    old_ln_ext = "/volume/software/common/packages/links_and_nodes/0.12.2/share/python/links_and_nodes/obj/osl42-x86_64_py2.7"
    sys.path.insert(-1, old_ln_ext)

import links_and_nodes

use_gobject = False

if use_gobject:
    import gobject
    import gtk
      
class service_provider(object):
    def __init__(self):
        self.clnt = links_and_nodes.client("odb", sys.argv)
        svc = self.list_property_service = self.clnt.get_service_provider("odb.svc_list_property", "odb_interface/list_property")
        svc.set_handler(self.svc_list_property)
        if use_gobject:
            svc.do_register_gobject()
        else:
            svc.do_register()
        self.n_calls = 0
        self.start_usage = None
        print "provider ready"

    def get_data_usage(self):
        with open("/proc/self/statm", "r") as fp:
            n_bytes = int(fp.read().strip().split(" ")[5]) * 1024 * 4
        if self.start_usage is None:
            self.start_usage = n_bytes
        return n_bytes - self.start_usage
        
    def svc_list_property(self, svc, req, resp):
        self.n_calls += 1
        print "list_property: %r %r" %(req.object_name, req.property) 
        try:
            resp.result = []       
            string = resp.new_string_packet()
            string.string = "a" * 100
            resp.result.append(string)    
        except:
            ex = traceback.format_exc()
            resp.error_message = ex
            print "had exception:\n%s" % ex
        finally:
            svc.respond()
        usage = self.get_data_usage()
        print "%dbytes, %.2fbytes/call" % (usage, usage / float(self.n_calls))

        if "-stop-after-10" in sys.argv and self.n_calls >= 1000:
            print "provider stopping now!"
            if use_gobject:
                gobject.idle_add(gtk.main_quit())
            else:
                self.keep_running = False
        return 0

if __name__ == '__main__': 
    app = service_provider()
    if "-stop-immidiately" in sys.argv:
        print "stopping"
    else:
        if use_gobject:
            gtk.main()
        else:
            app.keep_running = True
            while app.keep_running:
                app.clnt.wait_and_handle_service_group_requests(None, 1)
    print "exiting..."
            
