#include <stdio.h>
#include <exception>

#include <ln.h>
#include "ln_messages.h"

class service_provider :
	public odb_interface::list_property_base {
	ln::client clnt;
	
	unsigned int n_calls = 0;
	unsigned int start_usage;
	bool have_start_usage = false;
public:
	service_provider() : clnt("service_provider") {
		register_list_property(&clnt, "odb.svc_list_property");
	}

	unsigned int get_data_usage() {
		FILE* fp = fopen("/proc/self/statm", "r");
		if(!fp)
			throw std::runtime_error("could not open proc statm");
		char data[1024];
		int ret = fread(data, 1, sizeof(data) - 1, fp);
		fclose(fp);
		if(ret == -1)
			throw std::runtime_error("fread of proc statm");
		if(ret == 0)
			throw std::runtime_error("empty proc statm!");
		data[ret - 1] = 0;
		unsigned int field = 5;
		char* cp = data;
		while(true) {
			while(*cp && *cp != ' ')
				cp++;
			if(!*cp)
				throw std::runtime_error("field not found!");
			field --;
			cp++;
			if(field == 0)
				break;
		}
		unsigned int n_bytes = atoi(cp);
		if(!have_start_usage) {
			have_start_usage = true;
			start_usage = n_bytes;
		}
		return n_bytes - start_usage;		
	}

	int on_list_property(ln::service_request& req, odb_interface::list_property_t& data) {
		n_calls ++;
		
		std::string object_name(data.req.object_name, data.req.object_name_len);
		std::string property(data.req.property, data.req.property_len);
		printf("on_list_property: '%s' '%s'\n", object_name.c_str(), property.c_str());
		ln_string_t single_result;
		ln::string_buffer result(&single_result.string, std::string(100, 'a'));
		data.resp.result = &single_result;
		data.resp.result_len = 1;
		req.respond();
		
		unsigned int usage = get_data_usage();
		printf("%d bytes, %.2f bytes/call\n\n", usage, usage / (float)n_calls);
		fflush(stdout);
		return 0;
	}

	void run() {
		printf("ready\n");
		while(true)
			clnt.wait_and_handle_service_group_requests(NULL, 1);
	}
};

int main(int argc, char* argv[]) {
	service_provider sp;
	sp.run();	
	return 0;
}
