## nb properties ##
{'current_cell': 0, 'window_position': (1939, 498), 'window_size': (877, 513)}
## nb properties end ##
## cell 0 input ##
## autoexec

frame1 = clnt.publish("frame1", "ln/frame34")
frame1.packet.frame = eye(4)[:3, :4]
frame2 = clnt.publish("frame2", "ln/frame34")
frame2.packet.frame = eye(4)[:3, :4]

lwr_state = clnt.publish("lwr_state", "ln/frame34")
lwr_state.packet.frame = [0] * 12

import time
start_time = time.time()

r = 0.5
w = 1. / 4.

def on_update():
    if frame1 is None:
        return
    t = time.time() - start_time
    f = eye(4)
    f[:2, 3] = cos(t*w*2*pi), sin(t*w*2*pi)
    f[:2, 3] *= r
    frame1.packet.frame = f[:3, :4]
    frame1.write()
    f[:2, 3] = sin(2*t*w*2*pi), cos(2*t*w*2*pi)
    f[:2, 3] *= 0.5*r
    frame2.packet.frame = f[:3, :4]
    frame2.write()
    
    a = 30 / 180. * pi
    for i in xrange(7):
        lwr_state.packet.frame[i] = a * sin(t)
    lwr_state.write()
    return True

import gobject
gobject.timeout_add(int(1/60. * 1000), on_update)
## cell 0 end ##
## cell 1 input ##
clnt.unpublish(frame1)
clnt.unpublish(frame2)
clnt.unpublish(lwr_state)
frame1 = None
frame2 = None
lwr_state = None
## cell 1 end ##
## cell 2 input ##


## cell 2 end ##
