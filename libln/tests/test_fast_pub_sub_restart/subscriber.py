#!/usr/bin/python
# -*- mode: python -*-

import sys
import links_and_nodes as ln
import time

from numpy import *

def main(args):
    clnt = ln.client("py test pub", args)
    port = clnt.subscribe("counters", "tests/counters")

    print "ready"
    while True:
        port.read()
        print "%.3f: %d" % (
            port.packet.duration, port.packet.counter)
    
if __name__ == "__main__":
   main(sys.argv)
