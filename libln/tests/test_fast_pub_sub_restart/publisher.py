#!/usr/bin/python
# -*- mode: python -*-

import sys
import links_and_nodes as ln
import time
import signal

from numpy import *

keep_running = True

def on_sigterm(*args):
    global keep_running
    keep_running = False

def main(args):
    clnt = ln.client("py test pub", args)
    outport = clnt.publish("counters", "tests/counters")

    #signal.signal(signal.SIGTERM, on_sigterm)
    
    print "ready"
    start_time = time.time()
    while keep_running:
        outport.packet.duration = time.time() - start_time
        outport.packet.counter += 1
        outport.write()
        time.sleep(0.1)
    print "stopping"
    clnt.unpublish(outport)
    print "unpublished"
    time.sleep(1)
    print "done"
    
if __name__ == "__main__":
   main(sys.argv)
