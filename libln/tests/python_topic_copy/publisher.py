#!/usr/bin/python
# -*- mode: python -*-

import sys
import links_and_nodes as ln
import time

from numpy import *

def main(args):
    #seconds = 0.001
    
    clnt = ln.client("py test pub", args)
    outport = clnt.publish("counters", "pytest/counters")
    p = outport.packet

    skip = 0
    for i, arg in enumerate(args):
        if skip:
            skip -= 1
            continue
        if arg == "-sleep":
            skip = 1
            seconds = float(args[i + 1])

    start_time = time.time()
    p.counter = 0
    #p.frame = eye(4)
    #p.frame = p.frame[:3, :]
    p.frame = [0.1234] * 16
    while True:
        t = time.time() - start_time
        p.counter += 1
        p.duration = time.time() - start_time
        p.frame[0] = p.counter / 10.
        p.frame[1] = sin(t)
        p.frame[2] = cos(t)
        p.data[0] = p.counter

        msg = "sending counter: %d, %.3fs" % (p.counter, p.duration)
        p.message = msg
        #if p.counter % 100 == 0:
        #    print msg
        outport.write()

if __name__ == "__main__":
   main(sys.argv)
