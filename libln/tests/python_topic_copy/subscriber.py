#!/usr/bin/python
# -*- mode: python -*-

import sys
import links_and_nodes as ln
import time

from numpy import *
import copy

def main(args):
    #seconds = 0.001
    
    clnt = ln.client("py test pub", args)
    port = clnt.subscribe("counters", "pytest/counters", -1, 1)
    p = port.packet

    data = []
    for i in xrange(20):
        port.read()
        if p.counter != p.data[0]:
            print "mismatch: %d vs. %d" % (p.counter, p.data[0])

        data.append(copy.deepcopy(p))

    for p in data:
        #print id(p), p["counter"], p["data"][0]
        print "%#x, %d, %d" % (id(p), p.counter, p.data[0])
        
        

if __name__ == "__main__":
   main(sys.argv)
