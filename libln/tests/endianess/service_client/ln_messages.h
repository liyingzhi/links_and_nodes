#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/*
  /volume/USERSTORE/schm_fl/ln_base/library/tests/endianess/message_definitions/endianess/test
  endianess/test 23 bytes
*/
typedef struct __attribute__((packed)) {
	uint8_t data_uint8;
	uint16_t data_uint16;
	uint32_t data_uint32;
	uint64_t data_uint64;
	float data_float;
	float data_double;
} ln_packet_endianess_test;

/*
  /volume/USERSTORE/schm_fl/ln_base/message_definitions/ln/request_topic
  ln/request_topic
*/
typedef struct __attribute__((packed)) {
	struct __attribute__((packed))
#ifdef __cplusplus
		request_t
#else
		ln_service_ln_request_topic_request_t
#endif
	{
		uint32_t topic_name_len;
		char* topic_name;
		double timeout;
	} req;
	struct __attribute__((packed))
#ifdef __cplusplus
		response_t
#else
		ln_service_ln_request_topic_response_t
#endif
	{
		uint32_t error_message_len;
		char* error_message;
		uint32_t data_len;
		char* data;
		uint32_t counter;
		double timestamp;
	} resp;
} ln_service_ln_request_topic;
#define ln_service_ln_request_topic_signature "uint32_t 4 1,char* 1 1,double 8 1|uint32_t 4 1,char* 1 1,uint32_t 4 1,char* 1 1,uint32_t 4 1,double 8 1"

#ifdef __cplusplus
class ln_service_request_topic_base {
public:
	virtual ~ln_service_request_topic_base() {
#ifdef LN_UNREGISTER_SERVICE_IN_BASE_DETOR            
		unregister_request_topic();
#endif
	}
private:
	static int request_topic_cb(ln::client&, ln::service_request& req, void* user_data) {
		ln_service_request_topic_base* self = (ln_service_request_topic_base*)user_data;
		ln_service_ln_request_topic svc;
		req.set_data(&svc, ln_service_ln_request_topic_signature);
		memset(&svc.resp, 0, sizeof(svc.resp));
		return self->on_request_topic(req, svc);
	}
protected:
	ln::service* request_topic_service;
	ln_service_request_topic_base() : request_topic_service(NULL) {};
	void unregister_request_topic() {
		if(request_topic_service) {
			request_topic_service->clnt->unregister_service_provider(request_topic_service);
			request_topic_service = NULL;
		}
	}
	void register_request_topic(ln::client* clnt, const std::string service_name) {
		request_topic_service = clnt->get_service_provider(
			service_name,
			"ln/request_topic", 
			ln_service_ln_request_topic_signature);
		request_topic_service->set_handler(&request_topic_cb, this);
		request_topic_service->do_register();
	}
	virtual int on_request_topic(ln::service_request&/* req*/, ln_service_ln_request_topic&/* svc*/) {
		fprintf(stderr, "ERROR: no virtual int on_request_topic() handler overloaded for service request_topic!\n");
		return 1;
	}
};
#endif
#endif // LN_MESSAGES_H
