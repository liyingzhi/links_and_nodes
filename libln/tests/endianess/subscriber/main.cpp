#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include <string>
#include <vector>

#include <ln.h>

#include "ln_messages.h"

int main(int argc, char* argv[]) {

	ln::client clnt("my client", argc, argv);

	double subscriber_rate = -1;
	std::vector<std::string> args = clnt.get_remaining_args();
	for(unsigned int i = 0; i < args.size(); i++) {
		printf("arg: %s\n", args[i].c_str());
		if(args[i] == "via_service")
			subscriber_rate = RATE_ON_REQUEST_LAST;
	}
	
	ln::inport* port = clnt.subscribe("test", "endianess/test", subscriber_rate);
	ln_packet_endianess_test p;
	ln_packet_endianess_test cmp_p;

	p.data_uint8 = 0x01;
	p.data_uint16 = 0x0203;
	p.data_uint32 = 0x04050607;
	p.data_uint64 = 0x08090a0b0c0d0e0fLL;
	p.data_float = M_PI;
	p.data_double = M_PI / 2;

	printf("ready...\n");
	while(true) {
		printf("waiting for packet\n");
		if(subscriber_rate == RATE_ON_REQUEST_LAST)
			port->read(&cmp_p, false);
		else
			port->read(&cmp_p);

		if(!memcmp(&cmp_p, &p, sizeof(p)))
			printf("packets match!\n");
		else
			printf("packets DO NOT match!\n");

		printf("data_uint8 : 0x%02x\n",   cmp_p.data_uint8);
		printf("data_uint16: 0x%04x\n",   cmp_p.data_uint16);
		printf("data_uint32: 0x%08x\n",   cmp_p.data_uint32);
		printf("data_uint64: 0x%016llx\n", cmp_p.data_uint64);
		printf("data_float : %8.6f\n",    cmp_p.data_float);
		printf("data_double: %8.6f\n",    cmp_p.data_double);

		if(subscriber_rate == RATE_ON_REQUEST_LAST)
			usleep(500000);
	}
	
	return 0;
}
