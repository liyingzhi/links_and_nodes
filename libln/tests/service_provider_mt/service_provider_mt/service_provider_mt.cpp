#include <list>

#include "ln.h"

#define LN_UNREGISTER_SERVICE_IN_BASE_DETOR
#include "ln_messages.h"

using namespace std;

class provider : public ln_service_sleep_service_base {
public:
	ln::client clnt;
	
	provider(int argc, char* argv[]) : clnt("service_provider_mt", argc, argv) {
		register_sleep_service(&clnt, "sleep_service");
	}

	/*
	  // sync version
	virtual int on_sleep_service(ln::service_request& req, ln_service_tests_sleep_service& svc) {
		printf("got sleep request for %.3f\n", svc.req.sleep_time);
		svc.resp.start_time = ln_get_time();
		usleep(svc.req.sleep_time * 1e6);
		svc.resp.stop_time = ln_get_time();
		
		req.respond();
		return 0;
	}
	*/

	struct queue_item {
		ln::service_request req;
		ln_service_tests_sleep_service svc;

		queue_item(ln::service_request& req, ln_service_tests_sleep_service& svc)
			: req(req), svc(svc) {};
	};
	typedef list<queue_item> queue_t;
	queue_t queue;
	
	virtual int on_sleep_service(ln::service_request& req, ln_service_tests_sleep_service& svc) {
		printf("got sleep request for %.3f\n", svc.req.sleep_time);
		svc.resp.start_time = ln_get_time();
		queue.push_back(queue_item(req, svc));
		return -LNE_SVC_RESP_LATER;
	}
	
	int run() {
		printf("provider ready\n");
		while(true) {
			// check for next timeout
			double next_timeout = -1;
			double now = ln_get_time();
			for(queue_t::iterator i = queue.begin(); i != queue.end(); ++i) {
				double deadline = i->svc.resp.start_time + i->svc.req.sleep_time - now;
				if(next_timeout == -1 || deadline < next_timeout)
					next_timeout = deadline;
			}

			clnt.wait_and_handle_service_group_requests(std::nullptr, next_timeout);

			for(queue_t::iterator i = queue.begin(); i != queue.end(); ) {
				double deadline = i->svc.resp.start_time + i->svc.req.sleep_time - now;
				if(deadline > 0) {
					++i;
					continue;
				}
				// finish request!
				i->svc.resp.stop_time = ln_get_time();
				i->req.respond();
				queue.erase(i);
				i = queue.begin();
			}
		}
		return 0;
	}
};

int main(int argc, char* argv[]) {
	provider p(argc, argv);
	return p.run();
}
