#!/usr/bin/python

import links_and_nodes as ln

import pprint
import os
import sys

from numpy import *

class client(ln.services_wrapper):
    def __init__(self):
        self.clnt = ln.client(self.__class__.__name__, sys.argv)
        ln.services_wrapper.__init__(self, self.clnt, "test_prefix")

        sw = self.wrap_service("test_service", "tests/test_service")
        self.test_service_req = sw.svc.req

    def run_test(self):
        command = zeros((4, 4), dtype=float64)
        command[0, 0] = 0.1
        command[1, 1] = 0.2
        command[2, 2] = 0.3
        command[3, 3] = 1
        command[:3, 3] = 0.11, 0.22, 0.33
        print "sending command:\n%r" % command

        frame = self.test_service_req.command
        frame.data = command
        frame.name = "hallo"
        measured = self.test_service(frame)
        
        print "received measured:\n%r" % measured
        measured = measured["data"].reshape((4, 4))
        print "measured reshaped:\n%r" % measured
        

if __name__ == "__main__":
    clnt = client()
    clnt.run_test()
