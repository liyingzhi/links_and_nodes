#!/usr/bin/python

import time
import sys
import links_and_nodes as ln
import pprint

p = "/volume/software/common/packages/links_and_nodes/latest/share/python/links_and_nodes_manager/"
if p not in sys.path:
    sys.path.insert(0, p)

class my_watcher(ln.lnm_remote):
    def __init__(self, address="localhost:54123", debug=False):
        ln.lnm_remote.__init__(self, address, debug=debug, use_gobject=False)
        self.get_sysconf_interval = 1
        
    def run(self):
        self.last_get_sysconf = 0
        while True:
            try:
                self.use_mainloop_iterate(timeout=0.1)
            except:
                if "timeout" not in str(sys.exc_value):
                    raise
            now = time.time()
            if now - self.last_get_sysconf > self.get_sysconf_interval:
                self.last_get_sysconf = now
                sysconf = self.request("get_system_configuration")
                dl = self.dump(sysconf)
                print "got sysconf %d bytes" % len(dl)
                pprint.pprint(sysconf)
                file("/tmp/last_received", "wb").write(dl)

    def dump(self, what):
        import cPickle
        return cPickle.dumps(what, cPickle.HIGHEST_PROTOCOL)

    def on_output(self, name, output):
        pass
    def on_log_messages(self, msgs):
        pass

if __name__ == "__main__":
    w = my_watcher(debug="-debug" in sys.argv)
    w.run()
