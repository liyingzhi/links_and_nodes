
class gui_plugin(object):
    def __init__(self, manager_gui, this_plugin):
        self.gui = manager_gui
        self.this_plugin = this_plugin

        # monkey patch manager assuming manager was started with gui!
        import SystemConfiguration
        SystemConfiguration.Process.not_to_pickle.update(
            ["output", "environment", "output_to_display"])

        self.gui.info("manager patched!")
        
