#!/usr/bin/python

import sys
import os
import links_and_nodes as ln

clnt = ln.client("events test", sys.argv)

def on_ln_resource_event(ev):
    print "received event: %s, name: %s, md: %s, client: %s" % (
        ev.event,
        ev.name,
        ev.md,
        ev.client)

connection = clnt.connect_to_event(
    "ln.resource_event", "ln/resource_event", on_ln_resource_event,
    event_pattern="*",
    name_pattern="*",
    md_pattern="*",
    client_pattern="*"
)

n_events = 10
while True:
    n_events -= 1
    clnt.wait_and_handle_service_group_requests(None, -1)    
    if n_events == 0:
        break
    

