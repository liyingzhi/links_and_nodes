#!/usr/bin/python

from __future__ import print_function
import sys
import links_and_nodes as ln

class event_test(object):
    def __init__(self):
        self.clnt = ln.client("event test py", sys.argv)

        self.connection = self.clnt.connect_to_event(
            "ln.resource_event", "ln/resource_event",
            self.on_resource_event,
            event_pattern="*",
            name_pattern="*",
            md_pattern="*",
            client_pattern="*")
        print("self.connection", self.connection)

        self.keep_running = True
        
    def on_resource_event(self, ev):
        print("received event: %s, name: %s, client: %s" % (
            ev.event, ev.name, ev.client))

    def run(self):
        self.keep_running = True
        while self.keep_running:
            self.clnt.wait_and_handle_service_group_requests(None, 1)

if __name__ == "__main__":
    test = event_test()
    test.run()
