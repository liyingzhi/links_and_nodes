#include <stdio.h>

#ifdef TEST_C
#include "events_c.h"
#endif

#ifdef TEST_CPP
#include "events_cpp.h"
#endif

#ifdef TEST_CPP_OO
#include "events_cpp_oo.h"
#endif
