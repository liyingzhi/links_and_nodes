#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string>
#include <signal.h>
#include <unistd.h>

#include <pthread.h>

#include "ln.h"
#include "ln_messages.h"

using namespace std;

unsigned int max_runs = 30;
bool keep_running;

void on_ln_resource_event(ln_client clnt, ln_event_call call, void* user_data) {
	ln_event_ln_resource_event ev;
	ln_event_call_set_data(call, ln_event_ln_resource_event_signature, &ev);

	printf("received event: %*.*s, name: %*.*s, client: %*.*s\n",
	       ev.event_len, ev.event_len, ev.event,
	       ev.name_len, ev.name_len, ev.name,
	       ev.client_len, ev.client_len, ev.client
		);

	max_runs -= 1;
	if(max_runs == 0)
		keep_running = false;
}

int main(int argc, char* argv[]) {
	ln_client clnt;
	ln_init(&clnt, "event test", argc, argv);

	ln_event_connect_ln_resource_event evconnect;
	// service_{new|del}_provider | topic_{new|del}_{publisher|subscriber} }
	evconnect.event_pattern = "*"; evconnect.event_pattern_len = 1;
	evconnect.name_pattern = "*"; evconnect.name_pattern_len = 1;
	evconnect.md_pattern = "*"; evconnect.md_pattern_len = 1;
	evconnect.client_pattern = "*"; evconnect.client_pattern_len = 1;
	ln_event_connection ev_conn;
	int ret = ln_event_connect(
		clnt, &ev_conn,
		"ln.resource_event",
		ln_event_connect_ln_resource_event_signature, &evconnect,
		&on_ln_resource_event, NULL);
	if(ret) {
		printf("ln_event_connect: error %d: %s\nerror: %s\n",
		       ret, ln_format_error(ret),
		       ln_get_error_message(clnt)
			);
		ln_deinit(&clnt);
		return -1;
	}
	
	keep_running = true;
	while(keep_running)
		ln_wait_and_handle_service_group_requests(clnt, NULL, 1); // every second we check `keep_running`

	ln_deinit(&clnt);
	
	return 0;
}
