#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>

#ifdef __cplusplus
#include "ln_cppwrapper.h"
#endif
/*
  /volume/USERSTORE/schm_fl/ln_base/message_definitions/ln/resource_event
  ln/resource_event 32 bytes
*/
typedef struct __attribute__((packed)) {
	uint32_t event_len;
	char* event;
	uint32_t name_len;
	char* name;
	uint32_t md_len;
	char* md;
	uint32_t client_len;
	char* client;
} ln_event_ln_resource_event;
#define ln_event_ln_resource_event_signature "ln/resource_event: uint32_t 4 1,char* 1 1,uint32_t 4 1,char* 1 1,uint32_t 4 1,char* 1 1,uint32_t 4 1,char* 1 1"

typedef struct __attribute__((packed)) {
	uint32_t event_pattern_len;
	char* event_pattern;
	uint32_t name_pattern_len;
	char* name_pattern;
	uint32_t md_pattern_len;
	char* md_pattern;
	uint32_t client_pattern_len;
	char* client_pattern;
} ln_event_connect_ln_resource_event;
#define ln_event_connect_ln_resource_event_signature "ln/resource_event: uint32_t 4 1,char* 1 1,uint32_t 4 1,char* 1 1,uint32_t 4 1,char* 1 1,uint32_t 4 1,char* 1 1"

#ifdef __cplusplus
class ln_event_listener_ln_resource_event {
public:
	virtual ~ln_event_listener_ln_resource_event() {}
private:
	static void ln_resource_event_cb(ln::event_call& call, void* user_data) {
		ln_event_listener_ln_resource_event* self = (ln_event_listener_ln_resource_event*)user_data;
		ln_event_ln_resource_event ev;
		call.set_data(&ev, ln_event_ln_resource_event_signature);
		self->on_ln_resource_event(ev);
	}
protected:
	ln::event_connection* ln_resource_event_connection;
	void connect_to_ln_resource_event(ln::client* clnt, const std::string event_name, ln_event_connect_ln_resource_event* connect_data) {
		ln_resource_event_connection = clnt->connect_to_event(
			event_name, ln_event_connect_ln_resource_event_signature, connect_data,
			&ln_resource_event_cb, this);
	}
	virtual void on_ln_resource_event(ln_event_ln_resource_event& ev) {
		fprintf(stderr, "ERROR: no virtual int on_ln_resource_event() handler overloaded for event ln_resource_event!\n");
	}
};
#endif
    #endif // LN_MESSAGES_H
