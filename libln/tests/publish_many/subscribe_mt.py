#!/usr/bin/python

import sys
import time
import links_and_nodes as ln
import threading

class threaded_subscriber(threading.Thread):
    def __init__(self, manager, port, name):
        threading.Thread.__init__(self, name=name)
        self.manager = manager
        self.port = port
        self.last = None
        self.name = name

    def run(self):
        print "%s: starting" % self.name
        while True:
            print "%s: blocking read" % self.name
            self.port.read(True)
            print "%s: unblocked: %s" % (self.name, self.port.packet.frame[0])

class manager(object):
    def __init__(self):
        self.clnt = ln.client(sys.argv[0], sys.argv)
        self.N = int(sys.argv[1])

        self.subscribers = []
        for i in xrange(self.N):
            self.subscribers.append(threaded_subscriber(
                self, 
                self.clnt.subscribe("frame%d" % i, "ln/frame34"),
                "thread_%d" % i))

    def run(self):
        # now start threads
        for t in self.subscribers:
            t.start()
        # wait for finish
        for t in self.subscribers:
            t.join()

if __name__ == "__main__":
    mgr = manager()
    mgr.run()
