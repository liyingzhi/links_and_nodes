#!/usr/bin/python

import sys
import links_and_nodes as ln

class invalid_node(object):
    pass

class client(object):
    def __init__(self):
        self.clnt = ln.client(self.__class__.__name__, sys.argv)
        self.service = self.clnt.get_service("request_nodes", "nested_service/request")

    def run(self):
        r = self.service.req
        r.what = "give me!"
        #r.roots = [] # ["invalid!"]

        #r.roots = ["invalid!"]

        #n = invalid_node()
        #n.name = "ok"
        #n.children = []
        #r.roots = [n]

        n = r.new_node_t_packet()
        n.name = "yeay"
        r.roots = [n]
        
        self.service.call()
        print "response: %r" % self.service.resp.error_message
        print "n response_roots: %d" % len(self.service.resp.response_roots)

if __name__ == "__main__":
    c = client()
    c.run()
