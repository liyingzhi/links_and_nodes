import links_and_nodes as ln

class invalid_node(object):
    pass

class provider(object):
    def __init__(self):
        self.clnt = ln.client(self.__class__.__name__)

        self.service = self.clnt.get_service_provider("request_nodes", "nested_service/request")
        self.service.set_handler(self.handler)
        self.service.do_register()

    def run(self):
        print "ready"
        while True:
            self.clnt.wait_and_handle_service_group_requests(None, 1)
        
    def handler(self, request, req, resp):
        print "have request for %r" % req.what
        resp.error_message = "all fine!"

        #resp.response_roots = ["invalid"]
        
        #n = invalid_node()
        #n.name = "ok"
        #n.children = []
        #resp.response_roots = [n]

        n = resp.new_node_t_packet()
        n.name = "ok"
        resp.response_roots = [n]
        
        request.respond()        
        return 0
        
if __name__ == "__main__":
    p = provider()
    p.run()
