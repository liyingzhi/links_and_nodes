
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "ln.h"
#include "ln_messages.h"

#ifdef __WIN32__
#include <windows.h>
#endif

bool keep_running = true;

ln_client clnt;

void stop_handler(int signo) {
	keep_running = false;
	printf("subscriber exiting by signal!\n");
	ln_deinit(&clnt);
	exit(0);
}

int main(int argc, char* argv[]) {
	int ret;
	double rate = 100;
	unsigned int i = 0;
	int div = 10;
	
	printf("subscriber loading...\n");
	fflush(stdout);

	for(i = 1; i < (unsigned)argc; i++)
		if(!strcmp(argv[i], "-rate") && (i + 1) < (unsigned)argc)
			rate = atof(argv[++i]);
			
	if((ret = ln_init(&clnt, "test subscriber md", argc, argv))) {
		printf("could not init ln_client. returned error %d: %s\n", -ret, ln_format_error(ret));
		if(clnt)
			printf("dynamic error: %s\n", ln_get_error_message(clnt));
		return -1;
	}

	// register publish/output-port
	ln_inport counters_port;
	// rate = -1;
	// rate = RATE_ON_REQUEST_LAST;
	// rate = RATE_ON_REQUEST_NEXT;
	if((ret = ln_subscribe(clnt, "tests.counters", "tests/counters", &counters_port, rate, 0))) {
		printf("ln_subscribe error %d: %s\n", -ret, ln_format_error(ret));
		goto error_exit;
	}

	ln_packet_tests_counters p;

	if(ln_get_message_size(counters_port) != sizeof(p)) 
		printf("warning: sizeof(ln_packet_tests_counters): %d, size from manager: %d\n", sizeof(p), ln_get_message_size(counters_port));

	// read from port
	i = 0;
	// signal(SIGINT, stop_handler);
	// signal(SIGTERM, stop_handler);
	printf("subscriber ready - new2\n");
	fflush(stdout);
	while(keep_running) {
		printf("read %d, has_publisher: %d\n", i, ln_has_publisher(counters_port));
		fflush(stdout);
		double ts;
		ret = ln_read(counters_port, &p, sizeof(p), &ts, 1);
		// ret = ln_read_timeout(counters_port, &p, sizeof(p), &ts, 0);
		if(ret < 0) {
			printf("ln_write error %d: %s\n", -ret, ln_format_error(ret));
			goto error_exit;
		}
		if(true || (p.counter % div) == 0)
			printf("ret: %d, counter: %d, timestamp: %f\n", ret, p.counter, ts);
#ifdef __WIN32__ || __VXWORKS__
		// on win32 there is no clean way to open a non-window-console to have automatic line-buffering.
		// this flush fill flush the stdio's buffers.
		fflush(stdout);
#endif

		if(rate == RATE_ON_REQUEST_LAST) {
			printf("waiting before doing next request...\n");
#ifdef __WIN32__
			Sleep(1000);
#else
			sleep(1);
#endif
			}
	}
	goto exit;
error_exit:
	if(ln_get_error_message(clnt))
		printf("%s\n", ln_get_error_message(clnt));

exit:
	printf("subscriber exiting\n");
	ln_deinit(&clnt);

	return 0;
}

