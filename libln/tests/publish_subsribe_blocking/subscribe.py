#!/usr/bin/python

import sys
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)
port = clnt.subscribe("test", "pstest/test_topic")

print "ready, waiting for published packets!"
while True:
    port.read()
    print "received counter %d" % port.packet.counter

