#!/usr/bin/python

import sys
import links_and_nodes as ln

clnt = ln.client(sys.argv[0], sys.argv)
port = clnt.publish("test", "pstest/test_topic")

port.packet.counter = 0
print "ready"
while True:
    next = port.packet.counter + 1
    print "press enter to publish counter value %d" % next
    raw_input()

    port.packet.counter = next
    port.write()

