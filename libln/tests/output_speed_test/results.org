* results
  | host       | arch                 | term       | term size wo dec | started via  | test-speed lines/s | notes                                             |
  |------------+----------------------+------------+------------------+--------------+--------------------+---------------------------------------------------|
  | rmc-lx0142 | sled11-x86_64-gcc4.x | xterm      |           130x30 | enli c-a-ret | 60k                |                                                   |
  | rmc-lx0142 | sled11-x86_64-gcc4.x | konsole    |           130x30 | xterm        | 250k               | scrolling, fixed 1000, does only refresh every 1s |
  | rmc-lx0142 | sled11-x86_64-gcc4.x | xterm      |           130x30 | lnm          | 104k               |                                                   |
  | rmc-lx0142 | sled11-x86_64-gcc4.x | ln vte win |          ~130x30 |              | 512k               |                                                   |
  
