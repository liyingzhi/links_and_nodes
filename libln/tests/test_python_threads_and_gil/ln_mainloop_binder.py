import traceback

class registered_service(object):
    def __init__(self, parent, svc, handler):
        self.parent = parent
        self.svc = svc
        self.handler = handler
        self.svc.set_handler(handler)
        self.svc.do_register()
        self.main = self.parent.mainloop
        self.fd = self.svc.get_fd()
        self.source_id = self.parent.mainloop.add_io_watch(
            self.fd, self.main.IO_IN, self.on_io)
        
        self.client_source_ids = {}
        self.svc.set_client_fd_handler(self.on_client_fd)
                
    def on_io(self, fd, why):
        if self.parent.debug: print "%s io on fd %r" % (self.svc.name, fd)
        self.clnt.wait_and_handle_service_group_requests(None, 0)
        return True

    def on_client_fd(self, ev, fd):
        if self.parent.debug: print "%s client_fd_event: %r, %r" % (self.svc.name, ev, fd)
        if ev == "new":
            self.client_source_ids[fd] = self.main.add_io_watch(fd, self.main.IO_IN, self.on_io) # for new requests from existing clients
            
        elif ev == "remove" and fd in self.client_source_ids:
            self.main.source_remove(self.client_source_ids[fd])
            del self.client_source_ids[fd]
            
        elif ev == "new_svc_fd":
            self.client_source_ids[fd] = self.main.add_io_watch(fd, self.main.IO_IN, self.on_io) # for new connections
            
        elif ev == "remove_svc_fd" and fd in self.client_source_ids:
            self.main.source_remove(self.client_source_ids[fd])
            del self.client_source_ids[fd]
        
        return True

    def unregister(self):
        self.main.source_remove(self.source_id)
        self.source_id = None
        for fd, sid in self.client_source_ids.iteritems():
            self.main.source_remove(sid)
        self.client_source_ids = {}

class registered_client(object):
    def __init__(self, parent, clnt):
        self.parent = parent
        self.clnt = clnt
        self.multi_waiter = None
        self.fd = None
        self.source_id = None
        self.next_port_id = 0
        self.ports = {}

    def add_port(self, port):
        if self.multi_waiter is None:
            self.multi_waiter = self.clnt.get_multi_waiter()
            self.fd = self.multi_waiter.start_pipe_notifier_thread()
            self.source_id = self.parent.mainloop.add_io_watch(self.fd, self.parent.mainloop.IO_IN, self.on_event)

        port_id = self.next_port_id
        self.next_port_id += 1
        
        port.can_read = False
        self.multi_waiter.add_port(port.port)
        self.ports[port_id] = port
        
        return port_id

    def remove_port(self, port_id):
        port = self.ports[port_id]
        del self.ports[port_id]
        self.multi_waiter.remove_port(port.port)
        
    def on_event(self, fd, why):
        if self.parent.debug: print "client multiwaiter has event", fd, why
        for port_ids, port in self.ports.iteritems():
            if self.multi_waiter.can_read(port.port):
                port.can_read = True
        self.multi_waiter.ack_pipe_notification()
        to_remove = []
        for port_id, port in self.ports.iteritems():
            if not port.can_read:
                continue
            port.can_read = False
            try:
                port.port.read(False) # nonblocking!
                port.cb(port.port.packet, *port.cbargs)
                ret = True
            except:
                print "exception in handler for port %r:\n%s" % (port.port, traceback.format_exc())
                ret = False
            if not ret:
                to_remove.append(port_id)
        if to_remove:
            to_remove.reverse()
            for i in to_remove:
                port = self.ports[i]
                del self.ports[i]
                self.multi_waiter.remove_port(port.port)
        return True
        
class registered_port(object):
    def __init__(self, parent, port, cb, *cbargs):
        self.parent = parent
        self.port = port
        self.cb = cb
        self.cbargs = cbargs
        self.client = self.parent.get_client(self.port.client)
        self.port_id = self.client.add_port(self)

    def unregister(self):
        self.client.remove_port(self.port_id)
        
        
class ln_mainloop_binder(object):
    def __init__(self, mainloop, debug=False):
        self.mainloop = mainloop
        self.services = []
        self.ports = {}
        self.next_port_id = 0
        self.clients = {}
        self.debug = debug
        
    def register_service_provider(self, svc, handler):
        self.services.append(registered_service(self, svc, handler))
    def unregister_service_provider(self, svc):
        to_del = []
        for s in self.services:
            if s.svc == svc:
                to_del.append(i)
        to_del.reverse()
        for i in to_del:
            self.services[i].unregister()
            del self.services[i]
        
    def register_subscriber(self, port, cb, *cbargs):
        port_id = self.next_port_id
        self.next_port_id += 1
        self.ports[port_id] = registered_port(self, port, cb, *cbargs)
    def unregister_subscriber(self, port_id):
        if port_id in self.ports:
            self.ports[port_id].unregister()
            del self.ports[port_id]
        return port_id

    def get_client(self, clnt):
        rc = self.clients.get(clnt)
        if rc is None:
            rc = self.clients[clnt] = registered_client(self, clnt)
        return rc
            
