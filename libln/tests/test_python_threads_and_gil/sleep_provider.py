#!/usr/bin/python

import thread
import time
import sys
import links_and_nodes as ln

clnt = ln.client("sleep_provider", sys.argv)

def sleep_handler(call, req, resp):
    print "sleeping %.2fs" % req.sleep
    time.sleep(req.sleep)
    print "done"
    call.respond()
    return 0

svc = clnt.get_service_provider("sleep", "test/sleep")
svc.set_handler(sleep_handler)
svc.do_register()

print "ready"

while True:
    print "main iteration"
    clnt.wait_and_handle_service_group_requests()
