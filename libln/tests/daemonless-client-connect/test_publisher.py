#!/usr/bin/python

import os
import sys
import pprint
import time
import links_and_nodes as ln


print "connecting to manager..."
clnt = ln.client(os.path.basename(sys.argv[0]), sys.argv)
print "connected."

class my_provider(ln.service_provider):
    def __init__(self, clnt):
        ln.service_provider.__init__(self, clnt)
        self.wrap_service_provider("sleeper", "tests/sleep_service")

    def sleeper(self, sleep_time):
        print "sleeping %.3f" % sleep_time
        start_time = time.time()
        time.sleep(sleep_time)
        stop_time = time.time()
        print "done"
        return dict(start_time=start_time, stop_time=stop_time)
        
sleeper = my_provider(clnt)
clnt.handle_service_group_in_thread_pool(None, "main")

port = clnt.publish("test_topic", "tests/int8")

while True:
    port.packet.incre[0] += 1
    port.write()
    time.sleep(0.5)
