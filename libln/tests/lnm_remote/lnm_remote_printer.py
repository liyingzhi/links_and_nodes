#!/usr/bin/python

import sys
import gobject
import links_and_nodes as ln

class my_watcher(ln.lnm_remote):
    def __init__(self, address="localhost:54123", debug=False):
        self.main = gobject.MainLoop()
        ln.lnm_remote.__init__(self, address, debug=debug)

    def run(self):
        return self.main.run()

if __name__ == "__main__":
    w = my_watcher(debug="-debug" in sys.argv)
    w.run()
