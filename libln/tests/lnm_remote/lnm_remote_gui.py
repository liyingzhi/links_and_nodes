#!/usr/bin/python

import sys
import gtk
import gobject
import links_and_nodes as ln

class my_watcher(ln.lnm_remote):
    def __init__(self, address="localhost:54123", debug=False):
        ln.lnm_remote.__init__(self, address, use_gtk_mainloop=True, debug=debug)

        self.xml = gtk.Builder()
        self.xml.add_from_file("lnm_remote_gui.ui")
        self.xml.connect_signals(self)
        self.main_window.show_all()

        # log output
        self.log_buffer = self.log_tv.get_buffer()

        # process list
        tv = self.procs_tv
        m = self.procs_model = gtk.ListStore(
            gobject.TYPE_STRING, # process name
            gobject.TYPE_STRING, # process state
        )
        tv.set_model(m)
        tv.insert_column_with_attributes(-1, "process name", gtk.CellRendererText(), text=0)
        tv.insert_column_with_attributes(-1, "state", gtk.CellRendererText(), text=1)

        # this needs to have manager python dir in path!
        if ln.ln_manager_dir not in sys.path:
            sys.path.insert(0, ln.ln_manager_dir)
        self.sysconf = self.request("get_system_configuration")
        print "have %d processes defined in this config" % len(self.sysconf.processes)
        
        self.fill_process_list()
        
    def fill_process_list(self):
        m = self.procs_model
        m.clear()
        proc_names = self.sysconf.processes.keys()
        proc_names.sort()
        for pname in proc_names:
            p = self.sysconf.processes[pname]            
            m.append((pname, p.state))

    def __getattr__(self, name):
        widget = self.xml.get_object(name)
        if widget is None:
            raise AttributeError(name)
        setattr(self, name, widget)
        return widget

    def run(self):
        gtk.main()

    def on_main_window_delete_event(self, w, ev):
        gtk.main_quit()
        
    def scroll_to_end(self):
        self.log_tv.scroll_to_iter(self.log_buffer.get_end_iter(), 0.4)
        return False
        
    def on_log_messages(self, msgs):
        b = self.log_buffer
        for mid, ts, ll, src, msg, tb in msgs:
            b.insert(b.get_end_iter(), "%s %s\n" % (ts.strftime("%H:%M:%S"), msg))
        gobject.idle_add(self.scroll_to_end)
        return True

    def on_obj_state(self, obj_type, name, state):
        print "new state %r for object %s: %r" % (state, obj_type, name)
        if obj_type == "Process":
            # find process in our process list
            m = self.procs_model
            iter = m.get_iter_first()
            while iter:
                if m[iter][0] == name:
                    m[iter][1] = state
                    break
                iter = m.iter_next(iter)

    def on_procs_tv_row_activated(self, tv, path, tvc):
        m = self.procs_model
        pname = m[path][0]
        pstate = m[path][1]
        if pstate == "stopped":
            print "request start of %r" % pname
            self.request("set_process_state_request", ptype="Process", pname=pname, requested_state="start")
        else:
            print "request stop of %r" % pname
            self.request("set_process_state_request", ptype="Process", pname=pname, requested_state="stop")
        return True

if __name__ == "__main__":
    w = my_watcher(debug="-debug" in sys.argv)
    w.run()
